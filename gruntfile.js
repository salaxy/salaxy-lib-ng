// salaxy-lib-ng
// Grunt dev and build configuration
// MJ 20.2.2018

module.exports = function(grunt) {
    require('jit-grunt')(grunt);
    grunt.initConfig({
        clean: {
            npm: {
                src: ['dist/npm/@salaxy']
            },
            dictionary: {
                src: ['src/app/i18n/temp']
            }
        },
        exec: {
            ng: {
                command: 'npm run package_salaxy_ng'
            },
            ionic: {
                command: 'npm run package_salaxy_ionic'
            },
        },
        concat: {
            ng: {
                src: [
                    'src/@salaxy/ng/**/*.scss'
                ],
                dest: 'dist/npm/@salaxy/ng/styles.scss',
                nonull: true
            },
            ionic: {
                src: [
                    'src/@salaxy/ionic/**/*.scss'
                ],
                dest: 'dist/npm/@salaxy/ionic/styles.scss',
                nonull: true
            },
            dictionary: {
                options: {
                    banner: "/* tslint:disable */\n",
                    footer: "\n/** Internationalization dictionary\n@hidden */\nexport const dictionary = _dictionary;\n"
                },
                files: {
                    'src/@salaxy/ng/i18n/dictionary.ts': 'src/@salaxy/ng/i18n/dictionary.ts',
                    'src/@salaxy/ionic/i18n/dictionary.ts': 'src/@salaxy/ionic/i18n/dictionary.ts',
                    'src/app/i18n/dictionary.ts': 'src/app/i18n/dictionary.ts'
                },
                nonull: true
            }
        },
        copy: {
            npm: {
                files: [
                    { expand: true, cwd: 'src/@salaxy', src: ['**/*.css'], dest: 'dist/npm/@salaxy/' }
                ]
            }
        },
        http: {
            dictionary_ng: {
                options: {
                    url: 'http://docs.google.com/feeds/download/spreadsheets/Export?key=1vpHn7uZYGCR0_IQZnKwnU_bNKcqhS9iTN40-scT1yV0&exportFormat=csv&gid=0'
                },
                dest: 'src/app/i18n/temp/dictionary-ng.csv'
            },
            dictionary_ion: {
                options: {
                    url: 'http://docs.google.com/feeds/download/spreadsheets/Export?key=1Qor_oAazH7UE2sxS4hVH2twEhGikheK7y8cg3nUdbQY&exportFormat=csv&gid=0'
                },
                dest: 'src/app/i18n/temp/dictionary-ion.csv'
            }
        },
        csvjson: {
            dictionary_ng: {
                src: 'src/app/i18n/temp/dictionary-ng.csv',
                dest: 'src/app/i18n/temp/ng'
            },
            dictionary_ion: {
                src: 'src/app/i18n/temp/dictionary-ion.csv',
                dest: 'src/app/i18n/temp/ionic'
            }
        },
        json: {
            options: {
                namespace: '_dictionary',
                includePath: false,
                pretty: true
            },
            dictionary_ng: {
                src: 'src/app/i18n/temp/ng/*.json',
                dest: 'src/@salaxy/ng/i18n/dictionary.ts'
            },
            dictionary_ion: {
                src: 'src/app/i18n/temp/ionic/*.json',
                dest: 'src/@salaxy/ionic/i18n/dictionary.ts'
            },
            dictionary_app: {
                src: 'src/app/i18n/lang/*.json',
                dest: 'src/app/i18n/dictionary.ts'
            }
        }
    });

    // loadNpmTasks is needed for non-standard namings, typical cases are handled by jit-grunt
    grunt.loadNpmTasks('grunt-csv-json');
    grunt.loadNpmTasks('grunt-http');

    // ===========================================================================
    // BUILD JOBS 		  ========================================================
    // ===========================================================================
    grunt.registerTask('updatetranslation', ['http', 'csvjson', 'json', 'concat:dictionary', 'clean:dictionary']);
    var tasks = ['clean:npm', 'updatetranslation', 'exec', 'concat:ng', 'concat:ionic', 'copy:npm'];
    grunt.registerTask('default', tasks);
    grunt.registerTask('build', tasks);
};