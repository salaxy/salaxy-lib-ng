//READ this: \palkkaus-web-worker\node_modules\@ionic\app-scripts\README.md
//Default ionic webpack.config.js
const webpackConfig = require('./node_modules/@ionic/app-scripts/config/webpack.config.js');
//Extension
var path = require('path');
webpackConfig.dev.resolve.alias = {
    '@salaxy/ng': path.resolve('./src/@salaxy/ng'),
    '@salaxy/ionic': path.resolve('./src/@salaxy/ionic'),
};
webpackConfig.dev.externals = {
    "request-promise": "request-promise"
};

webpackConfig.prod.resolve.alias = {
    '@salaxy/ng': path.resolve('./src/@salaxy/ng'),
    '@salaxy/ionic': path.resolve('./src/@salaxy/ionic'),
};
webpackConfig.prod.externals = {
    "request-promise": "request-promise"
};