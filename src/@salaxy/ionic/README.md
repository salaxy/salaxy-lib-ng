Ionic libraries for Salaxy platform
===================================

Latest state-of-the-art and mobile-first user interface components for the Salaxy platfrom.

Usage:

- Run `npm install --save @salaxy/ionic@latest`
- Reference or copy files from `node_modules/@salaxy/ionic`

For more information and documentation about Salaxy platform, please go to https://developers.salaxy.com 
