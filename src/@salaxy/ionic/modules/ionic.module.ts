// Usecases: Angular and Ionic
import { NgModule } from "@angular/core";
import { HttpModule } from "@angular/http";
import { TranslateModule } from "@ngx-translate/core";
import { CalendarModule } from "ion2-calendar";
import { IonicModule, Label } from "ionic-angular";
import { IonAffixModule } from "ion-affix";
import { NgxBarcodeModule } from 'ngx-barcode';

/** Salaxy libraries */
import {
  NgTranslations,
  SalaxyNgComponentsModule,
  SalaxyNgServicesModule
} from "@salaxy/ng";

// This library
import {
  // Calc
  CalcAddRowComponent,
  CalcCardsComponent,
  CalcUsecaseComponent,
  CalcUsecaseBaseComponent,
  CalcUsecaseChildCareComponent,
  CalcUsecaseCleaningComponent,
  CalcUsecaseConstructionComponent,
  CalcUsecaseMllComponent,
  CalcUsecaseSantaClausComponent,
  CalcSelectUsecaseComponent,
  CalcGroupedListComponent,
  CalcListComponent,
  CalcOverviewComponent,
  CalcSharingComponent,
  CalcPaymentComponent,
  CalcPaymentListComponent,
  CalcPeriodComponent,
  CalcRowComponent,
  CalcWorkerComponent,
  CalcCalculatorBasicComponent,

  // Common
  AvatarEditorComponent,
  AvatarComponent,
  CardComponent,
  DebuggerComponent,
  IconWithBadgeComponent,
  InfoTextComponent,
  PopunderComponent,
  ProgressComponent,
  RoundedIonItemIconComponent,
  SpinnerComponent,
  SpinnerSecondaryComponent,

  // Contact
  ContactListComponent,
  ContactDetailsComponent,

  // Contract
  ContractListComponent,
  ContractEditSummaryComponent,
  ContractEditEmployerComponent,
  ContractEditWorkerComponent,
  ContractEditWorkerSelectComponent,
  ContractEditWorkTasksComponent,
  ContractEditSalaryComponent,
  ContractEditOtherComponent,
  ContractPreviewComponent,

  // Credit Transfer
  CreditTransferFormComponent,

  // Form-controls
  FormButtonComponent,
  FormButtonSecondaryComponent,
  InputNumberComponent,
  InputPeriodComponent,
  InputTextComponent,
  InputEmailComponent,
  SelectComponent,
  ValidationMessagesComponent,

  // TODO: Rename login as account, security or something
  LoginComponent,

  // Master-detail
  MasterDetailComponent,
  MasterDetailDefaultEmptyComponent,
  MasterDetailDetailTemplateComponent,
  MasterDetailPopDirective,
  MasterDetailPushDirective,
  MasterDetailService,
  MasterDetailMasterPushDirective,
  MasterDetailRootPushDirective,

  // Onboarding
  OnboardingIdentifierComponent,
  OnboardingPersonComponent,
  OnboardingPhoneVerifyComponent,
  OnboardingPhoneComponent,
  OnboardingTupasComponent,
  RecoverUidPwdComponent,

  // Settings
  SettingsEditButtonsComponent,
  SettingsGeneralComponent,
  SettingsHouseholdSubsidyComponent,
  SettingsInsuranceComponent,
  SettingsOverviewComponent,
  SettingsPensionComponent,
  SettingsTaxCardEmailComponent,
  SettingsTaxComponent,
  SettingsUnemploymentComponent,
  TaxCardComponent,

  // TaxCard
  TaxCardListComponent,
  TesterComponent,

  // Worker
  WorkerCalculationListComponent,
  WorkerDetailsComponent,
  WorkerListComponent,

  // Dummy
  DUMMYComponent,
  LanguageSelectorComponent,
} from "../components/index";

import { SxyUiHelpers } from "../services/index";
import { dictionary } from "../i18n/dictionary";

/** All components: goes to Declarations (for this module) and Exports (for apps and other modules using this module). */
const salaxyComponents = [
  // Calc
  CalcAddRowComponent,
  CalcCardsComponent,
  CalcUsecaseComponent,
  CalcUsecaseBaseComponent,
  CalcUsecaseChildCareComponent,
  CalcUsecaseCleaningComponent,
  CalcUsecaseConstructionComponent,
  CalcUsecaseMllComponent,
  CalcUsecaseSantaClausComponent,
  CalcSelectUsecaseComponent,
  CalcGroupedListComponent,
  CalcListComponent,
  CalcOverviewComponent,
  CalcSharingComponent,
  CalcPaymentComponent,
  CalcPaymentListComponent,
  CalcPeriodComponent,
  CalcRowComponent,
  CalcWorkerComponent,
  CalcCalculatorBasicComponent,

  // Common
  AvatarEditorComponent,
  AvatarComponent,
  CardComponent,
  DebuggerComponent,
  IconWithBadgeComponent,
  InfoTextComponent,
  LanguageSelectorComponent,
  PopunderComponent,
  ProgressComponent,
  RoundedIonItemIconComponent,
  SpinnerComponent,
  SpinnerSecondaryComponent,

  // Contact
  ContactListComponent,
  ContactDetailsComponent,

  // Contract
  ContractListComponent,
  ContractEditSummaryComponent,
  ContractEditEmployerComponent,
  ContractEditWorkerComponent,
  ContractEditWorkerSelectComponent,
  ContractEditWorkTasksComponent,
  ContractEditSalaryComponent,
  ContractEditOtherComponent,
  ContractPreviewComponent,

  // Credit Transfer
  CreditTransferFormComponent,

  // Form-controls
  FormButtonComponent,
  FormButtonSecondaryComponent,
  InputNumberComponent,
  InputPeriodComponent,
  InputTextComponent,
  InputEmailComponent,
  SelectComponent,
  ValidationMessagesComponent,

  // TODO: Rename login as account, security or something
  LoginComponent,

  // Master-detail
  MasterDetailComponent,
  MasterDetailDefaultEmptyComponent,
  MasterDetailDetailTemplateComponent,
  MasterDetailPopDirective,
  MasterDetailPushDirective,
  MasterDetailMasterPushDirective,
  MasterDetailPushDirective,
  MasterDetailRootPushDirective,

  // Onboarding
  OnboardingIdentifierComponent,
  OnboardingPersonComponent,
  OnboardingPhoneVerifyComponent,
  OnboardingPhoneComponent,
  OnboardingTupasComponent,
  RecoverUidPwdComponent,

  // Settings
  SettingsEditButtonsComponent,
  SettingsGeneralComponent,
  SettingsHouseholdSubsidyComponent,
  SettingsInsuranceComponent,
  SettingsOverviewComponent,
  SettingsPensionComponent,
  SettingsTaxCardEmailComponent,
  SettingsTaxComponent,
  SettingsUnemploymentComponent,

  // TaxCard
  TaxCardComponent,
  TaxCardListComponent,
  TesterComponent,

  // Worker
  WorkerCalculationListComponent,
  WorkerDetailsComponent,
  WorkerListComponent,

  // Dummy
  DUMMYComponent,
];

/**
 * Entry components: These components may be used as "Pages" in ion-nav or "Sub-pages" in "Detail pages".
 */
const salaxyEntryComponents = [

  // Common
  AvatarEditorComponent,

  // Calc
  CalcAddRowComponent,
  CalcCardsComponent,
  CalcUsecaseComponent,
  CalcUsecaseChildCareComponent,
  CalcUsecaseCleaningComponent,
  CalcUsecaseConstructionComponent,
  CalcUsecaseMllComponent,
  CalcUsecaseSantaClausComponent,
  CalcSelectUsecaseComponent,
  CalcGroupedListComponent,
  CalcListComponent,
  CalcOverviewComponent,
  CalcSharingComponent,
  CalcPaymentComponent,
  CalcPaymentListComponent,
  CalcPeriodComponent,
  CalcRowComponent,
  CalcWorkerComponent,
  CalcCalculatorBasicComponent,

  DebuggerComponent,
  TesterComponent,
  PopunderComponent,

  // Contact
  ContactListComponent,
  ContactDetailsComponent,

  // Contract
  ContractListComponent,
  ContractEditSummaryComponent,
  ContractEditEmployerComponent,
  ContractEditWorkerComponent,
  ContractEditWorkerSelectComponent,
  ContractEditWorkTasksComponent,
  ContractEditSalaryComponent,
  ContractEditOtherComponent,
  ContractPreviewComponent,

  // Credit Transfer
  CreditTransferFormComponent,

  // MasterDetail
  MasterDetailDefaultEmptyComponent,
  MasterDetailDetailTemplateComponent,

  // Onboarding
  OnboardingIdentifierComponent,
  OnboardingPersonComponent,
  OnboardingPhoneVerifyComponent,
  OnboardingPhoneComponent,
  OnboardingTupasComponent,
  RecoverUidPwdComponent,

  // Worker
  WorkerCalculationListComponent,
  WorkerDetailsComponent,
  WorkerListComponent,

  // TaxCard
  TaxCardComponent,
  TaxCardListComponent,

  // Settings
  SettingsTaxComponent,
  SettingsGeneralComponent,
  SettingsPensionComponent,
  SettingsUnemploymentComponent,
  SettingsInsuranceComponent,
  SettingsHouseholdSubsidyComponent,
  SettingsTaxCardEmailComponent,
];

/**
 * Salaxy user interface components with mobile-first design.
 * Impolemented using the Ionic framework.
 */
@NgModule({
  declarations: salaxyComponents,
  entryComponents: salaxyEntryComponents,
  exports: salaxyComponents,
  imports: [
    HttpModule,
    IonicModule,
    TranslateModule,
    CalendarModule,
    SalaxyNgComponentsModule,
    SalaxyNgServicesModule,
    IonAffixModule, // TODO: Consider do we really need this. Prefer adding a custom implementation.
    NgxBarcodeModule,
  ],
  providers: [
    MasterDetailService,
    SxyUiHelpers,
  ],
})
export class SalaxyIonicModule {
  constructor(translate: NgTranslations) {
    translate.addDictionaries(dictionary);
  }
}
