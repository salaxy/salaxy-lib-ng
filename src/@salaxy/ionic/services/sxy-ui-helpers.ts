import { Injectable } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";
import { LoadingController, ModalController, Loading, LoadingOptions } from "ionic-angular";

import { Dates, Calculation } from "@salaxy/core";
import { NgTranslations } from "@salaxy/ng";

import { ContractPreviewComponent } from "../components/contract/contract-preview";
import { PopunderComponent } from "../components/common/popunder";

import { CalendarComponentOptions, CalendarModalOptions, CalendarModal } from "ion2-calendar";
import * as momentImport from "moment";
/** workaround */
const moment = momentImport;


/**
 * Provides misc. user interface helper methods that are related to Ionic.
 * NOTE: Angular-specific helpers hould be in "@salaxy/ng" and framework independent logic in "@salaxy/core".
 */
@Injectable()
export class SxyUiHelpers {

  /** Default options for the ion calendar component */
  public defaultIonCalendarOptions: CalendarModalOptions = {
    autoDone: true,
    canBackwardsSelected: true,
    color: "cal-color",
    closeIcon: true,
    doneIcon: true,
    weekStart: 1,
    weekdays: ["Su", "Ma", "Ti", "Ke", "To", "Pe", "La"],
  };

  constructor(
    private loadingCtrl: LoadingController,
    private sanitizer: DomSanitizer,
    public modalCtrl: ModalController,
    private translate: NgTranslations,
  ) {}

  /**
   * Shows a customized Ionic Loading component (spinner) with optional messages.
   * Returns an Ionic Loading Control where you should call dismiss() method to close the dialog.
   * Note that there is separate control SXY-SPINNER to show the spinner as a control on a view.
   *
   * @param heading Translation key for heading text for the loader
   * @param text Optional translation key for additional text paragraph describing what is happening.
   * @param options Ionic loading options. Mainly to set enableBackdropDismiss or dismissOnPageChange to true.
   * Spinner is always set to 'hide' and content to custom HTML because we show a customized spinner.
   * Also, "sxy-loading-dialog" is added to cssClass.
   *
   * @example
   * const loader = this.uiHelper.showLoading("SALAXY.UI_TERMS.isSaving", "SALAXY.UI_TERMS.pleaseWait");
   * setTimeout(() => {
   *   loader.dismiss();
   * }, 2000);
   */
  public showLoading(heading: string, text?: string, options: LoadingOptions = {}): Loading {
    heading = this.translate.get(heading);
    if (text) {
      text = this.translate.get(text);
    }
    const content = this.sanitizer.bypassSecurityTrustHtml(`
      <div class="sxy-spinner">
        <svg class="sxy-spinner-bg" viewBox="0 0 64 64"><circle cx="32" cy="32" r="26"></circle></svg>
        <svg class="sxy-spinner-spin" viewBox="0 0 64 64">
          <circle transform="translate(32,32)" r="26"></circle>
        </svg>
      </div>
      <div text-center>
        <h2>${heading}</h2>
        <p>${text}</p>
      </div>
    `);
    options.spinner = "hide";
    options.cssClass = (options.cssClass ? options.cssClass + " " : "") + "sxy-loading-dialog";
    options.content = content as any;
    const loading = this.loadingCtrl.create(options);
    loading.present();
    return loading;
  }

    /**
     * Shows a confirm dialog. Like window.confirm() in JavaScript.
     * NOTE: The promise is rejected if user clicks cancel or close.
     * This means that you need to always catch the promise, otherwise you will get an error in console.
     *
     * @param heading Heading text of the confirm question as translation key
     * @param text Optional additional text paragraph describing what is happening.
     * @param okText Possibility to override the OK button text. The given text is run through translation.
     * @param cancelText Possibility to override the Cancel button text. The given text is run through translation.
     *
     * @returns A promise that resolves if user clicks OK.
     * The promise is rejected if user clicks cancel or close.
     * This means that you need to always catch, otherwise you will get an error in console.
     *
     * @example
     * this.uiHelpers
     *     .showConfirm("SALAXY.UI_TERMS.areYouSure", "SALAXY.UI_TERMS.sureToDeleteRecord")
     *     .then(() => { this.uiHelpers.showAlert("success"); })
     *     .catch(() => { this.uiHelpers.showAlert("error"); });
     */
    public showConfirm(heading: string, text?: string, okText = "SALAXY.UI_TERMS.ok", cancelText = "SALAXY.UI_TERMS.cancel"): Promise<void> {
      heading = this.translate.get(heading);
      text = this.translate.get(text);
      return new Promise<void>((resolve, reject) => {
        const modal = this.modalCtrl.create(PopunderComponent, {
          buttons: [{
            text: okText,
            value: true,
          }, {
            text: cancelText,
            value: false,
            transparent: true,
          }],
          heading,
          text,
          contentType: "confirm",
        }, {});
        modal.onDidDismiss((data, role) => {
          if (data.value) {
            resolve();
          } else {
            reject();
          }
        });
        modal.present();
      });
    }

    /**
     * Shows an alert dialog. Like window.alert() in JavaScript.
     *
     * @param heading Heading text as translation key
     * @param text Optional additional text paragraph describing what is happening.
     * @param buttonText Possibility to override the button text. The given text is run through translation.
     *
     * @returns A promise with boolean: True if OK button was clicked, false if the dialog is closed / dismissed in another way.
     *
     * @example this.uiHelpers.showAlert("Please note", "There is this thing that we want to tell you.");
     */
    public showAlert(heading: string, text?: string, buttonText = "SALAXY.UI_TERMS.ok"): Promise<boolean> {
      heading = this.translate.get(heading);
      return new Promise<boolean>((resolve, reject) => {
        const modal = this.modalCtrl.create(PopunderComponent, {
          buttons: [{
            text: buttonText,
            value: true,
          }],
          heading,
          text,
          contentType: "confirm",
        }, {});
        modal.onDidDismiss((data, role) => {
          resolve(data.value ? true : false);
        });
        modal.present();
      });
    }

    /**
     * Shows a popunder Calendar control.
     *
     * @param initialValue Initial value to set for the calendar. The value is parsed through Dates.asDate helper.
     *
     * @returns A promise with selected date as ISO date string.
     * NOTE: If user dismisses the popup, promise is not resolved nor rejected - this is subject to change.
     *
     * @example
     * this.uiHelpers
     *     .showPopunderCalendar("2018-09-21")
     *     .then((date) => { this.uiHelpers.showAlert("Here: " + date); })
     */
    public showPopunderCalendar(initialValue: any): Promise<any> {
      return new Promise<any>((resolve, reject) => {
        const modal = this.modalCtrl.create(PopunderComponent, {
          date: initialValue,
          contentType: "calendar",
          buttons: [{
            text: "SALAXY.UI_TERMS.cancel",
            value: null,
            type: "clear",
          }],
        }, {});
        modal.onDidDismiss((data, role) => {
          if (data.value) {
            resolve(Dates.asDate(data.value));
          }
        });
        modal.present();
      });
    }

    /**
     * Creates an Ion Calendar Modal with single date selection
     *
     * @param title Modal title as translation key
     * @param startDate Date as a string, to be set as the default date.
     * @param additionalOptions Options object to be merged with Salaxy's default options.
     *
     * @returns A promise with selected date as ISO date string.
     *
     * @example
     * this.uiHelpers
     *     .showIonCalendarSingle("Choose a date", "2018-09-21")
     *     .then((date) => { this.uiHelpers.showAlert("Here: " + date); });
     */
    public showIonCalendarSingle(title, startDate, additionalOptions: CalendarComponentOptions = {}): Promise<any> {
      title = this.translate.get(title);
      return new Promise<any>((resolve, reject) => {
        const opts = Object.assign(this.defaultIonCalendarOptions, {
          pickMode: "single",
          title,
          defaultDate: moment(startDate),
          autoDone: false,
        }, additionalOptions);
        const calendarModal = this.modalCtrl.create(CalendarModal, { options: opts }, {});
        calendarModal.onDidDismiss((date, type) => {
          if (type === "done") {
            resolve(date.dateObj);
          }
        });
        calendarModal.present();
      });
    }

    /**
     * Creates an Ion Calendar Modal with date range selection
     *
     * @returns A promise with selected date range an array of two ISO date strings.
     * @param title Modal title as translation key
     * @param startDate Start date as a string, to be set as the default start date.
     * @param endDate End date as a string, to be set as the default end date.
     * @param additionalOptions Options object to be merged with Salaxy's default options.
     *
     * @example
     * this.uiHelpers
     *       .showIonCalendarRange("Choose a date range", "2018-09-21", "2018-10-21")
     *       .then((datesArr) => { this.uiHelpers.showAlert("Here: " + Dates.asDate(datesArr[0]) + " - " + Dates.asDate(datesArr[1])); })
     */
    public showIonCalendarRange(title, startDate, endDate, additionalOptions: CalendarComponentOptions = {}): Promise<any> {
      title = this.translate.get(title);
      return new Promise<any>((resolve, reject) => {
        const opts = Object.assign(this.defaultIonCalendarOptions, {
          title,
          pickMode: "range",
          autoDone: false,
          defaultScrollTo: startDate,
          defaultDateRange: {
            from: startDate,
            to: endDate,
          },
        }, additionalOptions);
        const calendarModal = this.modalCtrl.create(CalendarModal, { options: opts }, {});
        calendarModal.onDidDismiss((date, type) => {
          if (type === "done") {
            resolve([ date.from.dateObj, date.to.dateObj ]);
          }
        });
        calendarModal.present();
      });
    }

    /**
     * Shows a popup for user to set the period to a Calculation.
     * Promise is resolved, if user clicks OK.
     * NOTE: If user dismisses the popup, promise is not resolved nor rejected - this is subject to change.
     *
     * NOTE: Current implementation of sxy-period componentn changes the calculation period value
     * immediately when calendar component is clicked.
     * I.e. the value may be set even when "Cancel" / "Close" / "Dismiss" is clicked.
     * This is expected to be fixed in the future.
     *
     * @param calculation Calculation where the period should be set.
     *
     * @example
     * this.uiHelpers.showSetPeriod(this.current);
     */
    public showSetPeriod(calculation: Calculation): Promise<void> {
      return new Promise<void>((resolve, reject) => {
        const modal = this.modalCtrl.create(PopunderComponent, {
          calc: calculation,
          contentType: "period",
          buttons: [{
            text: "SALAXY.UI_TERMS.cancel",
            value: null,
            type: "clear",
          }],
        }, {});
        modal.onDidDismiss((data, role) => {
          if (data.value) {
            resolve();
          }
        });
        modal.present();
      });
    }

    /**
     * Show Contract Preview Modal with autoscaling and zoom buttons.
     *
     * @param promisedContent Promise for getting contents from the server (as HTML string).
     *
     * @example
     * const promisedContent = this.context.contracts.getPreview(this.current)
     *   .then((html: string) => {
     *     html = html.substring(html.indexOf('<div class="document-preview'));
     *     // Remove two closing </div> tags from the end
     *     let nthLastToRemove = 2;
     *     while (nthLastToRemove > 0) {
     *       html = html.substring(0, html.lastIndexOf("</div>"));
     *       nthLastToRemove--;
     *     }
     *     return html;
     *   });
     * this.uiHelpers.showContractPreviewModal(promisedContent)
     *  .then(() => {
     *    // Could do something
     *  });
     */
    public showContractPreviewModal(promisedContent: Promise<any>): Promise<any> {
      return new Promise<void>((resolve, reject) => {
        const previewModal = this.modalCtrl.create(ContractPreviewComponent, { content: promisedContent }, { cssClass: "preview-A4" });
        previewModal.onDidDismiss((data) => {
          resolve();
        });
        previewModal.present();
      });
    }
}
