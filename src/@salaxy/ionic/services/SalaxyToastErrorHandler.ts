import { Inject } from "@angular/core";
import { ToastController } from "ionic-angular";

import { NgTranslations, SalaxyErrorHandler } from "@salaxy/ng";

/**
 * Salaxy error handler for ionic apps.
 */
export class SalaxyToastErrorHandler extends SalaxyErrorHandler {

  /**
   * Shows error in a toast control.
   *
   * @param toastController - ToasController.
   * @param translations - TranslateService.
   */
  constructor(
    @Inject(ToastController) private toastCtrl: ToastController,
    @Inject(NgTranslations) private translations: NgTranslations,
  ) {
    super();
  }

  /**
   * Handles any error from Ionic app.
   *
   * @param err - Error to handle.
   */
  public handleError(err: any): void {

    super.handleError(err, false);

    const defaultTranslationKey = "SALAXY.COMMON.ERRORS.defaultDescription";
    const closeTranslationKey = "SALAXY.UI_TERMS.close";
    const toast = this.toastCtrl.create({
      closeButtonText: this.translations.get(closeTranslationKey),
      cssClass: "salaxy-error-toast",
      dismissOnPageChange: false, // true dismisses right away after present
      duration: 15000,
      message:
        (err ? err.message : null) ||
        this.translations.get(defaultTranslationKey),
      position: "bottom",
      showCloseButton: true
    });
    // Show toast
    toast.present();
    // Rethrow
    throw err;
  }
}
