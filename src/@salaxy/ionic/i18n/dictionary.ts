/* tslint:disable */
var _dictionary = _dictionary || {};
_dictionary["en"] = {
  "SALAXY": {
    "ION": {
      "COMPONENTS": {
        "Avatar": {
          "hideTips": "E Piilota ohje",
          "showTips": "E Näytä ohje"
        },
        "CalcAddRow": {
          "benefits": "E Luontoisetuudet",
          "deductions": "E Vähennykset",
          "expenses": "E Verovapaat korvaukset",
          "mostCommon": "E Yleisimmät",
          "salaryAdditions": "E Palkan perusteella lasketut lisät",
          "salaryRows": "E Palkkarivit",
          "searchResults": "E Hakutulokset"
        },
        "CalcCalculatorBasic": {
          "addRow": "E Lisää rivi...",
          "addRowTitle": "E Lisää uusi rivi...",
          "asideTitle": "E Lähetä palkkalaskelma",
          "basicDetails": "E Laskelman perustiedot",
          "chooseWorker": "E Valitse työntekijä...",
          "continueEditing": "E Jatka muokkaamista",
          "costToEmployer": "E Kustannus työnantajalle",
          "editorFootnote": {
            "html": "E <p class='lead'>Voit lähettää palkkalaskelman työnantajallesi. Luo laskelma tässä ja klikkaa <strong>Laskelman jakaminen</strong>.</p>"
          },
          "noTel": "E Ei puhelinnumeroa",
          "paidToWorker": "E Maksetaan työntekijälle",
          "rowDetails": "E Rivin tiedot",
          "seeMoreDetails": "E Katso tarkempi laskelma",
          "sharingCalc": "E Laskelman jakaminen",
          "title": "E Palkkalaskuri",
          "totalGrossSalary": "E Bruttopalkka",
          "workFromPeriod": "E Työ ajalta",
          "youMarkedAsWorker": "E Sinut on merkitty työntekijäksi"
        },
        "CalcCards": {
          "employerCalc": "E Työnantajan laskelma",
          "paymentsCalc": "E Yhteiskunnan laskelma",
          "salaryPayment": "E Salary payment",
          "segmentEmployer": "E Työnantaja",
          "segmentPayments": "E Yhteiskunta",
          "segmentWorker": "E Työntekijä",
          "showBasicInfo": "E Näytä perustiedot...",
          "showDetails": "E Näytä yksityiskohdat",
          "workerCalc": "E Työntekijän laskelma"
        },
        "CalcOverview": {
          "employerCalc": "E Työnantajan laskelma",
          "hideChart": "Hide the chart",
          "hideTables": "Hide the tables",
          "noRows": "E This calculation doesn't have any rows",
          "paymentsCalc": "E Yhteiskunnan laskelma",
          "segmentEmployer": "E Työnantaja",
          "segmentPayments": "E Yhteiskunta",
          "segmentWorker": "E Työntekijä",
          "showBasicInfo": "E Näytä yksityiskohdat",
          "showDetails": "E Näytä yksityiskohdat",
          "showDetailsInTables": "Show detail in tables",
          "workerCalc": "E Työntekijän laskelma"
        },
        "CalcPayment": {
          "biweekly": "E Kahden viikon välein",
          "dueDate": "E Maksupäivä",
          "footnotes": "E Jos palkkapäivä on pyhäpäivä, lähetämme maksun edellisenä arkipäivänä. Toisto lopetaan, kun jätät palkan maksamatta tai peruuttamalla Palkkaus.fi-palvelusta.",
          "immediately": "E Heti",
          "monthly": "E Kuukausittain",
          "nextPayment": "E Seuraava maksukerta",
          "once": "E Vain kerran",
          "payDay": "E Palkkapäivä",
          "repeating": "E Maksun toistuvuus",
          "twiceamonth": "E Kahdesti kuussa",
          "type": "E Maksutapa:",
          "typeNordea": "E Verkkomaksu, Nordea (1h)",
          "typePaytrail": "E Verkkomaksu, muut (1 arkipäivä)",
          "typeSiirto": "E Siirto-maksu (1 min)",
          "weekly": "E Viikottain"
        },
        "CalcPaymentList": {
          "noPersonChosen": "E No person chosen",
          "salaryPayment": "E Salary payment"
        },
        "CalcPeriod": {
          "dateRange": "E Palkkaa ajalta",
          "workDayCount": "E Työpäivien määrä"
        },
        "CalcRow": {
          "add": "E Lisää rivi",
          "automaticallyCreated": "E Tämä rivi on automaattisen laskennan luoma.",
          "delete": "E Poista rivi",
          "editHere": "E Muokkaa riviä täältä",
          "updateCalc": "E Päivitä laskelma"
        },
        "CalcSelectUsecase": {
          "back": "E ...takaisin",
          "prevMenu": "E Edellinen valikko"
        },
        "CalcSharing": {
          "changePasswordOrDelete": "E Vaihda salasana tai poista jako",
          "createAgainOrDelete": "E Luo uudelleen tai poista",
          "deleteDescription": "E Voit poistaa laskelman jakamisen, kun haluat.",
          "deleteTitle": "E Poista jako",
          "easyLinkDescription": {
            "html": "E Helppo jakaa esim. tekstiviestinä tai puhelimessa, mutta ei suojaa yksityisyyttäsi, koska linkin voi arvata.<br>Esim. <a href=\"#\" target=\"_blank\">https://palkkaus.fi/GUVWXYZuvwxyz12345</a>"
          },
          "easyLinkTitle": "E Helppo linkki",
          "easyLinkWithPasswordDescription": "E Voit valita linkkikohtaisen salasanan. Jos salasanasi on hyvä, myös yksityisyyden suoja on hyvä.",
          "easyLinkWithPasswordTitle": "E Helppo linkki + salasana",
          "noteHaveSetPassword": "E Huomaa, että olet lisäksi asettanut salasanan.",
          "notSharedYet": "E Tätä laskelmaa ei ole vielä jaettu. Valitse tapa, jolla haluat jakaa laskelman.",
          "recreateOrRemove": "E Luo uudelleen tai poista",
          "safeLinkDescription": {
            "html": "E Pitkä linkki ei ole arvattavissa. Toimii hyvin sähköpostissa.<br>Esim. <a href=\"#\">https://palkkaus.fi/GUVWXYZuvwxyz12345</a>"
          },
          "safeLinkTitle": "E Turvallinen linkki",
          "shareLink": {
            "html": "E<p>Laskelma on jaettu. Linkki on:<br><p><p class=\"lead\"><strong><a href=\"{{ link }}\" target=\"_blank\">{{ link }}</a></strong></p>"
          }
        },
        "CalcUsecaseBase": {
          "dailyExpenses": "E Työkalukorvaus (€/pv)",
          "dailyTravelExpenses": "E Matkakorvaus (€/pv)",
          "dailyTravelExpensesKm": "E Matkat kotoa työpaikalle (km/pv)",
          "defaultPriceLabel": "E Hinta",
          "eurPerH": "E €/h",
          "expensesCustom": "E Työkalu- ja matkakorvaus",
          "hourCount": "E Tuntien määrä",
          "hoursPerPrice": "E {{amount}} tuntia x {{price}}",
          "hoursPerPriceWithSunday": "E {{amount}} tuntia x {{price}} + 100% sunnuntailisä",
          "householdDeduction": "E Kotitalousvähennys",
          "isSunday": "E Sunnuntai tai pyhäpäivä?",
          "longInfoText": {
            "html": "E <p>Laskelma ehdottaa kotitalousvähennystä työn tyypin mukaan, mutta ei automaattisesti laske lomakorvauksia yms. lisiä.</p><p>Voit lisätä tarvittavat lisät riveinä seuraavasta näkymästä. Lue lisää työnantajan velvollisuuksista osoitteesta Palkkaus.fi.</p><p>Teemme puolestasi kotitalousvähennysilmoituksen olettamuksella, että työ on tehty kotona tai vapaa-ajan asunnolla. Jos haluat tehdä ilmoituksen itse, poista valinta.</p>"
          },
          "noByTes": "E TES-taulukon perusteella",
          "noCalcAuto": "E Lasketaan automaattisesti",
          "periodText": "E Palkka ajalta (työpäivien määrä)",
          "pleaseInput": "E Syötä",
          "pricePerMonth": "E {{price}} / kk",
          "salaryKind": "E Palkkatyyppi",
          "salaryMessage": "E Viesti palkkalaskelmaan",
          "shortInfoText": {
            "html": "E <p>Yleinen palkkalaskelma ilman TES-sääntöjä.</p>"
          },
          "tesInclusions": "E TES-erät",
          "updatingCalc": "E Päivitetään laskelmaa",
          "wait": "E Odota hetkinen",
          "workDescription": "E Työn kuvaus",
          "yesAgreedPerDay": "E Erikseen sovittu (per päivä)",
          "yesAgreedTotalPayment": "E Sovittu kokonaiskorvaukseen"
        },
        "CalcUsecaseChildCare": {
          "childcareSubsidy": "E Yksityisen hoidon tuki",
          "footnoteIsChildcareSubsidy": "E Sivukulut lasketaan kokonaissummasta. Laskuri olettaa, että maksat palkan loma-ajalta erikseen. Voit myös lisätä loma-ajan palkan rivinä laskuriin. Kotitalousvähennystä ei voi käyttää, jos yksityisen hoidon tuki on mukana palkassa.",
          "footnoteIsNotChildcareSubsidy": "E Laskuri olettaa, että maksat palkan loma-ajalta erikseen. Voit myös lisätä loma-ajan palkan rivinä laskuriin.",
          "longInfoTextIfChildcareSubsidy": {
            "html": "E <p>Laskurilla voit laskea pitkäaikaisen lastenhoitajan palkan.</p><p>Laskuri ottaa huomioon myös yksityisen hoidon tuen. Tuki vaikuttaa sivukuluihin, vaikka Kela maksaa sen suoraan työntekijälle. Tuen kanssa ei voi samaan aikaan käyttää kotitalousvähennystä.</p>"
          },
          "longInfoTextIfNotChildcareSubsidyl": {
            "html": "E <p>Laskurilla voit laskea pitkäaikaisen lastenhoitajan palkan.</p><p>Jos valitset alla, teemme puolestasi kotitalousvähennysilmoituksen olettamuksella, että työ on tehty kotona tai vapaa-ajan asunnolla. Jos haluat tehdä ilmoituksen itse, poista valinta.</p>"
          },
          "shortInfoText": {
            "html": "E <p>Oma lastenhoitaja kotona.</p>"
          },
          "wageInAdditionToBenefits": "E Rahapalkka tuen lisäksi",
          "workerTotalPayment": "E Työntekijän palkka yhteensä"
        },
        "CalcUsecaseCleaning": {
          "contractLastedLessThanYear": "E Työsuhde kestänyt alle vuoden",
          "footnote": {
            "html": "E HUOM: Sinun tulee maksaa myös loma-ajan palkka ja sen lisäksi 50% lomarahaa loma-ajan palkasta. Voit lisätä ne riveinä tähän laskelmaan tai tehdä erillisen palkanmaksun.<a href='https://www.palkkaus.fi/Cms/Articles/siivous_ja_kiinteistoalan_tyoehtosopimukset' target='_blank'>Lue lisää täältä</a>."
          },
          "fullTime": "E Työtä yli 14pv/kk tai yli 35h/kk",
          "longInfoText": {
            "html": "E <p>Laskuri laskee automaattisesti lomakorvauksen maksettavaksi prosentteina palkasta.</p><p>Jos työtä tehdään yli 14pv/kk tai yli 35h/kk, pitää työntekijälle antaa mahdollisuus pitää loma. Tällöin maksetaan loma-ajalta palkka (jäljelle jäänyt osuus maksetaan työsuhteen päättyessä).</p>"
          },
          "shortInfoText": {
            "html": "E <p>Kodin tai vapaa-ajan asunnon siivous ja kunnossapito. Ilmoitetaan kotitalousvähennykseen.</p>"
          }
        },
        "CalcUsecaseConstruction": {
          "dailyExpenses": "E Työkalukorvaus (€/pv)",
          "dailyTravelExpenses": "E Matkakorvaus (€/pv)",
          "dailyTravelExpensesKm": "E Matkat kotoa työpaikalle (km/pv)",
          "expensesCustom": "E Työkalu- ja matkakorvaus",
          "longInfoText": {
            "html": "E <p>Lisäksi työehtosopimuksissa on määritelmät työkalukorvauksista ja matkakorvauksista. Laskurissa nämä voi laskea TES-taulukoiden mukaan tai vaihtoehtoisesti erikseen työntekijän kanssa sovittuina päiväkorvauksina.</p><p>Teemme puolestasi kotitalousvähennysilmoituksen olettamuksella, että työ on tehty kotona tai vapaa-ajan asunnolla. Jos haluat tehdä ilmoituksen itse, poista valinta.</p>"
          },
          "noByTes": "E TES-taulukon perusteella",
          "noCalcAuto": "E Lasketaan automaattisesti",
          "shortInfoText": {
            "html": "E <p>Laskuri huomioi yleissitovat työehtosopimukset rakennusalalla.<br>TES-eriä ovat lomakorvaus ja erillinen palkan osa (entinen 'pekkaset').</p>"
          },
          "subtitlePlusExpenses": "E {{ subtitle }} + TES-lisät ja korvaukset",
          "tesInclusions": "E TES-erät",
          "yesAgreedPerDay": "E Erikseen sovittu (per päivä)",
          "yesAgreedTotalPayment": "E Sovittu kokonaiskorvaukseen"
        },
        "CalcUsecaseMll": {
          "longInfoText": {
            "html": "E <p>Palkka maksetaan Mannerheimin Lastensuojeluliiton suositusten mukaan. Perhe maksaa lasten hoitajalle palkkana 8,20 €/tunti (sunnuntaina 16,40). Tämä on kokonaiskorvaus, joka sisältää lomakorvauksen. Tunnit ilmoitetaan puolen tunnin tarkkuudella, minimi 2 tuntia. Yhdellä hoitajalla voi olla hoidettavana enintään neljä lasta.</p><p>Huom! Kello 23 jälkeen perheen tulee huolehtia hoitajan turvallisesta kotimatkasta joko tarjoamalla autokyyti tai kustantamalla taksi.</p><p>Teemme puolestasi kotitalousvähennysilmoituksen olettamuksella, että työ on tehty kotona tai vapaa-ajan asunnolla. Jos haluat poistaa kotitalousvähennyksen tai tehdä ilmoituksen itse, käytä 'Muu lastenhoito'-laskuria.</p>"
          },
          "shortInfoText": {
            "html": "E <p>Mannerheimin Lastensuojeluliiton suositusten mukaan. Ilmoitetaan kotitalousvähennykseen.</p>"
          },
          "useCustomPrice": "E Muu kuin MLL:n palkkasuositus (8,20€/h)?"
        },
        "CalcUsecaseSantaClaus": {
          "agreedPayment": "E Sovittu palkkiosumma",
          "fixedPaymentAtChristmas": "E Kertakorvaus {{payment}} jouluaattona.",
          "longInfoText": {
            "html": "E <p>Kotitalousväehnnystä joulupukin palkkiosta ei verottajan mukaan saa.</p>"
          },
          "shortInfoText": {
            "html": "E <p>Joulupukkikin ansaitsee laillisen palkan. Palkkaus.fi ja Nordea tarjoavat sen tänä jouluna <strong>ilman palvelumaksua</strong></p>"
          }
        },
        "CalcWorker": {
          "askForTaxCard": "E Palkkaus.fi kysyy verokortin, henkilötunnuksen ja tilinumeron Siirto-puhelinnumeron työntekijältä.",
          "email": "E Sähköposti",
          "ibanNumber": "E Tilinumero (IBAN-muodossa)",
          "ssNumber": "E Henkilötunnus",
          "taxCard": "E Verokortti",
          "taxCardType": "E Verokortin tyyppi",
          "taxPercent": "E Veroprosentti",
          "tel": "E Puhelinnumero",
          "weAreAsking": "E Palkkaus.fi kysyy loput",
          "withoutIbanPaidViaSiirto": "E Ilman tilinumeroa palkka maksetaan Siirto-palvelun kautta."
        },
        "ContactDetails": {
          "address": "E Osoite",
          "businessName": "E Yrityksen nimi",
          "businessOrPerson": "E Luodaanko yritys vai Yksityishenkilö?",
          "email": "E Sähköposti",
          "firstName": "E Etunimi",
          "lastName": "E Sukunimi",
          "noteCannotDeleteExamples": "E Huom. Esimerkki-kontaktia ei voi tallentaa.",
          "postalArea": "E Postitoimipaikka",
          "postalCode": "E Postinumero",
          "removeContact": "E Poista kontakti",
          "ssNumber": "E Henkilötunnus",
          "sureToDeleteContact": "E Haluatko varmasti poistaa kontaktin?",
          "tel": "E Puhelinnumero",
          "vatId": "E Y-tunnus"
        },
        "ContactList": {
          "addNewCompany": "E Lisää yritys",
          "addNewPerson": "E Lisää henkilö",
          "noSearchResults": "E Kontakteja ei löytynyt tällä haulla...",
          "tryWithExamples": "E Kokeile esimerkkikontakteilla:"
        },
        "ContractEditEmployer": {
          "addressNotGiven": "E Osoitetta ei annettuna",
          "backToContactList": "E Vaihda valintaa",
          "displayNameNotGiven": "E Henkilöä ei valittuna",
          "editChosenContact": "E Muokkaa valittua kontaktia",
          "editMyDetails": "E Muokkaa omia tietoja",
          "emailNotGiven": "E Sähköpostia ei annettuna",
          "pageTitleChooseEmployerContact": "E Valitse työnantajakontakti",
          "pageTitleEditContactDetails": "E Muokkaa kontaktin tietoja",
          "pageTitleEditMyDetails": "E Muokkaa omia tietoja",
          "ssNumberNotGiven": "E Henkilötunnusta ei annettuna",
          "telNotGiven": "E Puhelinnumeroa ei annettuna",
          "updateMyCurrentDetails": "E Päivitä uudet yhteystietoni sopimukselle"
        },
        "ContractEditSalary": {
          "contractDateRange": "E Työsuhteen kesto",
          "contractStartDate": "E Työsuhde alkaa",
          "dateRange": "E Työsuhteen kesto",
          "noticeException": "E Irtisanomisaika määräytyy",
          "payDay": "E Palkanmaksupäivät",
          "salaryAmount": "E Palkan määrä {{amount}}",
          "salaryAmountPer": "E Salary amount",
          "salaryCompension": "E Työstä maksettava kertakorvaus",
          "salaryType": "E Palkan määräytymisen peruste",
          "startDate": "E Työsuhde alkaa",
          "testPeriod": "E Koeaika (kk)",
          "workHours": "E Työaika (tunteina)",
          "worksHoursType": {
            "label": "E Työajan jakso"
          }
        },
        "ContractEditSummary": {
          "chooseEmployerContact": "E Valitse työnantajakontakti",
          "chooseEmployerFromContacts": "E Valitse työnantaja kontakteista...",
          "chooseWorker": "E Valitse työntekijä",
          "chosenEmployerContact": "E Valittu työnantajakontakti",
          "chosenWorker": "E Valittu työntekijä",
          "collectiveContract": "E Työehtosopimus",
          "dateRange": "E Työsuhteen kesto",
          "displayNameNotGiven": "E Henkilöä ei valittuna",
          "emailNotGiven": "E Sähköpostia ei annettuna",
          "employer": "E Työnantaja",
          "employerAsSelf": "E Työnantaja (oma)",
          "mainWorkTasks": "E Tehtävät",
          "myWorkerDetails": "E Omat työntekijätiedot",
          "noticeException": "E Irtisanomisaika",
          "otherDetailsTitle": "E Muut sopimustiedot",
          "otherTerms": "E Muut sopimusehdot",
          "payDay": "E Palkanmaksupäivät",
          "salaryAmount": "E Palkan määrä",
          "salaryCompension": "E Työstä maksettava kertakorvaus",
          "salaryTitle": "E Palkkaus",
          "startDate": "E Työsuhde alkaa",
          "sureToDeleteContract": "E Haluatko varmasti poistaa sopimuksen?",
          "testPeriod": "E Koeaika (kk)",
          "worker": "E Työntekijä",
          "workerAsSelf": "E Työntekijä (oma)",
          "workHours": "E Työaika",
          "workTasksTitle": "E Työnkuva",
          "workType": "E Työsuhteen tyyppi"
        },
        "ContractEditWorker": {
          "addressNotGiven": "E Osoitetta ei annettuna",
          "backToWorkerList": "E Takaisin työntekijälistalle",
          "chooseWorker": "E Valitse työntekijä",
          "displayNameNotGiven": "E Henkilöä ei valittuna",
          "editMyDetails": "E Muokkaa omia tietoja",
          "emailNotGiven": "E Sähköpostia ei annettuna",
          "ibanNumberNotGiven": "E Tilinumeroa ei annettuna",
          "myWorkerDetaills": "E Omat työntekijätiedot",
          "ssNumberNotGiven": "E Henkilötunnusta ei annettuna",
          "telNotGiven": "E Puhelinnumeroa ei annettuna",
          "updateMyChangedDetailsToContract": "E Päivitä uudet yhteystietoni sopimukselle"
        },
        "ContractEditWorkerSelect": {
          "chosenWorker": "E Valittu työntekijä"
        },
        "ContractEditWorkTasks": {
          "collectiveContract": "E Sovellettava työehtosopimus",
          "mainWorkTasks": "E Pääasialliset työtehtävät",
          "workType": "E Työsuhteen tyyppi"
        },
        "ContractList": {
          "createFirst": "E Luo ensimmäinen sopimus",
          "createNew": "E Luo uusi sopimus"
        },
        "InfoText": {
          "showMoreLabel": "E Näytä lisää..."
        },
        "Login": {
          "pleaseLogin": "E Please log in"
        },
        "Onboarding": {
          "accountAlreadyExists": {
            "html": "E Account already exists. Please contact customer service (<a href=\"mailto:aspa@palkkaus.fi\">aspa@palkkaus.fi</a>) or phone (020 798 0230, between 9-16)."
          }
        },
        "OnboardingIdentifier": {
          "areYouPEP": "E Oletko poliittisesti vaikutusvaltainen henkilö (PEP)?",
          "byContinuingAgreeTerms": "E Jatkamalla eteenpäin hyväksyt pankkitunnuksellasi Palkkaus.fi-palvelun käyttöehdot.",
          "ssNumber": "E Henkilötunnus",
          "title": "E Henkilötunnus"
        },
        "OnboardingPerson": {
          "email": "E Sähköposti",
          "firstName": "E Etunimi",
          "lastName": "E Sukunimi",
          "title": "E Nimi ja sähköposti"
        },
        "OnboardingPhone": {
          "instruction": "E Puhelinnumeroa tarvitsemme yhteydenpitoa varten. Jos rekisteröit Siirto-palvelussa olevan puhelinnumeron, voit maksaa ja vastaanottaa palkkoja mobiilimaksuilla. Vahvistamme antamasi numeron.",
          "tel": "E Puhelinnumero",
          "title": "E Puhelinnumero"
        },
        "OnboardingPhoneVerify": {
          "instruction": "E Olemme lähettäneet sinulle SMS-viestin varmistaaksemme numerosi. Syötä viestissä oleva vahvistuskoodi tähän.",
          "phoneVerificationPin": "E Koodi SMS-viestistä",
          "title": "E Puhelinnumeron varmistaminen"
        },
        "OnboardingTupas": {
          "instruction": "E Siirry allekirjoittamaan",
          "title": "E Siirry allekirjoitukseen"
        },
        "SettingsEditButtons": {
          "generalSettingsTitle": "E General settings",
          "householdSubsidySettingsPageTitle": [
            "E Kotitalousvähennyksen asetukset TODO",
            "E Kotitalousvähennyksen asetukset TODO"
          ],
          "householdSubsidyTitle": "E Household subsidy",
          "insuranceSettingsPageTitle": "E Tapaturmavakuutuksen asetukset TODO",
          "insuranceTitle": "E Accident insurance",
          "pensionContractTitle": "E TyEL-vakuutus TODO",
          "pensionSettingsPageTitle": "E Pension settings",
          "taxCardAndEmailSettingsPageTitle": "E Palkkalaskelman lähetys- ja verokorttiasetukset TODO",
          "taxCardAndEmailTitle": "E Tax card and sending emails",
          "taxSettingsPageTitle": "E Ennakonpidätyksen asetukset TODO",
          "taxTitle": "E Tax prepayment to Tax Office",
          "unemploymentSettingsPageTitle": "E Unemployment insurance settings",
          "unemploymentTitle": "E Unemployment insurance"
        },
        "SettingsGeneral": {
          "employerIsResponsibleForTaxCardInputLabel": "E Työantaja kysyy verokorttitiedot työntekijältä",
          "isSalariesPaidThisYearQuestionLabel": "E Have you paid salaries through another service this year?",
          "otherSalariesAmountLabel": "E Salaries paid elsewhere (€)",
          "serviceSendsCalculationEmailsToWorkersLabel": "E Palvelu lähettää palkkalaskelman automaattisesti työntekijälle"
        },
        "SettingsHouseholdSubsidy": {
          "isHouseholdReportingLabel": "E Kotitalousvähennysraportin lähetys"
        },
        "SettingsInsurance": {
          "changeToPartnerInsurance": "E Haluan vaihtaa vakuutuksen If:iin",
          "insuranceCompanyLabel": "E Valitse vakuutusyhtiö",
          "insuranceContractNumberLabel": "E Vakuutusnumero",
          "insuranceContractQuestionLabel": "E Minulla on jo vakuutus",
          "orderNewPartnerInsurance": "E Haluan ottaa vakuutuksen If:ltä"
        },
        "SettingsOverview": {
          "if": {
            "html": "E <strong>HUOM:</strong> Jos maksat palkkoja yli 1200 € vuodessa, sinun täytyy ottaa lakisääteinen työtapaturmavakuutus."
          },
          "palkkaus": "E Työntekijä saa palvelusta palkkalaskelman ja kysymme verokortin. Arkistoimme tiedot 6 vuotta.",
          "tax": "E Ennakonpidätys, sairausvakuutusmaksut, ilmoitukset ja kotitalousvähennys",
          "tvr": "E Työttömyysvakuutusrahaston maksut ja ilmoitukset.",
          "varma": "E Työeläkemaksut ja -ilmoitukset (TyEL) tilapäisenä työnantajana."
        },
        "SettingsPension": {
          "changeToPartnerPension": "E Would you like to change your pension contract to Varma?",
          "noteAboutPartnerCompanies": "",
          "orderNewPartnerPension": "E Would you like to order a pension contract from Varma?",
          "pensionCompanyLabel": "E Pension contract company",
          "pensionContractNumberLabel": "E Pension contract number",
          "pensionContractQuestionLabel": "E Do you have a pension contract?",
          "pensionSelfHandlingQuestionLabel": "E Do you report pension payments and notifications?"
        },
        "SettingsTax": {
          "isMonthlyReportingEnabledLabel": "E Lähetämme kuukausi-ilmoituksen puolestasi",
          "isYearlyReportingEnabledLabel": "E Lähetämme vuosi-ilmoituksen puolestasi",
          "paymentReferenceNumberLabel": "E Tax reference number"
        },
        "SettingsUnemployment": {
          "prepaymentAmountLabel": "E Ennakkomaksujen määrä (€)",
          "prepaymentsQuestionLabel": "E Oletko maksanut TVR-ennakkomaksuja tälle vuodelle?",
          "selfHandlingQuestionLabel": "E Hoidatko TVR-ilmoitukset itse?"
        },
        "TaxCard": {
          "chooseType": "E Valitse verokortin tyyppi",
          "date": "E Voimaantulopäivä",
          "forYear": "E Vuosi",
          "incomeLimit": "E Vuosituloraja",
          "noIncomeLimit": "No income limit",
          "previousSalariesPaid": "E Palkat vuoden alusta",
          "step1Limit": "E alle 10 300€",
          "step1Percent": "E 0,0",
          "step2Limit": "E 10 300 - 17 650€",
          "step3Limit": "E yli 17 650€",
          "stepIncomePerYear": "E Tulot/vuosi",
          "stepsIncomeLimits": "E Portaikkoverokortin tulorajat, 2017",
          "stepTaxPercent": "E Veroprosentti",
          "taxPercent": "E Perusprosentti",
          "taxPercent2": "E Lisäprosentti",
          "type": "E Verokortin tyyppi"
        },
        "WorkerDetails": {
          "address": "E Osoite",
          "chosenWorker": "E Valittu työntekijä",
          "contactTitle": "E Yhteystiedot",
          "email": "E Sähköposti",
          "firstName": "E Etunimi",
          "ibanNumber": "E Tilinumero (IBAN-muodossa)",
          "identityTitle": "E Henkilötiedot",
          "lastName": "E Sukunimi",
          "paymentTitle": "E Maksutiedot",
          "postalArea": "E Postitoimipaikka",
          "postalCode": "E Postinumero",
          "removeWorker": "E Remove worker",
          "ssNumber": "E Henkilötunnus",
          "sureToDelete": "E Haluatko varmasti poistaa työntekijän?",
          "tel": "E Puhelinnumero",
          "workers": "E Työntekijät"
        },
        "WorkerList": {
          "addNew": "E Lisää uusi työntekijä",
          "changeRoleToEmployerForViewing": "E You are currently in Worker Role. Please change role to Employer (Household or Company) for viewing the list of workers.",
          "chosenWorker": "E Valittu työntekijä",
          "noSearchResults": "E Työntekijöitä ei löytynyt tällä haulla...",
          "tryWithExamples": "E Kokeile esimerkkityöntekijöillä",
          "workerDetails": "E Työntekijän tiedot"
        }
      },
      "LANG_SELECT": {
        "optionName": "In English",
        "title": "Choose language"
      },
      "PAGES": {
        "SwitchProfile": {
          "asideBody": {
            "html": "E <p>Palkkaus.fi service has the following user interfaces:</p><ul><li>Household as an employer</li><li>Worker</li><li>Company or Association</li></ul>"
          },
          "changeRoleOrLogout": "E You may change your active role or logout:",
          "companyOrAssocSubtitle": "E For entrepreneurs and associations",
          "householdSubtitle": "E Pay salaries for household workers",
          "title": "E User Profile",
          "titleAccountDetails": "E Account Details",
          "usingAsAnon": "E You are using DEMO-service in anonymous mode. Please login here.",
          "workerOldSubtitle": "E Old Palkkaus.fi user interface",
          "workerSubtitle": "E Add tax cards, find salary reports"
        },
        "WorkerProfile": {
          "addressLabel": "E Address",
          "citiesLabel": "E Kaupungit/kunnat, joissa työskentelen",
          "citiesPageTitle": "E Kaupungit/kunnat, joissa työskentelen",
          "contactInfoCardTitle": "E Yhteystiedot",
          "editGeneralInfoButtonText": "E Muokkaa tietoja",
          "emailLabel": "E Sähköposti",
          "generalInfoPageTitle": "E Osaajaprofiilin yleiset asetukset",
          "isPrivate": "E näkyy vain sinulle",
          "isPublic": "E on julkinen",
          "jobDescriptionLabel": "E Kerro millainen olet työntekijänä",
          "jobDetailDescriptionLabel": "E Tarkempi kuvaus töistä, joita teen",
          "jobDetailLabel": "E Tarkempi kuvaus töistä",
          "jobDetailPageTitle": "E Tarkempi kuvaus osaamisestasi",
          "jobsLabel": "E Työt, joita teen",
          "jobsPageTitle": "E Valitse työt, joita teet",
          "phoneNumberLabel": "E Puhelinnumero",
          "portfolioLabel": "E Työnäytteet/portfolio",
          "portfolioLinkLabel": "E Linkki työnäytteisiin tai portfolioon",
          "portfolioPageTitle": "E Työnäytteet / portfolio",
          "profilePageTitle": "E Osaajaprofiili",
          "profileVisibilityLabel": "E Profiilin näkyvyys",
          "searchCitiesPlaceholder": "E Etsi kaupunkeja/kuntia",
          "searchJobsPlaceholder": "E Etsi työnimikkeitä",
          "skillsCardTitle": "E Osaaminen",
          "yourProfileLabel": "E Profiilisi"
        }
      }
    }
  }
};
_dictionary["fi"] = {
  "SALAXY": {
    "ION": {
      "COMPONENTS": {
        "Avatar": {
          "hideTips": "Piilota ohje",
          "showTips": "Näytä ohje"
        },
        "CalcAddRow": {
          "benefits": "Luontoisetuudet",
          "deductions": "Vähennykset",
          "expenses": "Verovapaat korvaukset",
          "mostCommon": "Yleisimmät",
          "salaryAdditions": "Palkan perusteella lasketut lisät",
          "salaryRows": "Palkkarivit",
          "searchResults": "Hakutulokset"
        },
        "CalcCalculatorBasic": {
          "addRow": "Lisää rivi...",
          "addRowTitle": "Lisää uusi rivi...",
          "asideTitle": "Lähetä palkkalaskelma",
          "basicDetails": "Laskelman perustiedot",
          "chooseWorker": "Valitse työntekijä...",
          "continueEditing": "Jatka muokkaamista",
          "costToEmployer": "Kustannus työnantajalle",
          "editorFootnote": {
            "html": "<p class='lead'>Voit lähettää palkkalaskelman työnantajallesi. Luo laskelma tässä ja klikkaa <strong>Laskelman jakaminen</strong>.</p>"
          },
          "noTel": "Ei puhelinnumeroa",
          "paidToWorker": "Maksetaan työntekijälle",
          "rowDetails": "Rivin tiedot",
          "seeMoreDetails": "Katso tarkempi laskelma",
          "sharingCalc": "Laskelman jakaminen",
          "title": "Palkkalaskuri",
          "totalGrossSalary": "Bruttopalkka",
          "workFromPeriod": "Työ ajalta",
          "youMarkedAsWorker": "Sinut on merkitty työntekijäksi"
        },
        "CalcCards": {
          "employerCalc": "Työnantajan laskelma",
          "paymentsCalc": "Yhteiskunnan laskelma",
          "salaryPayment": "Salary payment",
          "segmentEmployer": "Työnantaja",
          "segmentPayments": "Yhteiskunta",
          "segmentWorker": "Työntekijä",
          "showBasicInfo": "Näytä perustiedot...",
          "showDetails": "Näytä yksityiskohdat",
          "workerCalc": "Työntekijän laskelma"
        },
        "CalcOverview": {
          "employerCalc": "Työnantajan laskelma",
          "hideChart": "Piilota kaavio",
          "hideTables": "Piilota taulukko",
          "noRows": "This calculation doesn't have any rows",
          "paymentsCalc": "Yhteiskunnan laskelma",
          "segmentEmployer": "Työnantaja",
          "segmentPayments": "Yhteiskunta",
          "segmentWorker": "Työntekijä",
          "showBasicInfo": "Näytä yksityiskohdat",
          "showDetails": "Näytä yksityiskohdat",
          "showDetailsInTables": "Näytä lisätiedot taulukossa",
          "workerCalc": "Työntekijän laskelma"
        },
        "CalcPayment": {
          "biweekly": "Kahden viikon välein",
          "dueDate": "Maksupäivä",
          "footnotes": "Jos palkkapäivä on pyhäpäivä, lähetämme maksun edellisenä arkipäivänä. Toisto lopetaan, kun jätät palkan maksamatta tai peruuttamalla Palkkaus.fi-palvelusta.",
          "immediately": "Heti",
          "monthly": "Kuukausittain",
          "nextPayment": "Seuraava maksukerta",
          "once": "Vain kerran",
          "payDay": "Palkkapäivä",
          "repeating": "Maksun toistuvuus",
          "twiceamonth": "Kahdesti kuussa",
          "type": "Maksutapa:",
          "typeNordea": "Verkkomaksu, Nordea (1h)",
          "typePaytrail": "Verkkomaksu, muut (1 arkipäivä)",
          "typeSiirto": "Siirto-maksu (1 min)",
          "weekly": "Viikottain"
        },
        "CalcPaymentList": {
          "noPersonChosen": "No person chosen",
          "salaryPayment": "Salary payment"
        },
        "CalcPeriod": {
          "dateRange": "Palkkaa ajalta",
          "workDayCount": "Työpäivien määrä"
        },
        "CalcRow": {
          "add": "Lisää rivi",
          "automaticallyCreated": "Tämä rivi on automaattisen laskennan luoma.",
          "delete": "Poista rivi",
          "editHere": "Muokkaa riviä täältä",
          "updateCalc": "Päivitä laskelma"
        },
        "CalcSelectUsecase": {
          "back": "...takaisin",
          "prevMenu": "Edellinen valikko"
        },
        "CalcSharing": {
          "changePasswordOrDelete": "Vaihda salasana tai poista jako",
          "createAgainOrDelete": "Luo uudelleen tai poista",
          "deleteDescription": "Voit poistaa laskelman jakamisen, kun haluat.",
          "deleteTitle": "Poista jako",
          "easyLinkDescription": {
            "html": "Helppo jakaa esim. tekstiviestinä tai puhelimessa, mutta ei suojaa yksityisyyttäsi, koska linkin voi arvata.<br>Esim. <a href=\"#\" target=\"_blank\">https://palkkaus.fi/GUVWXYZuvwxyz12345</a>"
          },
          "easyLinkTitle": "Helppo linkki",
          "easyLinkWithPasswordDescription": "Voit valita linkkikohtaisen salasanan. Jos salasanasi on hyvä, myös yksityisyyden suoja on hyvä.",
          "easyLinkWithPasswordTitle": "Helppo linkki + salasana",
          "noteHaveSetPassword": "Huomaa, että olet lisäksi asettanut salasanan.",
          "notSharedYet": "Tätä laskelmaa ei ole vielä jaettu. Valitse tapa, jolla haluat jakaa laskelman.",
          "recreateOrRemove": "Luo uudelleen tai poista",
          "safeLinkDescription": {
            "html": "Pitkä linkki ei ole arvattavissa. Toimii hyvin sähköpostissa.<br>Esim. <a href=\"#\">https://palkkaus.fi/GUVWXYZuvwxyz12345</a>"
          },
          "safeLinkTitle": "Turvallinen linkki",
          "shareLink": {
            "html": "<p>Laskelma on jaettu. Linkki on:<br><p><p class=\"lead\"><strong><a href=\"{{ link }}\" target=\"_blank\">{{ link }}</a></strong></p>"
          }
        },
        "CalcUsecaseBase": {
          "dailyExpenses": "Työkalukorvaus (€/pv)",
          "dailyTravelExpenses": "Matkakorvaus (€/pv)",
          "dailyTravelExpensesKm": "Matkat kotoa työpaikalle (km/pv)",
          "defaultPriceLabel": "Hinta",
          "eurPerH": "€/h",
          "expensesCustom": "Työkalu- ja matkakorvaus",
          "hourCount": "Tuntien määrä",
          "hoursPerPrice": "{{amount}} tuntia x {{price}}",
          "hoursPerPriceWithSunday": "{{amount}} tuntia x {{price}} + 100% sunnuntailisä",
          "householdDeduction": "Kotitalousvähennys",
          "isSunday": "Sunnuntai tai pyhäpäivä?",
          "longInfoText": {
            "html": "<p>Laskelma ehdottaa kotitalousvähennystä työn tyypin mukaan, mutta ei automaattisesti laske lomakorvauksia yms. lisiä.</p><p>Voit lisätä tarvittavat lisät riveinä seuraavasta näkymästä. Lue lisää työnantajan velvollisuuksista osoitteesta Palkkaus.fi.</p><p>Teemme puolestasi kotitalousvähennysilmoituksen olettamuksella, että työ on tehty kotona tai vapaa-ajan asunnolla. Jos haluat tehdä ilmoituksen itse, poista valinta.</p>"
          },
          "noByTes": "TES-taulukon perusteella",
          "noCalcAuto": "Lasketaan automaattisesti",
          "periodText": "Palkka ajalta (työpäivien määrä)",
          "pleaseInput": "Syötä",
          "pricePerMonth": "{{price}} / kk",
          "salaryKind": "Palkkatyyppi",
          "salaryMessage": "Viesti palkkalaskelmaan",
          "shortInfoText": {
            "html": "<p>Yleinen palkkalaskelma ilman TES-sääntöjä.</p>"
          },
          "tesInclusions": "TES-erät",
          "updatingCalc": "Päivitetään laskelmaa",
          "wait": "Odota hetkinen",
          "workDescription": "Työn kuvaus",
          "yesAgreedPerDay": "Erikseen sovittu (per päivä)",
          "yesAgreedTotalPayment": "Sovittu kokonaiskorvaukseen"
        },
        "CalcUsecaseChildCare": {
          "childcareSubsidy": "Yksityisen hoidon tuki",
          "footnoteIsChildcareSubsidy": "Sivukulut lasketaan kokonaissummasta. Laskuri olettaa, että maksat palkan loma-ajalta erikseen. Voit myös lisätä loma-ajan palkan rivinä laskuriin. Kotitalousvähennystä ei voi käyttää, jos yksityisen hoidon tuki on mukana palkassa.",
          "footnoteIsNotChildcareSubsidy": "Laskuri olettaa, että maksat palkan loma-ajalta erikseen. Voit myös lisätä loma-ajan palkan rivinä laskuriin.",
          "longInfoTextIfChildcareSubsidy": {
            "html": "<p>Laskurilla voit laskea pitkäaikaisen lastenhoitajan palkan.</p><p>Laskuri ottaa huomioon myös yksityisen hoidon tuen. Tuki vaikuttaa sivukuluihin, vaikka Kela maksaa sen suoraan työntekijälle. Tuen kanssa ei voi samaan aikaan käyttää kotitalousvähennystä.</p>"
          },
          "longInfoTextIfNotChildcareSubsidyl": {
            "html": "<p>Laskurilla voit laskea pitkäaikaisen lastenhoitajan palkan.</p><p>Jos valitset alla, teemme puolestasi kotitalousvähennysilmoituksen olettamuksella, että työ on tehty kotona tai vapaa-ajan asunnolla. Jos haluat tehdä ilmoituksen itse, poista valinta.</p>"
          },
          "shortInfoText": {
            "html": "<p>Oma lastenhoitaja kotona.</p>"
          },
          "wageInAdditionToBenefits": "Rahapalkka tuen lisäksi",
          "workerTotalPayment": "Työntekijän palkka yhteensä"
        },
        "CalcUsecaseCleaning": {
          "contractLastedLessThanYear": "Työsuhde kestänyt alle vuoden",
          "footnote": {
            "html": "HUOM: Sinun tulee maksaa myös loma-ajan palkka ja sen lisäksi 50% lomarahaa loma-ajan palkasta. Voit lisätä ne riveinä tähän laskelmaan tai tehdä erillisen palkanmaksun.<a href='https://www.palkkaus.fi/Cms/Articles/siivous_ja_kiinteistoalan_tyoehtosopimukset' target='_blank'>Lue lisää täältä</a>."
          },
          "fullTime": "Työtä yli 14pv/kk tai yli 35h/kk",
          "longInfoText": {
            "html": "<p>Laskuri laskee automaattisesti lomakorvauksen maksettavaksi prosentteina palkasta.</p><p>Jos työtä tehdään yli 14pv/kk tai yli 35h/kk, pitää työntekijälle antaa mahdollisuus pitää loma. Tällöin maksetaan loma-ajalta palkka (jäljelle jäänyt osuus maksetaan työsuhteen päättyessä).</p>"
          },
          "shortInfoText": {
            "html": "<p>Kodin tai vapaa-ajan asunnon siivous ja kunnossapito. Ilmoitetaan kotitalousvähennykseen.</p>"
          }
        },
        "CalcUsecaseConstruction": {
          "dailyExpenses": "Työkalukorvaus (€/pv)",
          "dailyTravelExpenses": "Matkakorvaus (€/pv)",
          "dailyTravelExpensesKm": "Matkat kotoa työpaikalle (km/pv)",
          "expensesCustom": "Työkalu- ja matkakorvaus",
          "longInfoText": {
            "html": "<p>Lisäksi työehtosopimuksissa on määritelmät työkalukorvauksista ja matkakorvauksista. Laskurissa nämä voi laskea TES-taulukoiden mukaan tai vaihtoehtoisesti erikseen työntekijän kanssa sovittuina päiväkorvauksina.</p><p>Teemme puolestasi kotitalousvähennysilmoituksen olettamuksella, että työ on tehty kotona tai vapaa-ajan asunnolla. Jos haluat tehdä ilmoituksen itse, poista valinta.</p>"
          },
          "noByTes": "TES-taulukon perusteella",
          "noCalcAuto": "Lasketaan automaattisesti",
          "shortInfoText": {
            "html": "<p>Laskuri huomioi yleissitovat työehtosopimukset rakennusalalla.<br>TES-eriä ovat lomakorvaus ja erillinen palkan osa (entinen 'pekkaset').</p>"
          },
          "subtitlePlusExpenses": "{{ subtitle }} + TES-lisät ja korvaukset",
          "tesInclusions": "TES-erät",
          "yesAgreedPerDay": "Erikseen sovittu (per päivä)",
          "yesAgreedTotalPayment": "Sovittu kokonaiskorvaukseen"
        },
        "CalcUsecaseMll": {
          "longInfoText": {
            "html": "<p>Palkka maksetaan Mannerheimin Lastensuojeluliiton suositusten mukaan. Perhe maksaa lasten hoitajalle palkkana 8,20 €/tunti (sunnuntaina 16,40). Tämä on kokonaiskorvaus, joka sisältää lomakorvauksen. Tunnit ilmoitetaan puolen tunnin tarkkuudella, minimi 2 tuntia. Yhdellä hoitajalla voi olla hoidettavana enintään neljä lasta.</p><p>Huom! Kello 23 jälkeen perheen tulee huolehtia hoitajan turvallisesta kotimatkasta joko tarjoamalla autokyyti tai kustantamalla taksi.</p><p>Teemme puolestasi kotitalousvähennysilmoituksen olettamuksella, että työ on tehty kotona tai vapaa-ajan asunnolla. Jos haluat poistaa kotitalousvähennyksen tai tehdä ilmoituksen itse, käytä 'Muu lastenhoito'-laskuria.</p>"
          },
          "shortInfoText": {
            "html": "<p>Mannerheimin Lastensuojeluliiton suositusten mukaan. Ilmoitetaan kotitalousvähennykseen.</p>"
          },
          "useCustomPrice": "Muu kuin MLL:n palkkasuositus (8,20€/h)?"
        },
        "CalcUsecaseSantaClaus": {
          "agreedPayment": "Sovittu palkkiosumma",
          "fixedPaymentAtChristmas": "Kertakorvaus {{payment}} jouluaattona.",
          "longInfoText": {
            "html": "<p>Kotitalousväehnnystä joulupukin palkkiosta ei verottajan mukaan saa.</p>"
          },
          "shortInfoText": {
            "html": "<p>Joulupukkikin ansaitsee laillisen palkan. Palkkaus.fi ja Nordea tarjoavat sen tänä jouluna <strong>ilman palvelumaksua</strong></p>"
          }
        },
        "CalcWorker": {
          "askForTaxCard": "Palkkaus.fi kysyy verokortin, henkilötunnuksen ja tilinumeron Siirto-puhelinnumeron työntekijältä.",
          "email": "Sähköposti",
          "ibanNumber": "Tilinumero (IBAN-muodossa)",
          "ssNumber": "Henkilötunnus",
          "taxCard": "Verokortti",
          "taxCardType": "Verokortin tyyppi",
          "taxPercent": "Veroprosentti",
          "tel": "Puhelinnumero",
          "weAreAsking": "Palkkaus.fi kysyy loput",
          "withoutIbanPaidViaSiirto": "Ilman tilinumeroa palkka maksetaan Siirto-palvelun kautta."
        },
        "ContactDetails": {
          "address": "Osoite",
          "businessName": "Yrityksen nimi",
          "businessOrPerson": "Luodaanko yritys vai Yksityishenkilö?",
          "email": "Sähköposti",
          "firstName": "Etunimi",
          "lastName": "Sukunimi",
          "noteCannotDeleteExamples": "Huom. Esimerkki-kontaktia ei voi tallentaa.",
          "postalArea": "Postitoimipaikka",
          "postalCode": "Postinumero",
          "removeContact": "Poista kontakti",
          "ssNumber": "Henkilötunnus",
          "sureToDeleteContact": "Haluatko varmasti poistaa kontaktin?",
          "tel": "Puhelinnumero",
          "vatId": "Y-tunnus"
        },
        "ContactList": {
          "addNewCompany": "Lisää yritys",
          "addNewPerson": "Lisää henkilö",
          "noSearchResults": "Kontakteja ei löytynyt tällä haulla...",
          "tryWithExamples": "Kokeile esimerkkikontakteilla:"
        },
        "ContractEditEmployer": {
          "addressNotGiven": "Osoitetta ei annettuna",
          "backToContactList": "Tyhjennä / Vaihda valintaa",
          "displayNameNotGiven": "Henkilöä ei valittuna",
          "editChosenContact": "Muokkaa valittua kontaktia",
          "editMyDetails": "Muokkaa omia tietoja",
          "emailNotGiven": "Sähköpostia ei annettuna",
          "pageTitleChooseEmployerContact": "Valitse työnantajakontakti",
          "pageTitleEditContactDetails": "Muokkaa kontaktin tietoja",
          "pageTitleEditMyDetails": "Muokkaa omia tietoja",
          "ssNumberNotGiven": "Henkilötunnusta ei annettuna",
          "telNotGiven": "Puhelinnumeroa ei annettuna",
          "updateMyCurrentDetails": "Päivitä uudet yhteystietoni sopimukselle"
        },
        "ContractEditSalary": {
          "contractDateRange": "Työsuhteen kesto",
          "contractStartDate": "Työsuhde alkaa",
          "dateRange": "Työsuhteen kesto",
          "noticeException": "Irtisanomisaika määräytyy",
          "payDay": "Palkanmaksupäivät",
          "salaryAmount": "Palkan määrä {{amount}}",
          "salaryAmountPer": "Palkan määrä",
          "salaryCompension": "Työstä maksettava kertakorvaus",
          "salaryType": "Palkan määräytymisen peruste",
          "startDate": "Työsuhde alkaa",
          "testPeriod": "Koeaika (kk)",
          "workHours": "Työaika (tunteina)",
          "worksHoursType": {
            "label": "Työajan jakso"
          }
        },
        "ContractEditSummary": {
          "chooseEmployerContact": "Valitse työnantajakontakti",
          "chooseEmployerFromContacts": "Valitse työnantaja kontakteista...",
          "chooseWorker": "Valitse työntekijä",
          "chosenEmployerContact": "Valittu työnantajakontakti",
          "chosenWorker": "Valittu työntekijä",
          "collectiveContract": "Työehtosopimus",
          "dateRange": "Työsuhteen kesto",
          "displayNameNotGiven": "Henkilöä ei valittuna",
          "emailNotGiven": "Sähköpostia ei annettuna",
          "employer": "Työnantaja",
          "employerAsSelf": "Työnantaja (oma)",
          "mainWorkTasks": "Tehtävät",
          "myWorkerDetails": "Omat työntekijätiedot",
          "noticeException": "Irtisanomisaika",
          "otherDetailsTitle": "Muut sopimustiedot",
          "otherTerms": "Muut sopimusehdot",
          "payDay": "Palkanmaksupäivät",
          "salaryAmount": "Palkan määrä",
          "salaryCompension": "Työstä maksettava kertakorvaus",
          "salaryTitle": "Palkkaus",
          "startDate": "Työsuhde alkaa",
          "sureToDeleteContract": "Haluatko varmasti poistaa sopimuksen?",
          "testPeriod": "Koeaika (kk)",
          "worker": "Työntekijä",
          "workerAsSelf": "Työntekijä (oma)",
          "workHours": "Työaika",
          "workTasksTitle": "Työnkuva",
          "workType": "Työsuhteen tyyppi"
        },
        "ContractEditWorker": {
          "addressNotGiven": "Osoitetta ei annettuna",
          "backToWorkerList": "Takaisin työntekijälistalle",
          "chooseWorker": "Valitse työntekijä",
          "displayNameNotGiven": "Henkilöä ei valittuna",
          "editMyDetails": "Muokkaa omia tietoja",
          "emailNotGiven": "Sähköpostia ei annettuna",
          "ibanNumberNotGiven": "Tilinumeroa ei annettuna",
          "myWorkerDetaills": "Omat työntekijätiedot",
          "ssNumberNotGiven": "Henkilötunnusta ei annettuna",
          "telNotGiven": "Puhelinnumeroa ei annettuna",
          "updateMyChangedDetailsToContract": "Päivitä uudet yhteystietoni sopimukselle"
        },
        "ContractEditWorkerSelect": {
          "chosenWorker": "Valittu työntekijä"
        },
        "ContractEditWorkTasks": {
          "collectiveContract": "Sovellettava työehtosopimus",
          "mainWorkTasks": "Pääasialliset työtehtävät",
          "workType": "Työsuhteen tyyppi"
        },
        "ContractList": {
          "createFirst": "Luo ensimmäinen sopimus",
          "createNew": "Luo uusi sopimus"
        },
        "InfoText": {
          "showMoreLabel": "Näytä lisää..."
        },
        "Login": {
          "pleaseLogin": "Kirjaudu sisään"
        },
        "Onboarding": {
          "accountAlreadyExists": {
            "html": "Tällä henkilötunnuksella on jo luotu käyttäjätili Palkkaus.fi-palveluun. Jos olet tekemässä uutta tiliä salasanan unohtumisen takia tai epäilet että joku toinen on tehnyt tilin henkilötunnuksellasi, otathan yhteyttä asiakaspalveluumme joko sähköpostitse (<a href=\"mailto:aspa@palkkaus.fi\">aspa@palkkaus.fi</a>) tai puhelimitse (020 798 0230, arkisin klo 9-16)."
          }
        },
        "OnboardingIdentifier": {
          "areYouPEP": "Oletko poliittisesti vaikutusvaltainen henkilö (PEP)?",
          "byContinuingAgreeTerms": "Jatkamalla eteenpäin hyväksyt pankkitunnuksellasi Palkkaus.fi-palvelun käyttöehdot.",
          "ssNumber": "Henkilötunnus",
          "title": "Henkilötunnus"
        },
        "OnboardingPerson": {
          "email": "Sähköposti",
          "firstName": "Etunimi",
          "lastName": "Sukunimi",
          "title": "Nimi ja sähköposti"
        },
        "OnboardingPhone": {
          "instruction": "Puhelinnumeroa tarvitsemme yhteydenpitoa varten. Jos rekisteröit Siirto-palvelussa olevan puhelinnumeron, voit maksaa ja vastaanottaa palkkoja mobiilimaksuilla. Vahvistamme antamasi numeron.",
          "tel": "Puhelinnumero",
          "title": "Puhelinnumero"
        },
        "OnboardingPhoneVerify": {
          "instruction": "Olemme lähettäneet sinulle SMS-viestin varmistaaksemme numerosi. Syötä viestissä oleva vahvistuskoodi tähän.",
          "phoneVerificationPin": "Koodi SMS-viestistä",
          "title": "Puhelinnumeron varmistaminen"
        },
        "OnboardingTupas": {
          "instruction": "Siirry allekirjoittamaan",
          "title": "Siirry allekirjoitukseen"
        },
        "SettingsEditButtons": {
          "generalSettingsTitle": "Yleiset asetukset",
          "householdSubsidySettingsPageTitle": [
            "Kotitalousvähennyksen asetukset",
            "Kotitalousvähennyksen asetukset"
          ],
          "householdSubsidyTitle": "Kotitalousvähennys",
          "insuranceSettingsPageTitle": "Tapaturmavakuutuksen asetukset",
          "insuranceTitle": "Tapaturmavakuutus",
          "pensionContractTitle": "Työeläkesopimus",
          "pensionSettingsPageTitle": "TyEL-asetukset",
          "taxCardAndEmailSettingsPageTitle": "Palkkalaskelman lähetys- ja verokorttiasetukset",
          "taxCardAndEmailTitle": "Palkkalaskelman lähetys ja verokortti",
          "taxSettingsPageTitle": "Ennakonpidätyksen asetukset",
          "taxTitle": "Verot",
          "unemploymentSettingsPageTitle": "Työttömyysvakuutuksen asetukset",
          "unemploymentTitle": "Työttömyysvakuutus"
        },
        "SettingsGeneral": {
          "employerIsResponsibleForTaxCardInputLabel": "Työantaja kysyy verokorttitiedot työntekijältä",
          "isSalariesPaidThisYearQuestionLabel": "Palkkoja maksettu muualla",
          "otherSalariesAmountLabel": "Muualla maksetut palkat (€)",
          "serviceSendsCalculationEmailsToWorkersLabel": "Palvelu lähettää palkkalaskelman automaattisesti työntekijälle"
        },
        "SettingsHouseholdSubsidy": {
          "isHouseholdReportingLabel": "Kotitalousvähennysraportin lähetys"
        },
        "SettingsInsurance": {
          "changeToPartnerInsurance": "Haluan vaihtaa vakuutuksen If:iin",
          "insuranceCompanyLabel": "Valitse vakuutusyhtiö",
          "insuranceContractNumberLabel": "Vakuutusnumero",
          "insuranceContractQuestionLabel": "Minulla on jo vakuutus",
          "orderNewPartnerInsurance": "Haluan ottaa vakuutuksen If:ltä"
        },
        "SettingsOverview": {
          "if": {
            "html": "<strong>HUOM:</strong> Jos maksat palkkoja yli 1200 € vuodessa, sinun täytyy ottaa lakisääteinen työtapaturmavakuutus."
          },
          "palkkaus": "Työntekijä saa palvelusta palkkalaskelman ja kysymme verokortin. Arkistoimme tiedot 6 vuotta.",
          "tax": "Ennakonpidätys, sairausvakuutusmaksut, ilmoitukset ja kotitalousvähennys",
          "tvr": "Työttömyysvakuutusrahaston maksut ja ilmoitukset.",
          "varma": "Työeläkemaksut ja -ilmoitukset (TyEL) tilapäisenä työnantajana."
        },
        "SettingsPension": {
          "changeToPartnerPension": "Haluan vaihtaa TyEL-sopimuksen Varmaan",
          "noteAboutPartnerCompanies": "HUOM: Palkkaus.fi-palvelu hoitaa TyEL-ilmoitukset ja maksut vain seuraaviin yhtiöihin: Elo, Etera ja Varma. Muihin yhtiöihin sinun tulee ilmoitukset itse. Saat palvelustamme raportit ilmoitusten tekemiseen.",
          "orderNewPartnerPension": "Haluan tilata TyEL-sopimuksen Varmalta",
          "pensionCompanyLabel": "Eläkevakuutusyhtiö",
          "pensionContractNumberLabel": "Vakuutusnumero",
          "pensionContractQuestionLabel": "Minulla on jo TyEL-sopimus",
          "pensionSelfHandlingQuestionLabel": "Teen TyEL-ilmoitukset ja maksut itse"
        },
        "SettingsTax": {
          "isMonthlyReportingEnabledLabel": "Kuukausi-ilmoituksen lähetys",
          "isYearlyReportingEnabledLabel": "Vuosi-ilmoituksen lähetys",
          "paymentReferenceNumberLabel": "Veronumero"
        },
        "SettingsUnemployment": {
          "prepaymentAmountLabel": "Ennakkomaksujen määrä (€)",
          "prepaymentsQuestionLabel": "TVR-ennakkomaksut",
          "selfHandlingQuestionLabel": "TVR-ilmoitusten itsenäinen lähetys"
        },
        "TaxCard": {
          "chooseType": "Valitse verokortin tyyppi",
          "date": "Voimaantulopäivä",
          "forYear": "Vuosi",
          "incomeLimit": "Vuosituloraja",
          "noIncomeLimit": "Ei tulorajaa",
          "previousSalariesPaid": "Palkat vuoden alusta",
          "step1Limit": "alle 10 300€",
          "step1Percent": "0,0",
          "step2Limit": "10 300 - 17 650€",
          "step3Limit": "yli 17 650€",
          "stepIncomePerYear": "Tulot/vuosi",
          "stepsIncomeLimits": "Portaikkoverokortin tulorajat, 2017",
          "stepTaxPercent": "Veroprosentti",
          "taxPercent": "Perusprosentti",
          "taxPercent2": "Lisäprosentti",
          "type": "Verokortin tyyppi"
        },
        "WorkerDetails": {
          "address": "Osoite",
          "chosenWorker": "Valittu työntekijä",
          "contactTitle": "Yhteystiedot",
          "email": "Sähköposti",
          "firstName": "Etunimi",
          "ibanNumber": "Tilinumero (IBAN-muodossa)",
          "identityTitle": "Henkilötiedot",
          "lastName": "Sukunimi",
          "paymentTitle": "Maksutiedot",
          "postalArea": "Postitoimipaikka",
          "postalCode": "Postinumero",
          "removeWorker": "Poista työntekijä",
          "ssNumber": "Henkilötunnus",
          "sureToDelete": "Haluatko varmasti poistaa työntekijän?",
          "tel": "Puhelinnumero",
          "workers": "Työntekijät"
        },
        "WorkerList": {
          "addNew": "Lisää uusi työntekijä",
          "changeRoleToEmployerForViewing": "You are currently in Worker Role. Please change role to Employer (Household or Company) for viewing the list of workers.",
          "chosenWorker": "Valittu työntekijä",
          "noSearchResults": "Työntekijöitä ei löytynyt tällä haulla...",
          "tryWithExamples": "Kokeile esimerkkityöntekijöillä",
          "workerDetails": "Työntekijän tiedot"
        }
      },
      "LANG_SELECT": {
        "optionName": "Suomeksi",
        "title": "Valitse kieli"
      },
      "PAGES": {
        "SwitchProfile": {
          "asideBody": {
            "html": "<p>Palkkaus.fi palvelussa on seuraavat käyttöliittymät:</p><ul><li>Kotitalous työnantajana</li><li>Työntekijä</li><li>Yritys tai yhdistys</li></ul>"
          },
          "changeRoleOrLogout": "Voit vaihtaa toiseen käyttäjärooliin tai kirjautua ulos palvelusta:",
          "companyOrAssocSubtitle": "Yrittäjille ja yhdistyksillle",
          "householdSubtitle": "Maksa palkkaa kodin töistä",
          "title": "Käyttäjäprofiili",
          "titleAccountDetails": "Tilin tiedot",
          "usingAsAnon": "Käytät DEMO-palvelua anonyymitilassa. Kirjaudu sisään klikkaamalla tästä.",
          "workerOldSubtitle": "Vanha Palkkaus.fi käyttöliittymä",
          "workerSubtitle": "Syötä verokortti, hae palkkalaskelma"
        },
        "WorkerProfile": {
          "addressLabel": "Osoite",
          "citiesLabel": "Kaupungit/kunnat, joissa työskentelen",
          "citiesPageTitle": "Kaupungit/kunnat, joissa työskentelen",
          "contactInfoCardTitle": "Yhteystiedot",
          "editGeneralInfoButtonText": "Muokkaa tietoja",
          "emailLabel": "Sähköposti",
          "generalInfoPageTitle": "Osaajaprofiilin yleiset asetukset",
          "isPrivate": "näkyy vain sinulle",
          "isPublic": "on julkinen",
          "jobDescriptionLabel": "Kerro millainen olet työntekijänä",
          "jobDetailDescriptionLabel": "Tarkempi kuvaus töistä, joita teen",
          "jobDetailLabel": "Tarkempi kuvaus töistä",
          "jobDetailPageTitle": "Tarkempi kuvaus osaamisestasi",
          "jobsLabel": "Työt, joita teen",
          "jobsPageTitle": "Valitse työt, joita teet",
          "phoneNumberLabel": "Puhelinnumero",
          "portfolioLabel": "Työnäytteet/portfolio",
          "portfolioLinkLabel": "Linkki työnäytteisiin tai portfolioon",
          "portfolioPageTitle": "Työnäytteet / portfolio",
          "profilePageTitle": "Osaajaprofiili",
          "profileVisibilityLabel": "Profiilin näkyvyys",
          "searchCitiesPlaceholder": "Etsi kaupunkeja/kuntia",
          "searchJobsPlaceholder": "Etsi työnimikkeitä",
          "skillsCardTitle": "Osaaminen",
          "yourProfileLabel": "Profiilisi"
        }
      }
    }
  }
};
_dictionary["sv"] = {
  "SALAXY": {
    "ION": {
      "COMPONENTS": {
        "Avatar": {
          "hideTips": "",
          "showTips": ""
        },
        "CalcAddRow": {
          "benefits": "",
          "deductions": "",
          "expenses": "",
          "mostCommon": "",
          "salaryAdditions": "",
          "salaryRows": "",
          "searchResults": ""
        },
        "CalcCalculatorBasic": {
          "addRow": "",
          "addRowTitle": "",
          "asideTitle": "",
          "basicDetails": "",
          "chooseWorker": "",
          "continueEditing": "",
          "costToEmployer": "",
          "editorFootnote": {
            "html": ""
          },
          "noTel": "",
          "paidToWorker": "",
          "rowDetails": "",
          "seeMoreDetails": "",
          "sharingCalc": "",
          "title": "",
          "totalGrossSalary": "",
          "workFromPeriod": "",
          "youMarkedAsWorker": ""
        },
        "CalcCards": {
          "employerCalc": "",
          "paymentsCalc": "",
          "salaryPayment": "",
          "segmentEmployer": "",
          "segmentPayments": "",
          "segmentWorker": "",
          "showBasicInfo": "",
          "showDetails": "",
          "workerCalc": ""
        },
        "CalcOverview": {
          "employerCalc": "",
          "hideChart": "",
          "hideTables": "",
          "noRows": "",
          "paymentsCalc": "",
          "segmentEmployer": "",
          "segmentPayments": "",
          "segmentWorker": "",
          "showBasicInfo": "",
          "showDetails": "",
          "showDetailsInTables": "",
          "workerCalc": ""
        },
        "CalcPayment": {
          "biweekly": "",
          "dueDate": "",
          "footnotes": "",
          "immediately": "",
          "monthly": "",
          "nextPayment": "",
          "once": "",
          "payDay": "",
          "repeating": "",
          "twiceamonth": "",
          "type": "",
          "typeNordea": "",
          "typePaytrail": "",
          "typeSiirto": "",
          "weekly": ""
        },
        "CalcPaymentList": {
          "noPersonChosen": "",
          "salaryPayment": ""
        },
        "CalcPeriod": {
          "dateRange": "",
          "workDayCount": ""
        },
        "CalcRow": {
          "add": "",
          "automaticallyCreated": "",
          "delete": "",
          "editHere": "",
          "updateCalc": ""
        },
        "CalcSelectUsecase": {
          "back": "",
          "prevMenu": ""
        },
        "CalcSharing": {
          "changePasswordOrDelete": "",
          "createAgainOrDelete": "",
          "deleteDescription": "",
          "deleteTitle": "",
          "easyLinkDescription": {
            "html": ""
          },
          "easyLinkTitle": "",
          "easyLinkWithPasswordDescription": "",
          "easyLinkWithPasswordTitle": "",
          "noteHaveSetPassword": "",
          "notSharedYet": "",
          "recreateOrRemove": "",
          "safeLinkDescription": {
            "html": ""
          },
          "safeLinkTitle": "",
          "shareLink": {
            "html": ""
          }
        },
        "CalcUsecaseBase": {
          "dailyExpenses": "",
          "dailyTravelExpenses": "",
          "dailyTravelExpensesKm": "",
          "defaultPriceLabel": "",
          "eurPerH": "",
          "expensesCustom": "",
          "hourCount": "",
          "hoursPerPrice": "",
          "hoursPerPriceWithSunday": "",
          "householdDeduction": "",
          "isSunday": "",
          "longInfoText": {
            "html": ""
          },
          "noByTes": "",
          "noCalcAuto": "",
          "periodText": "",
          "pleaseInput": "",
          "pricePerMonth": "",
          "salaryKind": "",
          "salaryMessage": "",
          "shortInfoText": {
            "html": ""
          },
          "tesInclusions": "",
          "updatingCalc": "",
          "wait": "",
          "workDescription": "",
          "yesAgreedPerDay": "",
          "yesAgreedTotalPayment": ""
        },
        "CalcUsecaseChildCare": {
          "childcareSubsidy": "",
          "footnoteIsChildcareSubsidy": "",
          "footnoteIsNotChildcareSubsidy": "",
          "longInfoTextIfChildcareSubsidy": {
            "html": ""
          },
          "longInfoTextIfNotChildcareSubsidyl": {
            "html": ""
          },
          "shortInfoText": {
            "html": ""
          },
          "wageInAdditionToBenefits": "",
          "workerTotalPayment": ""
        },
        "CalcUsecaseCleaning": {
          "contractLastedLessThanYear": "",
          "footnote": {
            "html": ""
          },
          "fullTime": "",
          "longInfoText": {
            "html": ""
          },
          "shortInfoText": {
            "html": ""
          }
        },
        "CalcUsecaseConstruction": {
          "dailyExpenses": "",
          "dailyTravelExpenses": "",
          "dailyTravelExpensesKm": "",
          "expensesCustom": "",
          "longInfoText": {
            "html": ""
          },
          "noByTes": "",
          "noCalcAuto": "",
          "shortInfoText": {
            "html": ""
          },
          "subtitlePlusExpenses": "",
          "tesInclusions": "",
          "yesAgreedPerDay": "",
          "yesAgreedTotalPayment": ""
        },
        "CalcUsecaseMll": {
          "longInfoText": {
            "html": ""
          },
          "shortInfoText": {
            "html": ""
          },
          "useCustomPrice": ""
        },
        "CalcUsecaseSantaClaus": {
          "agreedPayment": "",
          "fixedPaymentAtChristmas": "",
          "longInfoText": {
            "html": ""
          },
          "shortInfoText": {
            "html": ""
          }
        },
        "CalcWorker": {
          "askForTaxCard": "",
          "email": "",
          "ibanNumber": "",
          "ssNumber": "",
          "taxCard": "",
          "taxCardType": "",
          "taxPercent": "",
          "tel": "",
          "weAreAsking": "",
          "withoutIbanPaidViaSiirto": ""
        },
        "ContactDetails": {
          "address": "",
          "businessName": "",
          "businessOrPerson": "",
          "email": "",
          "firstName": "",
          "lastName": "",
          "noteCannotDeleteExamples": "",
          "postalArea": "",
          "postalCode": "",
          "removeContact": "",
          "ssNumber": "",
          "sureToDeleteContact": "",
          "tel": "",
          "vatId": ""
        },
        "ContactList": {
          "addNewCompany": "",
          "addNewPerson": "",
          "noSearchResults": "",
          "tryWithExamples": ""
        },
        "ContractEditEmployer": {
          "addressNotGiven": "",
          "backToContactList": "",
          "displayNameNotGiven": "",
          "editChosenContact": "",
          "editMyDetails": "",
          "emailNotGiven": "",
          "pageTitleChooseEmployerContact": "",
          "pageTitleEditContactDetails": "",
          "pageTitleEditMyDetails": "",
          "ssNumberNotGiven": "",
          "telNotGiven": "",
          "updateMyCurrentDetails": ""
        },
        "ContractEditSalary": {
          "contractDateRange": "",
          "contractStartDate": "",
          "dateRange": "",
          "noticeException": "",
          "payDay": "",
          "salaryAmount": "",
          "salaryAmountPer": "",
          "salaryCompension": "",
          "salaryType": "",
          "startDate": "",
          "testPeriod": "",
          "workHours": "",
          "worksHoursType": {
            "label": ""
          }
        },
        "ContractEditSummary": {
          "chooseEmployerContact": "",
          "chooseEmployerFromContacts": "",
          "chooseWorker": "",
          "chosenEmployerContact": "",
          "chosenWorker": "",
          "collectiveContract": "",
          "dateRange": "",
          "displayNameNotGiven": "",
          "emailNotGiven": "",
          "employer": "",
          "employerAsSelf": "",
          "mainWorkTasks": "",
          "myWorkerDetails": "",
          "noticeException": "",
          "otherDetailsTitle": "",
          "otherTerms": "",
          "payDay": "",
          "salaryAmount": "",
          "salaryCompension": "",
          "salaryTitle": "",
          "startDate": "",
          "sureToDeleteContract": "",
          "testPeriod": "",
          "worker": "",
          "workerAsSelf": "",
          "workHours": "",
          "workTasksTitle": "",
          "workType": ""
        },
        "ContractEditWorker": {
          "addressNotGiven": "",
          "backToWorkerList": "",
          "chooseWorker": "",
          "displayNameNotGiven": "",
          "editMyDetails": "",
          "emailNotGiven": "",
          "ibanNumberNotGiven": "",
          "myWorkerDetaills": "",
          "ssNumberNotGiven": "",
          "telNotGiven": "",
          "updateMyChangedDetailsToContract": ""
        },
        "ContractEditWorkerSelect": {
          "chosenWorker": ""
        },
        "ContractEditWorkTasks": {
          "collectiveContract": "",
          "mainWorkTasks": "",
          "workType": ""
        },
        "ContractList": {
          "createFirst": "",
          "createNew": ""
        },
        "InfoText": {
          "showMoreLabel": ""
        },
        "Login": {
          "pleaseLogin": ""
        },
        "Onboarding": {
          "accountAlreadyExists": {
            "html": ""
          }
        },
        "OnboardingIdentifier": {
          "areYouPEP": "",
          "byContinuingAgreeTerms": "",
          "ssNumber": "",
          "title": ""
        },
        "OnboardingPerson": {
          "email": "",
          "firstName": "",
          "lastName": "",
          "title": ""
        },
        "OnboardingPhone": {
          "instruction": "",
          "tel": "",
          "title": ""
        },
        "OnboardingPhoneVerify": {
          "instruction": "",
          "phoneVerificationPin": "",
          "title": ""
        },
        "OnboardingTupas": {
          "instruction": "",
          "title": ""
        },
        "SettingsEditButtons": {
          "generalSettingsTitle": "",
          "householdSubsidySettingsPageTitle": [
            "",
            ""
          ],
          "householdSubsidyTitle": "",
          "insuranceSettingsPageTitle": "",
          "insuranceTitle": "",
          "pensionContractTitle": "",
          "pensionSettingsPageTitle": "",
          "taxCardAndEmailSettingsPageTitle": "",
          "taxCardAndEmailTitle": "",
          "taxSettingsPageTitle": "",
          "taxTitle": "",
          "unemploymentSettingsPageTitle": "",
          "unemploymentTitle": ""
        },
        "SettingsGeneral": {
          "employerIsResponsibleForTaxCardInputLabel": "",
          "isSalariesPaidThisYearQuestionLabel": "",
          "otherSalariesAmountLabel": "",
          "serviceSendsCalculationEmailsToWorkersLabel": ""
        },
        "SettingsHouseholdSubsidy": {
          "isHouseholdReportingLabel": ""
        },
        "SettingsInsurance": {
          "changeToPartnerInsurance": "",
          "insuranceCompanyLabel": "",
          "insuranceContractNumberLabel": "",
          "insuranceContractQuestionLabel": "",
          "orderNewPartnerInsurance": ""
        },
        "SettingsOverview": {
          "if": {
            "html": ""
          },
          "palkkaus": "",
          "tax": "",
          "tvr": "",
          "varma": ""
        },
        "SettingsPension": {
          "changeToPartnerPension": "",
          "noteAboutPartnerCompanies": "",
          "orderNewPartnerPension": "",
          "pensionCompanyLabel": "",
          "pensionContractNumberLabel": "",
          "pensionContractQuestionLabel": "",
          "pensionSelfHandlingQuestionLabel": ""
        },
        "SettingsTax": {
          "isMonthlyReportingEnabledLabel": "",
          "isYearlyReportingEnabledLabel": "",
          "paymentReferenceNumberLabel": ""
        },
        "SettingsUnemployment": {
          "prepaymentAmountLabel": "",
          "prepaymentsQuestionLabel": "",
          "selfHandlingQuestionLabel": ""
        },
        "TaxCard": {
          "chooseType": "",
          "date": "",
          "forYear": "",
          "incomeLimit": "",
          "noIncomeLimit": "",
          "previousSalariesPaid": "",
          "step1Limit": "",
          "step1Percent": "",
          "step2Limit": "",
          "step3Limit": "",
          "stepIncomePerYear": "",
          "stepsIncomeLimits": "",
          "stepTaxPercent": "",
          "taxPercent": "",
          "taxPercent2": "",
          "type": ""
        },
        "WorkerDetails": {
          "address": "",
          "chosenWorker": "",
          "contactTitle": "",
          "email": "",
          "firstName": "",
          "ibanNumber": "",
          "identityTitle": "",
          "lastName": "",
          "paymentTitle": "",
          "postalArea": "",
          "postalCode": "",
          "removeWorker": "",
          "ssNumber": "",
          "sureToDelete": "",
          "tel": "",
          "workers": ""
        },
        "WorkerList": {
          "addNew": "",
          "changeRoleToEmployerForViewing": "",
          "chosenWorker": "",
          "noSearchResults": "",
          "tryWithExamples": "",
          "workerDetails": ""
        }
      },
      "LANG_SELECT": {
        "optionName": "På Svenska",
        "title": ""
      },
      "PAGES": {
        "SwitchProfile": {
          "asideBody": {
            "html": ""
          },
          "changeRoleOrLogout": "",
          "companyOrAssocSubtitle": "",
          "householdSubtitle": "",
          "title": "",
          "titleAccountDetails": "",
          "usingAsAnon": "",
          "workerOldSubtitle": "",
          "workerSubtitle": ""
        },
        "WorkerProfile": {
          "addressLabel": "",
          "citiesLabel": "",
          "citiesPageTitle": "",
          "contactInfoCardTitle": "",
          "editGeneralInfoButtonText": "",
          "emailLabel": "",
          "generalInfoPageTitle": "",
          "isPrivate": "",
          "isPublic": "",
          "jobDescriptionLabel": "",
          "jobDetailDescriptionLabel": "",
          "jobDetailLabel": "",
          "jobDetailPageTitle": "",
          "jobsLabel": "",
          "jobsPageTitle": "",
          "phoneNumberLabel": "",
          "portfolioLabel": "",
          "portfolioLinkLabel": "",
          "portfolioPageTitle": "",
          "profilePageTitle": "",
          "profileVisibilityLabel": "",
          "searchCitiesPlaceholder": "",
          "searchJobsPlaceholder": "",
          "skillsCardTitle": "",
          "yourProfileLabel": ""
        }
      }
    }
  }
};
/** Internationalization dictionary
@hidden */
export const dictionary = _dictionary;
