import { Component, Input } from "@angular/core";

import { NgLoginComponent as SalaxyLoginComponent } from "@salaxy/ng";

/**
 * Shows the login / logout button and optionally does the auto-login.
 */
@Component({
  selector: "sxy-login",
  templateUrl: "./login.html",
})
export class LoginComponent extends SalaxyLoginComponent {

  /** If true, will automatically show the login screen if the user is not logged in */
  @Input() public autoLogin = false;

  /** Starts loading article collection called Entrepreneur from the server */
  public ngOnInit() {
    if (this.autoLogin) {
      super.signIn(window.location.origin + "/");
    }
  }

  /** Shows the login screen */
  public signIn() {
    // HACK: Why is this not the default?
    super.signIn(window.location.origin + "/");
  }

}
