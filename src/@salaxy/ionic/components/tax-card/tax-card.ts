import { Component, OnInit } from "@angular/core";
import { NavController, NavParams } from "ionic-angular";

import { NgTaxCardComponent as SalaxyTaxCardComponent, SalaxyContext } from "@salaxy/ng";

/**
 * Editor and details view user interface for tax card.
 * This version always edits the currently selected tax card.
 *
 * <example-url>/#/examples/TaxCardExample</example-url>
 *
 */
@Component({
  selector: "sxy-tax-card",
  templateUrl: "./tax-card.html",
})
export class TaxCardComponent extends SalaxyTaxCardComponent implements OnInit {

  constructor(context: SalaxyContext) {
    super(context);
  }
}
