import { Component } from "@angular/core";

import { NgTaxCardListComponent as SalaxyTaxCardListComponent } from "@salaxy/ng";

/**
 * List of tax cards uploaded to the current user.
 *
 * <example-url>/#/examples/TaxCardListExample</example-url>
 *
 */
@Component({
  selector: "sxy-tax-card-list",
  templateUrl: "./tax-card-list.html",
})
export class TaxCardListComponent extends SalaxyTaxCardListComponent {}
