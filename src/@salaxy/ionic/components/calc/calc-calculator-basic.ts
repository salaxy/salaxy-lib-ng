// Framework components
import { Component, OnInit } from "@angular/core";
// Salaxy framework
import { Calculation, ResultRow, Usecase, UsecasesLogic, CalculatorLogic, EnumerationsLogic, CalculationRowType, CalcRowsLogic } from "@salaxy/core";
import { SalaxyContext, ComponentDoneEvent, NgTranslations } from "@salaxy/ng";
// Current project
import { SxyUiHelpers } from "../../services/sxy-ui-helpers";
import { CalcUsecaseComponent } from "./usecases/calc-usecase";
import { CalcPeriodComponent } from "./period/calc-period";
import { CalcWorkerComponent } from "./worker/calc-worker";
import { CalcRowComponent } from "./rows/calc-row";
import { CalcAddRowComponent } from "./rows/calc-add-row";
import { CalcOverviewComponent } from "./overview/calc-overview";
import { CalcSharingComponent } from "./overview/calc-sharing";
import { MasterDetailService } from "../master-detail/master-detail.service";

/**
 * Basic calculator optimized for Household usecases and Worker creating a calculation for Household.
 * However, the same calculator may later be extended for Company usecases.
 * The component is based on Master-Detail component so it should be used as a page directly.
 *
 * <example-url>/#/examples/CalcCalculatorBasicExample</example-url>
 *
 * @example
 * <sxy-calc-calculator-basic></sxy-calc-calculator-basic>
 */
@Component({
  selector: "sxy-calc-calculator-basic",
  templateUrl: "calc-calculator-basic.html",
})
export class CalcCalculatorBasicComponent implements OnInit {

  /** Component link for view */
  public CalcUsecaseComponent = CalcUsecaseComponent;
  /** Component link for view */
  public CalcPeriodComponent = CalcPeriodComponent;
  /** Component link for view */
  public CalcWorkerComponent = CalcWorkerComponent;
  /** Component link for view */
  public CalcRowComponent = CalcRowComponent;
  /** Component link for view */
  public CalcAddRowComponent = CalcAddRowComponent;
  /** Component link for view */
  public CalcOverviewComponent = CalcOverviewComponent;
  /** Component link for view */
  public CalcSharingComponent = CalcSharingComponent;

  /** View that is shown in the calculator: Either the editor or read-only overview */
  protected view: "editor" | "overview" = "editor";

  /**
   * Default contstructor creates a new component.
   * @param context - Salaxy context
   * @param uiHelpers - User interface helpers in @salaxy/ionic framework
   */
  constructor(
    private context: SalaxyContext,
    private uiHelpers: SxyUiHelpers,
    private masterDetailSrv: MasterDetailService,
    public translate: NgTranslations,
  ) {
  }

  /** Component initialization */
  public ngOnInit(): void {
    // Init here
  }

  /** Gets the current calculation to which the page is bound */
  public get current(): Calculation {
    return this.context.calculations.current;
  }

  /**
   * Gets all the rows in a way that the editable rows are taken from
   * the rows property instead of result.rows. What this means is that the rows are added
   * immediately even before the server-side calculation is done.
   * TODO: Not sure if this is really needed - could we just use the result rows.
   */
  public get allRows(): ResultRow[] {
    return [
      ...(this.current.result ? this.current.result.rows.filter((x) => x.userRowIndex < 0) : []),
      ...this.current.rows,
    ];
  }

  /** Gets the current role */
  public get role() {
    return this.context.session.role;
  }

  /**
   * Gets the usecase object for the current calculation
   */
  public get usecase(): Usecase {
    return UsecasesLogic.getUsecaseData(this.current);
  }

  /** Closes the detail view */
  public closeDetail = ($event: ComponentDoneEvent<any>) => {
    this.masterDetailSrv.closeDetails();
  }

  /** Recalculates the calculation and closes the detail view. */
  public recalculateAndCloseDetail = ($event: ComponentDoneEvent<any>) => {
    this.masterDetailSrv.closeDetails();
    const loader = this.uiHelpers.showLoading("SALAXY.UI_TERMS.updatingCalculation", "SALAXY.UI_TERMS.pleaseWait");
    this.context.calculations.recalculateCurrent().then((calc) => { loader.dismiss(); });
  }

  /** Add row selection returns. */
  public addRow = ($event: ComponentDoneEvent<CalculationRowType>) => {
    if ($event.action === "ok") {
      this.masterDetailSrv.goToDetailsPage(CalcRowComponent, {
        title: this.translate.get("SALAXY.UI_TERMS.addRow"),
        done: this.recalculateAndCloseDetail,
        row: {
          rowType: $event.data,
        },
      });
    } else {
      this.masterDetailSrv.closeDetails();
    }
  }

  /** If true, the calculation is in such a s state that totals should be shown */
  public shouldShowTotals() {
    return CalculatorLogic.shouldShowTotals(this.current);
  }

  /** Get a label for enumeration */
  public getEnumLabel(enumType, enumValue) {
    return EnumerationsLogic.getEnumLabel(enumType, enumValue);
  }

  /** Gets the dates summary text */
  protected getDatesSummary() {
    return CalculatorLogic.getDatesSummary(this.current);
  }

  /** Gets a color for a calculation row */
  protected getColor(row: ResultRow) {
    const type = new CalcRowsLogic("household").getRowConfigs().find((x) => x.name === row.rowType);
    return type ? type.color : "#eee";
  }
}
