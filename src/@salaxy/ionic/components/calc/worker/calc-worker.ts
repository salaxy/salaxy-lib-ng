import { Component, Output, EventEmitter } from "@angular/core";
import { NavController } from "ionic-angular";

import { EnumerationsLogic, Calculation, TaxCardType, CalcWorker } from "@salaxy/core";
import { SalaxyContext, ComponentDoneEvent } from "@salaxy/ng";

/**
 * Shows the view / edit Worker component for a Calculation.
 * Allows viewing / editing of the properties of a Worker, in the future also potentially selecting one.
 *
 * <example-url>/#/examples/CalcWorkerExample</example-url>
 *
 * @example <sxy-calc-worker (done)="done($event)"></sxy-calc-worker>
 *
 */
@Component({
  selector: "sxy-calc-worker",
  templateUrl: "calc-worker.html",
})
export class CalcWorkerComponent {

  /**
   * Called when the user does a final action: OK / Cancel
   */
  @Output() public done = new EventEmitter<ComponentDoneEvent<CalcWorker>>();

  /**
   * Age group for calculations if the date of birth is not known.
   */
  public ageGroup = "age18_52";

  /** Component level flag - only valid during the lifetime of this component. */
  private _askForTaxCardSetToFalse = false;

  /** Creates a new instance of CalcWorkerComponent  */
  constructor(
    protected context: SalaxyContext,
    public navCtrl: NavController,
  ) {
    this._askForTaxCardSetToFalse = !this.isAnyDataMissing();
  }

  /** Returns true if the Worker is editable. */
  public get isEditable(): boolean {
    if (this.model.worker.isSelf) {
      return false;
    }
    // TODO: Handle linked workers here - Payment data may still be editable.
    return true;
  }

  /**
   * If true, Salaxy backoffice should as fo the tax card.
   */
  public get askForTaxCard(): boolean {
    // HACK: Need to store this as an enumeration.
    if (this._askForTaxCardSetToFalse) {
      // User has explicitely set the flag to false - this will be stored only for the lifetime of the component.
      return false;
    }
    // If not set to component level, any missing data will make this true (we will ask for the rest of the data).
    return this.isAnyDataMissing();
  }
  public set askForTaxCard(value: boolean) {
    if (value) {
      this._askForTaxCardSetToFalse = false;
      this.model.worker.paymentData.taxCardType = null;
    } else {
      this._askForTaxCardSetToFalse = true;
    }
  }

  /** Gets or sets the tax card type in a way that example is shown as null  */
  public get taxCardType() {
    return this.model.worker.paymentData.taxCardType === TaxCardType.Example ? null : this.model.worker.paymentData.taxCardType;
  }
  public set taxCardType(value: TaxCardType) {
    this.model.worker.paymentData.taxCardType = value;
    if (this.model.worker.paymentData.taxCardType === TaxCardType.NoTaxCard) {
      this.model.worker.paymentData.taxPercent = 60;
    }
  }

  /** The calculation to which the component is bound. */
  public get model(): Calculation {
    return this.context.calculations.current;
  }

  /** Gets the age ranges. */
  public getAgeRanges() {
    return EnumerationsLogic.getEnumMetadata("AgeRange").values;
  }

  /**
   * Recalculate and move back to main screen.
   */
  public submit(): void {
    this.context.calculations.recalculateCurrent();
    this.done.emit({
      action: "ok",
      data: this.model.worker,
    });
  }

  private isAnyDataMissing() {
    return this.model.worker.paymentData.taxCardType === TaxCardType.Example
      || !this.model.worker.paymentData.taxCardType
      || !this.model.worker.paymentData.taxPercent
      || !this.model.worker.paymentData.email
      || !this.model.worker.paymentData.socialSecurityNumber;
  }
}
