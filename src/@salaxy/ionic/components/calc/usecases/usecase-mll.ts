import { Component, Output, EventEmitter } from "@angular/core";
import { NavController, NavParams } from "ionic-angular";

import { Dates, SalaryKind, UsecaseChildCare, Numeric } from "@salaxy/core";
import { SalaxyContext, ComponentDoneEvent, NgTranslations } from "@salaxy/ng";

import { CalcUsecaseBaseComponent } from "./usecase-base";
import { SxyUiHelpers } from "../../../services/index";

/**
 * Usecase editor for childcare salary based on MLL (Mannerheimin Lastensuojeluliitto) recommendations.
 */
@Component({
    selector: "sxy-usecase-mll",
    templateUrl: "usecase-mll.html",
})
export class CalcUsecaseMllComponent extends CalcUsecaseBaseComponent {

  /**
   * Event emitted when object chosen / OK.
   */
  @Output() public done = new EventEmitter<ComponentDoneEvent<void>>();

  /** Getter for usecase data typed for this particular usecase. */
  public get usecaseMll() {
    return this.usecase as UsecaseChildCare;
  }

  /** Getter for card subtitle */
  public get cardSubtitle(): string {
    const key = (this.usecaseMll.isSunday) ? "SALAXY.ION.COMPONENTS.CalcUsecaseBase.hoursPerPriceWithSunday" : "SALAXY.ION.COMPONENTS.CalcUsecaseBase.hoursPerPrice";
    return this.translate.get(key, { amount: this.amountWithMin, price: Numeric.formatPrice(this.calc.salary.price) });
  }

  /** Getter for the amount (hours) with minimum amount (2 hours in MLL case) */
  public get amountWithMin(): number {
    return (!this.calc.salary.amount || this.calc.salary.amount < 2) ? 2 : this.calc.salary.amount;
  }

  /** Constructor */
  constructor(
    navCtrl: NavController,
    navParams: NavParams,
    context: SalaxyContext,
    uiHelpers: SxyUiHelpers,
    translate: NgTranslations,
  ) {
    super(navCtrl, navParams, context, uiHelpers, translate);
  }

  /** Gets the salary sum as calculated for this usecase without going to server. */
  public getSum(): number {
    return this.amountWithMin * (this.usecaseMll.useCustomPrice ? this.calc.salary.price : 8.20) * (this.usecaseMll.isSunday ? 2 : 1);
  }

}
