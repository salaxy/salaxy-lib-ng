import { Component, Output, EventEmitter } from "@angular/core";
import { NavController, NavParams } from "ionic-angular";

import { SalaryKind, UsecaseChildCare, Numeric } from "@salaxy/core";
import { SalaxyContext, ComponentDoneEvent, NgTranslations } from "@salaxy/ng";

import { CalcUsecaseBaseComponent } from "./usecase-base";
import { SxyUiHelpers } from "../../../services/index";

/**
 * Usecase editor for childcare salary.
 */
@Component({
  selector: "sxy-usecase-child-care",
  templateUrl: "usecase-child-care.html",
})
export class CalcUsecaseChildCareComponent extends CalcUsecaseBaseComponent {

  /**
   * Event emitted when object chosen / OK.
   */
  @Output() public done = new EventEmitter<ComponentDoneEvent<void>>();

  /** Base */
  constructor(
    navCtrl: NavController,
    navParams: NavParams,
    context: SalaxyContext,
    uiHelpers: SxyUiHelpers,
    translate: NgTranslations,
  ) {
    super(navCtrl, navParams, context, uiHelpers, translate);
  }

  /** uscase data typed for this particular usecase. */
  public get usecaseChildCare() {
    return this.usecase as UsecaseChildCare;
  }

  /** Get price label attribute dynamically */
  public get childCarePriceLabel(): string {
    if (this.usecaseChildCare.isChildcareSubsidy) {
      return this.translate.get('SALAXY.ION.COMPONENTS.CalcUsecaseChildCare.wageInAdditionToBenefits');
    }
    return this.translate.get('SALAXY.ION.COMPONENTS.CalcUsecaseBase.pleaseInput') + ' ' + this.priceLabel;
  }

  /** Gets the sub title that explains the salary calculation. */
  public getSubTitle() {
    if (this.usecaseChildCare.isChildcareSubsidy) {
      return super.getSubTitle() + " + " + this.translate.get("SALAXY.ION.COMPONENTS.CalcUsecaseChildCare.childcareSubsidy").toLowerCase() + Numeric.formatPrice(this.usecaseChildCare.subsidyAmount);
    }
    return super.getSubTitle() + " - " + this.translate.get("SALAXY.ION.COMPONENTS.CalcUsecaseBase.householdDeduction").toLowerCase();
  }
}
