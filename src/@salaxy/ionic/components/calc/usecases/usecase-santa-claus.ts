import { Component, Output, EventEmitter } from "@angular/core";
import { NavController, NavParams } from "ionic-angular";

import * as momentImport from "moment";
/** workaround */
const moment = momentImport;

import { Dates, SalaryKind } from "@salaxy/core";
import { SalaxyContext, ComponentDoneEvent, NgTranslations } from "@salaxy/ng";

import { CalcUsecaseBaseComponent } from "./usecase-base";
import { SxyUiHelpers } from "../../../services/index";

/**
 * Santa Claus defaults.
 * TODO: This Usecase editor has never been in production and it has not been tested.
 * However, it is quite simple, so it probably works.
 * The Usecase may be finalized / tested before Christmas 2018.
 */
@Component({
    selector: "sxy-usecase-santa-claus",
    templateUrl: "usecase-santa-claus.html",
})
export class CalcUsecaseSantaClausComponent extends CalcUsecaseBaseComponent {

  /**
   * Event emitted when object chosen / OK.
   */
  @Output() public done = new EventEmitter<ComponentDoneEvent<void>>();

  /** Base */
  constructor(
    navCtrl: NavController,
    navParams: NavParams,
    context: SalaxyContext,
    uiHelpers: SxyUiHelpers,
    translate: NgTranslations,
  ) {
    super(navCtrl, navParams, context, uiHelpers, translate);
  }

}
