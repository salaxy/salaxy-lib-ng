import { Component, Output, EventEmitter } from "@angular/core";

import { Calculation, SalaryKind, UsecasesLogic, Usecase } from "@salaxy/core";
import { SalaxyContext, ComponentDoneEvent } from "@salaxy/ng";

import { TesParameters } from "@salaxy/core";
import { NavParams } from "ionic-angular";

/**
 * Edits the properties of the current Usecase.
 * This is basically just a switch-statement that chooses
 * the appropriate Usecase editor:
 *
 * - CalcUsecaseBaseComponent
 * - CalcUsecaseChildCareComponent
 * - CalcUsecaseCleaningComponent
 * - CalcUsecaseConstructionComponent
 * - CalcUsecaseMllComponent
 * - CalcUsecaseSantaClausComponent
 *
 */
@Component({
  selector: "sxy-calc-usecase",
  templateUrl: "calc-usecase.html"
})
export class CalcUsecaseComponent {

  /**
   * Event emitted when object chosen / OK.
   */
  @Output() public done = new EventEmitter<ComponentDoneEvent<void>>();

  /** Known use case uris. */
  public knownUsecases = UsecasesLogic.knownUseCases;

  /** Creates a new instance */
  constructor(protected context: SalaxyContext, private navParams: NavParams) {
    if (navParams.get("done")) {
      this.done.subscribe(navParams.get("done"));
    }
  }

  /**
   * Calculation to which the row being edited belongs
   */
  public get calc(): Calculation {
    return this.context.calculations.current;
  }

  /**
   * The use case that we are editing
   */
  public get usecase(): Usecase {
    return UsecasesLogic.getUsecaseData(this.calc);
  }

  /** Commits the changes to the framework */
  public commit(): void {
    this.done.emit({ action: "ok", data: null });
  }
}
