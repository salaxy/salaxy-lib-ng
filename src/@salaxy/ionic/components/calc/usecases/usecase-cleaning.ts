import { Component, Output, EventEmitter } from "@angular/core";
import { NavController, NavParams } from "ionic-angular";

import { SalaryKind } from "@salaxy/core";
import { SalaxyContext, ComponentDoneEvent, NgTranslations } from "@salaxy/ng";

import { CalcUsecaseBaseComponent } from "./usecase-base";
import { SxyUiHelpers } from "../../../services/index";

/**
 * Generic cleaning / household cearing framework / use-case input
 *
 * <example-url>/#/examples/CalcUsecaseCleaningExample</example-url>
 */
@Component({
  selector: "sxy-usecase-cleaning",
  templateUrl: "usecase-cleaning.html"
})
export class CalcUsecaseCleaningComponent extends CalcUsecaseBaseComponent {

  /**
   * Event emitted when object chosen / OK.
   */
  @Output() public done = new EventEmitter<ComponentDoneEvent<void>>();

  /** Base */
  constructor(
    navCtrl: NavController,
    navParams: NavParams,
    context: SalaxyContext,
    uiHelpers: SxyUiHelpers,
    translate: NgTranslations,
  ) {
    super(navCtrl, navParams, context, uiHelpers, translate);
  }
}
