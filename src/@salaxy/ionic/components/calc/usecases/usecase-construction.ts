import { Component, Output, EventEmitter } from "@angular/core";
import { NavController, NavParams } from "ionic-angular";

import { SalaryKind, TesSubtype } from "@salaxy/core";
import { SalaxyContext, ComponentDoneEvent, NgTranslations } from "@salaxy/ng";

import { CalcUsecaseBaseComponent } from "./usecase-base";
import { SxyUiHelpers } from "../../../services/index";

/**
 * Construction / Raksa usecase editor user interface.
 */
@Component({
  selector: "sxy-usecase-construction",
  templateUrl: "usecase-construction.html",
})
export class CalcUsecaseConstructionComponent extends CalcUsecaseBaseComponent {

  /**
   * Event emitted when object chosen / OK.
   */
  @Output() public done = new EventEmitter<ComponentDoneEvent<void>>();

  /** View model specific for this use case */
  public model: any = {};

  /** Base */
  constructor(
    navCtrl: NavController,
    navParams: NavParams,
    context: SalaxyContext,
    uiHelpers: SxyUiHelpers,
    translate: NgTranslations,
  ) {
    super(navCtrl, navParams, context, uiHelpers, translate);
  }

  /**
   * True if expenses are specified based on TES tables.
   * Otherwise, they are separately agreed per day.
   */
  public get isExpensesTesBased(): boolean {
    const fx = this.calc.framework;
    return fx.subType === TesSubtype.ConstructionOther || fx.subType === TesSubtype.ConstructionCarpenter || fx.subType === TesSubtype.ConstructionFloor;
  }
  public set isExpensesTesBased(value) {
    const fx = this.calc.framework;
    if (value) {
      if (!fx.subType || fx.subType === TesSubtype.ConstructionFreeContract) {
        fx.subType = TesSubtype.ConstructionCarpenter;
      }
    } else {
      fx.subType = TesSubtype.ConstructionFreeContract;
    }
  }
}
