import { Component, Output, EventEmitter, Input } from "@angular/core";
import { NavController, NavParams } from "ionic-angular";
import { Page } from "ionic-angular/navigation/nav-util";

import { UsecasesLogic, Usecase, UsecaseGroup } from "@salaxy/core";
import { SalaxyContext, ComponentDoneEvent } from "@salaxy/ng";

/**
 * User interface for the user to select the Calculation Usecase.
 * Typically, a calculation is created by first selecting a usecase.
 * However, it is possible to change the Usecase later or start the creation of the calculation in some other way.
 * The current implementation supports only household usecases.
 * Company usecases are expected to be added later.
 *
 * <example-url>/#/examples/CalcSelectUsecaseExample</example-url>
 */
@Component({
  selector: "sxy-select-usecase",
  templateUrl: "select-usecase.html",
  styles: [
    ".sxy-back-icon { text-align: center; font-size 1.5em; line-height: 56px; color: gray; }",
  ],
})
export class CalcSelectUsecaseComponent {

  /**
   * Event emitted when object chosen / OK.
   */
  @Output() public done = new EventEmitter<ComponentDoneEvent<Usecase>>();

  /** Called when the user selects a usecase group (not the final usecase selection). */
  @Output() public groupSelected = new EventEmitter<ComponentDoneEvent<UsecaseGroup>>();

  /**
   * If true (default) sets the current calculation based on the selected use case automatically.
   * This will also create the current calculation if it is not null.
   * Set this flag to false to use your custom logic instead.
   * Note that if set to false, you must also handle the groupSelected event: Typically "selectedItem = $event.data"
   */
  @Input() public setCurrent = true;

  /**
   * If set to true, will show "...back" selection when a group is selected.
   * This takes the user back to the beginning of selection
   */
  @Input() public showBackToRoot = false;

  /** The currently selected item in the tree */
  public selectedItem;

  /** The tree for selecting the calculation usecase (household) */
  private tree = UsecasesLogic.getUsecaseTree("household");

  /** Creates a new instance of CalcSelectUsecase */
  constructor(
    public navCtrl: NavController,
    navParams: NavParams,
    protected context: SalaxyContext,
  ) {
    if (navParams.get("item")) {
      this.selectedItem = navParams.get("item");
    }
    if (navParams.get("done")) {
      this.done.subscribe(navParams.get("done"));
    }
  }

  /** Handles a node click */
  public selectNode(item: UsecaseGroup | Usecase) {
    if ((item as UsecaseGroup).children) {
      if (this.setCurrent) {
        this.selectedItem = item;
      }
      this.groupSelected.emit({
        action: "selectionchange",
        data: item as UsecaseGroup,
      });
    } else {
      const usecase = item as Usecase;
      if (this.setCurrent) {
        if (!this.context.calculations.current) {
          this.context.calculations.newCurrent();
        }
        UsecasesLogic.setUsecase(this.context.calculations.current, usecase);
      }
      this.done.emit({
        action: "ok",
        data: usecase,
      });
    }
  }

  /** The current list of usecases (from the tree) */
  public get currentList() {
    return (this.selectedItem) ? this.selectedItem.children : this.tree;
  }

  /** Go to parent element in the tree */
  public goToParent(): void {
    if (!this.selectedItem) {
      return;
    }
    // Returns a parent of a usecase or null if the selectedItem is a group.
    const group = UsecasesLogic.findGroupById("household", this.selectedItem.id);
    this.selectedItem = group.id === this.selectedItem.id ? null : group;
  }
}
