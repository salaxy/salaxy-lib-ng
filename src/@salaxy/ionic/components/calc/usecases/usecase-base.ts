import { Component, Output, EventEmitter, Input } from "@angular/core";
import { NavController, NavParams } from "ionic-angular";

import { Calculation, SalaryKind, TesParameters, CalculatorLogic, EnumerationsLogic, UsecasesLogic, Usecase, Numeric, TaxDeductionWorkCategories } from "@salaxy/core";
import { SalaxyContext, ComponentDoneEvent, NgTranslations } from "@salaxy/ng";

import { SxyUiHelpers } from "../../../services/index";
import { PopunderComponent } from "../../common/popunder";

/**
 * Base Component for showing the edit user interface for a calculation Usecase.
 * Extend this to create your own Usecase editor or use as-is as a generic user interface.
 */
@Component({
  selector: "sxy-usecase",
  templateUrl: "usecase-base.html"
})
export class CalcUsecaseBaseComponent {

  /**
   * Event emitted when object chosen / OK.
   */
  @Output() public done = new EventEmitter<ComponentDoneEvent<void>>();

  /**
   * Calculation to which the Usecase being edited belongs
   */
  @Input() public calc: Calculation;

  /** Creates a new instance of CalcUsecaseBase */
  constructor(
    protected navCtrl: NavController,
    protected navParams: NavParams,
    protected context: SalaxyContext,
    private uiHelpers: SxyUiHelpers,
    public translate: NgTranslations,
  ) {}

  /**
   * Returns the current use case.
   */
  public get usecase(): Usecase {
    return UsecasesLogic.getUsecaseData(this.calc);
  }

  /** Initializes the component */
  public ngOnInit(): void {
    // Potential init code
  }

  /**
   * Returns true if this particular row type should have the possibility to select household deduction on/off.
   * Currently, only checks that the salary type is not compensation
   */
  public get showHouseholdDeductionSelection(): boolean {
    if (this.calc.salary.isHouseholdDeductible) {
      return true;
    }
    // TODO: Check subsidy and move to logic
    // canBeHouseholdDeductible
    if (this.calc.salary.kind === SalaryKind.Compensation) {
      return false;
    }
    return true;
  }

  /** Gets the label for price input */
  public get priceLabel(): string {
    switch (this.calc.salary.kind) {
      case SalaryKind.Compensation: return this.translate.get("SALAXY.ENUM.SalaryKind.compensation.label");
      case SalaryKind.FixedSalary: return this.translate.get("SALAXY.ENUM.SalaryKind.fixedSalary.label");
      case SalaryKind.HourlySalary: return this.translate.get("SALAXY.ENUM.SalaryKind.hourlySalary.label");
      case SalaryKind.MonthlySalary: return this.translate.get("SALAXY.ENUM.SalaryKind.monthlySalary.label");
      case SalaryKind.TotalEmployerPayment: return this.translate.get("SALAXY.ENUM.SalaryKind.totalEmployerPayment.label");
      case SalaryKind.TotalWorkerPayment: return this.translate.get("SALAXY.ENUM.SalaryKind.totalWorkerPayment.label");
      default: return this.translate.get("SALAXY.ION.COMPONENTS.CalcUsecaseBase.defaultPriceLabel");
    }
  }

  /** Gets or sets the value indicating whether the period has been set. */
  public get periodText(): string {
    return UsecasesLogic.getPeriodText(this.calc);
  }
  public set periodText(value: string) {
    UsecasesLogic.setPeriodText(this.calc, value);
  }

  /** Commits the changes to the usecase */
  public commit(): void {
    const loader = this.uiHelpers.showLoading("SALAXY.ION.COMPONENTS.CalcUsecaseBase.updatingCalc", "SALAXY.ION.COMPONENTS.CalcUsecaseBase.wait");
    UsecasesLogic.applyUseCase(this.calc);
    this.context.calculations.recalculateCurrent().then(calc => {
      loader.dismiss();
      this.done.emit({ action: "ok", data: null });
    })
    .catch((err) => {
      loader.dismiss();
    });
  }

  /** Gets the subtitle for the usecase. This is typically the formula for salary calculation. */
  public getSubTitle(): string {
    if (this.calc.salary.kind === SalaryKind.HourlySalary) {
      return this.translate.get("SALAXY.ION.COMPONENTS.CalcUsecaseBase.hoursPerPrice", {
        amount: this.calc.salary.amount,
        price: Numeric.formatPrice(this.calc.salary.price || 0)
      });
    } else if (this.calc.salary.kind === SalaryKind.MonthlySalary) {
      return this.translate.get("SALAXY.ION.COMPONENTS.CalcUsecaseBase.pricePerMonth", {
        price: Numeric.formatPrice(this.getSum())
      });
    } else if (this.calc.salary.kind === SalaryKind.Compensation) {
      return this.translate.get("SALAXY.ENUM.SalaryKind.compensation.label") + ': ' + Numeric.formatPrice(this.getSum());
    } else {
      return `${Numeric.formatPrice(this.getSum())}`;
    }
  }

  /** Gets the salary sum as calculated for this usecase without going to server. */
  public getSum(): number {
    return (this.calc.salary.amount || 0) * (this.calc.salary.price || 0);
  }

  /** Resets the price and amount when salary kind is changed. */
  public changeSalaryKind(): void {
    this.calc.salary.price = null;
    if (this.calc.salary.kind === SalaryKind.HourlySalary) {
      this.calc.salary.amount = null;
    } else {
      this.calc.salary.amount = 1;
    }
  }
}
