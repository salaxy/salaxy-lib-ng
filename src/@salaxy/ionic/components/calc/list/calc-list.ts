import { Component, Input } from "@angular/core";
import { NavController } from "ionic-angular";
import { Page } from "ionic-angular/navigation/nav-util";

import { Calculation } from "@salaxy/core";
import { NgCalcListComponent as SalaxyCalcListComponent, SalaxyContext, NgTranslations } from "@salaxy/ng";

import { MasterDetailService } from "../../master-detail/master-detail.service";

/**
 * Shows a simple list of calculations.
 *
 * <example-url>/#/examples/CalcListExample</example-url>
 *
 * @example
 * <sxy-calc-list [itemSxyDetailPush]="detailComponent" [navParams]="{}" limitTo="10" filterStatuses="draft,paymentSucceeded"></sxy-calc-list>
 *
 */
@Component({
  selector: "sxy-calc-list",
  templateUrl: "./calc-list.html",
})
export class CalcListComponent extends SalaxyCalcListComponent {

  /**
   * Adds ionic navigation push functionality to each item.
   * Basically the same as adding navPush to each item (https://ionicframework.com/docs/api/components/nav/NavPush/).
   * Also supports navParams.
   * If navParams is not null, also the calculation is added to the navParams as "calc".
   */
  @Input() public itemNavPush: string | Page;

  /**
   * Adds Salaxy Master-detail Detail-push functionality to each item.
   * Basically the same as addin sxyDetailPush to each item.
   * Also supports navParams.
   * If navParams is not null, also the calculation is added to the navParams as "calc".
   */
  @Input() public itemSxyDetailPush: string | Page;

  /**
   * If not null, will be sent together with either itemNavPush or itemSxyDetailPush functionality.
   * In this case, also the calculation is added to the navParams as "calc".
   */
  @Input() public navParams: any;

  constructor(
    public context: SalaxyContext,
    public navCtrl: NavController,
    public masterDetailSrv: MasterDetailService,
    public translate: NgTranslations,
  ) {
    super(context);
    this.itemSelect.subscribe((calc: Calculation) => {
      if (this.navParams != null) {
        this.navParams.calc = calc;
      }
      if (this.itemNavPush) {
        this.navCtrl.push(this.itemNavPush, this.navParams);
      }
      if (this.itemSxyDetailPush) {
        masterDetailSrv.goToDetailsPage(this.itemSxyDetailPush, this.navParams);
      }
    });
  }
}
