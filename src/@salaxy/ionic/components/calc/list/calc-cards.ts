import { Component } from "@angular/core";
import { NavController } from "ionic-angular";

import { Calculation, CalculationStatus, Dates } from "@salaxy/core";
import { SalaxyContext, NgTranslations } from "@salaxy/ng";

import { MasterDetailService } from "../../master-detail/master-detail.service";
import { CalcListComponent } from "./calc-list";

/**
 * Shows a list of calculations in a card user interface.
 * Used on service front pages and portal pages.
 *
 * <example-url>/#/examples/CalcCardsExample</example-url>
 *
 * @example
 * <div style="max-width: 460px; margin: auto">
 *   <sxy-calc-cards [detailsPage]="detailPage" limitTo="5"></sxy-calc-cards>
 * </div>
 *
 */
@Component({
  selector: "sxy-calc-cards",
  templateUrl: "./calc-cards.html",
})
export class CalcCardsComponent extends CalcListComponent {

  constructor(
    public context: SalaxyContext,
    public navCtrl: NavController,
    public masterDetailSrv: MasterDetailService,
    public translate: NgTranslations,
  ) {
    super(context, navCtrl, masterDetailSrv, translate);
  }

  /** Gets the subtitle text */
  public getSubTitle(calc: Calculation): string {
    const date = calc.workflow.paidAt || calc.updatedAt || calc.createdAt;
    const dateText = Dates.getFormattedDate(date);
    switch (calc.workflow.status) {
      case CalculationStatus.Draft: return this.translate.get("SALAXY.ENUM.CalculationStatus.draft.label") + " " + dateText;
      case CalculationStatus.PaymentCanceled: return this.translate.get("SALAXY.ENUM.CalculationStatus.paymentCanceled.label");
      case CalculationStatus.PaymentRefunded: return this.translate.get("SALAXY.ENUM.CalculationStatus.paymentRefunded.label");
      case CalculationStatus.PaymentError: return this.translate.get("SALAXY.ENUM.CalculationStatus.paymentErro.labelr");
      case CalculationStatus.PaymentStarted: return this.translate.get("SALAXY.ENUM.CalculationStatus.paymentStarted.label");
      case CalculationStatus.PaymentSucceeded: return this.translate.get("SALAXY.ENUM.CalculationStatus.paymentSucceeded.label") + " " + dateText;
      case CalculationStatus.PaymentWorkerCopy: return this.translate.get("SALAXY.ENUM.CalculationStatus.paymentWorkerCopy.label") + " " + dateText;
    }
  }

}
