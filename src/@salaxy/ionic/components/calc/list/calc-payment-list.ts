import { Component } from "@angular/core";

import { Calculation } from "@salaxy/core";

import { CalcListComponent } from "./calc-list";

/**
 * Shows a list of payments made by the user
 * TODO: Is this really in use and could it be done simply as part of the sxy-calc-list?
 *
 * <example-url>/#/examples/CalcPaymentListExample</example-url>
 *
 * @example
 * <sxy-calc-payment-list></sxy-calc-payment-list>
 *
 */
@Component({
  selector: "sxy-calc-payment-list",
  templateUrl: "./calc-payment-list.html"
})
export class CalcPaymentListComponent extends CalcListComponent {

  /** Gets a payment for the calculations */
  public getPayment(calculation: Calculation): number {

    if (!this.context.session.avatar) {
      return 0;
    }

    // Resolve by worker/employer

    // This actually is not correct (should use soc sec number)
    if (calculation.worker && calculation.worker.accountId === this.context.session.avatar.id) {
      return calculation.result.totals.totalGrossSalary;
    }

    if (calculation.employer &&calculation.employer.accountId === this.context.session.avatar.id) {
      return -1.0 * calculation.result.employerCalc.totalPayment;
    }

    return -1.0 * calculation.result.employerCalc.totalPayment;
  }

  /** Get a description from the calculation */
  public getDescription(calculation: Calculation): string {
    return calculation.info.workDescription || this.translate.get("SALAXY.ION.COMPONENTS.CalcPaymentList.salaryPayment");
  }
}
