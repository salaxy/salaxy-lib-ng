import { Component } from "@angular/core";
import { NavController } from "ionic-angular";

import { EnumerationsLogic } from "@salaxy/core";
import { SalaxyContext, NgTranslations } from "@salaxy/ng";

import { MasterDetailService } from "../../master-detail/master-detail.service";
import { CalcListComponent } from "./calc-list";

/**
 * Shows a grouped list of calculations.
 *
 * <example-url>/#/examples/CalcGroupedListExample</example-url>
 *
 * @example
 * <sxy-calc-grouped-list filterStatuses="draft,paymentSucceeded" groupBy="month"></sxy-calc-grouped-list>
 *
 */
@Component({
    selector: "sxy-calc-grouped-list",
    templateUrl: "./calc-grouped-list.html",
})
export class CalcGroupedListComponent extends CalcListComponent {

  constructor(
    context: SalaxyContext,
    navCtrl: NavController,
    masterDetailSrv: MasterDetailService,
    translate: NgTranslations,
  ) {
    super(context, navCtrl, masterDetailSrv, translate);
  }

  /** Returns the formatted / end-user friendly grouping key. */
  public getGroupingText(groupKey: string): string {
    if (!groupKey) {
      return "-";
    }
    if (!this.groupBy || this.groupBy === "status") {
      return EnumerationsLogic.getEnumLabel("CalculationStatus", groupKey);
    }
    return groupKey;
  }

}
