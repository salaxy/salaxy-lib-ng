import { Component, Output, EventEmitter } from "@angular/core";
import { NavController } from "ionic-angular";

import { Dates } from "@salaxy/core";
import { SalaxyContext, ComponentDoneEvent } from "@salaxy/ng";

import { Calculation } from "@salaxy/core";

/**
 * Component for defining the payment properties
 * HACK: This is a demo control of future features. This is not for production use.
 */
@Component({
  selector: "sxy-calc-payment",
  templateUrl: "calc-payment.html"
})
export class CalcPaymentComponent {

  /**
   * Called when the user does a final action: OK / Cancel
   */
  @Output() public done = new EventEmitter<ComponentDoneEvent<void>>();

  /** Creates a new instance of CalcPaymentPage  */
  constructor(protected context: SalaxyContext, public navCtrl: NavController) {
    if (!(this.calc as any).uiTemp_payment) {
      (this.calc as any).uiTemp_payment = "siirto";
    }
    if (!(this.calc as any).uiTemp_recurrence) {
      (this.calc as any).uiTemp_recurrence = "";
    }
  }

  /** Gets the current calculation */
  public get calc(): Calculation {
    return this.context.calculations.current;
  }

  /** Naive implementation for payment dates */
  public get days(): object {
    // HACK: This is naive implementation for DEMO purposes. The real logic
    // must count for weekends and holidays in Finland.
    const paidAtDay =
      Dates.getMoment(this.calc.workflow.paidAt) || Dates.getTodayMoment();
    const salaryDay = paidAtDay.clone();
    switch ((this.calc as any).uiTemp_payment) {
      default:
      case "siirto":
      case "nordea":
        break;
      case "creditcard":
        salaryDay.add(7, "days");
        break;
      case "paytrail":
        salaryDay.add(1, "days");
        break;
    }

    const nextPaidAtDay = paidAtDay.clone();
    const nextSalaryDay = salaryDay.clone();

    switch ((this.calc as any).uiTemp_recurrence) {
      case "weekly":
        nextPaidAtDay.add(1, "week");
        nextSalaryDay.add(1, "week");
        break;
      case "biweekly":
        nextPaidAtDay.add(2, "week");
        nextSalaryDay.add(2, "week");
        break;
      case "twiceamonth":
        nextPaidAtDay.add(15, "days");
        nextSalaryDay.add(15, "days");
        break;
      case "monthly":
        nextPaidAtDay.add(1, "month");
        nextSalaryDay.add(1, "month");
        break;
    }

    return {
      nextPayment: nextPaidAtDay.format("D.M.YYYY"),
      nextSalary: nextSalaryDay.format("D.M.YYYY"),
      payment: paidAtDay.format("D.M.YYYY"),
      salary: salaryDay.format("D.M.YYYY")
    };
  }

  /** Gets the today date as ISO */
  public get todayAsIso(): string {
    return Dates.getTodayMoment().format("YYYY-MM-DD");
  }

  /** Recalculate and move back to main screen. */
  public setValue(): void {
    this.navCtrl.pop();
  }
}
