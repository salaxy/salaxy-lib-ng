import { Component, Output, EventEmitter } from "@angular/core";
import { NavParams } from "ionic-angular";

import { CalendarComponentOptions } from "ion2-calendar";
import * as momentImport from "moment";
/** workaround */
const moment = momentImport;

import { Dates, Calculation } from "@salaxy/core";
import { SalaxyContext, ComponentDoneEvent } from "@salaxy/ng";

/**
 * Editor component for selecting the work period and potentially modifying the number of working days.
 */
@Component({
  selector: "sxy-calc-period",
  templateUrl: "calc-period.html"
})
export class CalcPeriodComponent {

  /**
   * Called when the user does a final action: OK / Cancel
   */
  @Output() public done = new EventEmitter<ComponentDoneEvent<void>>();

  /**
   * Timezone corrected and simplified date ISO strings (in format YYYY-MM-DD) as well as
   * calculated / inputted days count.
   * This is a non-null object, check hasSelection to see whether values should be used or not.
   */
  public result = {
    from: null,
    to: null,
    workDayCount: null,
  };

  /** Date range where the calendar component is bound to */
  public dateRange = {
    from: null,
    to: null,
  };

  /** Options for the calendar component */
  public calendarOptions: CalendarComponentOptions = {
    from: moment().subtract(6, "months").startOf("year").toDate(),
    pickMode: "range",
    to: moment().add(6, "months").endOf("year").toDate(),
    weekStart: 1,
    color: "cal-color",
    weekdays: ["Su", "Ma", "Ti", "Ke", "To", "Pe", "La"]
  };

  /** Creates a new instance of CalcRowBase */
  constructor(
    protected context: SalaxyContext,
    private navParams: NavParams,
  ) {}

  /** Initializes the control */
  ngOnInit(): void {
    this.dateRange.from = Dates.asDate(this.calc.info.workStartDate);
    this.dateRange.to = Dates.asDate(this.calc.info.workEndDate);
    this.result.from = this.calc.info.workStartDate;
    this.result.to = this.calc.info.workEndDate;
    this.result.workDayCount = this.calc.framework.numberOfDays;
  }

  /**
   * Fired when the ion-calendar component changes.
   * Does the workDayCount calculation.
   * @param $event ion2-calendar event.
   */
  public calendarChange($event): void {
    this.result.from = Dates.asDate($event.from);
    this.result.to = Dates.asDate($event.to);
    this.result.workDayCount = Dates.getWorkdayCount(this.result.from, this.result.to);
  }

  /**
   * Fired when the ion-calendar component selection is started.
   * This event is handled so that a single-click sets the period to one day.
   * Does the workDayCount calculation. If a period is selected,
   * this event is fired and then the calendarChange is fired immediately afterwards.
   * @param $event ion2-calendar event.
   */
  public calendarSelectStart($event): void {
    const timeAsDate = Dates.asDate($event.time);
    this.result.from = timeAsDate;
    this.result.to = timeAsDate;
    this.result.workDayCount = 1;
  }

  private get calc(): Calculation {
    return this.context.calculations.current;
  }

  /** Commits the changes to the framework */
  public commit(): void {
    this.calc.info.workStartDate = this.result.from;
    this.calc.info.workEndDate = this.result.to;
    this.calc.framework.numberOfDays = this.result.workDayCount;
    this.done.emit({ action: "ok", data: null });
  }
}
