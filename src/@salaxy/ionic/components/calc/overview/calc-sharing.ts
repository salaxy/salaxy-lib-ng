// Framework components
import { Component, OnInit, Inject } from "@angular/core";
// Salaxy framework
import { Client } from "@salaxy/core";
import { SalaxyContext, ClientNg, NgTranslations } from "@salaxy/ng";
// Current project
import { SxyUiHelpers } from "../../../services/sxy-ui-helpers";

/** Temporary shareType: This would be provided by the API */
export type shareType = "" | "short" | "shortWithPwd" | "unguessable";

/**
 * Provides a user interface for sharing a Calculation.
 *
 * <example-url>/#/examples/CalcSharingExample</example-url>
 *
 * @example
 * <sxy-calc-sharing></sxy-calc-sharing>
 */
@Component({
  selector: "sxy-calc-sharing",
  templateUrl: "calc-sharing.html",
})
export class CalcSharingComponent implements OnInit {

  /** If true, the user interface is in the middle of switching the sharing type (show selection) */
  protected doSwitch = false;

  /**
   * Default contstructor creates a new component.
   * @param context - Salaxy context
   * @param uiHelpers - User interface helpers in @salaxy/ionic framework
   */
  constructor(private context: SalaxyContext, private uiHelpers: SxyUiHelpers, @Inject(ClientNg) private client: Client) {}

  /** Component initialization */
  public ngOnInit(): void {
    // Init here
  }

  /**
   * Type of current sharing.
   * No sharing (default) may be null or empty string.
   */
  public get type(): shareType {
    const calc = this.context.calculations.current as any;
    return (calc.sharing || { type: "noSharing" }).type || "noSharing";
  }

  /** Gets the sharing link. */
  public get link(): string {
    const calc = this.context.calculations.current as any;
    return (calc.sharing || {}).link;
  }

  /** Does the sahering operation */
  public share(type: shareType): void {
    this.doSwitch = false;
    if (type === "shortWithPwd") {
      // TODO: Add uiHelpers.prompt() and uiHelpers.promptPwd() ... change strings to translation keys below
      // this.uiHelpers.showConfirm("Syötä salasana", "Voit syöttää tähän salasanan, jonka lähetät linkin vastaanottajalle. Älä käytä salasanoja, joita käytät muissa palveluissa.");
    }
    const method = `/calculations/sharing/${this.context.calculations.current.id}?type=` + type;
    this.client.ajax.postJSON(method, null).then(data => {
      const calc = this.context.calculations.current as any;
      calc.sharing = data;
    });
  }
}
