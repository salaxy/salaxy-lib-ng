import { Component, Output, EventEmitter, Input } from "@angular/core";
import { NavController } from "ionic-angular";

import { Calculation, CalculatorLogic } from "@salaxy/core";
import { ClientNg, SalaxyContext } from "@salaxy/ng";

/**
 * Shows the Calculation overview:
 * A bar chart and tables that show the calculation data.
 * This is used in calculator and typically also as the detail view that appears when a list item is first clicked.
 *
 * <example-url>/#/examples/CalcOverviewExample</example-url>
 *
 * @example
 * <sxy-calc-overview showDetailTables="true" hideChart="true"></sxy-calc-overview>
 */
@Component({
  selector: "sxy-calc-overview",
  templateUrl: "calc-overview.html",
})
export class CalcOverviewComponent {

  /** If true, the chart is hidden: Only the tables are shown. */
  @Input() public hideChart: boolean = false;

  /** If true, the tables part is hidden: Only the overview chart is shown. */
  @Input() public hideTables: boolean = false;

  /**
   * If true, shows the detail tables of the views (percentages etc.).
   * The default condensed view is more readable in mobile views.
   */
  @Input() public showDetailTables: boolean = false;

  /**
   * Type of the report to show in the component.
   * If not set, the default is depending on the user role either "employer" or "worker".
   */
  @Input()
  public get report(): "employer" | "worker" | "payments" {
    return this._report || (this.context.session.isInRole("worker") ? "worker" : "employer");
  }
  public set report(value: "employer" | "worker" | "payments") {
    this._report = value;
  }

  /** Cache for the report HTML. */
  protected reportHtml = {
    employer: null,
    worker: null,
    payments: null,
  };

  private _report = null;

  /** Creates a new instance of CalcOverviewPage */
  constructor(public navCtrl: NavController, private context: SalaxyContext, private client: ClientNg) {
    this.refreshReports();
  }

  /**
   * The current calculation based on which the overview page is shown
   */
  public get current(): Calculation {
    return this.context.calculations.current;
  }

  /** Returns true if the report is loading. */
  public get isLoading(): boolean {
    switch (this.report) {
      case "employer":
        return !this.reportHtml.employer;
      case "payments":
        return !this.reportHtml.payments;
      case "worker":
        return !this.reportHtml.worker;
    }
  }

  /**
   * If true, there is enough input in the calculation so that
   * result / totals tables, charts etc. visualization should be shown.
   */
  public shouldShowTotals(): boolean {
    return CalculatorLogic.shouldShowTotals(this.current);
  }

  /** Gets the current role */
  public get role(): "worker" | "household" | "company" {
    return this.context.session.role;
  }

  /** Toggle the details view */
  public toggleDetails(report: string): void {
    this.showDetailTables = !this.showDetailTables;
  }

  /** Refresh the report HTML */
  public refreshReports(): void {
    if (this.context.calculations.current) {
      /*
        NOTE: We are fetching ALL the reports from server here
        instead of trying to optimize => fetching only the active report type.
        The reason is that in the near-time future we will use client-side rendering for these basic reports
        => No need for that optimization any more.
      */
      this.client.reportsApi.getReportHtmlWithPost("employer", this.context.calculations.current as any).then(
        (data) => { this.reportHtml.employer = data; },
      );
      this.client.reportsApi.getReportHtmlWithPost("worker", this.context.calculations.current as any).then(
        (data) => { this.reportHtml.worker = data; },
      );
      this.client.reportsApi.getReportHtmlWithPost("payments", this.context.calculations.current as any).then(
        (data) => { this.reportHtml.payments = data; },
      );
    }
  }
}
