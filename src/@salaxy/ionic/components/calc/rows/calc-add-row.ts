import { Component, Output, EventEmitter } from "@angular/core";

import { Calculation, CalculationRowType, UserDefinedRow, CalculatorLogic, CalcRowsLogic } from "@salaxy/core";
import { SalaxyContext, ComponentDoneEvent } from "@salaxy/ng";

import { MasterDetailService } from "../../master-detail/master-detail.service";
import { CalcRowComponent } from "./calc-row";
import { NavParams } from "ionic-angular";

/** Component to add a row to a calculation. In practice, selects the type of the row. */
@Component({
  selector: "sxy-calc-add-row",
  templateUrl: "calc-add-row.html",
})
export class CalcAddRowComponent  {

  /**
   * Called when the user does a final action: OK / Cancel
   */
  @Output() public done = new EventEmitter<ComponentDoneEvent<CalculationRowType>>();

  /** Image for the row */
  public img: string = "";

  /** Search results for when user has initiated a row search */
  protected searchResults = [];

  /** Creates a new instanc of CalcAddRowPage */
  constructor(
    protected context: SalaxyContext,
    private masterDetailSrv: MasterDetailService,
    private navParams: NavParams,
  ) {
    if (navParams.get("done")) {
      this.done.subscribe(navParams.get("done"));
    }
  }

  /** Current calculation where the row is added. */
  private get current(): Calculation {
    return this.context.calculations.current;
  }

  /**
   * Returns to the calling view with the selected row type.
   */
  public setRow(rowType: CalculationRowType): void {
    this.done.emit({ action: "ok", data: rowType });
  }

  /**
   * Gets enumeration metadata for row types that are enabled.
   * @param categoryOrFavorites - Either a row category or keyword
   * "favorites" to fetch most common rows accross all categories. Leave null
   * to fetch all enabled row types.
   */
  /* tslint:disable-next-line:max-line-length */
  public getRowTypes(categoryOrFavorites?: "favorites" | "salary" | "salaryAdditions" | "benefits" | "expenses" | "deductions" | "calculation" | "other"): any {
    if (categoryOrFavorites === "favorites") {
      return new CalcRowsLogic("household").getRowConfigs().filter((x) => x.favorite).sort((a, b) => (a.favorite || 0) - (b.favorite || 0));
    }
    if (categoryOrFavorites) {
      return new CalcRowsLogic("household").getRowConfigs().filter((x) => x.category === categoryOrFavorites);
    }
    return new CalcRowsLogic("household").getRowConfigs();
  }

  /** Starting the search */
  protected setSearch(ev: any): void {
    this.searchResults = [];
    const val = ev.target.value;
    if (val && val.trim() !== "") {
      this.searchResults = this.getRowTypes().filter((item) => {
        return (item.label.toLowerCase().indexOf(val.toLowerCase()) > -1) || (item.descr.toLowerCase().indexOf(val.toLowerCase()) > -1);
      });
    }
  }

  /** Gets the fade image for card background */
  protected getCombinedBackgroundImage(rowType: any): string {
    return "linear-gradient(" + rowType.color + ", " + rowType.color + "), url('" + this.img + "')";
  }

}
