import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { NavController, NavParams } from "ionic-angular";

import { Calculation, CalculationRowType, CalculationRowUnit, UserDefinedRow, CalculatorLogic, EnumerationsLogic, Numeric, CalcRowsLogic } from "@salaxy/core";
import { SalaxyContext, ComponentDoneEvent } from "@salaxy/ng";

import { MasterDetailService } from "../../master-detail/master-detail.service";
import { CalcUsecaseComponent } from "../usecases/calc-usecase";

/** Page for editing a single calculation row */
@Component({
  selector: "sxy-calc-row",
  templateUrl: "calc-row.html",
})
export class CalcRowComponent implements OnInit {

  /** Row that is being edited */
  @Input() public row: UserDefinedRow;

  /**
   * Called when the user does a final action: OK / Cancel / Delete
   */
  @Output() public done = new EventEmitter<ComponentDoneEvent<UserDefinedRow>>();

  /** If true, shows the usecase UI instead of the row UI. */
  protected showUsecase = false;

  private _calc: Calculation;

  /** Creates a new instanc of CalcRowPage */
  constructor(
    protected context: SalaxyContext,
    private navParams: NavParams,
  ) {
    if (navParams.get("row")) {
      this.row = navParams.get("row");
    }
    if (navParams.get("done")) {
      this.done.subscribe(navParams.get("done"));
    }
  }

  /** Initializes the component */
  public ngOnInit(): void {
    if (this.type) {
      if (!this.row.unit) {
        // HACK: This may not work correctly in all cases.
        this.row.unit = this.rowConfig.amount.unit as any;
      }
      if (!this.row.count) {
        if (this.rowConfig.amount.default !== 1) {
          this.row.count = this.rowConfig.amount.default;
        } else if (this.row.unit === CalculationRowUnit.Days) {
          this.row.count = this.calc.framework.numberOfDays;
        } else {
          // Default is 1
          this.row.count = this.rowConfig.amount.default;
        }
      }
      if (!this.row.price) {
        this.row.price = this.rowConfig.price.default;
      }
    }
  }

  /**
   * Current calculation to which the row being edited belongs
   */
  @Input()
  public get calc(): Calculation {
    return this._calc || this.context.calculations.current;
  }
  public set calc(value) {
    this._calc = value;
  }

  /** Type enumeration for the row */
  public get type() {
    if (!this.row) {
      throw new Error("Row not set. CalcRowComponent requires parameter 'row'. Set it either using CalcRowComponent.row property or navParam['row'].");
    }
    return this.row.rowType;
  }

  /** Automatically calculated total */
  public get total(): number {
    return this.row.count * this.row.price;
  }

  /** Returns true, if this is a new row (not added to the rows collection yet). */
  public get isNew(): boolean {
    return this.row.rowIndex == null && !(this.row as any).userRowIndex;
  }

  /** Returns true if the row is read-only => must be edited in the Use-case view */
  public get isReadOnly(): boolean {
    return this.row.rowIndex == null && (this.row as any).userRowIndex;
  }

  /** Gets the configuration of the row (the row metadata). */
  public get rowConfig(): any {
    // TODO: Move to initialization
    return CalcRowsLogic.getRowConfig(this.type);
  }

  /**
   * Gets or sets taking into consideration that the count may be a percent => Should be shown multiplied by 100.
   * Does rounding as well
   */
  public get count(): number | null {
    if (!this.row.count) {
      return null;
    }
    if (this.rowConfig.amount.unit === "percent") {
      return Numeric.round(this.row.count * 100);
    }
    return Numeric.round(this.row.count);
  }
  public set count(value) {
    if (this.rowConfig.amount.unit === "percent") {
      this.row.count = Numeric.round(value / 100, 4);
    } else {
      this.row.count = value;
    }
  }

  /** Gets the text for the sub title */
  public get subTitle(): string {
    if (this.rowConfig.amount.unit === "percent") {
      return ((this.row.count || 1) * 100) + "% x " + (this.row.price || 0) + "€";
    }
    return (this.row.count || 1) + this.getUnitMarker() + " x " + (this.row.price || 0) + "€";
  }

  /** Delete the current row */
  public delete(): void {
    this.calc.rows.splice(this.row.rowIndex, 1);
    this.context.calculations.recalculateCurrent();
    this.done.emit({
      action: "delete",
      data: this.row,
    });
  }

  /** Go to a usecase page */
  public goToUsecase(): void {
    this.showUsecase = true;
  }

  /** When Usecase component says that it is done (no recalculation). */
  public usecaseComponentDone(): void {
    this.done.emit({
      action: "ok",
      data: this.row,
    });
  }

  /** Commits the calculation update. */
  public commit(): void {
    if (this.isNew) {
      // New row => Add it
      this.calc.rows.push(this.row);
    }
    this.context.calculations.recalculateCurrent();
    this.done.emit({
      action: "ok",
      data: this.row,
    });
  }

  /** Gets a shor marker element for row unit */
  protected getUnitMarker(): string {
    // TODO Translate unit metrics, but this may already come from Mattia from Core enums...
    switch (this.rowConfig.amount.unit) {
      case "kilometers" as any:
      case "km" as any:
        return "km";
      case "days":
        return "pv";
      case "percent":
        return "%";
      case "count":
        return "kpl";
      default:
        return "";
    }
  }
}
