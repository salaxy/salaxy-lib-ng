import { Component, Input, Optional, Inject, Self } from "@angular/core";
import { NG_VALIDATORS, NG_ASYNC_VALIDATORS, NgControl } from "@angular/forms";

import { CalculatorLogic } from "@salaxy/core";
import { ElementBase, NgTranslations, SalaxyContext } from "@salaxy/ng";

import { SxyUiHelpers } from "../../services/index";

/**
 * Specialized input control for editing a calculation period (Period when the work is done).
 *
 * NOTE: Currently this is not a real form control / it does not yet support [(ngModel)].
 * However, this support is planned for the future as we need to check for required etc.
 *
 * <example-url>/#/examples/InputPeriodExample</example-url>
 *
 * @example
 * <sxy-input-period label="Palkka ajalta (työpäivien määrä)" required [(ngModel)]="periodText" name="period"></sxy-input-period>
 */
@Component({
  selector: "sxy-input-period",
  templateUrl: "./input-period.html",
})
export class InputPeriodComponent extends ElementBase<string> {

  /** Readonly mode */
  @Input() public readonly: boolean | void = false;

  /**
   * Default contstructor creates a new Form control component.
   * @param validators - Synchronous validators
   * @param asyncValidators - Asynchronous validators
   * @param ngControl - NgControl if one is bound to this component
   * @param translations - Translations service
   */
  constructor(
    @Optional() @Inject(NG_VALIDATORS) validators: any[],
    @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: any[],
    @Optional() @Self() ngControl: NgControl,
    translations: NgTranslations,
    private uiHelpers: SxyUiHelpers,
    private context: SalaxyContext,
  ) {
    super(validators, asyncValidators, ngControl, translations);
  }

  /** Open the Salaxy-style confirmation modal to dismiss calculation and
   * go back to the home page
   */
  public showPeriodEdit() {
    this.uiHelpers.showSetPeriod(this.context.calculations.current)
        .then(() => {
          // TODO: Binding to a calculation through ngModel
          const calc = this.context.calculations.current;
          if (calc.info.workStartDate && calc.info.workEndDate && calc.framework.numberOfDays) {
            this.value = this.getDatesSummary();
          } else {
            this.value = null;
          }
        });
  }

  /** Gets the dates summary text */
  public getDatesSummary() {
    // TODO: Binding to a calculation through ngModel
    return CalculatorLogic.getDatesSummary(this.context.calculations.current);
  }
}
