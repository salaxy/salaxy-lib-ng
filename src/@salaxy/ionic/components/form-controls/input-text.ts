import { Component, Input, Optional, Inject, Self } from "@angular/core";
import { NG_VALIDATORS, NG_ASYNC_VALIDATORS, NgControl } from "@angular/forms";

import { ElementBase, NgTranslations } from "@salaxy/ng";

/**
 * Specialized input control for text editing with auto validation.
 *
 * <example-url>/#/examples/InputTextExample</example-url>
 *
 * @example
 * <sxy-input-text label="Label here" name="message" class="emphases" required [(ngModel)]="message"></sxy-input-text>
 * <sxy-input-text readonly="true" label="Am only read" name="readmeonly" [(ngModel)]="readmeonly"></sxy-input-text>
 */
@Component({
  selector: "sxy-input-text",
  templateUrl: "./input-text.html",
})
export class InputTextComponent extends ElementBase<string> {

  /** Readonly mode */
  @Input() public readonly: boolean | void = false;

  /**
   * Default contstructor creates a new Form control component.
   * @param validators - Synchronous validators
   * @param asyncValidators - Asynchronous validators
   * @param ngControl - NgControl if one is bound to this component
   * @param translations - Translations service
   */
  constructor(
    @Optional() @Inject(NG_VALIDATORS) validators: any[],
    @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: any[],
    @Optional() @Self() ngControl: NgControl,
    translations: NgTranslations,
  ) {
    super(validators, asyncValidators, ngControl, translations);
  }
}
