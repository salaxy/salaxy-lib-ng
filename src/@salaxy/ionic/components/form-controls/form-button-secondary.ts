import { Component, Input } from "@angular/core";
import { FormButtonComponent } from "./form-button";

/**
 * Shows a secondary styled form button: Submit, Reset or Button.
 *
 * <example-url>/#/examples/FormButtonSecondaryExample</example-url>
 *
 * @example
 * <sxy-form-button-secondary type="button" label="SALAXY.UI_TERMS.delete_item" icon="ios-trash-outline" label="Delete item" [form]="currentForm" (click)="onItemDelete()"></sxy-form-button-secondary>
 * <sxy-form-button-secondary type="button" label="SALAXY.UI_TERMS.delete_item" color="danger" [form]="currentForm" icon="remove" [label]="discernButtonLabel()" (click)="removeItem()"></sxy-form-button-secondary>
 */
@Component({
  selector: "sxy-form-button-secondary",
  templateUrl: "./form-button-secondary.html",
})
export class FormButtonSecondaryComponent extends FormButtonComponent {}
