import { Component, Input, Optional, Inject, Self } from "@angular/core";
import { NG_VALIDATORS, NG_ASYNC_VALIDATORS, NgControl } from "@angular/forms";

import { ElementBase, NgTranslations } from "@salaxy/ng";

/**
 * Specialized input control for number editing with auto validation.
 *
 * <example-url>/#/examples/InputNumberExample</example-url>
 *
 * @example
 * <sxy-input-number label="Label here" name="price" required [(ngModel)]="price"></sxy-input-number>
 */
@Component({
  selector: "sxy-input-number",
  templateUrl: "./input-number.html",
})
export class InputNumberComponent extends ElementBase<number> {

  /** Readonly mode */
  @Input() public readonly: boolean | void = false;

  /** Minimum value, Default is 0 */
  @Input() public min = 0;

  /** Maximum value, Default is 100 000 */
  @Input() public max = 100000;

  /** Step for up / down buttons, default is 1 */
  @Input() public step = 1;

  /**
   * Default contstructor creates a new Form control component.
   * @param validators - Synchronous validators
   * @param asyncValidators - Asynchronous validators
   * @param ngControl - NgControl if one is bound to this component
   * @param translations - Translations service
   */
  constructor(
    @Optional() @Inject(NG_VALIDATORS) validators: any[],
    @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: any[],
    @Optional() @Self() ngControl: NgControl,
    translations: NgTranslations,
  ) {
    super(validators, asyncValidators, ngControl, translations);
  }

  /** Increment field value */
  public increment() {
    let newVal = this.getInitialValue();
    newVal = newVal + Number(this.step);
    newVal = this.fixNumber(newVal);
    this.value = this.assureMinMax(newVal);
  }

  /** Decrement field value */
  public decrement() {
    let newVal = this.getInitialValue();
    newVal = newVal - Number(this.step);
    newVal = this.fixNumber(newVal);
    this.value = this.assureMinMax(newVal);
  }

  private getInitialValue(): number {
    let newVal = Number(this.value);
    if (!newVal) {
      newVal = (Number(this.min) < 0) ? 0 : Number(this.min);
    }
    return this.assureMinMax(newVal);
  }

  private assureMinMax(numberToCheck: number) {
    if (numberToCheck < Number(this.min)) {
      return Number(this.min);
    }
    if (numberToCheck > Number(this.max)) {
      return Number(this.max);
    }
    return numberToCheck;
  }

  private fixNumber(f: number) {
    // Fix trailing .999999X or .000000X
    return parseFloat(f.toFixed(10));
  }
}
