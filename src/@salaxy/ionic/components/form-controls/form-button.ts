import { Component, Input } from "@angular/core";
import { NgForm } from "@angular/forms";

/**
 * Shows a form button: Submit, Reset or Button.
 *
 * <example-url>/#/examples/FormButtonExample</example-url>
 *
 * @example
 * <sxy-form-button type="submit" label="SALAXY.UI_TERMS.add_item" [form]="currentForm" icon="add" [label]="discernButtonLabel()" (click)="addItem()"></sxy-form-button>
 * <sxy-form-button type="button" label="SALAXY.UI_TERMS.remove_item" color="danger" [form]="currentForm" icon="remove" [label]="discernButtonLabel()" (click)="removeItem()"></sxy-form-button>
 *
 */
@Component({
  selector: "sxy-form-button",
  templateUrl: "./form-button.html"
})
export class FormButtonComponent {

  /**
   * If true, the button is disabled.
   * NOTE: Using NgForm is in most cases easier way of performing the same task. If form is set, it will override this value.
   * @example
   * <form (ngSubmit)="submit()" #currentForm="ngForm">
   * ...
   * <sxy-form-button label="SALAXY.UI_TERMS.recalculate" [disabled]="!currentForm.form.valid"></sxy-form-button>
   */
  @Input() public disabled: boolean = false;

  /**
   * Reference to the parent form.
   * Easier way of setting the disabled property. Potentially will also affect other behaviors in the future.
   * @example
   * <form (ngSubmit)="submit()" #currentForm="ngForm">
   * ...
   * <sxy-form-button label="SALAXY.UI_TERMS.recalculate" [form]="currentForm"></sxy-form-button>
   */
  @Input() public form: NgForm;

  /** Label to show in the button */
  @Input() public label: string;

  /** Icon name to show in the button */
  @Input() public icon: string | void;

  /**
   * Type of the button. Submit is the default.
   * If form property is defined, submit and reset will be directed to Angular.
   * If not, default HTML form functionality is used (typically not what is wanted for submit / reset).
   */
  @Input() public type: "submit" | "button" | "reset";

  /** Color of the button, default is "primary" */
  @Input() public color: string = "primary";

  /** Handles the button click for submit and reset. */
  public click() {
    if (this.form) {
      if (this.type == "submit") {
        this.form.ngSubmit.emit();
        return;
      }
      if (this.type == "reset") {
        this.form.reset();
        return;
      }
    }
  }

  /** Gets the disabled value either based on form or disabled property. */
  protected getDisabled() {
    if (this.form) {
      if (this.type == "reset") {
        return !this.form.dirty;
      }
      if (!this.type || this.type == "submit") {
        return !this.form.valid;
      }
    }
    return this.disabled;
  }
}
