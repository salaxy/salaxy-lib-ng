import { Component, Input, Optional, Inject, Self } from "@angular/core";
import { NG_VALIDATORS, NG_ASYNC_VALIDATORS, NgControl } from "@angular/forms";

import { ElementBase, NgTranslations } from "@salaxy/ng";

/**
 * Specialized input control for email.
 *
 * <example-url>/#/examples/InputEmailExample</example-url>
 */
@Component({
    selector: "sxy-input-email",
    template: `
      <ion-item [ngClass]="getInputStateClasses()">
        <ion-label *ngIf="labelType == 'floating'" floating [attr.for]="id + '_inner'">{{ labelText }}</ion-label>
        <ion-label *ngIf="labelType == 'stacked'" stacked [attr.for]="id + '_inner'">{{ labelText }}</ion-label>
        <ion-label *ngIf="labelType == 'fixed'" fixed [attr.for]="id + '_inner'">{{ labelText }}</ion-label>
        <ion-input
          type="email"
          readonly="{{ readonly }}"
          [(ngModel)]="value"
          [placeholder]="placeholderText"
          [id]="id + '_inner'"
          (ionBlur)="touch()"
          [disabled]="getIsDisabled()"
          ></ion-input>
      </ion-item>
      <span class="sxy-md-last-item-fix"></span>
      <sxy-validation
        *ngIf="model.control.touched && invalid | async"
        [messages]="getMessages() | async">
      </sxy-validation>
    `,
})
export class InputEmailComponent extends ElementBase<string> {

  /** Readonly mode */
  @Input() public readonly: boolean | void = false;

  /**
   * Default contstructor creates a new Form control component.
   * @param validators - Synchronous validators
   * @param asyncValidators - Asynchronous validators
   * @param ngControl - NgControl if one is bound to this component
   * @param translations - Translations service
   */
  constructor(
    @Optional() @Inject(NG_VALIDATORS) validators: any[],
    @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: any[],
    @Optional() @Self() ngControl: NgControl,
    translations: NgTranslations,
  ) {
    super(validators, asyncValidators, ngControl, translations);
  }
}
