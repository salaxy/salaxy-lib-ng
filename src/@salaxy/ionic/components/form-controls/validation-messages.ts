import { Component, Input } from "@angular/core";

/**
 * Shows the validation messages automatically within the input.
 *
 * <example-url>/#/examples/ValidationMessagesExample</example-url>
 *
 * @example
 * <sxy-validation *ngIf="model.control.touched && invalid | async" [messages]="getMessages() | async"></sxy-validation>
 */
@Component({
  selector: "sxy-validation",
  template: `
    <ion-item *ngIf="messages && messages.length">
      <p *ngFor="let message of messages">{{ message | sxyTranslate }}</p>
    </ion-item>
  `,
})
export class ValidationMessagesComponent {

  /** Messages that are shown in the validation messages control */
  @Input() public messages: string[];
}
