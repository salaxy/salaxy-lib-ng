import { Component, Input, Optional, Inject, Self } from "@angular/core";
import { NG_VALIDATORS, NG_ASYNC_VALIDATORS, NgControl } from "@angular/forms";

import { Arrays, EnumerationsLogic } from "@salaxy/core";
import { ElementBase, NgTranslations } from "@salaxy/ng";

/**
 * Select control with auto-binding to enumerations, automatic validation messages and integrated label.
 *
 * <example-url>/#/examples/SelectExample</example-url>
 *
 * @example
 * <sxy-select [(ngModel)]="pensionCompany" name="pensionCompany" enum="PensionCompany" label="Company:"></sxy-select>
 */
@Component({
  selector: "sxy-select",
  templateUrl: "./select.html",
})
export class SelectComponent extends ElementBase<string> {

  /**
   * Binds to an enumeration defined by the Salaxy API.
   * Set the name of the enumeration.
   */
  @Input() public enum: string;

  /** Options of the select control as a key-value object. */
  @Input() public options: any;

  /**
   * These values are visible only if they are selected in the data.
   * I.e. after something else is selected, hidden value cannot be selected back.
   * Use for not-selected values ("Please choose...") when you do not want selection reversed
   * or legacy data that is not selectable, but may still exist on the server.
   */
  @Input() public hiddenOptions: string[];

  /**
   * Default contstructor creates a new Form control component.
   * @param validators - Synchronous validators
   * @param asyncValidators - Asynchronous validators
   * @param ngControl - NgControl if one is bound to this component
   * @param translations - Translations service
   */
  constructor(
    @Optional() @Inject(NG_VALIDATORS) validators: any[],
    @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: any[],
    @Optional() @Self() ngControl: NgControl,
    translations: NgTranslations,
  ) {
    super(validators, asyncValidators, ngControl, translations);
  }

  /** Gets the select options as expected by the component template */
  protected getOptions() {
    let allValues = [];
    const selectedValue = this.value;
    if (this.options) {
      allValues = Object.keys(this.options)
        .map((key) => ({value: key, text: this.options[key] }));
    } else if (this.enum) {
      allValues = EnumerationsLogic.getEnumMetadata(this.enum).values
        .map((e) => ({value: e.name, text: e.label }));
    }
    if (this.hiddenOptions) {
      allValues = allValues.filter((option) => {
        const matchingHiddenValue = Arrays.assureArray(this.hiddenOptions).find((hiddenValue) => hiddenValue === option.value);
        // Hidden is shown only if it is currently selected in data.
        return matchingHiddenValue == null || matchingHiddenValue === selectedValue;
      });
    }
    return allValues;
  }
}
