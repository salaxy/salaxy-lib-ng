import { Component, ViewChild, ElementRef, HostBinding, Renderer, Input, OnInit } from "@angular/core";
import { DomSanitizer, SafeHtml } from "@angular/platform-browser";
import { ViewController, NavParams } from "ionic-angular";

import { Barcodes } from "@salaxy/core";

/**
 * Credit Transfer Form for SEPA euro payments.
 *
 * <example-url>/#/examples/CreditTransferExample</example-url>
 *
 * @example
 * <sxy-credit-transfer-form [lang]="fi"></sxy-credit-transfer-form>
 */
@Component({
  selector: "sxy-credit-transfer-form",
  templateUrl: "./credit-transfer-form.html",
})
export class CreditTransferFormComponent implements OnInit {

  @Input() public lang: string = "fi";

  @Input() public bcHidden: boolean = false;

  public recipientIbanMain: string = "FI2212344567890123";

  public refNo: string = "123456789";

  public dueDate: string = "180609"; // YYMMDD

  public paymentAmountInCents: number = 1234567;

  public get bcShown(): boolean { return !this.bcHidden && this.paymentAmountInCents <= 99999999 && this.isFinnishIBAN; }

  public get isFinnishIBAN(): boolean { return this.recipientIbanMain.substring(0, 2).toUpperCase() === "FI"; }

  public get isValidIBAN(): boolean { return true; } // Todo iban validation

  public bcValue: string = "";

  ngOnInit() {
    if (this.bcShown && this.isValidIBAN) {
      this.bcValue = Barcodes.getValue(this.recipientIbanMain, this.refNo, this.dueDate, this.paymentAmountInCents);
    }
  }

}
