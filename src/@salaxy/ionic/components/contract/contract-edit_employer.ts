import { NgTranslations } from '@salaxy/ng';
import { Component, Output, EventEmitter } from "@angular/core";

import { ContractLogic, ContractParty, EmploymentContract } from "@salaxy/core";
import { SalaxyContext, ComponentDoneEvent } from "@salaxy/ng";

import { MasterDetailService } from "../master-detail/master-detail.service";
import { SxyUiHelpers } from "../../services/sxy-ui-helpers";
import { ContactDetailsComponent } from "../contact/contact-details";

/**
 * Viewing, linking and edititing the employer details.
 * If user is in worker role, letting to choose employer from contacts.
 *
 * @example
 * <sxy-contract-edit-employer></sxy-contract-edit-employer>
 */
@Component({
  selector: "sxy-contract-edit-employer",
  templateUrl: "./contract-edit_employer.html",
})
export class ContractEditEmployerComponent {

  /** Gets the current contract */
  public get current(): EmploymentContract { return this.context.contracts.current; }

  /** Called when employer chosen / editing is done. */
  @Output() public done = new EventEmitter<ComponentDoneEvent<any>>();

  /** Gets the employer on current contract */
  public get employer(): ContractParty { return this.context.contracts.current.employer; }

  /** Gets the formatted worker address */
  public get address(): string { return ContractLogic.constructAddressText(this.employer); }

  /** Gets the current session's role */
  public get role(): "worker" | "household" | "company" { return this.context.session.role; }

  constructor(
    protected context: SalaxyContext,
    protected masterDetailSrv: MasterDetailService,
    protected uiHelpers: SxyUiHelpers,
    private translate: NgTranslations,
  ) {}

  /** Called when user selects a contact from list. */
  public itemSelect (contact: ContractParty) {
    if (contact) {
      this.context.contracts.current.employer = contact;
    }
    this.masterDetailSrv.closeDetails();
  }

  /** Called when user click "Create new" in the contact list. */
  public createNewClicked (newBlankContact: ContractParty) {
    this.masterDetailSrv.goToDetailsPage(ContactDetailsComponent, {
      title: "Luo uusi työnantaja",
      editMode: true,
      done: (event: ComponentDoneEvent<ContractParty>) => {
        if (event.action == "ok") {
          this.context.contracts.current.employer = event.data;
        }
        this.masterDetailSrv.closeDetails();
      }
    });
  }

  /** Clears the current employer selection => Shows the selection / "Create new" again */
  public clearEmployerSelection() {
    this.current.employer = {
      avatar: {},
      contact: {},
    };
  }

  /**
   * Updates the contact information from current account information to the current contract.
   * In practice, this is just a Save() - method, the update is done on the server.
   */
  public updateCurrentAccountToContract(): void {
    const loader = this.uiHelpers.showLoading("SALAXY.UI_TERMS.isSaving", "SALAXY.UI_TERMS.pleaseWait");
    this.context.contracts.saveCurrent().then(() => {
      loader.dismiss();
    })
    .catch((err) => {
      loader.dismiss();
    });
  }


  /** Edit the current account data */
  public editCurrentAccount() {
    // TODO: Current account edit not implemented.
    this.uiHelpers.showAlert("TODO: Not implemented.");
  }

  /** Shows the edit view for the contact */
  public editContact(): void {
    this.context.contacts.setCurrent(this.employer);
    this.masterDetailSrv.goToDetailsPage(ContactDetailsComponent, {
      title: this.translate.get("SALAXY.ION.COMPONENTS.ContractEditEmployer.pageTitleEditContactDetails"),
      editMode: true,
      hideDelete: true,
      done: (event: ComponentDoneEvent<ContractParty>) => {
        if (event.action == "ok") {
          this.context.contracts.current.employer = event.data;
        }
        this.masterDetailSrv.closeDetails();
      }
    });
  }
}
