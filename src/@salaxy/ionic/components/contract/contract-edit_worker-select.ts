import { Component, Output, EventEmitter } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";
import { NavController } from "ionic-angular";

import { EmploymentContract, ContractLogic, WorkerAccount, ContractPartyType, ContractPartyLogic } from "@salaxy/core";
import { SalaxyContext, ComponentDoneEvent, NgTranslations } from "@salaxy/ng";

import { MasterDetailService } from "../master-detail/master-detail.service";
import { ContractEditWorkerComponent } from "./contract-edit_worker";

/**
 * Selecting worker for contract.
 *
 * @example
 * <sxy-contract-edit-worker-select></sxy-contract-edit-worker-select>
 */
@Component({
  selector: "sxy-contract-edit-worker-select",
  templateUrl: "./contract-edit_worker-select.html",
})
export class ContractEditWorkerSelectComponent {

  /**
   * Event emitted when employer chosen / OK.
   */
  @Output() public done = new EventEmitter<ComponentDoneEvent<any>>();

  constructor(
    protected context: SalaxyContext,
    protected masterDetailSrv: MasterDetailService,
    private translate: NgTranslations,
  ) {}

  /** Method to be called when a worker has been selected from sxy-worker-list */
  public workerSelect(worker: WorkerAccount): void {
    if (worker) {
      this.context.contracts.current.worker = ContractPartyLogic.getFromWorker(worker);
      this.masterDetailSrv.goToDetailsPage(ContractEditWorkerComponent, { title: this.translate.get("SALAXY.ION.COMPONENTS.ContractEditWorkerSelect.chosenWorker") });
    }
  }

  /** Emits component done event with empty data */
  public editDone(): void {
    this.done.emit({ action: "ok", data: {} });
    this.masterDetailSrv.closeDetails();
  }
}
