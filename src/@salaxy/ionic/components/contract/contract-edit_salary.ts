import { Component, Output, EventEmitter } from "@angular/core";

import { Dates, ContractLogic, ContractSalary, EmploymentContract, EmployerType, EmploymentType, EmploymentSalaryType } from "@salaxy/core";
import { SalaxyContext, ComponentDoneEvent, NgTranslations } from "@salaxy/ng";

import { MasterDetailService } from "../master-detail/master-detail.service";
import { SxyUiHelpers } from "../../services/index";

/**
 * Editing salary related contract details.
 *
 * @example
 * <sxy-contract-edit-salary></sxy-contract-edit-salary>
 */
@Component({
  selector: "sxy-contract-edit-salary",
  templateUrl: "./contract-edit_salary.html",
})
export class ContractEditSalaryComponent {

  /** Define EmploymentSalaryType typings as a property to be used on template */
  public employmentType = EmploymentType;

  /** Getter for current contract */
  public get contract(): EmploymentContract { return this.context.contracts.current; }

  /** Getter salary info from the current contract */
  public get salary(): ContractSalary { return this.context.contracts.current.salary; }

  /** Getter for salary formatted date text */
  public get salaryTermDateText(): string {
    return ContractLogic.constructTermDateText(this.salary.workType, this.salary.startDate, this.salary.endDate);
  }

  /** Getter for salary amount per text */
  public get salaryAmountPerText(): string {
    return this.translate.get("SALAXY.ION.COMPONENTS.ContractEditSalary.salaryAmountPer", ContractLogic.constructPricePerText(this.salary.salaryType, ""));
  }

  constructor(
    protected context: SalaxyContext,
    protected masterDetailSrv: MasterDetailService,
    private uiHelpers: SxyUiHelpers,
    private translate: NgTranslations,
  ) {}

  /** Present a calendar modal for a single start date */
  public salaryStartDateCalendar(): void {
    this.uiHelpers
        .showIonCalendarSingle("SALAXY.ION.COMPONENTS.ContractEditSalary.contractStartDate", this.salary.startDate)
        .then((date) => {
          this.salary.startDate = date;
        });
  }

  /** Present a calendar modal for a date range */
  public salaryDateRangeCalendar(): void {
    this.uiHelpers
        .showIonCalendarRange("SALAXY.ION.COMPONENTS.ContractEditSalary.contractDateRange", this.salary.startDate, this.salary.endDate)
        .then((datesArr) => {
          this.salary.startDate = datesArr[0];
          this.salary.endDate = datesArr[1];
        });
  }

  /** Emits component done event with empty data */
  public editDone(): void {
    this.masterDetailSrv.closeDetails();
  }

}
