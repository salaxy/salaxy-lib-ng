import { Component, Output, EventEmitter } from "@angular/core";

import { ContractLogic, ContractSalary, EmploymentContract } from "@salaxy/core";
import { SalaxyContext, ComponentDoneEvent } from "@salaxy/ng";

import { MasterDetailService } from "../master-detail/master-detail.service";

/**
 * Editing work tasks related contract details.
 *
 * @example
 * <sxy-contract-edit-work-tasks></sxy-contract-edit-work-tasks>
 */
@Component({
  selector: "sxy-contract-edit-work-tasks",
  templateUrl: "./contract-edit_work-tasks.html",
})
export class ContractEditWorkTasksComponent {


  /** Getter for current contract */
  public get contract(): EmploymentContract { return this.context.contracts.current; }

  constructor(protected context: SalaxyContext, protected masterDetailSrv: MasterDetailService) {}

  /** Emits component done event with empty data */
  public editDone(): void {
    this.masterDetailSrv.closeDetails();
  }
}
