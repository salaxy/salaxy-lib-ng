import { Component, Output, EventEmitter } from "@angular/core";

import { ContractLogic, ContractParty, ContractSalary, EmploymentContract, EmploymentType, EmploymentSalaryType, EmploymentWorkHoursType } from "@salaxy/core";
import { SalaxyContext, ComponentDoneEvent, NgTranslations } from "@salaxy/ng";

import { SxyUiHelpers } from "../../services";
import { MasterDetailService } from "../master-detail/master-detail.service";
import { ContractEditEmployerComponent } from "./contract-edit_employer";
import { ContractEditWorkerComponent } from "./contract-edit_worker";
import { ContractEditWorkerSelectComponent } from "./contract-edit_worker-select";
import { ContractEditWorkTasksComponent } from "./contract-edit_work-tasks";
import { ContractEditSalaryComponent } from "./contract-edit_salary";
import { ContractEditOtherComponent } from "./contract-edit_other";
import { ContactListComponent } from "../contact/contact-list";

/**
 * Showing an overview of the contract.
 * Individual sub sections are meant to be presented for editing those contract details in-actual.
 *
 * @example
 * <sxy-contract-edit-summary></sxy-contract-edit-summary>
 */
@Component({
  selector: "sxy-contract-edit-summary",
  templateUrl: "./contract-edit-summary.html",
})
export class ContractEditSummaryComponent {

  /** Define EmploymentSalaryType typings as a property to be used on template */
  public employmentType = EmploymentType;

  /** Is this a new draft or an already saved contract? */
  public mode: "draft" | "saved" = "saved";

  /** Component link for view */
  public contactListComponent = ContactListComponent;

  /** Component link for view */
  public contractEditWorkerSelectComponent = ContractEditWorkerSelectComponent;
  /** Component link for view */
  public contractEditWorkTasksComponent = ContractEditWorkTasksComponent;
  /** Component link for view */
  public contractEditSalaryComponent = ContractEditSalaryComponent;
  /** Component link for view */
  public contractEditOtherComponent = ContractEditOtherComponent;

  /** Getter for ContractEditEmployerComponent or ContactListComponent */
  public get employerOrContactComponent(): any { return this.isEmployer || this.isEmployerSet ? ContractEditEmployerComponent : ContactListComponent; }
  /** Getter for contract worker editor page or select list page component depending if worker has been set. */
  public get workerSelectOrEditComponent(): any { return this.isWorkerSet ? ContractEditWorkerComponent : ContractEditWorkerSelectComponent; }

  /** Getter for current contract */
  public get contract(): EmploymentContract { return this.context.contracts.current; }

  /** Getter for employer of current contract */
  public get employer(): ContractParty { return this.context.contracts.current.employer; }
  /** Getter for worker of current contract */
  public get worker(): ContractParty { return this.context.contracts.current.worker; }
  /** Getter for salary info of current contract */
  public get salary(): ContractSalary { return this.context.contracts.current.salary; }

  /** Getter for if current contract worker contact self (session user) */
  public get isWorker(): boolean { return this.worker.isSelf; }
  /** Getter for if current contract employer contact self (session user) */
  public get isEmployer(): boolean { return this.employer.isSelf; }
  /** Getter for if current contract worker been chosen (true if isSelf) */
  public get isWorkerSet(): boolean { return this.contract.worker.isSelf || Object.keys(this.contract.worker.avatar).length > 0; }
  /** Getter for if current contract employer been chosen (true if isSelf) */
  public get isEmployerSet(): boolean { return this.contract.employer.isSelf || Object.keys(this.contract.employer.avatar).length > 0; }

  /** Getter for salary formatted date text */
  public get salaryTermDateText(): string {
    return ContractLogic.constructTermDateText(this.salary.workType, this.salary.startDate, this.salary.endDate);
  }

  /** Getter for salary info of current contract */
  public get salaryWorkHoursText(): string {
    return ContractLogic.constructSalaryWorkHoursText(this.salary.worksHoursType, this.salary.workHours);
  }

  /** Getter for salary amount per text */
  public get salaryAmountPerText(): string {
    return this.translate.get("SALAXY.ION.COMPONENTS.ContractEditSalary.salaryAmountPer", ContractLogic.constructPricePerText(this.salary.salaryType, this.salary.salaryAmount));
  }

  /** Getter for title according to the above method. */
  public get workerSelectOrEditTitle(): string {
    return this.translate.get(
            this.isWorker ? "SALAXY.ION.COMPONENTS.ContractEditSummary.myWorkerDetails"
            : this.isWorkerSet ? "SALAXY.ION.COMPONENTS.ContractEditSummary.chosenWorker" : "SALAXY.ION.COMPONENTS.ContractEditSummary.chooseWorker");
  }

  /**
   * Called when the user wants to edit the Employer.
  */
  public editEmployer() {
    this.masterDetailSrv.goToDetailsPage(ContractEditEmployerComponent, {
      title: this.translate.get("SALAXY.ION.COMPONENTS.ContractEditSummary.chosenEmployerContact"),
    })
  }

  constructor(
    private context: SalaxyContext,
    private uiHelpers: SxyUiHelpers,
    protected masterDetailSrv: MasterDetailService,
    private translate: NgTranslations,
  ) {}

  /**
   * Saves currently active employment contract with a loader.
   */
  public save(): void {
    const loader = this.uiHelpers.showLoading("SALAXY.UI_TERMS.isSaving", "SALAXY.UI_TERMS.pleaseWait");
    this.context.contracts.saveCurrent().then(() => {
      loader.dismiss();
    })
    .catch((err) => {
      loader.dismiss();
    });
  }

  /**
   * Removes currently active employment contract with confirmation.
   */
  public removeContractWithConfirmation(): void {
    this.uiHelpers
        .showConfirm("SALAXY.UI_TERMS.areYouSure", "SALAXY.ION.COMPONENTS.ContractEditSummary.sureToDeleteContract")
        .then(() => {
          const loader = this.uiHelpers.showLoading("SALAXY.UI_TERMS.isDeleting", "SALAXY.UI_TERMS.pleaseWait");
          this.context.contracts.delete(this.contract.id).then(() => {
            loader.dismiss();
          })
          .catch((err) => {
            loader.dismiss();
          });
        });
  }

}
