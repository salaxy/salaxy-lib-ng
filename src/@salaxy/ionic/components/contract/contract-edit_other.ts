import { Component, Output, EventEmitter } from "@angular/core";

import { ContractLogic, EmploymentContract } from "@salaxy/core";
import { SalaxyContext, ComponentDoneEvent } from "@salaxy/ng";

import { MasterDetailService } from "../master-detail/master-detail.service";

/**
 * Editing other contract details: annualLeave, benefitsException, noticeExeption, otherTerms
 *
 * @example
 * <sxy-contract-edit-other></sxy-contract-edit-other>
 */
@Component({
  selector: "sxy-contract-edit-other",
  templateUrl: "./contract-edit_other.html",
})
export class ContractEditOtherComponent {

  /** Getter for current contract */
  public get contract(): EmploymentContract { return this.context.contracts.current; }

  constructor(protected context: SalaxyContext, protected masterDetailSrv: MasterDetailService) {}

  /**
   * Emits component done event with empty data
   */
  public editDone(): void {
    this.masterDetailSrv.closeDetails();
  }
}
