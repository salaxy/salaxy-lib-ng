import { Component, ViewChild, ElementRef, HostBinding, Renderer, Input, OnInit } from "@angular/core";
import { DomSanitizer, SafeHtml } from "@angular/platform-browser";
import { ViewController, NavParams } from "ionic-angular";

import { EmploymentContract } from "@salaxy/core";
import { SalaxyContext, NgTranslations } from "@salaxy/ng";

/**
 * Displays contract as HTML PDF/print-look-alike with autoscaling and zoom buttons features.
 *
 * @example
 * <sxy-contract-preview [content]="myHtmlString"></sxy-contract-preview>
 *
 * @example
 * const previewModal = this.modalCtrl.create(ContractPreviewComponent, { promisedContent: promise }, { cssClass: "preview-A4" });
 * previewModal.onDidDismiss((data) => {
 *   // Could do something
 * });
 * previewModal.present();
 */
@Component({
  selector: "sxy-contract-preview",
  templateUrl: "./contract-preview.html",
  host: {
    "(window:resize)": "onResize($event)"
  },
})
export class ContractPreviewComponent implements OnInit {

  /** Container element ref */
  @ViewChild("contractContainer") public contractContainer: ElementRef;

  /** Binding to modal's transform style (style="transform: xxx") */
  @HostBinding("style.transform") public transform: string;

  /** Zoom factor for adjusting the transform. Default 1 = 100%. */
  @Input() public zoomFactor: number = 1;

  /** Zoom factor upper limit */
  @Input() public zoomFactorLimitUpper: number = 3;

  /** Preview HTML (Sanitized). */
  public previewHTML: SafeHtml;

  /** If true, the content has loaded. */
  public hasLoaded: boolean = false;

  private zoomFactorLimitBottom: number = 1;
  private zoomFactorIncrement: number = 0.25;

  /**
   * Breakpoint, as 210mm as converted to fixed px value.
   * Is used as the divider to calculate transform scale factor.
   */
  private documentMaxWidth: number = 794;

  /** Content HTML to show in the preview window.  */
  @Input() public content: Promise<string> | string;

  /** Getters for for elements and their dimensions */
  private get hostElem(): number { return this.hostContainer.nativeElement; }
  private get contElem(): number { return this.contractContainer.nativeElement; }
  private get hostWidth(): number { return this.hostContainer.nativeElement.clientWidth; }
  private get contWidth(): number { return this.contractContainer.nativeElement.clientWidth; }
  private get contHeight(): number { return this.contractContainer.nativeElement.clientHeight; }

  constructor(
    protected sanitizer: DomSanitizer,
    private renderer: Renderer,
    private hostContainer: ElementRef,
    private viewCtrl: ViewController,
    private navParams: NavParams,
    private translate: NgTranslations,
  ) {
    if (navParams.get("content")) {
      this.content = navParams.get("content");
    }
  }

  ngOnInit(): void {
    if (this.content) {
      if ((this.content as any).then) {
        (this.content as Promise<string>).then(htmlString => {
          this.loadContent(htmlString);
        })
        .catch(e => {
          this.loadContent("<h2 text-center style='margin-top: 80px'>" + this.translate.get("SALAXY.NG.ERRORS.defaultDescription") + "</h2>");
        });
      } else {
        this.loadContent(this.content as string);
      }
    }
  }

  /** Close modal */
  public dismiss(): void {
    this.viewCtrl.dismiss();
  }

  /** Zoom in / out */
  public zoom(direction: "in" | "out"): void {
    if (direction === "in" && this.zoomFactor < this.zoomFactorLimitUpper) {
      this.zoomFactor += this.zoomFactorIncrement;
      this.updateScale(this.calcScale(this.hostWidth));
    } else if (direction === "out" && this.zoomFactor > this.zoomFactorLimitBottom) {
      this.zoomFactor -= this.zoomFactorIncrement;
      this.updateScale(this.calcScale(this.hostWidth));
    }
  }

  private calcScale(currentWidth: number): number {
    return currentWidth / this.documentMaxWidth;
  }

  private calcScaledHeight(scale: number): number {
    return Math.ceil(this.contHeight * scale) + 20;
  }

  private onResize(event): void {
    if (this.hostContainer && this.contractContainer) {
      const width = event.target.innerWidth;
      if (width > this.documentMaxWidth && this.zoomFactor > 1) {
        // Reset user zoomed factor when the client width has broadened
        this.zoomFactor = 1;
        this.updateScale(this.calcScale(width));
      } else {
        const scale = this.calcScale(width);
        if (scale <= 1) {
          this.updateScale(scale);
        }
      }
    }
  }

  private updateInitialScale(): void {
    const scale = this.calcScale(Math.min(this.hostWidth, this.contWidth));
    this.updateScale((scale > 1) ? 1 : scale);
    this.renderer.setElementClass(this.hostElem, "has-init", true);
  }

  private updateScale(scale: number, updateHeight: boolean = true): void {
    scale = scale * this.zoomFactor;
    this.renderer.setElementStyle(this.contElem, "transform", "translate(0) scale(" + scale + ")");
    if (updateHeight) {
      this.updateScaledHeight(this.calcScaledHeight(scale));
    }
  }

  private updateScaledHeight(height: number): void {
    this.renderer.setElementStyle(this.contElem, "height", height + "px");
  }

  private loadContent(html: string): void {
    this.previewHTML = this.sanitizer.bypassSecurityTrustHtml(html);
    setTimeout(() => {
      this.updateInitialScale();
      this.hasLoaded = true;
    }, 10); // Give a bit of machine cycles for the html to be initialized
  }

}
