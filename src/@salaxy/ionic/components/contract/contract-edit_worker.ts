import { Component, Output, EventEmitter } from "@angular/core";

import * as fastDeepEqualImport from "fast-deep-equal";
/** fastDeepEqual variable */
const fastDeepEqual = fastDeepEqualImport;

import { ContractLogic, ContractParty, PersonAccount } from "@salaxy/core";
import { SalaxyContext, ComponentDoneEvent, NgTranslations } from "@salaxy/ng";

import { ContractEditWorkerSelectComponent } from "./contract-edit_worker-select";
import { ContactDetailsComponent } from "../contact/contact-details";
import { ContractEditEmployerComponent } from "./contract-edit_employer";
import { MasterDetailService } from "../master-detail/master-detail.service";
import { SxyUiHelpers } from "./../../services/sxy-ui-helpers";

/**
 * Viewing, linking and edititing the worker details.
 * If user is in worker role, letting to choose employer from contacts.
 *
 * @example
 * <sxy-contract-edit-worker></sxy-contract-edit-worker>
 */
@Component({
  selector: "sxy-contract-edit-worker",
  templateUrl: "./contract-edit_worker.html",
})
export class ContractEditWorkerComponent {

  /**
   * Event emitted when object chosen / OK.
   */
  @Output() public done: EventEmitter<ComponentDoneEvent<any>> = new EventEmitter<ComponentDoneEvent<any>>();

  /** Getter for worker object in current contract */
  public get worker(): ContractParty { return this.context.contracts.current.worker; }

  /** Getter for current session's person account as the worker account */
  public get currentWorkerAccount(): PersonAccount { return this.context.session.personAccount; }

  /** Getter for formatted worker address */
  public get address(): string { return ContractLogic.constructAddressText(this.worker); }

  constructor(
    protected context: SalaxyContext,
    protected masterDetailSrv: MasterDetailService,
    protected uiHelpers: SxyUiHelpers,
    private translate: NgTranslations,
  ) {}

  /** Load worker list in MD Detail View */
  public goToWorkerList(): void {
    this.masterDetailSrv.goToDetailsPage(ContractEditWorkerSelectComponent, {
      title: this.translate.get("SALAXY.ION.COMPONENTS.ContractEditWorker.chooseWorker")
    });
  }

  /**
   * Edit own contact details as an Worker.
   * @todo Test this
   */
  public editMyDetails(): void {
    this.context.contacts.setCurrent(this.worker);
    this.masterDetailSrv.goToDetailsPage(ContactDetailsComponent, {
      title: this.translate.get("SALAXY.ION.COMPONENTS.ContractEditWorker.editMyDetails"),
      editMode: true,
    });
  }

  /**
   * Emits component done event with empty data
   */
  public editDone(): void {
    this.done.emit({ action: "ok", data: {} });
    this.masterDetailSrv.closeDetails();
  }

  /**
   * Showing a button for updating my changed contact details as an employer.
   */
  public shownUpdateChangedContactDetails(): boolean {
    return this.worker.isSelf && !this.isMyContactDetailsUpToDate();
  }

  /**
   * Check if employer details differ from current session's contact details (when in employer role).
   */
  public isMyContactDetailsUpToDate(): boolean {
    return fastDeepEqual(this.worker.contact, this.currentWorkerAccount.contact);
  }

  /**
   * As in Employer Role, when contract has outdated contact details for me, I can update those (button shown conditionally)
   * @todo Test this
   */
  public updateContactDetailsToContract(): void {
    const loader = this.uiHelpers.showLoading("SALAXY.UI_TERMS.isSaving", "SALAXY.UI_TERMS.pleaseWait");
    this.context.contracts.saveCurrent().then(() => {
      loader.dismiss();
    })
    .catch((err) => {
      loader.dismiss();
    });
  }
}
