import { Component, Input, EventEmitter, Output } from "@angular/core";
import { NavController, NavParams } from "ionic-angular";
import { Page } from "ionic-angular/navigation/nav-util";

import { EmploymentContract, ContractLogic } from "@salaxy/core";
import { NgContractListComponent as SalaxyContractListComponent, SalaxyContext, ComponentDoneEvent } from "@salaxy/ng";

import { MasterDetailService } from "../master-detail/master-detail.service";

/**
 * Shows a simple list of contracts.
 *
 * <example-url>/#/examples/ContractListExample</example-url>
 *
 * @example
 * <sxy-contract-list [itemNavPush]="ContractEditMainComponent" [navParams]="{}" limitTo="10" filterBySelfRole="{{ role }}" (itemSelect)="onContractSelect()></sxy-contract-list>
 */
@Component({
  selector: "sxy-contract-list",
  templateUrl: "./contract-list.html",
})
export class ContractListComponent extends SalaxyContractListComponent {

  /**
   * Called when the user clicks the "Create new" button.
   * Before the call, a new item is createad and set to current (unless skipCurrentItemSet=true)
   * and navigation to itemNavPush/itemSxyDetailPush has finalized.
   */
  @Output() public createNewClicked = new EventEmitter<EmploymentContract>();

  /**
   * Adds ionic navigation push functionality to each item.
   * Basically the same as adding navPush to each item (https://ionicframework.com/docs/api/components/nav/NavPush/).
   * Also supports navParams.
   * If navParams is not null, also the calculation is added to the navParams as "contract".
   */
  @Input() public itemNavPush: string | Page;

  /**
   * Adds Salaxy Master-detail Detail-push functionality to each item.
   * Basically the same as adding sxyDetailPush to each item.
   * Also supports navParams.
   * If navParams is not null, also the contract is added to the navParams as "contract".
   */
  @Input() public itemSxyDetailPush: string | Page;

  /**
   * If not null, will be sent together with either itemNavPush or itemSxyDetailPush functionality.
   * In this case, also the contract is added to the navParams as "contract".
   */
  @Input("navParams") public navParamsData: any;

  /**
   * If set to to true, hides the "Add New" button on top of the listing.
   * The button is shown by default and creates a new blank contract in contracts context and sets it as current.
   * For discerning if a contract is an unsaved draft, it's id is null.
   */
  @Input() public hideCreateButton: boolean = false;

  /**
   * If false (default), when user sets the contract, it is automatically set as the current contract.
   * If true, the current contract is not set - only itemSelect events are fired.
   */
  @Input() public skipCurrentItemSet = false;

  constructor(
    protected context: SalaxyContext,
    private masterDetailSrv: MasterDetailService,
    private navCtrl: NavController,
    navParams: NavParams,
  ) {
    super(context);

    if (navParams.get("itemSelect")) {
      this.itemSelect.subscribe(navParams.get("itemSelect"));
    }
    if (navParams.get("hideCreateButton")) {
      this.hideCreateButton = navParams.get("hideCreateButton");
    }
    if (navParams.get("groupBy")) {
      this.groupBy = navParams.get("groupBy");
    }
    if (navParams.get("limitTo")) {
      this.limitTo = navParams.get("limitTo");
    }
    if (navParams.get("filterBySelfRole")) {
      this.filterBySelfRole = navParams.get("filterBySelfRole");
    }
  }

  /**
   * Accessor for ContractLogic's constructPricePerText method.
   */
  public constructPricePerText(amount: any, type: any): string {
    return ContractLogic.constructPricePerText(amount, type);
  }

  /**
   * Create a new contract. The functionality is similar to select():
   * If itemNavPush or itemSxyDetailPush is defined, uses the same method does the navigation
   * with the same navParams except that editMode=true, except if explicitly defined.
   * Naturally, emits the createNewClicked event instead of itemSelect event.
   */
  public createNew(): void {
    const newItem = this.context.contracts.getBlank();
    if (!this.skipCurrentItemSet) {
      this.context.contacts.setCurrent(newItem);
    }
    const createNewNavParams = Object.assign({ contract: newItem }, this.navParamsData);
    if (this.itemNavPush) {
      this.navCtrl.push(this.itemNavPush, createNewNavParams)
        .then((value) => {this.createNewClicked.emit(newItem)});
    }
    else if (this.itemSxyDetailPush) {
      this.masterDetailSrv.goToDetailsPage(this.itemSxyDetailPush, createNewNavParams)
        .then((value) => {this.createNewClicked.emit(newItem)});
    } else {
      this.createNewClicked.emit(newItem)
    }
  }

  /**
   * Selects a single contract: Sets it as current (unless skipCurrentItemSet is set)
   * and navigates according to itemNavPush or itemSxyDetailPush if one is defined (potentially adding navParams).
   * After potential navigation has finalized emits itemSelect event.
   */
  public select(contract: EmploymentContract): void {
    if (!this.skipCurrentItemSet) {
      this.context.contracts.setCurrent(contract);
    }
    if (this.navParamsData != null) {
      this.navParamsData.contract = contract; // This is always overriden when going further => no need for Object.assign().
    }
    if (this.itemNavPush) {
      this.navCtrl.push(this.itemNavPush, this.navParamsData)
        .then((value) => { this.itemSelect.emit(contract) });
    }
    else if (this.itemSxyDetailPush) {
      this.masterDetailSrv.goToDetailsPage(this.itemSxyDetailPush, this.navParamsData)
        .then((value) => { this.itemSelect.emit(contract) });
    } else {
      this.itemSelect.emit(contract);
    }
  }
}
