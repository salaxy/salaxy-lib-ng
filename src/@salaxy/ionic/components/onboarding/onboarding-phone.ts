import { Component } from "@angular/core";
import { NavParams } from "ionic-angular";

import { NgOnboardingBaseComponent, OnboardingService, NgTranslations } from "@salaxy/ng";

import { SxyUiHelpers } from "../../services/index";


/**
 * User interface for editing the phone number in the onboarding process.
 *
 * <example-url>/#/examples/OnboardingEmbeddedExample</example-url>
 *
 * @example <sxy-onboarding-phone (done)="next()"></sxy-onboarding-phone>
 *
 */
@Component({
  selector: "sxy-onboarding-phone",
  templateUrl: "./onboarding-phone.html",
})
export class OnboardingPhoneComponent extends NgOnboardingBaseComponent {

  constructor(private navParams: NavParams, private onboardingService: OnboardingService, private sxyUiHelpers: SxyUiHelpers, private translations: NgTranslations) {
    super(onboardingService);
  }

  /** Run when the component is initialized. */
  public ngOnInit() {
    const id = this.navParams.get("id");
    if (id && (!this.current || !this.current.id)) {
      const loader = this.sxyUiHelpers.showLoading("SALAXY.UI_TERMS.pleaseWait");
      this.onboardingService.loadSingle(id).then(() => {
        loader.dismiss();
      })
      .catch((err) => {
        loader.dismiss();
      });
    }
  }

  /** Saves current onboarding. */
  public next() {
    const loader = this.sxyUiHelpers.showLoading("SALAXY.UI_TERMS.pleaseWait");
    this.onboardingService.sendSmsVerificationPin().then(() => {
      super.next();
      loader.dismiss();
    })
    .catch((err) => {
      loader.dismiss();
    });
  }
}
