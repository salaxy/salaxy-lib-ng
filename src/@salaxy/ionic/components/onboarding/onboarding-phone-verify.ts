import { Component, ViewChild } from "@angular/core";
import { NgForm, ValidationErrors } from "@angular/forms";
import { NavParams } from "ionic-angular";

import { NgOnboardingBaseComponent, OnboardingService, NgTranslations } from "@salaxy/ng";

import { SxyUiHelpers } from "../../services/index";


/**
 * User interface for verifying the phone number in the onboarding process.
 *
 * <example-url>/#/examples/OnboardingEmbeddedExample</example-url>
 *
 * @example <sxy-onboarding-phone-verify (done)="next()"></sxy-onboarding-phone-verify>
 *
 */
@Component({
  selector: "sxy-onboarding-phone-verify",
  templateUrl: "./onboarding-phone-verify.html",
})
export class OnboardingPhoneVerifyComponent extends NgOnboardingBaseComponent {

  @ViewChild("currentForm") private currentForm: NgForm;

  constructor(private navParams: NavParams, private onboardingService: OnboardingService, private sxyUiHelpers: SxyUiHelpers, private translations: NgTranslations) {
    super(onboardingService);
  }

  /** Run when the component is initialized. */
  public ngOnInit() {
    const id = this.navParams.get("id");
    if (id && (!this.current || !this.current.id)) {
      const loader = this.sxyUiHelpers.showLoading("SALAXY.UI_TERMS.pleaseWait");
      this.onboardingService.loadSingle(id).then(() => {
        loader.dismiss();
      })
      .catch((err) => {
        loader.dismiss();
      });
    }
  }

  /** Shows error */
  protected getErrorMsg(): string {
    // tslint:disable-next-line
    const control = this.currentForm.controls["phoneVerificationPin"];
    if (!control) {
      return;
    }
    const error = control.getError("sxyManual");
    if (error) {
      return error;
    }
    return null;
  }

  /** Verifies the pin before going to next step. */
  public next() {
    const loader = this.sxyUiHelpers.showLoading("SALAXY.UI_TERMS.pleaseWait");
    this.onboardingService.saveCurrent().then(() => {
      if (this.current.validation) {
        const error = this.getValidationError("Person.ContactVerification.Telephone.Pin");
        if (!error) {
          super.next();
        } else {
          // tslint:disable-next-line
          const control = this.currentForm.controls["phoneVerificationPin"];
          control.setErrors({ sxyManual: error.msg });
        }
      }
      loader.dismiss();
    })
    .catch((err) => {
      loader.dismiss();
    });
  }
}
