import { Component } from "@angular/core";
import { NavParams } from "ionic-angular";

import { NgOnboardingBaseComponent, OnboardingService, NgTranslations } from "@salaxy/ng";

import { SxyUiHelpers } from "../../services/index";

/**
 * User interface for editing basic information for person: Name and E-mail.
 *
 * <example-url>/#/examples/OnboardingEmbeddedExample</example-url>
 *
 * @example <sxy-onboarding-person (done)="next()"></sxy-onboarding-person>
 *
 */
@Component({
  selector: "sxy-onboarding-person",
  templateUrl: "./onboarding-person.html",
})
export class OnboardingPersonComponent extends NgOnboardingBaseComponent {

  constructor(private navParams: NavParams, private onboardingService: OnboardingService, private sxyUiHelpers: SxyUiHelpers, private translations: NgTranslations) {
    super(onboardingService);
  }

  /** Run when the component is initialized. */
  public ngOnInit() {
    const id = this.navParams.get("id");
    if (id && (!this.current || !this.current.id)) {
      const loader = this.sxyUiHelpers.showLoading("SALAXY.UI_TERMS.pleaseWait");
      this.onboardingService.loadSingle(id).then(() => {
        loader.dismiss();
      })
      .catch((err) => {
        loader.dismiss();
      });
    }
  }

  /** Saves current onboarding. */
  public next() {
    const loader = this.sxyUiHelpers.showLoading("SALAXY.UI_TERMS.pleaseWait");
    this.onboardingService.saveCurrent().then(() => {
      super.next();
      loader.dismiss();
    })
    .catch((err) => {
      loader.dismiss();
    });
  }

  /** Returns true if e-mail address can be edited by user (SoMe-authentication methods). */
  protected isEmailEditable() {
    return !this.onboardingService.current ||
      !this.onboardingService.current.credentials ||
      !this.onboardingService.current.credentials.credentialId ||
      !this.onboardingService.current.credentials.credentialId.startsWith("auth0");
  }

}
