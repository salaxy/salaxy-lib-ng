import { Component } from "@angular/core";

import { NgOnboardingBaseComponent, OnboardingService } from "@salaxy/ng";

/**
 * User interface for Recovering password (for e-mail accounts) or user ID for other types of accounts.
 *
 * <example-url>/#/examples/RecoverUidPwdExample</example-url>
 *
 * @example <sxy-recover-uid-pwd"></sxy-recover-uid-pwd>
 */
@Component({
  selector: "sxy-recover-uid-pwd",
  templateUrl: "./recover-uid-pwd.html",
})
export class RecoverUidPwdComponent {

  constructor() {
    //
  }
}
