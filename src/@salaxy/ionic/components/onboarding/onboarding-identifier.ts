import { Component, ViewChild } from "@angular/core";
import { NgForm, ValidationErrors } from "@angular/forms";
import { NavParams } from "ionic-angular";

import { NgOnboardingBaseComponent, OnboardingService, NgTranslations } from "@salaxy/ng";

import { SxyUiHelpers } from "../../services/index";

/**
 * User interface for editing the Personal Identifier and other legal stuff in the onboarding process.
 *
 * <example-url>/#/examples/OnboardingEmbeddedExample</example-url>
 *
 * @example <sxy-onboarding-identifier (done)="next()"></sxy-onboarding-identifier>
 *
 */
@Component({
  selector: "sxy-onboarding-identifier",
  templateUrl: "./onboarding-identifier.html",
})
export class OnboardingIdentifierComponent extends NgOnboardingBaseComponent {

  @ViewChild("currentForm") private currentForm: NgForm;

  constructor(
    private navParams: NavParams,
    private onboardingService: OnboardingService,
    private sxyUiHelpers: SxyUiHelpers,
    private translations: NgTranslations,
  ) {
    super(onboardingService);
  }

  /** Run when the component is initialized. */
  public ngOnInit() {
    const id = this.navParams.get("id");
    if (id && (!this.current || !this.current.id)) {
      const loader = this.sxyUiHelpers.showLoading(this.translations.get("SALAXY.UI_TERMS.pleaseWait"), "");
      this.onboardingService.loadSingle(id).then(() => {
        loader.dismiss();
      })
      .catch((err) => {
        loader.dismiss();
      });
    }
  }

  /** Shows error */
  protected getErrorHtml(): any {
    // tslint:disable-next-line
    const control = this.currentForm.controls["officialId"];
    if (!control) {
      return;
    }
    const error = control.getError("sxyManual");
    if (error) {
      return error;
    }
    return null;
  }

  /** Proceeds to the next step  */
  public next() {
    const loader = this.sxyUiHelpers.showLoading(this.translations.get("SALAXY.UI_TERMS.pleaseWait"), "");
    this.onboardingService.saveCurrent().then(() => {
      if (this.current.person.resolvedId) {
        const error = this.translations.get("SALAXY.ION.OnboardingComponent.accountAlreadyExists.html");
        // tslint:disable-next-line
        const control = this.currentForm.controls["officialId"];
        control.setErrors({ sxyManual: error });
      } else {
        super.next();
      }
      loader.dismiss();
    })
    .catch((err) => {
      loader.dismiss();
    });
  }
}
