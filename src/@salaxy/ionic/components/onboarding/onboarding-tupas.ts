import { Component } from "@angular/core";
import { NavParams } from "ionic-angular";

import { NgOnboardingBaseComponent, OnboardingService, NgTranslations } from "@salaxy/ng";

import { SxyUiHelpers } from "../../services/index";


/**
 * User interface for starting the digital signature process in the onboarding process.
 *
 * <example-url>/#/examples/OnboardingEmbeddedExample</example-url>
 *
 * @example <sxy-onboarding-tupas (done)="next()"></sxy-onboarding-tupas>
 *
 */
@Component({
  selector: "sxy-onboarding-tupas",
  templateUrl: "./onboarding-tupas.html",
})
export class OnboardingTupasComponent extends NgOnboardingBaseComponent {

  constructor(private navParams: NavParams, private onboardingService: OnboardingService, private sxyUiHelpers: SxyUiHelpers, private translations: NgTranslations) {
    super(onboardingService);
  }

  /** Run when the component is initialized. */
  public ngOnInit() {
    const id = this.navParams.get("id");
    if (id && (!this.current || !this.current.id)) {
      const loader = this.sxyUiHelpers.showLoading("SALAXY.UI_TERMS.pleaseWait");
      this.onboardingService.loadSingle(id).then(() => {
        loader.dismiss();
      })
      .catch((err) => {
        loader.dismiss();
      });
    }
  }

  /** Called when the user clicks a TUPAS method link. */
  protected select(method: string) {
    const loader = this.sxyUiHelpers.showLoading("SALAXY.UI_TERMS.pleaseWait");
    this.onboardingService.beginSignature(method).then(() => {
      loader.dismiss();
    })
    .catch((err) => {
      loader.dismiss();
    });
  }
}
