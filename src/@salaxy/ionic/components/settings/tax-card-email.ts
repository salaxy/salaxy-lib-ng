import { Component } from "@angular/core";

import { SalaxyContext } from "@salaxy/ng";

/* tslint:disable */
/* TODO: Waiting for Go-through */

/** Editor for the TaxCard e-mailing settings. */
@Component({
  selector: "sxy-settings-tax-card-email",
  templateUrl: "./tax-card-email.html",
})
export class SettingsTaxCardEmailComponent {

  /** Default constructor */
  constructor(private context: SalaxyContext) {
    // TODO: Implementation
  }

  /** HACK: employerIsResponsibleForTaxCardInput not implemented */
  public employerIsResponsibleForTaxCardInput: boolean = true;

  /** HACK: serviceSendsCalculationEmailsToWorkers not implemented */
  public serviceSendsCalculationEmailsToWorkers: boolean = true;

}
