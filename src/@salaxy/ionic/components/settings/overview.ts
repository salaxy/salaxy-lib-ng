import { Component } from "@angular/core";

import { SalaxyContext } from "@salaxy/ng";

/** Overview of the settings. */
@Component({
  selector: "sxy-settings-overview",
  templateUrl: "./overview.html",
})
export class SettingsOverviewComponent {

  /** Default constructor */
  constructor(private context: SalaxyContext) {}
}
