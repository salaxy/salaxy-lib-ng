import { Component } from "@angular/core";

import { InsuranceProduct } from "@salaxy/core";
import { SalaxyContext } from "@salaxy/ng";

/** Editor for the Mandatory Accident Insurance settings. */
@Component({
  selector: "sxy-settings-insurance",
  templateUrl: "./insurance.html",
})
export class SettingsInsuranceComponent {

  /** Default constructor */
  constructor(private context: SalaxyContext) {}

  /** Gets the product to edit. */
  public get insurance(): InsuranceProduct {
    return this.context.products.getProductByType("InsuranceProduct");
  }
}
