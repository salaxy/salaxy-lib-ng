import { Component } from "@angular/core";

import { TaxProduct } from "@salaxy/core";
import { SalaxyContext } from "@salaxy/ng";

/** Editor for the General settings. */
@Component({
  selector: "sxy-settings-general",
  templateUrl: "./general.html",
})

export class SettingsGeneralComponent {

  // HACK: employerIsResponsibleForTaxCardInput not implemented. Think through before implementing
  /** If true employer is responsible for tax card input by default. Or what is the point of this? */
  public employerIsResponsibleForTaxCardInput: boolean = true;

  // HACK: employerIsResponsibleForTaxCardInput not implemented. Think through before implementing
  /** If true, Salaxy sends the emails Salary slips to Workers */
  public serviceSendsCalculationEmailsToWorkers: boolean = true;

  // HACK: otherSalariesAmount not implemented. Think through before implementing
  /** Salaries paid outside Salaxy */
  public otherSalariesAmount: number;

  /** Default constructor */
  constructor(private context: SalaxyContext) {}

  /** Gets the product to edit. */
  public get tax(): TaxProduct {
    return this.context.products.getProductByType("TaxProduct");
  }

}
