import { Component } from "@angular/core";

import { PensionProduct, PensionCompany } from "@salaxy/core";
import { SalaxyContext } from "@salaxy/ng";

/** Editor for the Pension insurance settings. */
@Component({
  selector: "sxy-settings-pension",
  templateUrl: "./pension.html",
})
export class SettingsPensionComponent {

  /** Default constructor */
  constructor(private context: SalaxyContext) {}

  /** Gets the product to edit. */
  public get pension(): PensionProduct {
    return this.context.products.getProductByType("PensionProduct");
  }

  /**
   * Gets or sets the value of whether the company handles the pension reports and payments directly,
   * without the involvement of Salaxy. This is always true if isPartnerCompany=false.
   */
  public get isPensionSelfHandling() {
    return this.pension.isPensionSelfHandling || !this.isPartnerCompany;
  }
  public set isPensionSelfHandling(value) {
    this.pension.isPensionSelfHandling = value;
  }

  /**
   * Returns true if the Pension company is a partner company:
   * If Salaxy handles the payments and reports automatically.
   */
  public get isPartnerCompany() {
    return this.pension.pensionCompany === PensionCompany.Elo
        || this.pension.pensionCompany === PensionCompany.Etera
        || this.pension.pensionCompany === PensionCompany.Varma;
  }

  /** Saves the settings */
  public save() {
    alert("TODO: Save here");
  }
}
