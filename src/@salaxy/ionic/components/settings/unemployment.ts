import { Component } from "@angular/core";

import { UnemploymentProduct } from "@salaxy/core";
import { SalaxyContext } from "@salaxy/ng";

/* tslint:disable */
/* TODO: Waiting for Go-through */

/** Editor for the Unemployment insurance (TVR) settings. */
@Component({
  selector: "sxy-settings-unemployment",
  templateUrl: "./unemployment.html",
})
export class SettingsUnemploymentComponent {

  /** Default constructor */
  constructor(private context: SalaxyContext) {}

  /** Gets the insurance product or empty object if none is selected. */
  public get unemployment(): UnemploymentProduct {
    return this.context.products.getProductByType("UnemploymentProduct");
  }

  // HACK: prepaymentAmount is not available in the UnemploymentProduct object.

}
