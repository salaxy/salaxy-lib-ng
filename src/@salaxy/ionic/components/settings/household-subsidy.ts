import { Component } from "@angular/core";

import { TaxProduct } from "@salaxy/core";
import { SalaxyContext } from "@salaxy/ng";

/** Editor for the Household Subsidy settings. */
@Component({
  selector: "sxy-settings-household-subsidy",
  templateUrl: "./household-subsidy.html",
})
export class SettingsHouseholdSubsidyComponent {

  /** Default constructor */
  constructor(private context: SalaxyContext) {}

  /** Gets the product to edit. */
  public get tax(): TaxProduct {
    return this.context.products.getProductByType("TaxProduct");
  }

}
