import { Component } from "@angular/core";

import { SalaxyContext } from "@salaxy/ng";

import { SettingsGeneralComponent } from "./general";
import { SettingsHouseholdSubsidyComponent } from "./household-subsidy";
import { SettingsInsuranceComponent } from "./insurance";
import { SettingsPensionComponent } from "./pension";
import { SettingsTaxCardEmailComponent } from "./tax-card-email";
import { SettingsTaxComponent } from "./tax";
import { SettingsUnemploymentComponent } from "./unemployment";


/** Overview of the settings. */
@Component({
  selector: "sxy-settings-edit-buttons",
  templateUrl: "./edit-buttons.html",
})
export class SettingsEditButtonsComponent {

  /** Componennt reference for view */
  protected SettingsGeneralComponent = SettingsGeneralComponent;
  /** Componennt reference for view */
  protected SettingsHouseholdSubsidyComponent = SettingsHouseholdSubsidyComponent;
  /** Componennt reference for view */
  protected SettingsInsuranceComponent = SettingsInsuranceComponent;
  /** Componennt reference for view */
  protected SettingsPensionComponent = SettingsPensionComponent;
  /** Componennt reference for view */
  protected SettingsTaxCardEmailComponent = SettingsTaxCardEmailComponent;
  /** Componennt reference for view */
  protected SettingsTaxComponent = SettingsTaxComponent;
  /** Componennt reference for view */
  protected SettingsUnemploymentComponent = SettingsUnemploymentComponent;

  /** Default constructor */
  constructor(private context: SalaxyContext) {}
}
