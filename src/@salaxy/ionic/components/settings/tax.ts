import { Component } from "@angular/core";

import { TaxProduct } from "@salaxy/core";
import { SalaxyContext } from "@salaxy/ng";

/** Editor for the Tax payments and reports settings. */
@Component({
  selector: "sxy-settings-tax",
  templateUrl: "./tax.html",
})
export class SettingsTaxComponent {

  /** Default constructor */
  constructor(private context: SalaxyContext) {}

  /** Gets the product to edit. */
  public get tax(): TaxProduct {
    return this.context.products.getProductByType("TaxProduct");
  }

  /** TODO: Saving of the settings. */
  public save() {
    alert("TODO: Save here");
  }
}
