import { Component, Input, Output, EventEmitter } from "@angular/core";
import { NavParams, NavController } from "ionic-angular";
import { Page } from "ionic-angular/navigation/nav-util";

import { ContractParty, ContractPartyLogic, LegalEntityType } from "@salaxy/core";
import { NgWorkerListComponent as SalaxyWorkerListComponent, SalaxyContext, ComponentDoneEvent } from "@salaxy/ng";

import { ContactDetailsComponent } from "./contact-details";
import { MasterDetailService } from "./../master-detail/master-detail.service";

/**
 * Shows a list of contacts with select functionality.
 * By default displays "create new" button, which can be removed by setting [hideCreateButton] input set to true.
 *
 * <example-url>/#/examples/ContactEditorExample</example-url>
 *
 * @example
 * <sxy-contact-list itemSxyDetailPush="ContactDetailsComponent"></sxy-contact-list>
 */
@Component({
  selector: "sxy-contact-list",
  templateUrl: "./contact-list.html"
})
export class ContactListComponent {

  /**
   * Called when the user clicks the "Create new" button.
   * Before the call, a new item is createad and set to current (unless skipCurrentItemSet=true)
   * and navigation to itemNavPush/itemSxyDetailPush has finalized.
   */
  @Output() public createNewClicked = new EventEmitter<ContractParty>();

  /**
   * Called when the user clicks a single item in the list.
   */
  @Output() public itemSelect = new EventEmitter<ContractParty>();

  /**
   * Adds ionic navigation push functionality to each item.
   * Basically the same as adding navPush to each item (https://ionicframework.com/docs/api/components/nav/NavPush/).
   * Also supports navParams.
   * If navParams is not null, also the calculation is added to the navParams as "contract".
   */
  @Input() public itemNavPush: string | Page;

  /**
   * Adds Salaxy Master-detail Detail-push functionality to each item.
   * Basically the same as adding sxyDetailPush to each item.
   * Also supports navParams.
   * If navParams is not null, also the contract is added to the navParams as "contract".
   */
  @Input() public itemSxyDetailPush: string | Page;

  /**
   * If not null, will be sent together with either itemNavPush or itemSxyDetailPush functionality.
   * In this case, also the contract is added to the navParams as "contract".
   */
  @Input("navParams") public itemNavParams: any;

  /**
   * If set to to true, hides the "Add New" button on top of the listing.
   * The button is shown by default.
   */
  @Input() public hideCreateButton: boolean = false;

  /**
   * If false (default), when user sets the contract, it is automatically set as the current contract.
   * If true, the current contract is not set - only itemSelect events are fired.
   */
  @Input() public skipCurrentItemSet = false;

  /**
   * Filter list by text variable. Checks against avatar.displayName field.
   */
  @Input() public filterByText: string;

  constructor(
    protected context: SalaxyContext,
    private masterDetailSrv: MasterDetailService,
    private navCtrl: NavController,
    navParams: NavParams,
  ) {
    if (navParams.get("itemSelect")) {
      this.itemSelect.subscribe(navParams.get("itemSelect"));
    }
    if (navParams.get("createNewClicked")) {
      this.createNewClicked.subscribe(navParams.get("createNewClicked"));
    }
    if (navParams.get("hideCreateButton")) {
      this.hideCreateButton = navParams.get("hideCreateButton");
    }
    if (navParams.get("filterByText")) {
      this.filterByText = navParams.get("filterByText");
    }
  }

  /** The item that is currently in edit / detail view */
  public get current(): ContractParty { return this.context.contacts.current; }

  /**
   * List of contacts or if the list is empty list of example contacts.
   * This is the original list, not affected by filterByText.
   */
  public get list(): ContractParty[] {
    return this.context.contacts.list.length < 1 ? ContractPartyLogic.getSampleContacts() : this.context.contacts.list;
  }

  /** List of items filtered by filterByText property. */
  public get filteredList(): ContractParty[] {
    if (this.filterByText) {
      return this.list.filter((item) => {
        item.avatar.displayName.toLowerCase().indexOf(this.filterByText.toLowerCase()) >= 0
      });
    }
    return this.list;
  }

  /** Returns true if the component is showing example contacts. */
  public get isExample() {
    return this.context.contacts.list.length < 1;
  }

  /**
   * Create a new contact. The functionality is similar to select():
   * If itemNavPush or itemSxyDetailPush is defined, uses the same method does the navigation
   * with the same navParams except that editMode=true, except if explicitly defined.
   * Naturally, emits the createNewClicked event instead of itemSelect event.
   */
  public createNew(entityType: "company" | "person" | null): void {
    const newItem = this.context.contacts.getBlank();
    newItem.avatar.entityType = entityType as any;
    if (!this.skipCurrentItemSet) {
      this.context.contacts.setCurrent(newItem);
    }
    const createNewNavParams = Object.assign({
      contact: newItem,
      editMode: true, // Don't want to override this for default itemNavPush/itemSxyDetailPush link => Reason for Object.assign().
    }, this.itemNavParams);
    if (this.itemNavPush) {
      this.navCtrl.push(this.itemNavPush, createNewNavParams)
        .then((value) => {this.createNewClicked.emit(newItem)});
    }
    else if (this.itemSxyDetailPush) {
      this.masterDetailSrv.goToDetailsPage(this.itemSxyDetailPush, createNewNavParams)
        .then((value) => {this.createNewClicked.emit(newItem)});
    } else {
      this.createNewClicked.emit(newItem)
    }
  }

  /**
   * Selects a single contact: Sets it as current (unless skipCurrentItemSet is set)
   * and navigates according to itemNavPush or itemSxyDetailPush if one is defined (potentially adding itemNavParams).
   * After potential navigation has finalized emits itemSelect event.
   */
  public select(contact: ContractParty): void {
    if (!this.skipCurrentItemSet) {
      this.context.contacts.setCurrent(contact);
    }
    if (this.itemNavParams != null) {
      this.itemNavParams.contact = contact; // This is always overriden when going further => no need for Object.assign().
    }
    if (this.itemNavPush) {
      this.navCtrl.push(this.itemNavPush, this.itemNavParams)
        .then((value) => { this.itemSelect.emit(contact) });
    }
    else if (this.itemSxyDetailPush) {
      this.masterDetailSrv.goToDetailsPage(this.itemSxyDetailPush, this.itemNavParams)
        .then((value) => { this.itemSelect.emit(contact) });
    } else {
      this.itemSelect.emit(contact);
    }
  }

}
