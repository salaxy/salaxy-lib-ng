import { Component, Input, Output, EventEmitter } from "@angular/core";
import { NavParams } from "ionic-angular";

import { ContractParty, LegalEntityType } from "@salaxy/core";
import { SalaxyContext, ComponentDoneEvent } from "@salaxy/ng";

import { SxyUiHelpers } from "./../../services/sxy-ui-helpers";
import { MasterDetailService } from "./../master-detail/master-detail.service";

/**
 * Details / Edit view for a single contact.
 * Set editMode to true for editor view.
 *
 * <example-url>/#/examples/ContactEditorExample</example-url>
 *
 * @example
 * this.masterDetailSrv.goToDetailsPage(ContactDetailsComponent, { title: "Create a New Contact", editMode: true });
 */
@Component({
  selector: "sxy-contact-details",
  templateUrl: "./contact-details.html",
})
export class ContactDetailsComponent {

  /**
   * Called when the user does a final action: OK / Cancel
   */
  @Output() public done = new EventEmitter<ComponentDoneEvent<ContractParty>>();

  /**
   * If true, the component is editable.
   * Default is read-only view.
   */
  @Input() public editMode: boolean = false;

  /**
   * If true, hides the delete button.
   * Shows the delete button by default.
   * Button may be disabled, if the item cannot be deleted.
   */
  @Input() public hideDelete: boolean = false;

  constructor(
    private context: SalaxyContext,
    private uiHelpers: SxyUiHelpers,
    private masterDetailSrv: MasterDetailService,
    navParams: NavParams,
  ) {
    if (navParams.get("done")) {
      this.done.subscribe(navParams.get("done"));
    }
    if (navParams.get("editMode")) {
      this.editMode = navParams.get("editMode");
    }
    if (navParams.get("hideDelete")) {
      this.hideDelete = navParams.get("hideDelete");
    }
  }

  /** The currently selected contact */
  public get current(): ContractParty { return this.context.contacts.current; }

  /** If true, the item being edited is an example - it cannot be saved. */
  public get isExample() { return this.current && this.current.id && this.current.id.startsWith("example-"); };

  /**
   * Gets the entity type of the contact:
   * "company", "person" or null as undefined.
   */
  public get entityType(): "company" | "person" | null {
    switch (this.current.avatar.entityType) {
      case LegalEntityType.Person:
        return "person";
      case LegalEntityType.Company:
        return "company";
      default:
        return null;
    }
  }

  /**
   * True if the delete button should be shown:
   * Object is not read-only, component in edit mode and hideDelete not set.
   */
  public get showDelete(): boolean {
    // HACK: isReadOnly should be in root.
    return !this.hideDelete && this.editMode && !this.current.avatar.isReadOnly;
  }

  /**
   * Toggles edit mode.
   */
  public toggleEditMode(): void {
    this.editMode = !this.editMode;
  }

  /**
   * Saves currently active contact.
   */
  public save(): void {
    if (this.isExample) {
      this.done.emit({
        action: "ok",
        data: this.current,
      });
      return;
    }
    const loader = this.uiHelpers.showLoading("SALAXY.UI_TERMS.isSaving", "SALAXY.UI_TERMS.pleaseWait");
    this.context.contacts.saveCurrent().then(() => {
      this.editMode = false;
      this.done.emit({
        action: "ok",
        data: this.current,
      });
      loader.dismiss();
    })
    .catch((err) => {
      loader.dismiss();
    });
  }

  /**
   * Removes currently active contact with confirmation.
   */
  public delete(): void {
    this.uiHelpers
      .showConfirm("SALAXY.UI_TERMS.areYouSure", "SALAXY.ION.COMPONENTS.ContactDetails.sureToDeleteContact")
      .then(() => {
        const loader = this.uiHelpers.showLoading("SALAXY.UI_TERMS.isDeleting", "SALAXY.UI_TERMS.pleaseWait");
        this.context.contacts.delete(this.current.id).then(() => {
          loader.dismiss();
          this.masterDetailSrv.closeDetails();
        })
        .catch((err) => {
          loader.dismiss();
        });
      });
  }
}
