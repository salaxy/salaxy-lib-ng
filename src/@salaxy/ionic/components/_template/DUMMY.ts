// Framework components
import { Component, Input, OnInit } from "@angular/core";
// Salaxy framework
import { SalaxyContext } from "@salaxy/ng";
// Current project
import { SxyUiHelpers } from "../../services/sxy-ui-helpers";

/**
 * This is a Hello World component.
 * It shows the text Hello world, or optionally your name in place of "world".
 *
 * <example-url>/#/examples/HelloWorldExample</example-url>
 *
 * @example
 * <sxy-hello-world name="Matti"></sxy-hello-world>
 */
@Component({
  selector: "sxy-hello-world",
  template: `<ion-content><h1>Hello {{ name }}!</h1></ion-content>`,
})
export class DUMMYComponent implements OnInit {

  @Input() public name: string = null;

  /**
   * Default contstructor creates a new component.
   * @param context - Salaxy context
   * @param uiHelpers - User interface helpers in @salaxy/ionic framework
   */
  constructor(
    private context: SalaxyContext,
    private uiHelpers: SxyUiHelpers,
  ) {}

  /** Component initialization */
  public ngOnInit(): void {
    if (!this.name) {
      this.name = "World";
    }
  }
}
