import { Component } from "@angular/core";

/** Rounded edit icon on the top right corner of ion-item.
 * <example-url>/#/examples/RoundedIonItemIconExample</example-url>
 * @example
 * <sxy-rounded-ion-item-icon></sxy-rounded-ion-item-icon>
 */
@Component({
  selector: "sxy-rounded-ion-item-icon",
  templateUrl: "./rounded-ion-item-icon.html",
})
export class RoundedIonItemIconComponent {}
