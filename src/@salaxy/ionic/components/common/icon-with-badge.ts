import { Component, Input, OnInit } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";
import { DomSanitizer } from "@angular/platform-browser";
import { NavParams } from "ionic-angular";
// Salaxy framework
import { SalaxyContext } from "@salaxy/ng";
// Current project
import { SxyUiHelpers } from "../../services/sxy-ui-helpers";

/**
 * Shows a larger image (typically used in avatar or thumbnail) with a smaller badge image.
 *
 * Size can be one either large or small.
 * Defaults to small.
 * <example-url>/#/examples/IconWithBadgeExample</example-url>
 *
 * @example
 * Childcare icon (svg) with MLL badge (png)
 * <sxy-icon-with-badge icon="https://cdn.salaxy.com/ng/img/framework/icon/childcare.svg" badge="https://cdn.salaxy.com/ng/img/framework/badge/mll.png"></sxy-icon-with-badge>
 * Construction/carpenter icon with Rakennusliitto badge
 * <sxy-icon-with-badge size="large" icon="https://cdn.salaxy.com/ng/img/framework/icon/construction-carpenter.svg" badge="https://cdn.salaxy.com/ng/img/framework/badge/raksa.png"></sxy-icon-with-badge>
 */
@Component({
  selector: "sxy-icon-with-badge",
  templateUrl: "./icon-with-badge.html",
})
export class IconWithBadgeComponent implements OnInit {

  /**
   * Path to the main icon, it is expected being a square.
   */
  @Input() public icon: string;

  /**
   * Path to the badge image.
   * Typically a PNG, badge is expected being a square icon.
   */
  @Input() public badge: string;

  /** size of the icon */
  @Input() public size: string = "small";

  /** Initializes the component */
  public ngOnInit(): void {
    // Init here
  }
}
