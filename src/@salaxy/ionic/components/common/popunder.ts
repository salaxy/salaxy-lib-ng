import { Component } from "@angular/core";
import { NavParams, ViewController } from "ionic-angular";

import { CalendarComponentOptions } from "ion2-calendar";
import * as momentImport from "moment";
/** workaround */
const moment = momentImport;

/**
 * Underlying view component for showing modal dialogs in Salaxy components.
 * Typically, you would use SxyUiHelpers to create and show the dialog, but in edge cases you may use the component directly.
 *
 * salaxy-popunder is an element bit like the ionic ActionSheet:
 * https://ionicframework.com/docs/api/components/action-sheet/ActionSheetController/,
 * but with more control over the content inside.
 * <example-url>/#/examples/PopunderExample</example-url>
 * @example
 * // Typical use:
 * this.uiHelpers
 *   .showConfirm("Here is a question?", "Please answer OK or Cancel to this question.")
 *   .then(() => { this.uiHelpers.showAlert("Operation here"); })
 *   .catch(() => { this.uiHelpers.showAlert("NOOP"); });
 *
 * @example
 * // Advanced use with Ionic ModalController
 * const modal = this.modalCtrl.create(PopunderComponent, {
 *   buttons: [{
 *     text: "Agree",
 *     value: true,
 *   }, {
 *     text: "Cancel",
 *     value: false,
 *     transparent: true,
 *   }],
 *   heading,
 *   text,
 *   contentType: "confirm",
 * }, {
 *   showBackdrop: false,
 * });
 * modal.onDidDismiss((data, role) => {
 *   if (data.value) {
 *     // Operation here
 *   } else {
 *     // NoOp here
 *   }
 * });
 * modal.present();
 *
 */
@Component({
  selector: "sxy-popunder",
  templateUrl: "./popunder.html",
})
export class PopunderComponent {

  /**
   * Type of the dialog:
   *
   * - confirm: Confirmation like window.confirm() in JavaScript
   * - alert: Confirmation like window.alert() in JavaScript
   * - calendar: Dialog that shows a calendar view for setting the date.
   *   Note that in many user interfaces, ion-datetime may be a better option than this popup control.
   * - period: Shows a dialog that allows setting start and end dates and the number of working days between (sxy-calc-period)
   */
  public contentType: "confirm" | "alert" | "period" | "calendar";

  /**
   * Buttons configuration.
   */
  public buttons: [{
    /** Text of the button */
    text: string,
    /** Value returned from the component when the button is clicked. */
    value: boolean,
    /** Type of the button, "solid" is default. */
    type: "solid" | "clear" | "outline",
    /** Color of the button, "primary is default." */
    color: string,
  }];

  /**
   * Heading text for confirm, alert or potentially other components in the future.
   */
  public heading: string;

  /**
   * Explaining text for confirm, alert or potentially other components in the future.
   */
  public text: string;

  /** Date of calendar control or start date of range control. */
  public date: string;

  constructor(
    private params: NavParams,
    private viewCtrl: ViewController,
  ) {
    this.contentType = this.params.get("contentType");
    this.heading = this.params.get("heading");
    this.text = this.params.get("text");
    this.buttons = this.params.get("buttons");
    this.date = this.params.get("date");
  }

  /** Called when user clicks a button. */
  public dismiss(value: string): void {
    this.viewCtrl.dismiss({ value });
  }

  /** Called when a selection is made in the calendar component. */
  protected calendarChange($event): void {
    this.viewCtrl.dismiss({ value: this.date });
  }

  /** Called when OK button is clicked in the period component. */
  protected periodComponentDone($event): void {
    this.viewCtrl.dismiss({ value: true });
  }

  /**
   * Add and remove class to the created modal that will
   * allow us to style this type of modal differently.
   */
  private ionViewWillEnter() {
    const modal = document.getElementsByTagName("ion-modal")[0];
    modal.classList.add("popunder-modal");
  }
  private ionViewDidLeave() {
    const modal = document.getElementsByTagName("ion-modal")[0];
    modal.classList.remove("popunder-modal");
  }

}
