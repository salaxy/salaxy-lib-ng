import { Component, OnInit, Input } from "@angular/core";

/**
 * Component that takes a long and short text, and creates a link that
 * allows toggling the long text to and from the view.
 * <example-url>/#/examples/InfoTextExample</example-url>
 * @example
 * <sxy-info-text [hasLongText]="true">
 *   <div shortText>We have some info for you...</div>
 *   <div longText padding>
 *     <h1>This is the information</h1>
 *     <p>Lorem ipsum dolor sit amet.</p>
 *   </div>
 * </sxy-info-text>
 */
@Component({
  selector: "sxy-info-text",
  templateUrl: "./info-text.html",
})
export class InfoTextComponent {

  /**
   * Label in the link / button to show the long text.
   * Default is SALAXY.ION.InfoTextComponent.showMoreLabel ("Show more...").
   * To not show this link, either set this text to null or hasLongText to false.
   */
  @Input() public showMoreLabel: string = "SALAXY.ION.COMPONENTS.InfoText.showMoreLabel";

  /**
   * Text to be displayed in the link that hides the long text.
   * By default, this label is null and thus the label is not shown,
   * because the typical use case is that parent component (card) hides the entire info-text.
   * Set this property to a value to show a "Show less.." text.
   */
  @Input() public showLessLabel: string = null;

  /**
   * Set this to true if there is a long text:
   * If there is a text in child element with attribute "longText".
   */
  @Input() public hasLongText: boolean = true;

  /**
   * State of the long text display.
   * By default, this is false, i.e. short text is shown.
   */
  @Input() public showLongText: boolean = false;

  /** Toggle the long text (show/hide) */
  public toggleLongText(): void {
    this.showLongText = !this.showLongText;
  }
}
