import { Component, Input } from "@angular/core";

/**
 * <example-url>/#/examples/SpinnerExample</example-url>
 * @example
 * <sxy-spinner></sxy-spinner>
 * Component that shows the spinner while loading content.
 */
@Component({
  selector: "sxy-spinner",
  // Note that this template is separately in the SxyUiHelpers component.
  template: `
    <div class="sxy-spinner">
      <svg class="sxy-spinner-bg" viewBox="0 0 64 64"><circle cx="32" cy="32" r="26"></circle></svg>
      <svg class="sxy-spinner-spin" viewBox="0 0 64 64">
        <circle transform="translate(32,32)" r="26"></circle>
      </svg>
    </div>
    `
})
export class SpinnerComponent {}
