import { Component, Input } from "@angular/core";

/**
 * <example-url>/#/examples/SpinnerSecondaryExample</example-url>
 * @example
 * <sxy-spinner-secondary-secondary></sxy-spinner-secondary-secondary>
 * Component that shows the spinner while loading content with the secondary three horizontal dots look.
 */
@Component({
  selector: "sxy-spinner-secondary",
  // Note that this template is separately present within the SxyUiHelpers component.
  template: `
    <div class="sxy-spinner-secondary">
      <ion-spinner name="dots"></ion-spinner>
    </div>
    `
})
export class SpinnerSecondaryComponent {}
