import { Component, Input } from "@angular/core";

import { Avatar } from "@salaxy/core";
import { NgTranslations } from "@salaxy/ng";

/**
 * Salaxy card component:
 * Shows a card that summarizes a screen or concept in a visually appealing way.
 * Optionally shows an info-text component for more detailed data.
 * <example-url>/#/examples/CardExample</example-url>
 * @example
 * <sxy-card
 *             img="https://cdn.salaxy.com/ng/img/cards/boy-with-bubbles.jpg"
 *             title="Tilapäinen lastenhoito"
 *             subTitle="Lastenhoito MLL:n suositusten mukaan"
 *             price="22 €/h"
 *             avatar="palkkaus"
 *             [showInfoText]="true"
 *             >
 *             <div shortInfoText>
 *                 <p>Lastenhoitajan palkka Mannerheimin Lastensuojeluliiton suositusten
 *                 mukaan. Palkka ilmoitetaan kotitalousvähennykseen.</p>
 *             </div>
 *             <div longInfoText>
 *                 <p>
 *                 Perhe maksaa käteisellä lasten hoitajalle palkkana 8,20 €/tunti
 *                 (sunnuntaina 16,40). Tämä on kokonaiskorvaus, joka sisältää
 *                 lomakorvauksen.  Tunnit ilmoitetaan puolen tunnin tarkkuudella,
 *                 minimi 2 tuntia.
 *                 </p>
 *                 <p>...</p>
 *             </div>
 * </sxy-card>
 */
@Component({
  selector: "sxy-card",
  templateUrl: "./card.html"
})
export class CardComponent {

  /** Avatar shown in the bottom left area of the card */
  @Input() public avatar: Avatar;

  /** SVG icon shown instead of Avatar */
  @Input() public icon: string;

  /** An optional PNG badge shown on top of icon () */
  @Input() public badge: string;

  /** Main text in the top left corner of the image  */
  @Input() public title: string;

  /** Smaller text below the title */
  @Input() public subTitle: string;

  /** Background image for the card */
  @Input() public img: string;

  /**
   * Color mask for the card image if img is specified. Default is rgba(62, 132, 214, 0.80).
   * ... OR primary color for color gradient if img is not defined. Default is "#53b89e".
   * This is typically an RGBA value. The .
   */
  @Input() public color = null;

  /** Second color for color gradient. Default is "#3466be". */
  @Input() public color2 = null;

  /** The price text in bottom right corner of the image area */
  @Input() public price: string;

  /** Switch for info text display extension balow the card. */
  @Input() public showInfoText: boolean;

  /** Switch to determine whether or not the info text extension has long text */
  @Input() public hasLongInfoText: boolean = true;

  /**
   * State of info text:
   * "intro" is a short introduction text
   */
  @Input() public infoTextState: "intro" | "hidden" | "visible";

  /**
   * Constructor
   */
  constructor(public translate: NgTranslations) {}

  /** Gets a background image with the fade effect.  */
  protected getCombinedBackgroundImage() {
    if (this.img) {
      let color = this.color || "rgba(62, 132, 214, 0.80)";
      if (color.indexOf("#") === 0) {
        const regex = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(color);
        const rgb = regex
          ? {
              b: parseInt(regex[3], 16),
              g: parseInt(regex[2], 16),
              r: parseInt(regex[1], 16)
            }
          : { r: 0, g: 0, b: 0 };
        color = "rgba(" + rgb.r + ", " + rgb.g + ", " + rgb.b + ", 0.80)";
      }
      return (
        "linear-gradient(" + color + ", " + color + "), url('" + this.img + "')"
      );
    } else {
      const color1 = this.color || "#53b89e";
      const color2 = this.color2 || "#3466be";
      return "linear-gradient(to right, " + color1 + ", " + color2 + ")";
    }
  }
}
