import { Component } from "@angular/core";

import { NgAvatarComponent } from "@salaxy/ng";

/**
 * TODO: This component is a placeholder: It has not been implemented yet.
 * Idea is that the component would be a place where user can modify:
 *
 * - First- and last names (if not readonly).
 * - Picture for avatar and potentially intials/color.
 * <example-url>/#/examples/AvatarEditorExample</example-url>
 * @example
 * <sxy-avatar-editor></sxy-avatar-editor>
 */
@Component({
  selector: "sxy-avatar-editor",
  templateUrl: "./avatar-editor.html",
})
export class AvatarEditorComponent extends NgAvatarComponent {
  // TODO: Implementation
}
