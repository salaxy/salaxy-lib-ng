import { Component, ViewEncapsulation } from "@angular/core";

import { NgTranslations } from "@salaxy/ng";

/**
 * LanguageSelectorComponent
 * Template wraps using ion-item and ion-select
 */
@Component({
  selector: "sxy-language-selector",
  template: `
    <ion-item class="sxy-language-selector">
      <ion-icon name="flag" item-start></ion-icon>
      <ion-select [(ngModel)]="currentLang" (ngModelChange)="languageChanged()" [selectOptions]="selectOptions"
        okText="{{ 'SALAXY.UI_TERMS.select' | sxyTranslate }}"
        cancelText="{{ 'SALAXY.UI_TERMS.cancel' | sxyTranslate }}">
        <ion-option value="fi">Suomeksi</ion-option>
        <ion-option value="sv">På svenska</ion-option>
        <ion-option value="en">In English</ion-option>
      </ion-select>
    </ion-item>
  `,
  encapsulation: ViewEncapsulation.None
})
export class LanguageSelectorComponent {

  /** Currently chosen language. To be populated in constructor. */
  public currentLang: string;

  /** Options for ion-select */
  public get selectOptions(): object {
    return {
      title: this.translate.get('SALAXY.ION.LANG_SELECT.title')
    };
  }

  constructor(private translate: NgTranslations) {
    this.currentLang = this.translate.getLanguage();
  }

  /** languageChanged */
  public languageChanged(): void {
    this.translate.setLanguage(this.currentLang);
  }

}
