import { Component, Input } from "@angular/core";
import { NavParams } from "ionic-angular";

/**
 * Debugging component for development and testing purposes.
 *
 * @example
 * this.navCtrl.push(DebuggerComponent, {
 *  text: "DebuggerComponent pushed from code-behind. REFRESH the page to get back.",
 *  color: "secondary", dump: { title: "Some data here" },
 * });
 *
 * @example
 * <sxy-debugger [dump]="getDebugData()" text="Here is some debug data:" color="primary"></sxy-debugger>
 *
 * <example-url>/#/examples/DebuggerExample</example-url>
 *
 */
@Component({
  selector: "sxy-debugger",
  template: `
    <div *ngIf="!asContent" class="sxy-debugger-container {{ color ? 'sxy-debugger-container-' + color : '' }}" padding>
      <div *ngIf="text">{{ text }}</div>
      <ng-content select="content"></ng-content>
      <div *ngIf="dump" style="width: 100%; overflow: auto">
        <pre>{{ dump | json }}</pre>
      </div>
    </div>
    <ion-content *ngIf="asContent" class="sxy-debugger-container {{ color ? 'sxy-debugger-container-' + color : '' }}" padding>
      <ng-content select="content"></ng-content>
      <div *ngIf="text">{{ text }}</div>
      <div *ngIf="dump" style="width: 100%; overflow: auto">
        <pre>{{ dump | json }}</pre>
      </div>
    </ion-content>
    `,
})
export class DebuggerComponent {

  /**
   * Specify an object to dump:
   * The component shows the dumped object it as json in a PRE-element (width: 100% * max-height: 400px).
   */
  @Input() public dump: any;

  /**
   * The color to use from your Sass `$colors` map.
   * Default options are: `"primary"`, `"secondary"`, `"tertiary"`, `"success"`, `"warning"`, `"danger"`, `"light"`, `"medium"`, and `"dark"`.
   * For more information, see [Theming your App](/docs/theming/theming-your-app).
   */
  @Input() public color: string;

  /**
   * If set to true, will render the component as a IonContent component.
   * This is makes the component behave basically as a page:
   * The component is 100% high taking into account the space for footers and headers.
   */
  @Input() public asContent = false;

  /**
   * Add text to the component.
   * Note that you can also add content element below to add HTML-formatted content.
   */
  @Input() public text: string;

  /** Creates a new instance of DebuggerComponent  */
  constructor(navParams: NavParams) {
    this.color = navParams.get("color") || this.color;
    this.dump = navParams.get("dump") || this.dump;
    this.text = navParams.get("text") || this.text;
    if (navParams.get("asContent") != null) {
      this.asContent = navParams.get("asContent");
    }
  }

}
