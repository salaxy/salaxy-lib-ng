import { Component, Input } from "@angular/core";

/**
 * Component that shows the progress bar.
 * Primary usecase is Wizard progress, but may be extended later for other uses.
 * <example-url>/#/examples/ProgressExample</example-url>
 * @example
 * <sxy-progress></sxy-progress>
 * 30%
 * <sxy-progress value="0.3"></sxy-progress>
 * 90%
 * <sxy-progress value="0.9"></sxy-progress>
 */
@Component({
  selector: "sxy-progress",
  template: `
    <div class="sxy-progress">
      <div class="sxy-progress-bar-bg">
        <div class="sxy-progress-bar" [ngStyle]="{'width': getPercent() + '%'}"></div>
      </div>
    </div>
    `
})
export class ProgressComponent {

  /** Value of progress from 0 to 1. */
  @Input() public value;

  private getPercent() {
    return Number(this.value) * 100;
  }
}
