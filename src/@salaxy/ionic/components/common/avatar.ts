import { Component } from "@angular/core";

import { NgAvatarComponent } from "@salaxy/ng";

/**
 * Shows the Avatar picture for a person or company.
 * Picture is either an image (jpg/png) or an artificial icon created from initials and a random color.
 * <example-url>/#/examples/AvatarExample</example-url>
 * @example
 * <p>Avatar of the employer of a calculation</p>
 * <sxy-avatar [avatar]="calc.employer.avatar" item-start></sxy-avatar>
 * <p>Shortcut for palkkaus logo</p>
 * <sxy-avatar avatar="palkkaus" item-start></sxy-avatar>
 * <p>Shortcut for anonymous user</p>
 * <sxy-avatar avatar="palkkaus" item-start></sxy-avatar>
 * <p>Custom object</p>
 * <sxy-avatar [avatar]="{ initials: 'OK', color: 'green', entityType: LegalEntityType.Company }" item-start></sxy-avatar>
 */
@Component({
  selector: "sxy-avatar",
  templateUrl: "./avatar.html",
})
export class AvatarComponent extends NgAvatarComponent {}
