import { Component, OnInit, Output, EventEmitter } from "@angular/core";
import { NavParams, NavController } from "ionic-angular";

import { ContractParty, LegalEntityType, WorkerAccount, TaxCard, TaxCardLogic, TaxCardType } from "@salaxy/core";
import { SalaxyContext, NgTranslations, ComponentDoneEvent } from "@salaxy/ng";

import { SxyUiHelpers } from "./../../services/sxy-ui-helpers";
import { MasterDetailService } from "./../master-detail/master-detail.service";

/**
 * Details / Edit view for Worker (employee) objects.
 * Set editMode to true for edit - read-only is the default.
 *
 * <example-url>/#/examples/WorkerEditorExample</example-url>
 *
 * @example
 * this.masterDetailSrv.goToDetailsPage(WorkerDetailsComponent, { title: "Edit Worker Details", editMode: true });
 */
@Component({
  selector: "sxy-worker-details",
  templateUrl: "./worker-details.html",
})
export class WorkerDetailsComponent {

  /**
   * Called when the user does a final action: OK / Cancel
   */
  @Output() public done = new EventEmitter<ComponentDoneEvent<ContractParty>>();

  /**
   * If true, the component is editable.
   * Default is read-only view.
   */
  public editMode: boolean = false;

  /** If set to true, the Delete button is not shown. */
  public hideDelete: boolean = false;

  constructor(
    private navParams: NavParams,
    private context: SalaxyContext,
    private uiHelpers: SxyUiHelpers,
    private navCtrl: NavController,
    private masterDetailSrv: MasterDetailService,
    private translate: NgTranslations,
  ) {
    if (navParams.get("editMode")) {
      this.editMode = navParams.get("editMode");
    }
    if (navParams.get("hideDelete")) {
      this.hideDelete = navParams.get("hideDelete");
    }
  }

  /** Gets the Current Worker object */
  public get current(): WorkerAccount {
    return this.context.workers.current;
  }

  /** If true, the item being edited is an example - it cannot be saved. */
  public get isExample() {
    return this.current && this.current.id && this.current.id.startsWith("example-");
  };

  /**
   * True if the delete button should be shown:
   * Object is not read-only, component in edit mode and hideDelete not set.
   */
  public get showDelete() {
    return !this.hideDelete && this.editMode && !this.current.isReadOnly
  }

  /**
   * Toggle edit mode.
   */
  public toggleEditMode(): void {
    this.editMode = !this.editMode;
  }

  /**
   * Saves currently active worker.
   */
  public save(): void {
    if (this.isExample) {
      this.done.emit({
        action: "ok",
        data: this.current,
      });
      return;
    }
    const loader = this.uiHelpers.showLoading("SALAXY.UI_TERMS.isSaving", "SALAXY.UI_TERMS.pleaseWait");
    this.context.workers.saveCurrent().then(() => {
      this.editMode = false;
      this.done.emit({
        action: "ok",
        data: this.current,
      });
      loader.dismiss();
    })
    .catch((err) => {
      loader.dismiss();
    });
  }

  /**
   * Removes currently active worker with confirmation.
   */
  public delete(): void {
    this.uiHelpers
      .showConfirm("SALAXY.UI_TERMS.areYouSure", "SALAXY.ION.COMPONENTS.WorkerDetails.sureToDelete")
      .then(() => {
        const loader = this.uiHelpers.showLoading("SALAXY.UI_TERMS.isDeleting", "SALAXY.UI_TERMS.pleaseWait");
        this.context.workers.delete(this.current.id).then(() => {
          loader.dismiss();
          this.masterDetailSrv.closeDetails();
        })
        .catch((err) => {
          loader.dismiss();
        });
      });
  }
}
