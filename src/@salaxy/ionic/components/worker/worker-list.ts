import { Component, Input, Output, OnInit, EventEmitter } from "@angular/core";
import { NavParams, NavController } from "ionic-angular";
import { Page } from "ionic-angular/navigation/nav-util";

import { WorkerAccount, ContractParty, WorkerLogic } from "@salaxy/core";
import { NgWorkerListComponent as SalaxyWorkerListComponent, SalaxyContext, NgTranslations } from "@salaxy/ng";

import { WorkerDetailsComponent } from "./worker-details";
import { MasterDetailService } from "./../master-detail/master-detail.service";
import { SxyUiHelpers } from "../../services";

/**
 * A List of  for Employer
 * Shows a list of Workers for Employer Account with select functionality.
 * By default displays "create new" button, which can be removed by setting [hideCreateButton] input set to true.
 *
 * <example-url>/#/examples/WorkerListExample</example-url>
 *
 * @example
 * <sxy-worker-list itemSxyDetailPush="WorkerDetailsComponent"></sxy-worker-list>
 */
@Component({
  selector: "sxy-worker-list",
  templateUrl: "./worker-list.html",
})
export class WorkerListComponent extends SalaxyWorkerListComponent {

  /**
   * Adds ionic navigation push functionality to each item.
   * Basically the same as adding navPush to each item (https://ionicframework.com/docs/api/components/nav/NavPush/).
   * Also supports navParams.
   * If navParams is not null, also the calculation is added to the navParams as "contract".
   */
  @Input() public itemNavPush: string | Page;

  /**
   * Adds Salaxy Master-detail Detail-push functionality to each item.
   * Basically the same as adding sxyDetailPush to each item.
   * Also supports navParams.
   * If navParams is not null, also the contract is added to the navParams as "contract".
   */
  @Input() public itemSxyDetailPush: string | Page;

  /**
   * If not null, will be sent together with either itemNavPush or itemSxyDetailPush functionality.
   * In this case, also the contract is added to the navParams as "contract".
   */
  @Input("navParams") public itemNavParams: any;

  constructor(
    protected context: SalaxyContext,
    private masterDetailSrv: MasterDetailService,
    private navCtrl: NavController,
    protected uiHelpers: SxyUiHelpers,
    translate: NgTranslations,
  ) {
    super(context, translate);

    if (!this.itemNavParams) {
      this.itemNavParams = new NavParams();
    }

    if (this.itemNavParams.get("itemSelect")) {
      this.itemSelect.subscribe(this.itemNavParams.get("itemSelect"));
    }
    if (this.itemNavParams.get("createNewClicked")) {
      this.createNewClicked.subscribe(this.itemNavParams.get("createNewClicked"));
    }

    this.itemNavParams = this.itemNavParams.data;
  }

  /** Extends items by sample workers if there are no workers */
  public get extendedList(): ContractParty[] { return (this.itemsCount < 1) ? WorkerLogic.getSampleWorkers() : this.items; }

  /** Returns true if the component is showing example contacts. */
  public get isExample() {
    return this.itemsCount < 1;
  }

  /**
   * Create a new Worker. The functionality is similar to select():
   * If itemNavPush or itemSxyDetailPush is defined, uses the same method does the navigation
   * with the same navParams except that editMode=true, except if explicitly defined.
   * Naturally, emits the createNewClicked event instead of itemSelect event.
   */
  public createNew(): void {
    const newItem = this.context.workers.getBlank();
    if (!this.skipCurrentItemSet) {
      this.context.workers.setCurrent(newItem);
    }
    const createNewNavParams = Object.assign({
      contact: newItem,
      editMode: true, // Don't want to override this for default itemNavPush/itemSxyDetailPush link => Reason for Object.assign().
    }, this.itemNavParams);
    if (this.itemNavPush) {
      this.navCtrl.push(this.itemNavPush, createNewNavParams)
        .then((value) => {this.createNewClicked.emit(newItem)});
    }
    else if (this.itemSxyDetailPush) {
      this.masterDetailSrv.goToDetailsPage(this.itemSxyDetailPush, createNewNavParams)
        .then((value) => {this.createNewClicked.emit(newItem)});
    } else {
      this.createNewClicked.emit(newItem)
    }
  }

  /**
   * Selects a single Worker: Sets it as current (unless skipCurrentItemSet is set)
   * and navigates according to itemNavPush or itemSxyDetailPush if one is defined (potentially adding itemNavParams).
   * After potential navigation has finalized emits itemSelect event.
   */
  public select(worker: WorkerAccount): void {
    if (!this.skipCurrentItemSet) {
      this.context.workers.setCurrent(worker);
    }
    if (this.itemNavParams != null) {
      this.itemNavParams.contact = worker; // This is always overriden when going further => no need for Object.assign().
    }
    if (this.itemNavPush) {
      this.navCtrl.push(this.itemNavPush, this.itemNavParams)
        .then((value) => { this.itemSelect.emit(worker) });
    }
    else if (this.itemSxyDetailPush) {
      this.masterDetailSrv.goToDetailsPage(this.itemSxyDetailPush, this.itemNavParams)
        .then((value) => { this.itemSelect.emit(worker) });
    } else {
      this.itemSelect.emit(worker);
    }
  }


}
