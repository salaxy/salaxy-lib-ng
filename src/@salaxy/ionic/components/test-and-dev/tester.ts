import { Component } from "@angular/core";

// Just a testing component, no need to document
// tslint:disable

/** Tester component for developers */
@Component({
  selector: "salaxy-tester",
  templateUrl: "./tester.html"
})
export class TesterComponent {

  public model = {
    ionicValue: 0
  };

  public submit() {
    alert("Submit here");
  }
}
