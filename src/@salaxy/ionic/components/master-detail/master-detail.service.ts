import { Injectable, Inject } from "@angular/core";
import { NavController, NavOptions } from "ionic-angular";
import { Page } from "ionic-angular/navigation/nav-util";

import { SalaxyContext } from "@salaxy/ng";

import { MasterDetail } from "./master-detail.interface";

/** Provides communication between the master-detail component and related components and directives. */
@Injectable()
export class MasterDetailService {

  /** The component that is currently active */
  private component: MasterDetail;

  /**
   * Registers the Master-Detail component to the service.
   * Only one master-detail component can now be registered per application.
   *
   * @param component - Component to register
   */
  public registerMasterDetailComponent(component: MasterDetail) {
    this.component = component;
    // HACK: Go through this logic before going to production.
  }

  /** Gets a reference to the current master-detail component. */
  public getComponent(): MasterDetail {
    if (!this.component) {
      throw new Error("Master-detail component not defined. Use registerMasterDetailComponent");
    }
    return this.component;
  }

  /**
   * Navigates to the master page.
   * If the split pane is active, the master page will be displayed in the main content panel.
   *
   * @param page An Angular / Ionic component that renders the page payload in the details view.
   * Typically everything except a header (perhaps a footer).
   * This is either component type or name defined in deepLinksConfig.
   * @param params Navigation parameters.
   * @param opts Options.
   * @param done Callback after navigation.
   */
  public goToMasterPage(page: Page | string, params?: any, opts?: NavOptions, done?: () => void): Promise<any> {
    return this.getComponent().goToMasterPage(page, params, opts, done);
  }

  /**
   * Navigates to the details page.
   * If the split pane is active, the details page will be displayed in the aside panel.
   * @param page Details page: A component that renders the payload of the page.
   * Typically everything except a header (perhaps a footer).
   * This is either component type or name defined in deepLinksConfig.
   * @param params Navigation parameters.
   * @param opts  Options.
   * @param done Callback after navigation.
   */
  public goToDetailsPage(page: Page | string, params: any = {}, opts?: NavOptions, done?: () => void): Promise<any> {
    return this.getComponent().goToDetailsPage(page, params, opts, done);
  }

  /**
   * Closes the details page.
   * @param opts Navigation options.
   * @param done Callback.
   */
  public closeDetails(opts?: NavOptions, done?: () => void): Promise<any> {
    return this.getComponent().closeDetails(opts, done);
  }

}
