import { Directive, HostListener, Input, Host } from "@angular/core";
import { NavController  } from "ionic-angular";

import { MasterDetailService } from "./master-detail.service";

/**
 * Pushes a page to root navController.
 * Like navPush, but instead of pushing to current navController,
 * finds the the root navController and pushes to it.
 *
 * @example
 * <button ion-item [sxyRootPush]="contractEditSalaryComponent">Edit Salary</button>
 */
@Directive({
  selector: "[sxyRootPush]",
})
export class MasterDetailRootPushDirective {

  /**
   * The component class or deeplink name you want to push onto the navigation stack.
   */
  @Input() public sxyRootPush: any | string;

  /**
   * Any NavParams you want to pass along to the next view.
   */
  @Input() public navParams: { [k: string]: any };

  /** Provide the title for the detail view */
  @Input() public title: string;

  /** Default constuctor */
  constructor(protected navCtrl: NavController, private masterDetailSrv: MasterDetailService) {}

  /** Click event in the host element */
  @HostListener("click") public onClick(): Promise<any> {
    if (!this.masterDetailSrv) {
      throw new Error("The sxyDetailPush can currently only be used inside master-detail component.");
    }
    if (!this.sxyRootPush) {
      throw new Error("Page is not specified in sxyDetailPush attribute. This is not allowed (check code behind).");
    }
    if (this.title) {
      this.navParams = this.navParams || {};
      this.navParams.title = this.title;
    }
    let navCtrl = this.navCtrl;
    while (navCtrl.parent != null) {
      navCtrl = navCtrl.parent;
    }
    return navCtrl.push(this.sxyRootPush, this.navParams);
  }
}
