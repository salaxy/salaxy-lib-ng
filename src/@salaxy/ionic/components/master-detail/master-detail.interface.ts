import { NavOptions } from "ionic-angular";

/**
 * Defines an interface that a master-detail component must implement.
 */
export interface MasterDetail {

  /**
   * Navigates to the master "page" meaning a component that renders the payload of a page:
   * Typically everything except a header (perhaps a footer).
   *
   * @param pageComponent An Angular / Ionic component that renders the page payload in the details view.
   * @param params Navigation parameters.
   * @param opts Options.
   * @param done Callback after navigation.
   */
  goToMasterPage(pageComponent: any, params?: any, opts?: NavOptions, done?: () => void): Promise<any>;

  /**
   * Navigates to the details "page" meaning a component that renders the payload of a page:
   * Typically everything except a header (perhaps a footer).
   *
   * @param pageComponent An Angular / Ionic component that renders the page payload in the details view.
   * @param params Navigation parameters.
   * @param opts Options.
   * @param done Callback after navigation.
   */
  goToDetailsPage(pageComponent: any, params?: any, opts?: NavOptions, done?: () => void): Promise<any>;

  /**
   * Closes the details page.
   * In a split page this is closing of a panel whereas in flat (mobile only) navigation, this means navCtrl.pop().
   *
   * @param opts Navigation options.
   * @param done Callback.
   */
  closeDetails(opts?: NavOptions, done?: () => void): Promise<any>;
}
