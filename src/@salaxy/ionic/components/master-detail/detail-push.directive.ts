import { Directive, HostListener, Input, Host } from "@angular/core";
import { NavController  } from "ionic-angular";

import { MasterDetailService } from "./master-detail.service";

/**
 * Similar to ionic navPush directive.
 * Can be used in master page in master-detail navigation
 * implemented using MasterDetailService. Opens detail page in side view.
 *
 * @example
 * <button ion-item [sxyDetailPush]="contractEditSalaryComponent" title="Palkkaus"><h2>Palkkaus</h2></button>
 */
@Directive({
  selector: "[sxyDetailPush]",
})
export class MasterDetailPushDirective {

  /**
   * The component class or deeplink name you want to push onto the navigation stack.
   */
  @Input() public sxyDetailPush: any | string;

  /**
   * Any NavParams you want to pass along to the next view.
   */
  @Input() public navParams: { [k: string]: any };

  /** Provide the title for the detail view */
  @Input() public title: string;

  /** Default constuctor */
  constructor(protected navCtrl: NavController, private masterDetailSrv: MasterDetailService) {}

  /** Click event in the host element */
  @HostListener("click")
  public onClick(): Promise<any> {
    if (!this.masterDetailSrv) {
      throw new Error("The sxyDetailPush can currently only be used inside master-detail component.");
    }
    if (!this.sxyDetailPush) {
      throw new Error("Page is not specified in sxyDetailPush attribute. This is not allowed (check code behind).");
    }
    if (this.title) {
      this.navParams = this.navParams || {};
      this.navParams.title = this.title;
    }
    return this.masterDetailSrv.goToDetailsPage(this.sxyDetailPush, this.navParams);
  }
}
