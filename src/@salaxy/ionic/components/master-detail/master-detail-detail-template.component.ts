import { Component, OnInit, ViewChild, ViewContainerRef, ComponentFactoryResolver, ComponentFactory, Type, ComponentRef } from "@angular/core";
import { NavController, NavParams } from "ionic-angular";

import { SalaxyContext } from "@salaxy/ng";

import { MasterDetailDefaultEmptyComponent } from "./master-detail-default-empty.component";

/**
 * Page template for detail components that are shown in the detail view.
 */
@Component({
  templateUrl: "master-detail-detail-template.component.html",
})
export class MasterDetailDetailTemplateComponent {

  /**
   * Reference to the initialized component:
   * You may set properties like this: this.componentRef.instance.myProp = "my value";
   */
  public componentRef: ComponentRef<any>;

  /** Container where the actual payload component is placed. */
  @ViewChild("componentContainer", { read: ViewContainerRef })
  protected componentContainer: ViewContainerRef;

  /** Page title for the detail page. */
  protected title = "No title (init)";

  /** Type of control to initialize. */
  protected controlType: Type<any>;

  /** Default constructor */
  constructor(
    public navCtrl: NavController,
    navParams: NavParams,
    private context: SalaxyContext,
    private resolver: ComponentFactoryResolver,
  ) {
    if (navParams) {
      this.title = navParams.get("title") || "";
      this.controlType = navParams.get("ctrl") || MasterDetailDefaultEmptyComponent;
    }
  }

  /** Initializes the component */
  private ngOnInit() {
    this.createComponent();
  }

  /** Destroys the component */
  private ngOnDestroy() {
    if (this.componentRef) {
      this.componentRef.destroy();
    }
  }

  /** Creates the child component using ComponentFactory */
  private createComponent() {
    // Based on https://netbasal.com/dynamically-creating-components-with-angular-a7346f4a982d
    this.componentContainer.clear();
    const factory: ComponentFactory<any> = this.resolver.resolveComponentFactory(this.controlType);
    this.componentRef = this.componentContainer.createComponent(factory);
  }

}
