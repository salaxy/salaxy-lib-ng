import { Component } from "@angular/core";

/** Default empty page for Master-detail component. */
@Component({
  template: "<ion-content><!-- MasterDetailDefaultEmptyComponent --></ion-content>",
})
export class MasterDetailDefaultEmptyComponent {}
