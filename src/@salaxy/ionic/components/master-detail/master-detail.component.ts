import { Component, ViewChild, Input, OnInit } from "@angular/core";
import { Nav, NavController, NavOptions, DeepLinkConfig, App, IonicModule } from "ionic-angular";
import { Page } from "ionic-angular/navigation/nav-util";

import { MasterDetailDefaultEmptyComponent } from "./master-detail-default-empty.component";
import { MasterDetailDetailTemplateComponent } from "./master-detail-detail-template.component";
import { MasterDetailService } from "./master-detail.service";

/**
 * Provides a conditional split master-detail view for wide layouts.
 * If the split pane is active, the details page will be displayed in the side panel.
 * In smaller mobile screens, shows master and detail views one after another.
 *
 * @todo Should expose "pop from detailsNavCtrl" method for going back on details tab.
 * When done, update contract "update my own details" back btn logic.
 *
 * @example
 * <sxy-master-detail-view [aside]="defaultDetailsComponentHere" title="Laskuri">
 *  <content>
 *    <div padding>Main content here</div>
 *  </content>
 * </sxy-master-detail-view>
 *
 * @example
 * <sxy-master-detail-view title="Palkkaus.fi kotitalouden palkanmaksuun" asideTitle="Tänne selittävä teksti">
 *  <content>
 *    <div padding>Main content here</div>
 *  </content>
 *  <aside>
 *    <div padding>Right-side content</div>
 *  </aside>
 * </sxy-master-detail-view>
 */
@Component({
  templateUrl: "./master-detail.component.html",
  selector: "sxy-master-detail-view",
  styles: ["master-detail.scss"],
})
export class MasterDetailComponent implements OnInit {

  /** Title for the view / page */
  @Input() public title: string = null;

  /** Title for the details view (aside) */
  @Input() public asideTitle: string = null;

  /**
   * Default component for the details area (aside).
   * The component will be shown within a template that provides the header / footer:
   * This is currently protected detailTemplate, but it could be later exposed as @Input if necessary.
   */
  @Input() public aside: any = null;

  /**
   * When input property contentContainer is set to true, content is wrapped inside a div with a default class of .sxy-component-container.
   */
  @Input() public contentContainer: boolean | void;

  /**
   * Input property contentContainerClasses takes a string of space-deliminated class names (without dots)
   * which is appended to the container div element's class attribute.
   */
  @Input() public contentContainerClasses: string | void;

  /** Holds master component ... */
  public masterComponent: any;

  /**
   * Details Nav control (ion-nav) from the template "master-detail.component.html".
   */
  @ViewChild(Nav) protected detailsNavCtrl: Nav;

  /**
   * Page template for the detail view: Responsible for the header/toolbar etc.
   * This is currently protected, but it could be later exposed as @Input if necessary.
   */
  protected asideTemplate: any = MasterDetailDetailTemplateComponent;

  /** Returns true if the split pane is active. */
  private isSplitPaneActive: boolean = false;

  /**
   * Returns true if the aside component should be shown in the detail view.
   * Otherwise content area is shown with content potentially defined in the "aside" tag.
   */
  public get showDetailComponent() {
    if (this.currentDetailsContentDetailsPageName) {
      return true;
    }
    return false;
  }

  /** Gets a debugger component that shows the main properties of the master-detail view */
  public get masterDetailDebugger() {
    const view = this.detailsNavCtrl.getActive();
    return {
      "aside (component)": this.aside ? this.aside.name : "null",
      "asideTemplate": this.asideTemplate ? this.asideTemplate.name : "null",
      "isSplitPaneActive": this.isSplitPaneActive,
    };
  }

  /**
   * Current name of the details component in main content stack.
   * This is used as flag to determine whether the component was added by goToDetailsPage() method and
   * therefore whether it should be moved to Details view in activateSplitView().
   * @todo The logic may not be 100% solid in edge cases.
   */
  private currentMainContentDetailsPageName = "";

  /**
   * Current name of the details component in details content stack.
   * This is used as flag to determine whether the component was added by goToDetailsPage() method and
   * therefore whether it should be moved to Main view in deactivateSplitView().
   * @todo The logic may not be 100% solid in edge cases.
   */
  private currentDetailsContentDetailsPageName = "";

  constructor(private navCtrl: NavController, private masterDetailSrv: MasterDetailService) {}

  /** Initializes the component */
  public ngOnInit() {
    this.masterDetailSrv.registerMasterDetailComponent(this);
    this.currentDetailsContentDetailsPageName = this.aside;
  }

  /**
   * Navigates to the details page. If the split pane is active, the details page will be displayed in the side panel.
   * @param page Details page.
   * @param params Navigation parameters.
   * @param opts  Options.
   * @param done Callback after navigation.
   */
  public goToDetailsPage(page: Page | string, params: any = {}, opts: NavOptions = {}, done?: () => void): Promise<any> {
    page = this.assurePageComponent(page);
    params = this.handleContentContainerSettings(params);
    params.ctrl = page;

    if (this.isSplitPaneActive) {
      // HACK: Instead of checking these parameters, consider using params as flags to tell which pages were added by this method
      this.currentDetailsContentDetailsPageName = this.asideTemplate.name;
      return this.detailsNavCtrl.setRoot(this.asideTemplate, params, opts, done);
    } else {
      if (this.isMainNaviStackSetByGoToDetailsPage()) {
        this.currentMainContentDetailsPageName = this.asideTemplate.name;
        // TODO: This approach doesn't tigger Ionic Life Cycle Events https://ionicframework.com/docs/api/navigation/NavController/
        // Would be far more useful...
        return this.navCtrl.push(this.asideTemplate, params, opts, done)
          .then(() => {
            this.navCtrl.remove(this.navCtrl.getActive().index - 1);
          });
      } else {
        this.currentMainContentDetailsPageName = this.asideTemplate.name;
        return this.navCtrl.push(this.asideTemplate, params, opts, done);
      }
    }
  }

  /**
   * Navigates to the master page. If the split pane is active, it's page will be set empty.
   * Note that currently, this navigation will not update the navigation stack. This may later change.
   * @param page Master page.
   * @param params Navigation parameters.
   * @param opts  Options.
   * @param done Callback after navigation.
   */
  public goToMasterPage(page: Page | string, params: any = {}, opts: NavOptions = {}, done?: () => void): Promise<any> {
    page = this.assurePageComponent(page);
    params = this.handleContentContainerSettings(params);
    params.ctrl = page;
    this.currentMainContentDetailsPageName = null;
    this.masterComponent = page;
    return this.closeDetails(opts, done);
  }

  /**
   * If page is defined as a string finds the page in the deepLinkConfig.
   * Otherwise, assumes the given page is already a component.
   */
  public assurePageComponent(page: Page | string): Page {
    if (typeof page !== "string") {
      return page;
    }
    // TODO: Check if there is a way of getting this through a supported API. Perhaps from entryComponents?
    const link = (this.navCtrl as any)._linker._serializer.getLinkFromName(page);
    if (link != null) {
      return link.component;
    }
    throw Error("Page not in the deeplinks: " + page);
  }

  /**
   * Closes the details page.
   * @param opts Navigation options.
   * @param done Callback.
   */
  public closeDetails(opts?: NavOptions, done?: () => void): Promise<any> {
    if (this.isSplitPaneActive) {
      this.currentDetailsContentDetailsPageName = null;
      return this.detailsNavCtrl.setRoot(this.asideTemplate, { ctrl: this.aside }, opts, done);
    } else {
      this.currentMainContentDetailsPageName = null;
      return this.navCtrl.pop(opts, done);
    }
  }

  /**
   * Called when the ion-split-pane component changes
   * @param isSplitPaneActive If true, the split panel will be set visible.
   *
   * @example Bind this in the template to ionChange of ion-split-pane.
   * <ion-split-pane when="xl" (ionChange)="onSplitPaneChanged($event._visible)">...</ion-split-pane>
   */
  protected onSplitPaneChanged(isSplitPaneActive) {
    this.isSplitPaneActive = isSplitPaneActive;

    if (this.navCtrl && this.detailsNavCtrl) {
      if (isSplitPaneActive) {
        this.activateSplitView();
      } else {
        this.deactivateSplitView();
      }
    }

  }

  /**
   * Synchronizes content container settings that are received component input params with those that are given via navparams.
   * @param params Navigation options.
   */
  private handleContentContainerSettings(params: any = {}) {
    if (params.contentContainer) {
      this.contentContainer = params.contentContainer;
    } else if (this.contentContainer) {
      params.contentContainer = this.contentContainer;
    }
    if (params.contentContainerClasses) {
      this.contentContainer = params.contentContainerClasses;
    } else if (this.contentContainerClasses) {
      params.contentContainerClasses = this.contentContainerClasses;
    }
    return params;
  }

  /**
   * When split pane becomes visible, details page is displayed in a separate panel.
   * This means that the last component in the main panel stack is potentially moved to the details view.
   */
  private activateSplitView() {
    if (this.isMainNaviStackSetByGoToDetailsPage()) {
      // Get current from main navi stack
      const currentView = this.navCtrl.last();
      // Remove from main navi stack
      this.navCtrl.pop();
      // And add to details
      this.detailsNavCtrl.setRoot(currentView.component, currentView.data);
      // set the flags
      this.currentDetailsContentDetailsPageName = this.currentMainContentDetailsPageName;
      this.currentMainContentDetailsPageName = null;
    }
  }

  /** Returns true if the last page in Main navi stack was set by GoToDetailsPage function (this component) */
  private isMainNaviStackSetByGoToDetailsPage() {
    const currentView = this.navCtrl.last();
    return currentView.name === this.currentMainContentDetailsPageName;
  }

  /**
   * When split pane becomes hidden, details page is inserted to main.
   */
  private deactivateSplitView() {
    // Get current from details
    const currentView = this.detailsNavCtrl.last();
    // Set to empty
    this.detailsNavCtrl.setRoot(MasterDetailDefaultEmptyComponent);
    if (currentView.name === this.currentDetailsContentDetailsPageName) {
      // Add to main
      this.navCtrl.push(currentView.component, currentView.data);
      this.currentMainContentDetailsPageName = this.currentDetailsContentDetailsPageName;
    }
    this.currentDetailsContentDetailsPageName = null;
  }
}
