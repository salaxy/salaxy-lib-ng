import { Directive, HostListener, Host } from "@angular/core";
import { NavController  } from "ionic-angular";

import { MasterDetailService } from "../../components/master-detail/master-detail.service";

/**
 * Similar to ionic NavPop directive excepts doesn't go back but closes master-detail side view.
 * Can be used in details page in master-detail navigation.
 *
 * @example
 * <button ion-button sxyDetailPop clear large icon-only float-right class="hidden-mobile"><ion-icon name="close"></ion-icon></button>
 */
@Directive({
  selector: "[sxyDetailPop]",
})
export class MasterDetailPopDirective {

  constructor(protected navCtrl: NavController, protected masterDetailSrv: MasterDetailService) {}

  /** Handles the click event in the parent (host) element. */
  @HostListener("click")
  public onClick(): Promise<any> {
    // throw new Error("not implemented");
    if (!this.masterDetailSrv) {
      // Default to normal path pop (closing the last page).
      return this.navCtrl.pop();
    } else {
      return this.masterDetailSrv.closeDetails();
    }
  }
}
