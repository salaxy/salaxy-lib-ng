import { Pipe, PipeTransform } from "@angular/core";

import { Dates } from "@salaxy/core";

/**
 * Provides a pipe that prints either one date, or a date range from two dates.
 * Parameter value can be a string, JS Date object, Moment etc.
 *
 * <example-url>/#/examples/DatePipeExample</example-url>
 *
 * @example
 * <p>{{ date | salaxyDate }}</p>
 * <p>{{ startDate | salaxyDate:endDate }}</p>
 */
@Pipe({
  name: "salaxyDate"
})
export class SalaxyDatePipe implements PipeTransform {

  /**
   * The Transform method that implements the Pipe
   * @param startDate - First date to be formatted.
   * @param endDate - Second date to be formatted. If exists, we print a date range.
   */
  public transform(startDate: any, endDate: any): any {
    return Dates.getFormattedDate(startDate, endDate);
  }
}
