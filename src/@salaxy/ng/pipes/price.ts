import { Pipe, PipeTransform } from "@angular/core";

import { Numeric } from "@salaxy/core";

/**
 * Provides a pipe that prints correctly formatted price.
 *
 * <example-url>/#/examples/PricePipeExample</example-url>
 *
 * @example
 * {{ calc.result.totals.totalGrossSalary | salaxyPrice }}
 */
@Pipe({
  name: "salaxyPrice"
})
export class SalaxyPricePipe implements PipeTransform {

  /**
   * The Transform method that implements the Pipe
   * @param value - value that we want to display as price.
   */
  public transform(value: any): any {
    return Numeric.formatPrice(value);
  }
}
