import { Pipe, PipeTransform } from "@angular/core";

import { Iban } from "@salaxy/core";

/**
 * Provides a pipe that formats Iban number in 4 digits segments
 * and trims all but numbers after the first two characters.
 *
 * <example-url>/#/examples/IbanPipeExample</example-url>
 *
 * @example
 * <p>{{ 'FI24€%42131241242  14' | salaxyIban }}</p>
 */
@Pipe({
  name: "salaxyIban"
})
export class SalaxyIbanPipe implements PipeTransform {

  /**
   * The Transform method that implements the Pipe
   * @param value - value that we want to display as Iban number.
   */
  public transform(value: any): any {
    return (value && value.length > 0) ? Iban.formatIban(value) : "";
  }
}
