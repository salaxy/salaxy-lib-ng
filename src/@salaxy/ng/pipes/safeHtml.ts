import { Pipe, PipeTransform } from "@angular/core";
import { DomSanitizer, SafeHtml } from "@angular/platform-browser";

/**
 * Renders safe HTML string as HTML.
 * Please note to pay special attention to whether the actual HTML source content can be regarded safe!
 *
 * <example-url>/#/examples/SafeHtmlPipeExample</example-url>
 *
 * @example
 * <on-card-content [innerHTML]="reportHtml.employer | safeHtml"></ion-card-content>
 */
@Pipe({
  name: "safeHtml"
})
export class SafeHtmlPipe implements PipeTransform {

  constructor(private sanitizer: DomSanitizer) {}

  /**
   * The Transform method that implements the Pipe
   * @param html - The HTML that we want to display.
   */
  public transform(html: string) {
    return this.sanitizer.bypassSecurityTrustHtml(html);
  }
}
