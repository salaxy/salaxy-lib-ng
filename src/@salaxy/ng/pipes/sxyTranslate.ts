import { Pipe, PipeTransform } from "@angular/core";
import { DomSanitizer, SafeHtml } from "@angular/platform-browser";

import { NgTranslations } from "../services/index";

import * as  markedImport from "marked";
/** workaround */
const marked = markedImport;

/**
 * Translate pipe with key and interpolation parameter.
 * Converts content with translation key ending with '.html' to safe html.
 * Converts content with translation key ending with '.md' first to html and then to safe html.
 * If the text contains a variable placeholder like: 'Hello {{name}}!' it will be replaced by given parameter.
 *
 * <example-url>/#/examples/TranslatePipeExample</example-url>
 *
 * @todo for enums should use a specialized enum translation pipe instead
 *
 * @example
 * <p>{{ 'SALAXY.NG.TEST.test1' | sxyTranslate }}</p>
 * <p>{{ 'SALAXY.NG.TEST.hello' | sxyTranslate: { name: 'John' } }}</p>
 */
@Pipe({
  name: "sxyTranslate",
  pure: false
})
export class SxyTranslatePipe implements PipeTransform {

  constructor(private sanitizer: DomSanitizer, private translate: NgTranslations) { }

  /**
   * The Transform method that implements the Pipe
   * @param translationId - Key for translation.
   * @param interpolateParams - Parameters for translation.
   */
  public transform(translationId: string, interpolateParams: object) {

    const endsWith = (str1: string, str2: string) => {
      return str1.substring(str1.length - str2.length, str1.length) === str2;
    };

    const str = this.translate.get(translationId, interpolateParams) as string;
    if (endsWith(translationId, ".html")) {
      return this.sanitizer.bypassSecurityTrustHtml(str);
    } else if (endsWith(translationId, ".md")) {
      return this.sanitizer.bypassSecurityTrustHtml(marked(str));
    }
    return str;
  }
}
