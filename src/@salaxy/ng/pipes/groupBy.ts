import { Pipe, PipeTransform } from "@angular/core";

/**
 * Provides a pipe that groups an array based on a field.
 *
 * <example-url>/#/examples/GroupByPipeExample</example-url>
 *
 * @example
 * {{ obj | groupBy:'color' }}
 */
@Pipe({
  name: "groupBy"
})
export class GroupByPipe implements PipeTransform {

  /**
   * The Transform method that implements the Pipe
   * @param value - Array to group
   * @param field - Name of the field based on which the grouping is done.
   */
  public transform(value: any[], field: string): any[] {
    const groupedObj = value.reduce((prev, cur) => {
      if (!prev[cur[field]]) {
        prev[cur[field]] = [cur];
      } else {
        prev[cur[field]].push(cur);
      }
      return prev;
    }, {});
    return Object.keys(groupedObj).map((key) => ({ key, value: groupedObj[key] }));
  }
}
