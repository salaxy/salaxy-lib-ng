import { NgModule } from "@angular/core";
import { HTTP_INTERCEPTORS } from "@angular/common/http";
import {
    AjaxNg,
    LoadingInterceptor,
    CalculationRepository,
    ClientNg,
    MyProfileRepository,
    NgTranslations,
    ProductRepository,
    SalaxyContext,
    SalaxyErrorHandler,
    TaxCardRepository,
    Token,
    WorkerRepository,
    ContactRepository,
    ContractRepository,
    OnboardingService,
} from "./index.services";

/**
 * Services module for Angular.io.
 * Can be used with either Ionic UX components (advanced with mobila capabilities)
 * or plain Angular components (simple, more a starting point implementation).
 */
@NgModule({
    declarations: [],
    exports: [],
    imports: [],
    providers: [
        AjaxNg,
        ClientNg,
        Token,
        NgTranslations,
        CalculationRepository,
        MyProfileRepository,
        ProductRepository,
        SalaxyContext,
        TaxCardRepository,
        WorkerRepository,
        ContactRepository,
        ContractRepository,
        OnboardingService,
        SalaxyErrorHandler,
       { provide: HTTP_INTERCEPTORS, useClass: LoadingInterceptor, multi: true },

    ],
})
export class SalaxyNgServicesModule { }
