import { Inject, Injectable } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { Translations } from "@salaxy/core";

/**
 * Wrapper on top of ngx-translate to provide consistent Translations and other
 * i18n infrastructure to Salaxy components and web sites using them.  NOTE: If
 * you need new methods from ngx-translate, you may add them here.  Also, the
 * idea is to always uyse this service instead of using ngx-translate
 * TranslateService directly, so that later we may move to another translations
 * framework
 * Note that views may use the Translate pipe directly (e.g.
 * "{{'SALAXY.ION.ComponentName.term' | sxyTranslate }}") We can then later
 * easily change how this pipe is resolved.
 */
@Injectable()
export class NgTranslations {

  /**
   * Constructor - creates a new instance of TranslateService with dependency injection.
   */
  constructor(@Inject(TranslateService) private translateSrv: TranslateService) {}

  /**
   * Gets the translated value of a key (or an array of keys) synchronously
   * Main point here is that we do not use the async promise infrastructure
   * of ngx-translate as it is obsolete for us. The translations must be
   * loaded as the app loads: It does not make sense to allow loading them
   * from server asyncronously.
   * @todo If target language is missing requested translation key, @ngx-translate
   * doesn't fallback to defaultLanguage's translation. Instead defaultLang
   * is used only if the requested language's translations are entirely missing.
   * In that case instant method returns the requested key. When that happens
   * we could check if that same key was returned and try to get default language's
   * translation string, but @ngx-translate doesn't provide method with a target lang...
   * https://github.com/ngx-translate/core/issues/719
   *
   * @param key
   * @param interpolateParams
   * @returns the translated key, or an object of translated keys
   */
  public get(key: string, interpolateParams?: object) {
    return this.translateSrv.instant(key, interpolateParams);
  }

  /**
   * Adds dictionary to the service from an imported grunt generated .ts dictionary
   * file that is given as the parameter. Object may include any number of language
   * codes (ISO 639-1 format: en, fi, sv...) as the first level keys.
   * @param dictionary dictionary object
   */
  public addDictionaries(dictionary: object) {
    for (const lang in dictionary) {
      if (dictionary.hasOwnProperty(lang)) {
        this.addDictionary(lang, dictionary[lang]);
      }
    }
  }

  /**
   * Adds a dictionary to the service for the given lang. The dictionary
   * translations are merged to any existing translations already in the module.
   * @param lang ISO language code for the dictionary to be added: fi, en or sv
   * @param dictionary dictionary object as the language's translations
   */
  public addDictionary(lang: string, dictionary: object) {
    this.translateSrv.setTranslation(lang, dictionary, true);
  }

  /**
   * Set the active language of the current user interface to the given language.
   * @param lang ISO 639-1 language code (en, fi, sv, ...)
   */
  public setLanguage(lang: string): void {
    // Set the same language for translations in core library
    Translations.setLanguage(lang);
    // Set language for Angular
    this.translateSrv.use(lang);
  }

  /**
   * Gets the current user interface language
   * @returns One of the supported languages: "fi", "en" or "sv".
   */
  public getLanguage(): string {
    return this.translateSrv.currentLang;
  }
}
