import { Inject, Injectable } from "@angular/core";
import { Ajax, Client } from "@salaxy/core";
import { AjaxNg } from "./AjaxNg";

/** A http client based on Angular http */
@Injectable()
export class ClientNg extends Client {

  constructor(@Inject(AjaxNg) ajax: Ajax) {
    super(ajax);
  }
}
