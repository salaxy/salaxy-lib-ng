import { Injectable } from "@angular/core";
import { Cookies } from "@salaxy/core";

/**
 * Singleton component that stores the authentication token for the current session.
 */
@Injectable()
export class Token {

  /** Key for the cookie. Set this to null if you do not want store / check from cookie */
  public cookieKey: string = "salaxy-token";

  private currentTokenState: string;

  /**
   * Gets / sets the current token.
   * Getting and setting using this implementation will also store the token in cookie.
   */
  public get currentToken(): string {
    if (!this.currentTokenState && this.cookieKey) {
      this.currentTokenState = new Cookies().get(this.cookieKey) || "";
    }
    return this.currentTokenState;
  }
  public set currentToken(token: string) {
    if (this.cookieKey) {
      new Cookies().setCookie(this.cookieKey, token);
    }
    this.currentTokenState = token;
  }
}
