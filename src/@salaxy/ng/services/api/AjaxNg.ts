import { Inject, Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpResponse } from "@angular/common/http";

// import { Observable } from 'rxjs';
import "rxjs/Rx";
import "rxjs/add/operator/toPromise";

import { Ajax } from "@salaxy/core";

import { SalaxyErrorHandler } from "../errors/SalaxyErrorHandler";
import { Token } from "./Token";

/**
 * The HttpClient access to the server methods: GET, POST and DELETE
 * with different return types and authentication / error events.
 */
@Injectable()
export class AjaxNg implements Ajax {

  /**
   * By default credentials are not used in http-calls.
   * Enable credentials with this flag.
   */
  public useCredentials = false;

  /**
   * The server address - root of the server
   * This is settable field. Will probably be changed to a configuration object in the final version.
   */
  public serverAddress;

  constructor(
    @Inject(HttpClient) private http: HttpClient,
    @Inject(Token) private token: Token,
    @Inject(SalaxyErrorHandler) private errorHandler: SalaxyErrorHandler,
  ) {}

  /** Gets the API address with version information. E.g. 'https://test-api.salaxy.com/v02/api' */
  public getApiAddress() {
    return this.serverAddress + "/v02/api";
  }

  /** Gets the Server address that is used as bases to the HTML queries. E.g. 'https://test-api.salaxy.com' */
  public getServerAddress() {
    return this.serverAddress;
  }

  /**
   * Gets a JSON-message from server using the API
   *
   * @param method - The API method is the url path after the api version segments (e.g. '/v02/api')
   *        and starts with a forward slash, e.g. '/calculator/new'.
   *
   * @return A Promise with result data. Standard Promise rejection to be used for error handling.
   */
  public getJSON(method: string): Promise<any> {

    let headers = new HttpHeaders({
      "Content-Type": "application/json; charset=UTF-8",
      "Accept": "application/json; charset=UTF-8",
    });

    const token = this.getCurrentToken();
    if (token) {
      headers = headers.append("Authorization", "Bearer " + token);
    }

    return this.http.get(this.getUrl(method), {
      headers,
      observe: "response",
      responseType: "json",
      withCredentials: (token) ? false : this.useCredentials,
    })
    .map(response => response.body)
    .toPromise()
    .catch(err => this.errorHandler.handleError(err));
  }

  /**
   * Gets a HTML-message from server using the API
   *
   * @param method - The API method is the url path after the api version segments (e.g. '/v02/api')
   *        and starts with a forward slash, e.g. '/calculator/new'.
   *
   * @return A Promise with result data. Standard Promise rejection to be used for error handling.
   */
  public getHTML(method: string): Promise<string> {

    let headers = new HttpHeaders({
      "Content-Type": "application/json; charset=UTF-8",
      "Accept": "application/json; charset=UTF-8",
    });

    const token = this.getCurrentToken();
    if (token) {
      headers = headers.append("Authorization", "Bearer " + token);
    }

    return this.http.get(this.getUrl(method), {
      headers,
      observe: "response",
      responseType: "text",
      withCredentials: (token) ? false : this.useCredentials,
    })
    .map(response => (response as HttpResponse<any>).body)
    .toPromise()
    .catch(err => this.errorHandler.handleError(err));
  }

  /**
   * POSTS data to server and receives back a JSON-message.
   *
   * @param method - The API method is the url path after the api version segments (e.g. '/v02/api')
   *        and starts with a forward slash, e.g. '/calculator/new'.
   * @param data - The data that is posted to the server.
   *
   * @return A Promise with result data. Standard Promise rejection to be used for error handling.
   */
  public postJSON(method: string, data: any): Promise<any> {

    let headers = new HttpHeaders({
      "Content-Type": "application/json; charset=UTF-8",
      "Accept": "application/json; charset=UTF-8",
    });

    const token = this.getCurrentToken();
    if (token) {
      headers = headers.append("Authorization", "Bearer " + token);
    }

    return this.http.post(this.getUrl(method), data, {
      headers,
      observe: "response",
      responseType: "json",
      withCredentials: (token) ? false : this.useCredentials,
    })
    .map(response => response.body)
    .toPromise()
    .catch(err => this.errorHandler.handleError(err));
  }

  /**
   * POSTS data to server and receives back HTML.
   *
   * @param method - The API method is the url path after the api version segments (e.g. '/v02/api')
   *        and starts with a forward slash, e.g. '/calculator/new'.
   * @param data - The data that is posted to the server.
   *
   * @return A Promise with result data. Standard Promise rejection to be used for error handling.
   */
  public postHTML(method: string, data: string): Promise<string> {

    let headers = new HttpHeaders({
      "Content-Type": "application/json; charset=UTF-8",
      "Accept": "application/json; charset=UTF-8",
    });

    const token = this.getCurrentToken();
    if (token) {
      headers = headers.append("Authorization", "Bearer " + token);
    }

    return this.http.post(this.getUrl(method), data, {
      headers,
      observe: "response",
      responseType: "text",
      withCredentials: (token) ? false : this.useCredentials,
    })
    .map(response => (response as HttpResponse<any>).body)
    .toPromise()
    .catch(err => this.errorHandler.handleError(err));
  }

  /**
   * Sends a DELETE-message to server using the API
   *
   * @param method - The API method is the url path after the api version segments (e.g. '/v02/api')
   *        and starts with a forward slash, e.g. '/calculator/new'.
   *
   * @return A Promise with result data. Standard Promise rejection to be used for error handling.
   */
  public remove(method: string): Promise<string> {

    let headers = new HttpHeaders({
      "Content-Type": "application/json; charset=UTF-8",
      "Accept": "application/json; charset=UTF-8",
    });

    const token = this.getCurrentToken();
    if (token) {
      headers = headers.append("Authorization", "Bearer " + token);
    }

    return this.http.delete(this.getUrl(method), {
      headers,
      observe: "response",
      responseType: "text",
      withCredentials: (token) ? false : this.useCredentials,
    })
    .map(response => (response as HttpResponse<any>).body)
    .toPromise()
    .catch(err => this.errorHandler.handleError(err));
  }

  /**
   * Gets the current token.
   * Will check the salaxy-token cookie if the token is persisted there
   */
  public getCurrentToken(): string {
    return this.token.currentToken;
  }

  /**
   * Sets the current token. Persists it to cookie until the browser window
   *
   * @param token - the authentication token to persist.
   */
  public setCurrentToken(token: string): void {
    this.token.currentToken = token;
  }

  /** If missing, append the API server address to the given url method string */
  private getUrl(method: string): string {
    return method.indexOf("http") !== 0 ? this.getApiAddress() + method : method;
  }
}
