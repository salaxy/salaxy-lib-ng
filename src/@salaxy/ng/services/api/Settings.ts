/**
 * Environment specific configuration for Salaxy API's and JavaScript in general
 */
export interface Settings {
  config: {
    apiServer?: string | null;
    isTestData?: boolean | null;
    partnerSite?: string | null;
    useCredentials?: boolean | null;
  };
  /** Method for getting any config value */
  getConfigValue(key: string): any;
}
