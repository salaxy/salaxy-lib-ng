import { Injectable, EventEmitter } from "@angular/core";
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import { finalize, tap } from "rxjs/operators";

/** Emits the isLoadingEvent for http requests. */
@Injectable()
export class LoadingInterceptor implements HttpInterceptor {

  private loadingCount: number = 0;
  private isLoadingEvent: EventEmitter<boolean> = new EventEmitter<boolean>();

  /** Sets isLoading on/off */
  public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    // Add counter for request
    this.loadingCount++;
    // Flag on
    this.isLoadingEvent.emit(true);

    return next
          .handle(req)
          .pipe(
            // Remove counter for response
            finalize(() => {
              if ((--this.loadingCount) === 0) {
                // Flag off
                this.isLoadingEvent.emit(false);
              }
            }));
  }

  /**
  * Returns the emitter indicating the loading state.
  */
  public get isLoading() {
    return this.isLoadingEvent;
  }
}
