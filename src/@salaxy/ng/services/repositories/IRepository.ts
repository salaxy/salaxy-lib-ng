import { ISingleItemRepository } from "./ISingleItemRepository";

/** Defines the repository interface for Salaxy CRUD objects */
export interface IRepository<T> extends ISingleItemRepository<T> {

  /** List of items in the repository */
  list: T[];

  /**
   * Reloads the list from the server - called e.g. after delete and add new
   */
  reloadList(): Promise<T[]>;

  /**
   * Sets the current repository item.
   * If not loaded, starts loading from the server to the current.
   *
   * @param id - Identifier of the item
   */
  setCurrentId(id: string);

  /**
   * Sets the current item
   *
   * @param item - Item to set as current
   */
  setCurrent(item: T);
}
