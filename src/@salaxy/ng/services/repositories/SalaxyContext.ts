import { Inject, Injectable } from "@angular/core";

import { Client } from "@salaxy/core";

import { ClientNg } from "../api/ClientNg";
import { Token } from "../api/Token";
import { SessionService } from "../account/SessionService";
import { CalculationRepository } from "./CalculationRepository";
import { MyProfileRepository } from "./MyProfileRepository";
import { ProductRepository } from "./ProductRepository";
import { TaxCardRepository } from "./TaxCardRepository";
import { WorkerRepository } from "./WorkerRepository";
import { ContactRepository } from "./ContactRepository";
import { ContractRepository } from "./ContractRepository";

/**
 * Context for Salaxy session: Either anonymous or based on authenticated Salaxy account.
 * Typically, you define this to be provided as a singleton in AppModule,
 * but in special cases you may even have several instances.
 *
 * @example
 * @NgModule({
 *   // declarations and imports here
 *   bootstrap: [AppComponent],
 *   providers: [SalaxyContext]
 * })
 * export class AppModule { }
 */
@Injectable()
export class SalaxyContext {

  /**
   * Session service that authenticates to the server and then has the infomration about the current account.
   */
  public session: SessionService;

  /** Creates a new SalaxyContext */
  constructor(
    @Inject(ClientNg) client: Client,
    public calculations: CalculationRepository,
    public workers: WorkerRepository,
    public contacts: ContactRepository,
    public contracts: ContractRepository,
    public taxCards: TaxCardRepository,
    public products: ProductRepository,
    public myProfile: MyProfileRepository,
    token: Token,
  ) {
    // Uncomment this to Disable cookies
    // token.cookieKey = null;
    this.session = new SessionService(client, token);

    if (window.location.hash && window.location.hash.indexOf("access_token") >= 0) {
      const hash = window.location.hash.replace("#", "");
      const tokenValue = hash.split("&")[0].replace("access_token=", "");
      window.location.hash = "/";
      this.setToken(tokenValue);
      return;
    }
    if (token.currentToken) {
      this.setToken(token.currentToken);
    }
  }

  /**
   * Sets the the current token and refreshes the session from the server.
   * @param token - The Salaxy token that is used for authentication
   */
  public setToken(token: string) {
    this.session.currentToken = token;
    this.session.checkSession().then(() => {
      if (this.session.isAuthenticated) {
        // TODO: Now we are initializing AND loading all resources from API after login...
        // We could defer initialization by, for example, defining injection configs
        // https://stackoverflow.com/questions/40558593/angular2-inject-a-service-that-requires-parameter-for-initialization
        // So we could call individual repository's reloadList method manually
        // and thus could pass callback functions for async actions (catching BehaviorSubject like with ContactsRepository).
        this.calculations.init(this.session);
        this.taxCards.init(this.session);
        if (this.session.isInRole("employer")) {
          this.workers.init(this.session);
          this.products.init(this.session);
        }
        this.contacts.init(this.session); // Should this be "if isInRole worker"?
        this.contracts.init(this.session);
        // At the moment, we do not initialize myProfile.
        // this.myProfile.init(this.session);
      }
    });
  }

}
