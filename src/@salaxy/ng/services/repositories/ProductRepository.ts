import { EventEmitter, Inject, Injectable } from "@angular/core";

import { AccountProducts, Client, Product, ProductsLogic } from "@salaxy/core";

import { ClientNg } from "../api/ClientNg";
import { SessionService } from "../account/SessionService";
import { IRepository } from "./IRepository";

/**
 * Products are the services and external products that are available as part of Salaxy service:
 * Insurance, Pension company, Taxation and Unemployment options.
 * To the end user this is typically shown as settings in a user interface.
 */
@Injectable()
export class ProductRepository implements IRepository<Product> {

  /** The currently selected product */
  public current: Product;

  /**
   * Raw list of products as defined by IRepository
   * For user interfaces it is typically easier to bind to products property.
   */
  public get list(): Product[] {
    const products = this.products;
    const vals = [];
    for (const key of Object.keys(products)) {
      // TODO: Potentially add some restrictions (visibility)?
      vals.push(products[key]);
    }
    return vals;
  }

  /**
   * Returns either the model from the server or an empty object that can be used in data binding.
   */
  public get products(): AccountProducts {
    return this.serverModel || this.anonModel;
  }

  private serverModel: AccountProducts = null;

  private _anonModel = null;

  private get anonModel(): AccountProducts {
    if (!this._anonModel) {
      // HACK: How do we get the role here?
      this._anonModel = ProductsLogic.getDefault("company");
    }
    return this._anonModel;
  }

  /** Creates a new instance of ProductRepository with dependency injection. */
  constructor(@Inject(ClientNg) private client: Client) {}

  /**
   * Assures the repository initialization:
   * Initialization is done after successful authentication.
   * Typically the call to fetch the repository list, but may fetch other stuff from the server.
   *
   * @param session - The session service that contains information about the authenticated user.
   */
  public init(session: SessionService): void {
    this.reloadList();
  }

  /** Gets a blank blank object */
  public getBlank?(): Product {
    return {};
  }

  /**
   * Gets a product of a given type.
   *
   * @param typeName Name of the type (the same as generic type T).
   */
  public getProductByType<T extends Product>(typeName: string): T {
    return ProductsLogic.getProductByType<T>(this.products, typeName, {} as any);
  }

  /**
   * Gets a product of a given id.
   *
   * @param id Identifier of the product.
   */
  public getProductById(id: string): Product {
    return ProductsLogic.getProductById(this.products, id, {});
  }

  /** Sets the current item using an id */
  public setCurrentId(id: string) {
    this.setCurrent(this.getProductById(id));
  }

  /** Sets the current item as given object. */
  public setCurrent(item: Product) {
    this.current = item;
  }

  /**
   * Sets the current object as a new one.
   * In the case of products, essentially making it null.
   */
  public newCurrent?() {
    this.current = {};
  }

  /**
   * Saves the entire products collection.
   *
   * @return A Promise with result data (AccountProducts)
   */
  public saveProducts(): Promise<AccountProducts> {
    if (this.serverModel) {
      return this.client.accountsApi.updateProducts(this.serverModel).then(result => {
        return this.serverModel = result;
      });
    } else {
      throw new Error("Saving in anonymous mode is not enabled.");
    }
  }

  /**
   * Saves the current object.
   * Note that currently this method saves the entire products property.
   * This may change later so that only the current product is changed.
   * This is not considered a breaking change. If you need consistent implementation, use saveAll() instead.
   */
  public saveCurrent(): Promise<Product> {
    if (1) {
      throw new Error("Method not implemented.");
    }
    return null as Promise<Product>;
  }

  /**
   * Saves the given object,
   */
  public save(product: Product): Promise<Product> {
    if (1) {
      throw new Error("Method not implemented.");
    }
    return null as Promise<Product>;
  }

  /**
   * NOT IMPLEMENTED: Delete method is currently not implemented.
   * Will probably be implemented later as "Unsubscribe".
   */
  public delete(id: string): never {
    throw new Error("Method delete not implemented.");
  }

  /**
   * Reloads the list from the server - called e.g. after delete and add new
   *
   * @return A Promise with result data (AccountProducts)
   */
  public reloadAll(): Promise<AccountProducts> {
    return this.client.accountsApi.getProducts().then(result => {
      return this.serverModel = result;
    });
  }

  /**
   * Reloads the list from the server - called e.g. after delete and add new
   *
   * @return A Promise with result data (Product[])
   */
  public reloadList(): Promise<Product[]> {
    return this.reloadAll().then(result => {
      let products = [];
      for (const key in result) {
        if (result.hasOwnProperty(key)) {
          products.push(result[key] as Product);
        }
      }
      return products;
    });
  }
}
