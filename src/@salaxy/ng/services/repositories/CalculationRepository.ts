import { EventEmitter, Inject, Injectable } from "@angular/core";

import { Calculation, Client, LegalEntityType, FrameworkAgreement, CalculationStatus, CalculatorLogic } from "@salaxy/core";

import { ClientNg } from "../api/ClientNg";
import { SessionService } from "../account/SessionService";
import { IRepository } from "./IRepository";

/**
 * Implements the salaxy repository pattern for Calculations stored for the current account.
 */
@Injectable()
export class CalculationRepository implements IRepository<Calculation> {

  /** List of items in the repository */
  public list: Calculation[] = [];

  /** Currently selected item in the repository */
  public current: Calculation = null;

  private changeEvent: EventEmitter<Calculation> = new EventEmitter<Calculation>();

  /** Creates a new instance of CalculationRepository with dependency injection. */
  constructor(@Inject(ClientNg) private client: Client) {}

  /**
   * Assures the repository initialization:
   * Initialization is done after successful authentication.
   * Typically the call to fetch the repository list, but may fetch other stuff from the server.
   *
   * @param session - The session service that contains information about the authenticated user.
   */
  public init(session: SessionService): void {
    this.reloadList();
  }

  /**
   * Reloads the list from the server - called e.g. after delete and add new
   *
   * @return A Promise with result data (Calculation[])
   */
  public reloadList(): Promise<Calculation[]> {
    return this.client.calculationsApi.getAll().then((result) => {
      return this.list = result;
    });
  }

  /**
   * Gets a new blank object with default values, suitable for UI binding.
   * This is a synchronous method - does not go to the server.
   *
   * @return Blank Calculation object
   */
  public getBlank(): Calculation {
    return CalculatorLogic.getBlank();
  }

  /**
   * Sets the current repository item.
   * If not loaded, starts loading from the server to the current.
   *
   * @param id - Identifier of the calculation
   */
  public setCurrentId(id: string): void {
    for (const calc of this.list) {
      if (calc.id === id) {
        this.setCurrent(calc);
        return;
      }
    }
    this.client.calculationsApi.getSingle(id).then(result => this.setCurrent(result));
  }

  /**
   * Sets the current calculation
   *
   * @param item - Item to set as current
   */
  public setCurrent(item: Calculation): void {
    this.current = item;
    this.changeEvent.emit(this.current);
  }

  /**
   * Creates a new item and sets it as current for editing and saving.
   * The item is not yet sent to server and thus it is not in the list, nor does it have an identifier.
   */
  public newCurrent(): void {
    this.setCurrent(this.getBlank());
  }

  /**
   * Saves changes to the current item.
   */
  public saveCurrent(): Promise<Calculation> {
    if (this.current) {
      return this.save(this.current);
    } else {
      // console.error("SaveCurrent called when current is not set.");
    }
  }

  /**
   * Adds or updates a given item.
   * Operation is determined based on id: null/''/'new' adds, other string values update.
   *
   * @param item - Item that is updated.
   *
   * @return A Promise with result data (Calculation)
   */
  public save(item: Calculation): Promise<Calculation> {
    return this.client.calculationsApi.save(item).then(result => {
      this.setCurrent(result);
      return this.reloadList().then(() => {
        return result;
      });
    });
  }

  /**
   * Deletes the given item from repository if possible
   *
   * @param id - Identifier of the item to delete
   *
   * @return A Promise with result data ("Object deleted")
   */
  public delete(id: string): Promise<string> {
    return this.client.calculationsApi.delete(id).then(result => {
      return this.reloadList().then(() => {
        return result;
      });
    });
  }

  /**
   * Recalculates the current item
   *
   * @return A Promise with result data (Calculation)
   */
  public recalculateCurrent(): Promise<Calculation> {
    return this.client.calculatorApi.recalculate(this.current).then(result => {
      this.setCurrent(result);
      return result;
    });
  }

  /**
   * Returns change emitter.
   */
  public onChange(): EventEmitter<Calculation> {
    return this.changeEvent;
  }
}
