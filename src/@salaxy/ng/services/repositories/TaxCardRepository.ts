import { EventEmitter, Inject, Injectable } from "@angular/core";

import { Dates, Client, TaxCard, TaxCardType, TaxCardLogic } from "@salaxy/core";

import { ClientNg } from "../api/ClientNg";
import { SessionService } from "../account/SessionService";
import { IRepository } from "./IRepository";

/* tslint:disable */
/* TODO: Waiting for Go-through */

/**
 * Implements the salaxy repository pattern for TaxCards stored for the current account.
 */
@Injectable()
export class TaxCardRepository implements IRepository<TaxCard> {

  /** List of items in the repository */
  public list: TaxCard[] = [];

  /** The tax card that is currently selected to edit or detail view */
  public current: TaxCard = null;

  /** The latest tax card for the current user in role Worker. */
  public latestTaxCard: TaxCard = null;

  private session: SessionService;

  /** Creates a new instance of TaxCardRepository with dependency injection. */
  constructor(@Inject(ClientNg) private client: Client) {}

  /**
   * Assures the repository initialization:
   * Initialization is done after successful authentication.
   * Typically the call to fetch the repository list, but may fetch other stuff from the server.
   *
   * @param session - The session service that contains information about the authenticated user.
   */
  public init(session: SessionService): void {
    this.session = session;
    this.reloadList();
  }

  /**
   * Reloads the list from the server - called e.g. after delete and add new
   *
   * @return A Promise with result data (TaxCard[])
   */
  public reloadList(): Promise<TaxCard[]> {
    return this.client.taxcardsApi.getAll().then(result => {
      return this.list = result;
    });
  }

  /*
    * Active card for currrent user
    */
  public getActiveCardForCurrentUser(): TaxCard | void {
    // HACK: Dummy code => Make real implementation with proper logic (January with previous year etc).
    return this.list.filter((x) => x.card.forYear === 2017)[0];
  }

  /*
    * Get list of tax cards
    */
  public getList(): TaxCard[] {
    return this.list;
  }

  /**
   * Gets a new blank object with default values, suitable for UI binding.
   *
   * @return A Blank TaxCard object
   */
  public getBlank(): TaxCard {
    if (this.session && this.session.isInRole("worker")) {
      return TaxCardLogic.getBlank(this.session.personAccount);
    }
    return TaxCardLogic.getBlank();
  }

  /**
   * Sets the current repository item.
   * If not loaded, starts loading from the server to the current.
   *
   * @param id - Identifier of the calculation
   */
  public setCurrentId(id: string): void {
    const taxCard = this.list.find((x) => x.id === id);
    if (taxCard) {
      this.setCurrent(taxCard);
    }
  }

  /**
   * Sets the current calculation
   *
   * @param item - Item to set as current
   */
  public setCurrent(item: TaxCard): void {
    this.current = item;
  }

  /**
   * Creates a new item and sets it as current for editing and saving.
   * The item is not yet sent to server and thus it is not in the list, nor does it have an identifier.
   */
  public newCurrent(): void {
    this.setCurrent(this.getBlank());
  }

  /**
   * Saves changes to the current item.
   *
   * @return A Promise with result data (TaxCard)
   */
  public saveCurrent(): Promise<TaxCard> {
    if (this.current) {
      return this.save(this.current);
    } else {
      // console.error("SaveCurrent called when current is not set.");
    }
  }

  /**
   * Adds or updates a given item.
   * Operation is determined based on the given item's id value.
   * When id is null, '', or 'new' this method adds a new item, and otherwise updates.
   *
   * @param item - Item that is updated.
   *
   * @return A Promise with result data (TaxCard)
   */
  public save(item: TaxCard): Promise<TaxCard> {
    return this.client.taxcardsApi.save(item).then(result => {
      this.setCurrent(result);
      return this.reloadList().then(() => {
        return result;
      });
    });
  }

  /**
   * Deletes the given item from repository if possible
   *
   * @param id - Identifier of the item to delete
   *
   * @return A Promise with result data ("Object deleted")
   */
  public delete(id: string): Promise<string> {
    return this.client.taxcardsApi.delete(id).then(result => {
      return this.reloadList().then(() => {
        return result;
      });
    });
  }

  /**
   * Gets the latest tax card for given personal id (Finnish HETU or SOTU)
   *
   * @param personalId - Identifier for the person
   *
   * @return A Promise with result data (TaxCard)
   */
  public getLatestTaxCard(personalId: string): Promise<TaxCard> {
    if (personalId != null) {
      return this.client.taxcardsApi.getCurrentTaxCard(personalId).then(result => {
        return this.latestTaxCard = result;
      });
    }
  }
}
