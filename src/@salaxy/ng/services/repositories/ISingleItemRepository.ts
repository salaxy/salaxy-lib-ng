import { SessionService } from "../account/SessionService";

/**
 * Defines CRUD methods for editing a single (current) item.
 * This is simplified part of IRepository:
 * The part that does not require server to have a list of items.
 */
export interface ISingleItemRepository<T> {

  /** Currently selected item in the repository */
  current: T;

  /**
   * Assures the repository initialization:
   * Initialization is done after successful authentication (dependeing on the configuration).
   * Typically the call to fetch the repository list, but may fetch other stuff from the server.
   *
   * @param session - The session service that contains information about the authenticated user.
   */
  init?(session?: SessionService);

  /**
   * Gets a new blank object with default values, suitable for UI binding.
   * This is a synchronous method - does not go to the server and it is not 100% reliable in that way.
   * However, it shoud provide a basic object, good enough for most views.
   */
  getBlank?(): T;

  /**
   * Creates a new item and sets it as current for editing and saving.
   * The item is not yet sent to server and thus it is not in the list, nor does it have an identifier.
   */
  newCurrent?() ;

  /**
   * Saves changes to the current item.
   */
  saveCurrent(): Promise<T>;

  /**
   * Adds or updates a given item.
   * Operation is determined based on id: null/''/'new' adds, other string values update.
   *
   * @param item - Item that is updated.
   */
  save(item: T): Promise<T>;

  /**
   * Deletes the given item from repository if possible
   *
   * @param id - Identifier of the item to delete
   */
  delete?(id: string): Promise<any>;

}
