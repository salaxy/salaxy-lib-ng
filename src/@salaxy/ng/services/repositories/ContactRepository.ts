import { Inject, Injectable } from "@angular/core";

import { ContractParty, Client, AvatarPictureType, ContractPartyLogic, ContractPartyType } from "@salaxy/core";

import { ClientNg } from "../api/ClientNg";
import { SessionService } from "../account/SessionService";
import { IRepository } from "./IRepository";

/**
 * Implements the salaxy repository pattern for Contacts (ContractParty) stored for the current account.
 */
@Injectable()
export class ContactRepository implements IRepository<ContractParty> {

  /** List of items in the repository */
  public list: ContractParty[] = [];

  /** Currently selected item in the repository */
  public current: ContractParty = null;

  /** The session service, only available after init() method has been called, so this service is not available if init() has not been called. */
  private sessionService: SessionService;

  constructor(@Inject(ClientNg) private client: Client) {}

  /**
   * Assures the repository initialization:
   * Initialization is done after successful authentication.
   * Typically the call to fetch the repository list, but may fetch other stuff from the server.
   *
   * @param session - The session service that contains information about the authenticated user.
   */
  public init(session: SessionService): void {
    this.sessionService = session;
    this.reloadList();
  }

  /**
   * Reloads the list from the server - called e.g. after delete and add new.
   * ContactsAPI returns customContacts as well as persons (workers, employers).
   *
   * @return A Promise with result data (ContractParty[])
   */
  public reloadList(): Promise<ContractParty[]> {
    return this.client.contactsApi.getAll().then(result => {
      // Filter list to contain only CustomContacts and those Person contacts that have acted as employers for the current user
      return this.list = result.filter((item) => {
        return item.contractPartyType === ContractPartyType.CustomContact || (item.contractPartyType === ContractPartyType.Person && item.isEmployer === true);
      });
    });
  }

  /**
   * Gets a new blank Contact with default values, suitable for UI binding.
   *
   * @return A Blank ContractParty object
   */
  public getBlank(): ContractParty {
    return ContractPartyLogic.getBlank();
  }

  /**
   * Creates a new item and sets it as current for editing and saving.
   * The item is not yet sent to server and thus it is not in the list, nor does it have an identifier.
   */
  public newCurrent(): void {
    this.setCurrent(this.getBlank());
  }

  /**
   * Sets the current repository item.
   * If not loaded, starts loading from the server to the current.
   *
   * @param id - Identifier of the calculation
   */
  public setCurrentId(id: string): void {
    for (const contact of this.list) {
      if (contact.id === id) {
        this.setCurrent(contact);
        return;
      }
    }
    this.setCurrent(null);
  }

  /**
   * Sets the current calculation
   *
   * @param item - Item to set as current
   */
  public setCurrent(item: ContractParty): void {
    this.current = item;
  }

  /**
   * Saves changes to the current item.
   *
   * @return A Promise with result data (ContractParty)
   */
  public saveCurrent(): Promise<ContractParty> {
    if (this.current) {
      return this.save(this.current);
    }
  }

  /**
   * Adds or updates a given item.
   * Operation is determined based on id: null/''/'new' adds, other string values update.
   *
   * @param item - Item that is updated.
   *
   * @return A Promise with result data (ContractParty)
   */
  public save(item: ContractParty): Promise<ContractParty> {
    return this.client.contactsApi.save(item).then(result => {
      this.setCurrent(result);
      return this.reloadList().then(() => {
        return result;
      });
    });
  }

  /**
   * Deletes the given item from repository if possible
   *
   * @param id - Identifier of the item to delete
   *
   * @return A Promise with result data ("Object deleted")
   */
  public delete(id: string): Promise<string> {
    return this.client.contactsApi.delete(id).then(result => {
      return this.reloadList().then(() => {
        return result;
      });
    });
  }
}
