import { EventEmitter, Inject, Injectable } from "@angular/core";

import { City, Client, Profile, ProfileJob, SalaryPeriod, PartnerSite, Gender, DateOfBirthAccuracy } from "@salaxy/core";

import { ClientNg } from "../api/ClientNg";
import { SessionService } from "../account/SessionService";

/**
 * Repository for managing user Worker profile pages data ("Osaajaprofiili").
 * NOTE: It is unclear whether this repository will be supported in the future.
 * Therefore, the implementation is pretty light.
 * Please give feedback, if you need it! Otherwise we may remove it!
 */
@Injectable()
export class MyProfileRepository {

  /** List of Jobs for editor component */
  public jobsForEdit: ProfileJob[];

  /** List of cities for editor component */
  public citiesForEdit: City[];

  /**
   * The current editable profile.
   * When called the first time, initializes the service. Fetches the profile from server as well as Jobs and Cities lists.
   *
   * NOTE: If the current account is anonymous when the property is first called, this will lead to Profile never being fetched from server.
   * This is an intended behavior and should be fixed only if repository is used in production
   * in such a scenario where user is potentially first anonymous and only after that becomes authenticated.
   * When writing, the assumption was that such a simple lazy-loading mechanism is enough <= Way may never take this functionality wide use.
   * IF improving, the cities and jobs could be moved to core library - not fetched from the server at all.
   */
  public get myProfile(): Profile {
    if (this._myProfile == null) {
      this._myProfile = this.getBlankProfile();
      this.reloadMyProfile();
      this.getJobsForEdit();
      this.getCitiesForEdit();
    }
    return this._myProfile;
  }

  private _myProfile: Profile;

  /** Creates a new instance of MyProfileRepository with dependency injection. */
  constructor(@Inject(ClientNg) private client: Client) {}

  /**
   * Forces the reload of profile from the server.
   * Currently, the method catches the 401/Unauthorized.
   * This may be an issue if myProfile getter is called before the authentication happens.
   *
   * @return A Promise with result data (Profile), if session is active.
   */
  public reloadMyProfile(): Promise<Profile> {
    if (!this.client.ajax.getCurrentToken()) {
      console.info("MyProfileRepository running as ANON. See reloadMyProfile() for details if this is an issue.");
    } else {
      return this.client.profilesApi.getMyProfile().then(result => {
        return this._myProfile = result;
      });
    }
  }

  /**
   * Saves the profile to the server
   *
   * @return A Promise with result data (Profile)
   */
  public save(): Promise<Profile> {
    return this.client.profilesApi.saveMyProfile(this.myProfile);
  }

  /**
   * Gets the list of all jobs.
   * If the list has not been loaded from the server, returns an empty array and initializes the GET from the server.
   *
   * @return A Promise with result data (ProfileJob[]) when dataset not fetched yet, else the fetched dataset.
   */
  public getJobsForEdit(): Promise<ProfileJob[]> | ProfileJob[] {
    return (!this.jobsForEdit) ? this.client.profilesApi.getJobs().then(result => {
      return this.jobsForEdit = result;
    }) : this.jobsForEdit;
  }

  /**
   * Gets the list of Finnish cities.
   * If the list has not been loaded from the server, returns an empty array and initializes the GET from the server.
   *
   * @return A Promise with result data (City[]) when dataset not fetched yet, else the fetched dataset.
   */
  public getCitiesForEdit(): Promise<City[]> | City[] {
    return (!this.citiesForEdit) ? this.client.profilesApi.getCities().then(result => {
      return this.citiesForEdit = result;
    }) : this.citiesForEdit;
  }

  private todoGetBackgroundImages() {
    /*
    new SelectListItem() { Text = "arkkitehti", Value = "/img/pro/cover/arkkitehti.jpg"},
    new SelectListItem() { Text = "autonpesu", Value = "/img/pro/cover/autonpesu.jpg"},
    new SelectListItem() { Text = "autopesu auto", Value = "/img/pro/cover/autopesu_auto.jpg"},
    new SelectListItem() { Text = "betoniraudoittaja", Value = "/img/pro/cover/betoniraudoittaja.jpg"},
    new SelectListItem() { Text = "catering", Value = "/img/pro/cover/catering.jpg"},
    new SelectListItem() { Text = "elektroniikan asennus", Value = "/img/pro/cover/elektroniikan_asennus.jpg"},
    new SelectListItem() { Text = "esiintyja", Value = "/img/pro/cover/esiintyja.jpg"},
    new SelectListItem() { Text = "halkopino", Value = "/img/pro/cover/halkopino.jpg"},
    new SelectListItem() { Text = "harjat", Value = "/img/pro/cover/harjat.jpg"},
    new SelectListItem() { Text = "hieroja", Value = "/img/pro/cover/hieroja.jpg"},
    new SelectListItem() { Text = "hoitotyo", Value = "/img/pro/cover/hoitotyo.jpg"},
    new SelectListItem() { Text = "ikkunanpesu", Value = "/img/pro/cover/ikkunanpesu.jpg"},
    new SelectListItem() { Text = "johdot", Value = "/img/pro/cover/johdot.jpg"},
    new SelectListItem() { Text = "joulupukki", Value = "/img/pro/cover/joulupukki.jpg"},
    new SelectListItem() { Text = "kalusteidenkasaaja", Value = "/img/pro/cover/kalusteidenkasaaja.jpg"},
    new SelectListItem() { Text = "kamera", Value = "/img/pro/cover/kamera.jpg"},
    new SelectListItem() { Text = "kieltenopetus", Value = "/img/pro/cover/kieltenopetus.jpg"},
    new SelectListItem() { Text = "kiinteistonhoitaja", Value = "/img/pro/cover/kiinteistonhoitaja.jpg"},
    new SelectListItem() { Text = "kodinhoitaja", Value = "/img/pro/cover/kodinhoitaja.jpg"},
    new SelectListItem() { Text = "koodari", Value = "/img/pro/cover/koodari.jpg"},
    new SelectListItem() { Text = "kuvankasittely", Value = "/img/pro/cover/kuvankasittely.jpg"},
    new SelectListItem() { Text = "laatoittaja", Value = "/img/pro/cover/laatoittaja.jpg"},
    new SelectListItem() { Text = "lastenlelut", Value = "/img/pro/cover/lastenlelut.jpg"},
    new SelectListItem() { Text = "lauta naula", Value = "/img/pro/cover/lauta_naula.jpg"},
    new SelectListItem() { Text = "lemmikki", Value = "/img/pro/cover/lemmikki.jpg"},
    new SelectListItem() { Text = "lisaopetus", Value = "/img/pro/cover/lisaopetus.jpg"},
    new SelectListItem() { Text = "lumityot", Value = "/img/pro/cover/lumityot.jpg"},
    new SelectListItem() { Text = "lvi-asentaja", Value = "/img/pro/cover/lvi-asentaja.jpg"},
    new SelectListItem() { Text = "maalari", Value = "/img/pro/cover/maalari.jpg"},
    new SelectListItem() { Text = "metsanhoito", Value = "/img/pro/cover/metsanhoito.jpg"},
    new SelectListItem() { Text = "metsanhoito traktori", Value = "/img/pro/cover/metsanhoito_traktori.jpg"},
    new SelectListItem() { Text = "mittanauha", Value = "/img/pro/cover/mittanauha.jpg"},
    new SelectListItem() { Text = "musiikki", Value = "/img/pro/cover/musiikki.jpg"},
    new SelectListItem() { Text = "muuraus", Value = "/img/pro/cover/muuraus.jpg"},
    new SelectListItem() { Text = "muusikko", Value = "/img/pro/cover/muusikko.jpg"},
    new SelectListItem() { Text = "muusikko bandi", Value = "/img/pro/cover/muusikko_bandi.jpg"},
    new SelectListItem() { Text = "muuttoapu", Value = "/img/pro/cover/muuttoapu.jpg"},
    new SelectListItem() { Text = "opetus", Value = "/img/pro/cover/opetus.jpg"},
    new SelectListItem() { Text = "parturi", Value = "/img/pro/cover/parturi.jpg"},
    new SelectListItem() { Text = "pehmolelut", Value = "/img/pro/cover/pehmolelut.jpg"},
    new SelectListItem() { Text = "personal trainer", Value = "/img/pro/cover/personal_trainer.jpg"},
    new SelectListItem() { Text = "pesu ikkuna", Value = "/img/pro/cover/pesu_ikkuna.jpg"},
    new SelectListItem() { Text = "pesu puhdistus", Value = "/img/pro/cover/pesu_puhdistus.jpg"},
    new SelectListItem() { Text = "putkimies", Value = "/img/pro/cover/putkimies.jpg"},
    new SelectListItem() { Text = "puuseppa", Value = "/img/pro/cover/puuseppa.jpg"},
    new SelectListItem() { Text = "puutarha kottikarry", Value = "/img/pro/cover/puutarha_kottikarry.jpg"},
    new SelectListItem() { Text = "puutarha metsa", Value = "/img/pro/cover/puutarha_metsa.jpg"},
    new SelectListItem() { Text = "rakennuspiirtaja suunnittelija", Value = "/img/pro/cover/rakennuspiirtaja_suunnittelija.jpg"},
    new SelectListItem() { Text = "rappaaja", Value = "/img/pro/cover/rappaaja.jpg"},
    new SelectListItem() { Text = "ruohonleikkuri", Value = "/img/pro/cover/ruohonleikkuri.jpg"},
    new SelectListItem() { Text = "sahkomies", Value = "/img/pro/cover/sahkomies.jpg"},
    new SelectListItem() { Text = "siivooja", Value = "/img/pro/cover/siivooja.jpg"},
    new SelectListItem() { Text = "sisustussuunnittelija", Value = "/img/pro/cover/sisustussuunnittelija.jpg"},
    new SelectListItem() { Text = "sisustussuunnittelija varit", Value = "/img/pro/cover/sisustussuunnittelija_varit.jpg"},
    new SelectListItem() { Text = "soittimet", Value = "/img/pro/cover/soittimet.jpg"},
    new SelectListItem() { Text = "talonrakennus", Value = "/img/pro/cover/talonrakennus.jpg"},
    new SelectListItem() { Text = "tietokoneasentaja", Value = "/img/pro/cover/tietokoneasentaja.jpg"},
    new SelectListItem() { Text = "tyokalut", Value = "/img/pro/cover/tyokalut.jpg"},
    new SelectListItem() { Text = "vauva", Value = "/img/pro/cover/vauva.jpg"},
    new SelectListItem() { Text = "web-suunnittelija", Value = "/img/pro/cover/web-suunnittelija.jpg"},
    */
  }

  private getBlankProfile(): Profile {
    return {
      job: {
        backgroundImage: "https://cdn.salaxy.com/img/pro/cover/tyokalut.jpg",
        jobs: [],
        cities: [],
        links: [],
        salaryPeriod: SalaryPeriod.Hourly,
      },
      avatar: {
        firstName: "Anonyymi",
        lastName: "Käyttäjä",
        color: "#60df96",
        initials: "N.N.",
        displayName: "Anonyymi käyttäjä",
      },
      contact: {},
      entityType: "person",
      source: PartnerSite.Palkkaus,
      worker: {
        gender: Gender.Unknown,
        hasTaxCard: false,
        dateOfBirthAccuracy: DateOfBirthAccuracy.Assumption,
      },
    };
  }
}
