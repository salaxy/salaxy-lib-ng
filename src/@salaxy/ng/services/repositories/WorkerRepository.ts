
import { EventEmitter, Inject, Injectable} from "@angular/core";

import { Client, WorkerAccount, WorkerLogic } from "@salaxy/core";

import { ClientNg } from "../api/ClientNg";
import { SessionService } from "../account/SessionService";
import { IRepository } from "./IRepository";

/**
 * Implements the salaxy repository pattern for Workers stored for the current account.
 */
@Injectable()
export class WorkerRepository implements IRepository<WorkerAccount> {

  /** List of items in the repository */
  public list: WorkerAccount[] = [];

  /** Currently selected item in the repository */
  public current: WorkerAccount = null;

  constructor(@Inject(ClientNg) private client: Client) {}

  /**
   * Assures the repository initialization:
   * Initialization is done after successful authentication.
   * Typically the call to fetch the repository list, but may fetch other stuff from the server.
   *
   * @param session - The session service that contains information about the authenticated user.
   */
  public init(session: SessionService): void {
    if (session.companyAccount || (session.personAccount && session.personAccount.isEmployer)) {
      this.reloadList();
    }
  }

  /**
   * Reloads the list from the server - called e.g. after delete and add new
   *
   * @return A Promise with result data (WorkerAccount[])
   */
  public reloadList(): Promise<WorkerAccount[]> {
    return this.client.workersApi.getAll().then(workers => {
      return this.list = workers;
    });
  }

  /**
   * Gets a new blank Worker account object with default values, suitable for UI binding.
   *
   * @return A Blank WorkerAccount object
   */
  public getBlank(): WorkerAccount {
    return WorkerLogic.getBlank();
  }

  /**
   * Sets the current repository item.
   * If not loaded, starts loading from the server to the current.
   *
   * @param id - Identifier of the calculation
   */
  public setCurrentId(id: string): void {
    for (const worker of this.list) {
      if (worker.id === id) {
        this.setCurrent(worker);
        return;
      }
    }
    this.setCurrent(null);
  }

  /**
   * Sets the current calculation
   *
   * @param item - Item to set as current
   */
  public setCurrent(item: WorkerAccount): void {
    this.current = item;
  }

  /**
   * Creates a new item and sets it as current for editing and saving.
   * The item is not yet sent to server and thus it is not in the list, nor does it have an identifier.
   */
  public newCurrent(): void {
    this.setCurrent(this.getBlank());
  }

  /**
   * Saves changes to the current item.
   *
   * @return A Promise with result data (WorkerAccount)
   */
  public saveCurrent(): Promise<WorkerAccount> {
    if (this.current) {
      return this.save(this.current);
    } else {
      // console.error("SaveCurrent called when current is not set.");
    }
  }

  /**
   * Adds or updates a given item.
   * Operation is determined based on the given item's id value.
   * When id is null, '', or 'new' this method adds a new item, and otherwise updates.
   *
   * @param item - Item that is updated.
   *
   * @return A Promise with result data (WorkerAccount)
   */
  public save(item: WorkerAccount): Promise<WorkerAccount> {
    return this.client.workersApi.save(item).then(result => {
      this.setCurrent(result);
      return this.reloadList().then(() => {
        return result;
      });
    });
  }

  /**
   * Deletes the given item from repository if possible
   *
   * @param id - Identifier of the item to delete
   *
   * @return A Promise with result data ("Object deleted")
   */
  public delete(id: string): Promise<string> {
    return this.client.workersApi.delete(id).then(result => {
      return this.reloadList().then(() => {
        return result;
      });
    });
  }
}
