
import { EventEmitter, Inject, Injectable } from "@angular/core";

import { Client, EmploymentContract, ContractLogic } from "@salaxy/core";

import { ClientNg } from "../api/ClientNg";
import { SessionService } from "../account/SessionService";
import { IRepository } from "./IRepository";

/**
 * Implements the salaxy repository pattern for Contracts stored for the current account.
 */
@Injectable()
export class ContractRepository implements IRepository<EmploymentContract> {

  /** List of items in the repository */
  public list: EmploymentContract[] = [];

  /** Currently selected item in the repository */
  public current: EmploymentContract = null;

  /** The session service, only available after init() method has been called, so this service is not available if init() has not been called. */
  private sessionService: SessionService;

  constructor(@Inject(ClientNg) private client: Client) {}

  /**
   * Assures the repository initialization:
   * Initialization is done after successful authentication.
   * Typically the call to fetch the repository list, but may fetch other stuff from the server.
   *
   * @param session - The session service that contains information about the authenticated user.
   */
  public init(session: SessionService): void {
    this.sessionService = session;
    if (session.companyAccount || (session.personAccount && session.personAccount.isEmployer)) {
      this.reloadList();
    }
  }

  /**
   * Reloads the list from the server - called e.g. after delete and add new
   *
   * @return A Promise with result data (EmploymentContract[])
   */
  public reloadList(): Promise<EmploymentContract[]> {
    return this.client.contractsApi.getAll().then(result => {
      return this.list = result;
    });
  }

  /**
   * Gets a new blank Employment Contract with default values, suitable for UI binding.
   * Either Employer or Worker is set to current account depending on the current user role.
   *
   * @return A Blank EmploymentContract object
   */
  public getBlank(): EmploymentContract {
    if (this.sessionService) {
      return ContractLogic.getBlankForCurrentAccount(this.sessionService.isInRole("worker"), this.sessionService.companyAccount || this.sessionService.personAccount);
    }
    return ContractLogic.getBlank();
  }

  /**
   * Sets the current repository item.
   * If not loaded, starts loading from the server to the current.
   *
   * @param id - Identifier of the calculation
   */
  public setCurrentId(id: string): void {
    for (const contract of this.list) {
      if (contract.id === id) {
        this.setCurrent(contract);
        return;
      }
    }
    this.setCurrent(null);
  }

  /**
   * Sets the current calculation
   *
   * @param item - Item to set as current
   */
  public setCurrent(item: EmploymentContract): void {
    this.current = item;
  }

  /**
   * Creates a new item and sets it as current for editing and saving.
   * The item is not yet sent to server and thus it is not in the list, nor does it have an identifier.
   */
  public newCurrent(): void {
    this.setCurrent(this.getBlank());
  }

  /**
   * Saves changes to the current item.
   *
   * @return A Promise with result data (EmploymentContract)
   */
  public saveCurrent(): Promise<EmploymentContract> {
    if (this.current) {
      return this.save(this.current);
    }
  }

  /**
   * Adds or updates a given item.
   * Operation is determined based on the given item's id value.
   * When id is null, '', or 'new' this method adds a new item, and otherwise updates.
   *
   * @param item - Item that is updated.
   *
   * @return A Promise with result data (EmploymentContract)
   */
  public save(item: EmploymentContract): Promise<EmploymentContract> {
    return this.client.contractsApi.save(item).then(result => {
      this.setCurrent(result);
      return this.reloadList().then(() => {
        return result;
      });
    });
  }

  /**
   * Deletes the given item from repository if possible
   *
   * @param id - Identifier of the item to delete
   *
   * @return A Promise with result data ("Object deleted")
   */
  public delete(id: string): Promise<string> {
    return this.client.contractsApi.delete(id).then(result => {
      return this.reloadList().then(() => {
        return result;
      });
    });
  }

  /**
   * Gets the preview HTML for an employment contract.
   *
   * @param contract - Contract for which the preview is fetched.
   *
   * @returns A Promise with result data (preview HTML string)
   */
  public getPreview(contract: EmploymentContract): Promise<string> {
    return this.client.contractsApi.getPreview(contract);
  }

  /**
   * Gets an URL for the Employment contract preview.
   * May be used in linked previews (to new window) or iframes (you may need to sanitize the URL).
   *
   * @param id IDentifier of the contract.
   *
   * @return A Preview url string
   */
  public getPreviewUrl(id: string): string {
    return this.client.ajax.getServerAddress() + "/reporthtml/employmentcontract/" + id + "?access_token=" + this.client.ajax.getCurrentToken();
  }

}
