import { Inject, Injectable, EventEmitter } from "@angular/core";

import { Client, CompanyAccount, PersonAccount, WebSiteUserRole, UserSession } from "@salaxy/core";

import { ClientNg } from "../api/ClientNg";
import { Token } from "../api/Token";

/** Session that manages current session and provides information about the user, account and roles.  */
@Injectable()
export class SessionService {

  /**
   * Primary role of the current user.
   * This is more restricted system role than the ones in isInRole() function.
   * @readonly - Use switchRole: This field is considered readonly and may be changed later.
   */
  public role: "worker" | "household" | "company" = "household";

  /**
   * Gets an array of custom roles.
   * @readonly - Use addCustomRole: This array is considered readonly and may be changed later.
   */
  public customRoles: string[] = [];

  /** Avatar to show in the login screen */
  public avatarState: any;

  /** Called when the session is changed: Typically from not checked to authenticated or anonymous */
  public sessionChanged: EventEmitter<boolean> = new EventEmitter();

  /** If true, the user is authenticated */
  private isAuthenticatedState: boolean;

  /** If true, the session has been checked from the server */
  private isSessionCheckedState: boolean;

  /** The full session objcet */
  private sessionState: any;

  constructor(
    @Inject(ClientNg) private client: Client,
    private token: Token,
  ) {
    this.checkSession();
  }

  /** If true, the user is authenticated */
  public get isAuthenticated() {
    return this.isAuthenticatedState;
  }

  /**
   * Returns true if the current user is in the given role.
   *
   * Supports the following role strings:
   *
   * - Primary Salaxy roles: "worker" | "household" | "company"
   * - "employer" for Employer, which can be either "household" or "company"
   * - "auth" for Authenticated user (isAuthenticated)
   * - "anon" for Anonymous user (isSessionChecked AND !isAuthenticated)
   * - "unknown" for Session check in progress (!isSessionChecked)
   *
   * We will probably add new roles here: "expired", "partner" etc.
   * Also Consider adding possibility for application specific roles that could utilize this same structure.
   *
   * @param role Role that is being checked.
   */
  public isInRole(role: "worker" | "household" | "company" | "employer" | "auth" | "anon" | "unknown"): boolean {
    switch (role) {
      case "auth":
        return this.isAuthenticated;
      case "anon":
        return this.isSessionChecked && !this.isAuthenticated;
      case "employer":
        return this.role === "company" || this.role === "household";
      case "unknown":
        return !this.isSessionChecked;
      default:
        return this.role === role || this.customRoles.find((x) => x === role) != null;
    }
  }

  /**
   * More relaxed version of isInRole() that takes either any string or array of strings without the strongly typing of isInRole.
   *
   * @param roles See isInRole() for possible roles. However, any string is fine as an input.
   */
  public isInRoles(roles: string[]): boolean {
    if (!roles) {
      return false;
    }
    for (const role of roles) {
      if (role.startsWith("!") && this.isInRole(role.substr(1) as any)) {
        return false;
      }
      if (this.isInRole(role as any)) {
        return true;
      }
    }
  }

  /** If true, the session has been checked from the server */
  public get isSessionChecked(): boolean {
    return this.isSessionCheckedState;
  }

  /** Avatar to show in the login screen */
  public get avatar(): any {
    return this.avatarState;
  }

  /** The full session objcet */
  public get session(): any {
    return this.sessionState;
  }

  /**
   * The company account object if the currently selected account is a company.
   * Null if the current account is a person or anonymous.
   */
  public get companyAccount(): CompanyAccount {
    if (this.sessionState && this.sessionState.currentAccount && this.sessionState.currentAccount.avatar
      && this.sessionState.currentAccount.avatar.entityType === "company") {
      return this.sessionState.currentAccount;
    }
    return null;
  }

  /**
   * The Person account object if the currently selected account is a person.
   * Null if the current account is a company or anonymous.
   */
  public get personAccount(): PersonAccount {
    if (this.sessionState && this.sessionState.currentAccount && this.sessionState.currentAccount.avatar
      && this.sessionState.currentAccount.avatar.entityType === "person") {
      return this.sessionState.currentAccount;
    }
    return null;
  }

  /** Gets the address for the API server where the session is connected */
  public getServerAddress(): string {
    return this.client.sessionApi.getServerAddress();
  }

  /**
   * Gets / sets the current token.
   * Note that setting this property will not refresh the session
   * so typically, you would use setToken method instead.
   */
  public get currentToken(): string {
    return this.token.currentToken;
  }
  public set currentToken(token: string) {
    this.token.currentToken = token;
  }

  /**
   * Check the status of the current session
   */
  public checkSession(): Promise<UserSession|void> {
    if (!this.token.currentToken) {
      // No token => We are clearly anonymous
      this.isSessionCheckedState = true;
      this.isAuthenticatedState = false;
      this.avatarState = null;
      this.sessionState = null;
      this.sessionChanged.emit(false);
      return new Promise<any>((resolve, reject) => {
        resolve();
      });
    } else {
      return this.client.sessionApi.getSession().then(data => {
        if (data) {
          // Server says "yes" (this may still be anonymous)
          this.isAuthenticatedState = data.isAuthorized;
          this.avatarState = data.avatar;
          this.sessionState = data;
          if (this.isAuthenticated) {
            if (this.companyAccount) {
              this.role = "company";
            } else {
              if (this.personAccount.lastLoginAs === WebSiteUserRole.Worker || !this.personAccount.isEmployer) {
                this.role = "worker";
              } else {
                this.role = "household";
              }
            }
          }
        } else {
          // Computer sais "NO" => Anonymous.
          this.isAuthenticatedState = false;
          this.avatarState = null;
          this.sessionState = null;
        }
        // In any case the session has now been checked.
        this.isSessionCheckedState = true;
        this.sessionChanged.emit(this.isAuthenticatedState);
        return data;
      });
    }
  }

  /**
   * Show the Login screen
   *
   * @param redirectUrl - Redirect URL after the login. Defaults to root of the current site.
   * @param role - Default role if the user is going to Onboarding wizard for new users.
   * @param partnerSite - Identifier for the partenr site.
   * @param onboardingUrl - Url to custom onboarding
   */
  public signIn(redirectUrl: string = null, role: string = null, partnerSite: string = null, onboardingUrl: string = null): void {
    const partner = partnerSite || "unknown";
    const successUrl = redirectUrl || window.location.origin + "/";
    const url = this.getServerAddress()
      + "/oauth2/authorize?response_type=token&redirect_uri="
      + encodeURIComponent(successUrl || "")
      + "&client_id=" + encodeURIComponent(partner || "")
      + "&salaxy_role=" + encodeURIComponent(role || "")
      + "&salaxy_onboarding_uri=" + encodeURIComponent(onboardingUrl || "");

    window.location.href = url;
  }

  /**
   * Opens the login dialog with signUp / register functionality
   *
   * @param redirectUrl - The URL where the user is taken after a successfull login
   * @param role - Optional role (household, worker or company) for
   * @param partnerSite - Identifier for the partenr site.
   * @param onboardingUrl - Url to custom onboarding
   */
  public register(redirectUrl: string = null, role: string = null, partnerSite: string = null, onboardingUrl: string = null): void {
    this.signIn(redirectUrl, role, partnerSite, onboardingUrl);
  }

  /**
   * Sends the user to the Sign-out page
   *
   * @param redirectUrl - URL where user is redirected after log out. Must be absolute URL. Default is the root of the current server.
   */
  public signOut(redirectUrl?: string): void {
    this.currentToken = null;
    redirectUrl = redirectUrl || window.location.origin + "/";
    this.checkSession().then(() => {
      window.location.href = redirectUrl;
    });
  }

  /**
   * Switches the current web site usage role and refreshes the session.
   *
   * @param role - Household or worker.
   *
   * @returns A promise with the new role if the switch was succesful.
   */
  public switchRole(role: "worker" | "household"): Promise<"worker" | "household"> {
    return new Promise<"worker" | "household">((resolve, reject) => {
      if (this.companyAccount) {
        reject("Switching to Company role is currently not a role switch. It is an impersonation as another account. Switching company => person may be supported later.");
      } else {
        if (this.isAuthenticated) {
          return this.client.accountsApi.switchRole(role).then(data => {
            this.role = role;
            this.sessionChanged.emit(this.isAuthenticatedState);
            resolve(role);
          });
        } else {
          // Role change for non-authenticated users is for demo purposes: Many components work as anonymous and still need role information.
          this.role = role;
          this.sessionChanged.emit(this.isAuthenticatedState);
          resolve(role);
        }
      }
    });
  }

  /**
   * Discerns whether current session user can change role to an another role given.
   *
   * @param role - household or worker or company.
   */
  public canSwitchRoleTo(role: "worker" | "household" | "company"): boolean {
    return !(this.companyAccount || role === "company"); // Change from / to company not supported.
  }

  /**
   * Adds an application-specific custom role to the roles collection.
   * Any future system role may later throw an exception, so use strings that are clearly application specific (e.g. "myapp-authenticated").
   *
   * @param role An application-specific role.
   */
  public addCustomRole(role: string): void {
    this.customRoles.push(role);
  }

}
