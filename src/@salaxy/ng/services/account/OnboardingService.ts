import { Injectable, Inject } from "@angular/core";
import { DomSanitizer, SafeResourceUrl } from "@angular/platform-browser";

import { Onboarding, WebSiteUserRole, CompanyType, OnboardingStatus, Client } from "@salaxy/core";

import { ISingleItemRepository } from "../repositories/ISingleItemRepository";
import { ClientNg } from "../api/ClientNg";

/** Service that hanldes the interaction to server in the onboarding process. */
@Injectable()
export class OnboardingService implements ISingleItemRepository<Onboarding> {

  private _current: Onboarding;

  /** Creates a new instance of OnboardingService with dependency injection. */
  constructor(@Inject(ClientNg) private client: Client, private sanitizer: DomSanitizer) { }

  /** Current onboarding object. */
  public get current(): Onboarding {
    return this._current;
  }

  /**
   * Gets a new blank object with default values, suitable for UI binding.
   * This is a synchronous method - does not go to the server and it is not 100% reliable in that way.
   * However, it shoud provide a basic object, good enough for most views.
   */
  public getBlank(accountType = WebSiteUserRole.None): Onboarding {
    return {
      accountType: WebSiteUserRole.Worker,
      person: {
        contact: {},
      },
      company: {
        businessId: null,
        companyType: CompanyType.Unknown,
        name: null,
        contact: {},
      },
      signature: {},
      ui: {},
      validation: {},
      status: OnboardingStatus.Open,
      products: {},
    };
  }

  /**
   * Creates a new item and sets it as current for editing and saving.
   * The item is not yet sent to server and thus it is not in the list, nor does it have an identifier.
   */
  public newCurrent(accountType = WebSiteUserRole.None): void {
    this._current = this.getBlank(accountType);
  }

  /**
   * Saves changes to the current item.
   */
  public saveCurrent(): Promise<Onboarding> {
    if (this.current) {
      return this.save(this.current);
    }
  }

  /**
   * Adds or updates a given item.
   * Operation is determined based on id: null/''/'new' adds, other string values update.
   *
   * @param item - Item that is updated.
   */
  public save(item: Onboarding): Promise<Onboarding> {
    return this.client.onboardingsApi.saveOnboarding(item).then(result => {
      return this._current = result;
    });
  }

  /**
   * Deletes the given item from repository if possible
   *
   * @param id - Identifier of the item to delete
   */
  public delete(id: string): Promise<any> {
    return this.client.onboardingsApi.deleteOnboarding(id).then(result => {
      this._current = null;
      return result;
    });
  }

  /**
   * Loads the onboarding from the server and sets it as current.
   */
  public loadSingle(id: string): Promise<Onboarding> {
    return this.client.onboardingsApi.getOnboarding(id).then(result => {
      // Set worker as default
      if (!result.accountType || result.accountType === WebSiteUserRole.None) {
        result.accountType = WebSiteUserRole.Worker;
      }
      return this._current = result;
    });
  }

  /**
   * Sends SMM Verification pint to given telephone number.
   * Note that API does not send the message to the same telephone number again in 60 seconds.
   *
   * @return Promise's result data is true if the sms was really sent.
   */
  public sendSmsVerificationPin(): Promise<boolean> {
    return this.client.onboardingsApi.sendSmsVerificationPin(this.current);
  }

  /**
   * Sends the browser to the signature process.
   *
   * @param method - selected signature method
   */
  public beginSignature(method: string): Promise<Onboarding> {

    // Check that method has been given
    if (!method) {
      return;
    }

    // Ensure that the onboarding object has signature data
    if (!this.current.signature.email) {
      this.current.signature.email = this.current.person.contact.email;
    }
    if (!this.current.signature.telephone) {
      this.current.signature.telephone = this.current.person.contact.telephone;
    }
    if (!this.current.signature.personName) {
      this.current.signature.personName = [this.current.person.firstName, this.current.person.lastName].join(" ").trim();
    }
    if (this.current.accountType === WebSiteUserRole.Household ||
      this.current.accountType === WebSiteUserRole.Worker) {
      if (!this.current.signature.personalId) {
        this.current.signature.personalId = this.current.person.personalId;
      }
    }

    // Save and proceed to signing
    return this.saveCurrent().then((onboarding: Onboarding) => {
      const url = this.client.ajax.getServerAddress() + "/onboarding/VismaSignBegin?id="
        + this.current.owner + "|" + this.current.id
        + "&signerPersonalId=" + this.current.signature.personalId
        + "&authService=" + method;

      window.location.assign(url);

      return onboarding;
    });
  }

  /**
   * Gets the PDF preview address.
   */
  public getPdfPreviewAddress(): SafeResourceUrl {
    const url = this.client.ajax.getServerAddress() + "/onboarding/GetAuthorizationPdfPreview?id=" +
      this.current.owner + "|" + this.current.id + "&signer.personalId=" + (this.current.signature.personalId || "PPKKVV-nnnX");
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }

}
