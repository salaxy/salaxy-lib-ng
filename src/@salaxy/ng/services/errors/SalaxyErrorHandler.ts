import { ErrorHandler } from "@angular/core";

/**
 * Salaxy error handler.
 */
export class SalaxyErrorHandler extends ErrorHandler {

  /**
   * Default constructor.
   */
  constructor() {
    super();
  }

  /**
   * Error handler with error http-status info if available.
   *
   * @param err - Error to handle.
   * @param rethrow - Optional param whether to rethrow the error. Default true.
   *        Set to false when extending this class and calling super.handleError().
   */
  public handleError(err: any, rethrow = true) {
    if (err.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error("An error occurred:", err.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      console.error(
        `Backend returned code ${err.status}, ` + `body was: ${err.error}`
      );
    }
    if (rethrow) {
      throw err;
    }
  }
}
