Angular 5 libraries for Salaxy platform
=======================================

Angular 5 libraries are basic Angular-level infrastucture behind the mobile-first Salaxy Ionic libraries.
They also serve as a starting point if you wish to create a user interface using some other UX framework 
than Ionic.

For ready-to-use components that simply work, please refer to Ionic libraries (@salaxy/ionic).

Usage:

- Run `npm install --save @salaxy/ng@latest`
- Reference or copy files from `node_modules/@salaxy/ng`

For more information and documentation about Salaxy platform, please go to https://developers.salaxy.com 
