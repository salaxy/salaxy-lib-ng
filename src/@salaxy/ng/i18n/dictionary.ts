/* tslint:disable */
var _dictionary = _dictionary || {};
_dictionary["en"] = {
  "SALAXY": {
    "NG": {
      "ERRORS": {
        "defaultDescription": "Unexpected error, please try again."
      },
      "ROLES": {
        "anonUser": "Anonymous User",
        "asCompany": "as a company",
        "asEmployer": "as an employer",
        "asHousehold": "as a household",
        "asWorker": "as a worker",
        "companies": "Companies",
        "companiesOrAssociations": "Companies or Associations",
        "company": "Company",
        "companyOrAssociation": "Company or Association",
        "employer": "Employer",
        "employers": "Employers",
        "errorRoleNotDefined": "Error, user's role is not defined",
        "household": "Household",
        "households": "Households",
        "role": "Role",
        "roles": "Roles",
        "worker": "Worker",
        "workers": "Workers",
        "youAreUsingServiceAsRole": "You are using service {{asRole}}"
      },
      "TEST": {
        "hello": "Hello {{name}}",
        "testLabel": "This is a test label"
      },
      "VALIDATION": {
        "maxlength": "Teksti on liian pitkä",
        "minlength": "Teksti on liian lyhyt",
        "pattern": "Arvo ei ole oikeassa muodossa",
        "required": "Tämä on pakollinen kenttä",
        "sxyCompanyIdFi": "Tarkista Y-tunnus",
        "sxyEmail": "Sähköposti on virheellinen",
        "sxyIban": "IBAN-numero on virheellinen",
        "sxyMobilePhone": "Puhelinnumero on virheellinen  (+0-9' ')",
        "sxyPersonalIdFi": "Tarkista henkilötunnus",
        "sxyPostalCodeFi": "Postinumeron tulee olla 1-5 numeroa.",
        "sxySmsVerificationCode": "SMS-vahvistuskoodi on virheellinen"
      }
    }
  }
};
_dictionary["fi"] = {
  "SALAXY": {
    "NG": {
      "ERRORS": {
        "defaultDescription": "Virhe pyynnön käsittelyssä, ole hyvä kokeile uudelleen myöhemmin."
      },
      "ROLES": {
        "anonUser": "Anonyymi käyttäjä",
        "asCompany": "yrityksenä",
        "asEmployer": "työnantajana",
        "asHousehold": "kotitalous työnantajana",
        "asWorker": "työntekijänä",
        "companies": "Yritykset",
        "companiesOrAssociations": "Yritykset tai yhdistykset",
        "company": "Yritys",
        "companyOrAssociation": "Yritys tai yhdistys",
        "employer": "Työnantaja",
        "employers": "Työnantajat",
        "errorRoleNotDefined": "Virhe, käyttäjän roolia ei ole määritetty",
        "household": "Kotitalous",
        "households": "Kotitaloudet",
        "role": "Rooli",
        "roles": "Roolit",
        "worker": "Työntekijä",
        "workers": "Työntekijät",
        "youAreUsingServiceAsRole": "Käytät palvelua {{asRole}}"
      },
      "TEST": {
        "hello": "Hei {{name}}",
        "testLabel": "Tämä on testi labeli"
      },
      "VALIDATION": {
        "maxlength": "Teksti on liian pitkä",
        "minlength": "Teksti on liian lyhyt",
        "pattern": "Arvo ei ole oikeassa muodossa",
        "required": "Tämä on pakollinen kenttä",
        "sxyCompanyIdFi": "Tarkista Y-tunnus",
        "sxyEmail": "Sähköposti on virheellinen",
        "sxyIban": "IBAN-numero on virheellinen",
        "sxyMobilePhone": "Puhelinnumero on virheellinen (+0-9' ')",
        "sxyPersonalIdFi": "Tarkista henkilötunnus",
        "sxyPostalCodeFi": "Postinumeron tulee olla 1-5 numeroa.",
        "sxySmsVerificationCode": "SMS-vahvistuskoodi on virheellinen"
      }
    }
  }
};
_dictionary["sv"] = {
  "SALAXY": {
    "NG": {
      "ERRORS": {
        "defaultDescription": ""
      },
      "ROLES": {
        "anonUser": "",
        "asCompany": "",
        "asEmployer": "",
        "asHousehold": "",
        "asWorker": "",
        "companies": "",
        "companiesOrAssociations": "",
        "company": "",
        "companyOrAssociation": "",
        "employer": "",
        "employers": "",
        "errorRoleNotDefined": "",
        "household": "",
        "households": "",
        "role": "",
        "roles": "",
        "worker": "",
        "workers": "",
        "youAreUsingServiceAsRole": ""
      },
      "TEST": {
        "hello": "",
        "testLabel": ""
      },
      "VALIDATION": {
        "maxlength": "",
        "minlength": "",
        "pattern": "",
        "required": "",
        "sxyCompanyIdFi": "",
        "sxyEmail": "",
        "sxyIban": "",
        "sxyMobilePhone": "",
        "sxyPersonalIdFi": "",
        "sxyPostalCodeFi": "",
        "sxySmsVerificationCode": ""
      }
    }
  }
};
/** Internationalization dictionary
@hidden */
export const dictionary = _dictionary;
