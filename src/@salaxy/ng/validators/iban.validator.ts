import { Directive, forwardRef, Attribute } from "@angular/core";
import { NG_VALIDATORS, Validator, AbstractControl } from "@angular/forms";

import { Validation } from "@salaxy/core";

/**
 * IBAN number validation directive.
 * Note that the same validation can be used for Salaxy account ID's.
 * Angular Validator wrapper on top of @salaxy/core.Validation.isIban()
 *
 * <example-url>/#/examples/ValidationMessagesExample</example-url>
 *
 * @example
 * <sxy-input-text [(ngModel)]="tester.bankIban" name="sxyIbanTest" label="Bank account IBAN [sxyIban]" sxyIban></sxy-input-text>
 *
 */
@Directive({
  selector: "[sxyIban]",
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => IbanValidator),
      multi: true
    }
  ]
})
export class IbanValidator implements Validator {

  constructor(@Attribute("sxyIban") public sxyIban: string) {}

  /** Validates input */
  public validate(c: AbstractControl): { [key: string]: any } {
    if (c.value && !Validation.isIban(c.value)) {
      return { sxyIban: { value: c.value } };
    }
    return null;
  }
}
