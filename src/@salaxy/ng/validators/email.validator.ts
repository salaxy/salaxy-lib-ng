import { Directive, forwardRef, Attribute } from "@angular/core";
import { NG_VALIDATORS, Validator, AbstractControl } from "@angular/forms";

import { Validation } from "@salaxy/core";

/**
 * E-mail validation directive.
 * Angular Validator wrapper on top of @salaxy/core.Validation.isEmail()
 *
 * <example-url>/#/examples/ValidationMessagesExample</example-url>
 *
 * @example
 * <sxy-input-text [(ngModel)]="tester.email" name="sxyEmailTest" label="E-mail [sxyEmail]" sxyEmail></sxy-input-text>
 */
@Directive({
  selector: "[sxyEmail]",
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => EmailValidator),
      multi: true
    }
  ]
})
export class EmailValidator implements Validator {

  constructor(@Attribute("sxyEmail") public sxyEmail: string) {}

  /** Validates input */
  public validate(c: AbstractControl): { [key: string]: any } {
    if (c.value && !Validation.isEmail(c.value)) {
      return { sxyEmail: { value: c.value } };
    }
    return null;
  }
}
