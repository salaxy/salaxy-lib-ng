import { Directive, forwardRef, Attribute } from "@angular/core";
import { NG_VALIDATORS, Validator, AbstractControl } from "@angular/forms";

import { Validation } from "@salaxy/core";

/**
 * Finnish Company ID validation directive.
 *
 * <example-url>/#/examples/ValidationMessagesExample</example-url>
 *
 * @example
 * <sxy-input-text [(ngModel)]="current.companylId" name="sxyCompanyIdFi" label="Company id [sxyCompanyIdFi]" sxyCompanyIdFi></sxy-input-text>
 */
@Directive({
  selector: "[sxyCompanyIdFi]",
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => CompanyIdFiValidator),
      multi: true
    }
  ]
})
export class CompanyIdFiValidator implements Validator {

  constructor(@Attribute("sxyCompanyIdFi") public sxyCompanyIdFi: string) {}

  /** Validates input */
  public validate(c: AbstractControl): { [key: string]: any } {
    if (c.value && !Validation.formatCompanyIdFi(c.value)) {
      return { sxyCompanyIdFi: { value: c.value } };
    }
    return null;
  }
}
