import { Directive, forwardRef, Attribute } from "@angular/core";
import { NG_VALIDATORS, Validator, AbstractControl } from "@angular/forms";

import { Validation } from "@salaxy/core";

/**
 * Mobile phone number validation directive.
 * Angular Validator wrapper on top of @salaxy/core.Validation.isMobilePhone()
 *
 * <example-url>/#/examples/ValidationMessagesExample</example-url>
 *
 * @example
 * <sxy-input-text [(ngModel)]="tester.mobilePhone" name="sxyMobilePhoneTest" label="Phone number [sxyMobilePhone]" sxyMobilePhone></sxy-input-text>
 *
 */
@Directive({
  selector: "[sxyMobilePhone]",
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => MobilePhoneValidator),
      multi: true
    }
  ]
})
export class MobilePhoneValidator implements Validator {

  constructor(@Attribute("sxyMobilePhone") public sxyMobilePhone: string) {}

  /** Validates input */
  public validate(c: AbstractControl): { [key: string]: any } {
    if (c.value && !Validation.isMobilePhone(c.value)) {
      return { sxyMobilePhone: { value: c.value } };
    }
    return null;
  }
}
