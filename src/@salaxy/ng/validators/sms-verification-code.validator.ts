import { Directive, forwardRef, Attribute } from "@angular/core";
import { NG_VALIDATORS, Validator, AbstractControl } from "@angular/forms";

import { Validation } from "@salaxy/core";

/**
 * SMS verification code validation directive.
 * Angular Validator wrapper on top of @salaxy/core.Validation.isSmsVerificationCode()
 *
 * <example-url>/#/examples/ValidationMessagesExample</example-url>
 *
 * @example
 * <sxy-input-text [(ngModel)]="tester.smsVerificationCode" name="sxySmsVerificationCodeTest" label="SMS verification code [sxySmsVerificationCode]" sxySmsVerificationCode></sxy-input-text>
 *
 */
@Directive({
  selector: "[sxySmsVerificationCode]",
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => SmsVerificationCodeValidator),
      multi: true
    }
  ]
})
export class SmsVerificationCodeValidator implements Validator {

  constructor(@Attribute("sxySmsVerificationCode") public sxySmsVerificationCode: string) {}

  /** Validates input */
  public validate(c: AbstractControl): { [key: string]: any } {
    if (c.value && !Validation.isSmsVerificationCode(c.value)) {
      return { sxySmsVerificationCode: { value: c.value } };
    }
    return null;
  }
}
