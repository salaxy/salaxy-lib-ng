import { Directive, forwardRef, Attribute } from "@angular/core";
import { NG_VALIDATORS, Validator, AbstractControl } from "@angular/forms";

import { Validation } from "@salaxy/core";

/**
 * Finnish Personal ID (HETU) validation directive.
 * Angular Validator wrapper on top of @salaxy/core.Validation.isPersonalIdFi()
 *
 * <example-url>/#/examples/ValidationMessagesExample</example-url>
 *
 * @example
 * <sxy-input-text [(ngModel)]="tester.personalIdFi" name="sxyPersonalIdFiTest" label="Personal ID (HETU) [sxyPersonalIdFi]" sxyPersonalIdFi></sxy-input-text>
 */
@Directive({
  selector: "[sxyPersonalIdFi]",
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => PersonalIdFiValidator),
      multi: true
    }
  ]
})
export class PersonalIdFiValidator implements Validator {

  constructor(@Attribute("sxyPersonalIdFi") public sxyPersonalIdFi: string) {}

  /** Validates input */
  public validate(c: AbstractControl): { [key: string]: any } {
    if (c.value && !Validation.isPersonalIdFi(c.value)) {
      return { sxyPersonalIdFi: { value: c.value } };
    }
    return null;
  }
}
