import { Directive, forwardRef, Attribute } from "@angular/core";
import { NG_VALIDATORS, Validator, AbstractControl } from "@angular/forms";

import { Validation } from "@salaxy/core";

/**
 * Finnish Postal code validation directive.
 *
 * <example-url>/#/examples/ValidationMessagesExample</example-url>
 *
 * @example
 * <sxy-input-text [(ngModel)]="current.officialId" name="sxyPostalCodeFiTest" label="Postal code [sxyPostalCodeFi]" sxyPostalCodeFi></sxy-input-text>
 */
@Directive({
  selector: "[sxyPostalCodeFi]",
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => PostalCodeFiValidator),
      multi: true
    }
  ]
})
export class PostalCodeFiValidator implements Validator {

  constructor(@Attribute("sxyPostalCodeFi") public sxyPostalCodeFi: string) {}

  /** Validates input */
  public validate(c: AbstractControl): { [key: string]: any } {
    if (c.value && !Validation.isPostalCodeFi(c.value)) {
      return { sxyPostalCodeFi: { value: c.value } };
    }
    return null;
  }
}
