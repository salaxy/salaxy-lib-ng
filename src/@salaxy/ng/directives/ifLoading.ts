import { Directive, Inject, TemplateRef, ViewContainerRef } from "@angular/core";
import { HTTP_INTERCEPTORS, HttpInterceptor } from "@angular/common/http";
import { LoadingInterceptor } from "../services/api/loading.interceptor";

/**
 * Shows the element if the http request is in progress.
 *
 * @example
 * <ion-spinner *sxyIfLoading name="bubbles"></ion-spinner>
 *
 * @see [Based on Angular structural directive sample]{@link https://angular.io/guide/structural-directives#write-a-structural-directive}
 */
@Directive({
  selector: "[sxyIfLoading]"
})
export class IfLoadingDirective {

  /** If true, the template view has been created */
  private hasView = false;

  constructor(
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef,
    @Inject(HTTP_INTERCEPTORS) interceptors: HttpInterceptor[],
  ) {
    this.checkViewContainer(false);
    const loadingInterceptor = interceptors.find((x) => x instanceof LoadingInterceptor) as LoadingInterceptor;
    if (!loadingInterceptor) {
      return;
    }
    loadingInterceptor.isLoading.subscribe((isLoading) => {
      this.checkViewContainer(isLoading);
    });
  }

  /** Assures that the view is created if necessary. */
  private checkViewContainer(isLoading: boolean) {
    if (isLoading && !this.hasView) {
      this.viewContainer.createEmbeddedView(this.templateRef);
      this.hasView = true;
    } else if (!isLoading && this.hasView) {
      this.viewContainer.clear();
      this.hasView = false;
    }
  }
}
