import { Directive, Input, TemplateRef, ViewContainerRef } from "@angular/core";

import { SalaxyContext } from "../services/index";

/**
 * Shows the element if the user is in the given role / in one of the given roles.
 * The attribute value should resolve to either role string or array of role strings.
 *
 * @example
 * <home-household *sxyIfRole="'household'"></home-household>
 *
 * @see [Based on Angular structural directive sample]{@link https://angular.io/guide/structural-directives#write-a-structural-directive}
 */
@Directive({
  selector: "[sxyIfRole]"
})
export class IfRoleDirective {

  /** If true, the template view has been created */
  private hasView = false;

  /** Roles that are checked by this component. This is defined by the sxyIfRole input */
  private roles: string | string[] = null;

  /** Creates a new IfRoleDirective */
  constructor(
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef,
    private context: SalaxyContext,
  ) {
    context.session.sessionChanged.subscribe((isAuthenticated) => {
      // We could potentially use some generic lifecycle event for this check as it is not really expensive.
      this.checkViewContainer();
    });
  }

  /**
   * Value of the sxyIfRole attribute should resolve to either role string or array of role strings.
   */
  @Input()
  set sxyIfRole(roles: string | string[]) {
    this.roles = roles;
    this.checkViewContainer();
  }

  /** Assures that the view is created if necessary. */
  private checkViewContainer() {
    let isInRole;
    if (Array.isArray(this.roles)) {
      isInRole = this.context.session.isInRoles(this.roles);
    } else if (this.roles) {
      isInRole = this.context.session.isInRoles([this.roles]);
    }
    if (isInRole && !this.hasView) {
      this.viewContainer.createEmbeddedView(this.templateRef);
      this.hasView = true;
    } else if (!isInRole && this.hasView) {
      this.viewContainer.clear();
      this.hasView = false;
    }
  }
}
