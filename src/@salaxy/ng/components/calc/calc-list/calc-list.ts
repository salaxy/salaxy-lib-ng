import { Component, Input, Output, EventEmitter } from "@angular/core";

import { Calculation, CalculatorLogic, Avatar, Dates } from "@salaxy/core";

import { SalaxyContext } from "../../../services/index";

/**
 * Angular level base component for list of calculations.
 *
 * @todo Extract itemSelect, filtering etc. logic into trait classes
 */
@Component({
  selector: "salaxy-calc-list",
  templateUrl: "./calc-list.html"
})
export class NgCalcListComponent {

  /**
   * Events is fired when an item is clicked / otherwise selected.
   */
  @Output() public itemSelect: EventEmitter<Calculation> = new EventEmitter<Calculation>();

  /**
   * A comma separated list of statuses that are used as filter for the list.
   * In JavaScript
   */
  @Input() public filterStatuses: string | string[];

  /**
   * Property based on which the groupedList is grouped.
   */
  @Input() public groupBy: "employer" | "worker" | "otherParty" | "year" | "month" | "status" = "status";

  /** Limit number of items to show in the list */
  @Input() public limitTo: number;

  /**
   * If false (default), when user sets the calculation, it is automatically set as the current calculation.
   * If true, the current calculation is not set - only itemSelect events are fired.
   */
  @Input() public skipCurrentItemSet = false;

  constructor(protected context: SalaxyContext) {}

  /**
   * Gets the Avatar of other party in a calculation:
   * Employer or Worker
   */
  public getOtherParty = (calc: Calculation) => CalculatorLogic.getOtherParty(calc, this.context.session.avatar);

  /** The item that is currently in edit / detail view */
  public get current(): Calculation {
    return this.context.calculations.current;
  }

  /**
   * Derived components should call this method to select an item.
   */
  public select(calc: Calculation) {
    if (!this.skipCurrentItemSet) {
      this.context.calculations.setCurrent(calc);
    }
    this.itemSelect.emit(calc);
  }

  /**
   * Helper to creates a new calculation - this is often wanted as part of a list.
   * Will call the select() method with the newly created calculation.
   * Note that at this point the calculation is not yet in the list - it only comes there in save.
   */
  public createNew() {
    this.select(CalculatorLogic.getBlank());
  }

  /**
   * Delete the item from repository.
   * Note that this may not always be possible. At least check model.isReadOnly
   * @param id - Identifier for the item to be deleted.
   */
  public delete(id: string) {
    this.context.calculations.delete(id);
  }

  /**
   * List grouped based on groupBy value
   */
  public get groupedList() {
    const sortDescending = this.groupBy === "year" || this.groupBy === "month" ? true : false;
    const groupedObj = this.list.reduce((prev, cur) => {
      // Worker as the default
      let key;
      switch (this.groupBy) {
        case "employer":
          key = cur.employer.avatar.sortableName || "Työnantajaa ei valittu";
          break;
        case "worker":
          key = cur.worker.avatar.sortableName || "Työntekijää ei valittu";
          break;
        case "otherParty":
          key = this.getOtherParty(cur).sortableName;
          break;
        case "year": {
          // TODO: Move the getting of a date to a generic helper
          const date = Dates.getMoment(cur.workflow.paidAt || cur.updatedAt || cur.createdAt || new Date());
          key = "" + date.year;
          break;
        }
        case "month": {
          // TODO: Move the getting of a date to a generic helper
          const date = Dates.getMoment(cur.workflow.paidAt || cur.updatedAt || cur.createdAt || new Date());
          key = date.year + "-" + date.month;
          break;
        }
        case "status":
        default:
          key = cur.workflow.status as any;
          break;
      }

      if (!prev[key]) {
        prev[key] = [cur];
      } else {
        prev[key].push(cur);
      }
      return prev;
    }, {});
    return Object.keys(groupedObj)
      .map(key => ({ key, value: groupedObj[key] }))
      .sort((n1, n2) => {
        if (n1.key > n2.key) {
          return sortDescending ? -1 : 1;
        }
        if (n1.key < n2.key) {
          return sortDescending ? 1 : -1;
        }
        return 0;
      });
  }

  /**
   * List of calculations
   * The list is filtered if a filter is set
   */
  public get list() {
    let result = this.context.calculations.list;
    if (this.filterStatuses) {
      let statusArray: string[];
      if (Array.isArray(this.filterStatuses)) {
        statusArray = this.filterStatuses;
      } else {
        statusArray = this.filterStatuses.split(",");
      }
      if (statusArray.length === 0 || !statusArray[0]) {
        result = this.context.calculations.list;
      } else {
        result = this.context.calculations.list.filter(item => {
          return statusArray.some(statusValue => (item.workflow.status as any) === statusValue);
        });
      }
    }
    if (this.limitTo) {
      return result.slice(0, this.limitTo);
    }
    return result;
  }
}
