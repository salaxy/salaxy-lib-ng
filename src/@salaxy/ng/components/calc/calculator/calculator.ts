import { Component, Inject, OnInit, Input } from "@angular/core";

import { Calculation, Client, CalculatorLogic } from "@salaxy/core";

import { ClientNg, SalaxyContext } from "../../../services/index";

/**
 * Simple calculator user interface.
 * This is the anonymous version
 */
@Component({
  providers: [ClientNg],
  selector: "salaxy-calculator",
  templateUrl: "./calculator.html",
})
export class NgCalculatorComponent implements OnInit {

  /** Internal field for model */
  private modelState: Calculation;

  /** The calculation that we operate */
  @Input() public get model(): Calculation {
    return this.modelState;
  }
  public set model(value: Calculation) {
    this.modelState = value;
  }

  constructor(protected context: SalaxyContext, @Inject(ClientNg) protected client: Client) {}

  /** Initializes the component */
  public ngOnInit() {
    this.modelState = CalculatorLogic.getBlank();
  }

  /** Recalculates the calculation */
  public recalculate() {
    this.client.calculatorApi.recalculate(this.model).then((calc) => {
      this.modelState = calc;
    });
  }
}
