import { Component } from "@angular/core";
import { Calculation, WorkerAccount } from "@salaxy/core";
import { NgCalculatorComponent } from "../calculator/calculator";

/**
 * Provides a simple calculation edit UI for the repository model.
 * Binds to SalaxyContext.calculations.current.
 */
@Component({
    selector: "salaxy-calc-detail",
    templateUrl: "./calc-detail.html",
})
export class NgCalcDetailComponent extends NgCalculatorComponent {

    /** Recalculates the current calculation - not stored to database */
    public recalculate() {
        this.client.calculatorApi.recalculate(this.model).then((calc) => {
            this.model = calc;
        });
    }

    /** The model points to the SalaxyContext.calculations.current */
    public get model(): Calculation {
        return this.context.calculations.current;
    }
    public set model(value: Calculation) {
        this.context.calculations.current = value;
    }

    /** Creates a new calculation */
    public createNew() {
        return this.context.calculations.newCurrent();
    }

    /** Saves the current calculation to server */
    public save() {
        return this.context.calculations.saveCurrent();
    }

    /**
     * Delete this item from repository.
     * Note that this may not always be possible. At least check model.isReadOnly
     */
    public delete() {
        if (this.model && this.model.id) {
            this.context.calculations.delete(this.model.id)
                .catch((err) => {
                  // Possible component level error handling
                });
        }
        this.context.calculations.current = null;
    }

    /** Collection of workers that can be assigned to this calculation */
    public get workers() {
        return this.context.workers.list;
    }
}
