import { Component } from "@angular/core";
import { Calculation, CalculatorLogic } from "@salaxy/core";
import { SalaxyContext } from "../../../services/index";

/** Calculations list implementation based on the repository pattern */
@Component({
    selector: "sxdemo-calculations",
    templateUrl: "./calculations.html",
})
export class NgCalculationsComponent {

    /**
     * Default constructor creates a new CalcListComponent
     * @param calcApi - Raw API for the method
     * @param sessionService - Service to check whether the use s authenticated.
     */
    constructor(
        private context: SalaxyContext,
    ) { }

    /** List of calculations */
    public get calculations() {
        return this.context.calculations.list;
    }

    /** Currently selected calculation */
    public get current() {
        return this.context.calculations.current;
    }

    /**
     * Sets the current calculation
     * @param id - Identifier of the calculation
     */
    public setCurrentId(id: string) {
        this.context.calculations.setCurrentId(id);
    }

    /** Creates a new calculation nd sets it as current */
    public createNew() {
        return this.context.calculations.current = CalculatorLogic.getBlank();
    }
}
