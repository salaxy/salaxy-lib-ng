import { Component, Inject, OnInit } from "@angular/core";
import { Calculation, Client } from "@salaxy/core";
import { ClientNg, Token } from "../../../services/index";

/** Simple calculations listing implementation demo. */
@Component({
    selector: "sxdemo-calc-list-simple",
    templateUrl: "./calc-list-simple.html",
})
export class NgCalcListSimpleComponent implements OnInit {

    /** List of calculations */
    public calculations: Calculation[] = [];

    /** Currently selected calculation */
    public current: Calculation = {} as any;

    /**
     * Default constructor creates a new CalcListComponent
     * @param calcApi - Raw API for the method
     * @param sessionService - Service to check whether the use s authenticated.
     */
    constructor(
        @Inject(ClientNg) private client: Client,
        private token: Token,
    ) { }

    /**
     * Starts loading calculations for the user if there is a currentToken,
     * typically stored in the cookie.
     */
    public ngOnInit() {
        if (this.token && this.token.currentToken) {
            this.client.calculationsApi.getAll().then ( (calcs) => {
                this.calculations = calcs ;
            });
        }
    }

    /**
     * Sets the current calculation - starts loading from the server
     * @param id - Identifier of the calculation
     */
    public setCurrentId(id: string) {
        this.client.calculationsApi.get(id).then((calc) => this.current = calc);
    }
}
