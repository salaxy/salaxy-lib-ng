import { Component, OnDestroy, OnInit, Input } from "@angular/core";
import { Calculation} from "@salaxy/core";
import { SalaxyContext } from "../../../services/index";

/* tslint:disable */
/* TODO: Waiting for Go-through */

@Component({
  selector: "salaxy-calc-chart",
  templateUrl: "./calc-chart.html",
})

export class NgCalcChartComponent implements OnInit, OnDestroy {

  /**
   * Calculation that is used i n rendering.
   * If null, current calculation is taken from the context.
   */
  @Input() public calc: Calculation;

  /** Role for which the chart is rendered: employer or worker */
  @Input() public role: "employer" | "worker";

  // General chart properties
  // public htmlId;
  public chartLabel = "";
  public chartLabelTable = [this.chartLabel];
  @Input() public scaleYAxis;
  @Input() public chartType = "bar";

  // Properties used in chart generation
  public barChartData;
  public pieChartLabels;
  public pieData;

  // Should a bar or a pie chart be shown, default is bar chart
  public showBarChart = true;

  // Bar chart options
  public barChartOptions = {
    legend: {
      display: false,
    },
    tooltips: {
      enabled: true,
      bodySpacing: 6,
      callbacks: {
        label(tooltipItem, data) {
          const seriesLabel = data.datasets[tooltipItem.datasetIndex].label;
          return seriesLabel;
        },
      },
    },
    scales: {
      xAxes: [{
        stacked: true,
      }],
      yAxes: [{
        stacked: true,
        ticks: { min: 0, max: 100 },
      }],
    },
    events: ["mousemove", "click"],
    title: {
      fullWidth: false,
    },
  };

  // Pie chart options
  public pieChartOptions = {
    legend: {
      display: false,
    },
    tooltips: {
      enabled: true,
      mode: "single",
      bodySpacing: 6,
      callbacks: {
        label(tooltipItem, data) {
          const label = data.labels[tooltipItem.index];
          return label;
        },
      },
    },
    events: ["mousemove", "click"],
  };

  // Default chart colors
  public chartColors = [
    { // salary
      backgroundColor: "rgb(145,210,20)",
      pointBackgroundColor: "rgb(145,210,20)",
      pointHoverBackgroundColor: "rgb(145,210,20)",
      borderColor: "rgb(145,210,20)",
      pointBorderColor: "rgb(145,210,20)",
      pointHoverBorderColor: "rgb(145,210,20)",
    },
    { // darkgreen
      backgroundColor: "rgb(97,112,59)",
      pointBackgroundColor: "rgb(97,112,59)",
      pointHoverBackgroundColor: "rgb(97,112,59)",
      borderColor: "rgb(97,112,59)",
      pointBorderColor: "rgb(97,112,59)",
      pointHoverBorderColor: "rgb(97,112,59)",
    },
    { // orange
      backgroundColor: "rgb(255,145,30)",
      pointBackgroundColor: "rgb(255,145,30)",
      pointHoverBackgroundColor: "rgb(255,145,30)",
      borderColor: "rgb(255,145,30)",
      pointBorderColor: "rgb(255,145,30)",
      pointHoverBorderColor: "rgb(255,145,30)",
    },
    { // expenses
      backgroundColor: "rgb(209,0,116)",
      pointBackgroundColor: "rgb(209,0,116)",
      pointHoverBackgroundColor: "rgb(209,0,116)",
      borderColor: "rgb(209,0,116)",
      pointBorderColor: "rgb(209,0,116)",
      pointHoverBorderColor: "rgb(209,0,116)",
    },
  ];

  // Default chart colors
  public pieChartColors = [
    {
      backgroundColor: ["rgb(145,210,20)", "rgb(97,112,59)", "rgb(255,145,30)" , "rgb(209,0,116)"],
      pointBackgroundColor: ["rgb(145,210,20)", "rgb(97,112,59)", "rgb(255,145,30)", "rgb(209,0,116)"],
      pointHoverBackgroundColor: ["rgb(145,210,20)", "rgb(97,112,59)", "rgb(255,145,30)", "rgb(209,0,116)"],
      borderColor: ["rgb(145,210,20)", "rgb(97,112,59)", "rgb(255,145,30)", "rgb(209,0,116)"],
      pointBorderColor: ["rgb(145,210,20)", "rgb(97,112,59)", "rgb(255,145,30)", "rgb(209,0,116)"],
      pointHoverBorderColor: ["rgb(145,210,20)", "rgb(97,112,59)", "rgb(255,145,30)", "rgb(209,0,116)"],
    },
  ];

  // Colors from component bindings
  private colors;

  // General numbers
  private totalExpenses = 0;

  // Employer related numbers
  private totalGrossSalary = 0;
  private salaryAdditions = 0;
  private allSideCosts = 0;
  private employerChartScale = 100;
  private finalCost = 0;

  // Worker related numbers
  private salaryPayment = 0;
  private tax = 0;
  private workerSideCosts = 0;
  private benefits = 0;
  private totalWorkerPayment = 0;
  private workerChartScale = 100;

  private calculationSubscription: any;

  constructor(private context: SalaxyContext) {}

  /** Initializes the component */
  public ngOnInit() {
    // Initial
    this.doChart(this.calc || this.context.calculations.current);
    // Listen to changes
    this.calculationSubscription = this.context.calculations.onChange()
        .subscribe((item) => {
          this.doChart(item);
        });
  }

  public ngOnDestroy() {
    this.calculationSubscription.unsubscribe();
  }

  /**
   * Executes everything needed for chart generation:
   * fetched the current calculation, stores needed numbers from that calculation,
   * checks colors, sets labels and sets chart data
   */
  private doChart(calc: Calculation) {
    /*if (!calc){
      return;
    }*/
    this.calculateAllNumbers(calc);
    this.checkColors();

    if (!this.role || this.role === "employer") {
      this.doChartEmployer();
    } else if (this.role === "worker") {
      this.doChartWorker();
    } else {
      // console.error("Unknown role:" + this.role);
    }
  }

  /**
   * Checks if any overriding colors have been given as an attribute
   * Checks if colors are valid hex colors, and then replaces default colors
   */
  private checkColors() {
    if (this.colors != null) {
      const colorList = this.colors.split(",");

      for (const color of Object.keys(colorList)) {
        const colorNumber = Number(color);
        const regex = new RegExp("(^#[0-9A-Fa-f]{6}$)|(^#[0-9A-Fa-f]{3}$)");
        const trimmedColor = colorList[colorNumber].trim();
        const isColorHex = regex.test(trimmedColor);

        if (colorNumber < 4 && isColorHex) {
          const tempColor = this.chartColors[colorNumber];
          tempColor.backgroundColor = colorList[colorNumber];
          tempColor.pointBackgroundColor = colorList[colorNumber];
          tempColor.pointHoverBackgroundColor = colorList[colorNumber];
          tempColor.borderColor = colorList[colorNumber];
          tempColor.pointBorderColor = colorList[colorNumber];
          tempColor.pointHoverBorderColor = colorList[colorNumber];
          this.chartColors[colorNumber] = tempColor;
        }
      }
    }
  }

  /**
   * Calculates and sets a proper Y axis max value for a given value
   * @param value The number of the highest bar in chart
   */

  private setChartYAxisLimit(value: number) {
    const log = Math.round(Math.log10(value));
    const times = Math.ceil(value / Math.pow(10, log - 1));
    const newValue = Math.pow(10, log - 1) * times;

    this.barChartOptions.scales = {
      xAxes: [{
        stacked: true,
      }],
      yAxes: [{
        stacked: true,
        ticks: { min: 0, max: newValue },
      }],
    };
  }

  /**
   * Fetches all relevant numbers from the current calculation
   */

  private calculateAllNumbers(calc: Calculation) {
    if (calc != null && calc.result != null) {
      // Employer numbers
      this.totalGrossSalary = calc.result.totals.totalGrossSalary;
      this.salaryAdditions = calc.result.employerCalc.totalDeductions;
      this.allSideCosts = calc.result.employerCalc.allSideCosts;
      this.finalCost = calc.result.employerCalc.finalCost;

      // Worker numbers
      this.salaryPayment = calc.result.workerCalc.salaryPayment;
      this.tax = calc.result.workerCalc.tax;
      this.workerSideCosts = calc.result.workerCalc.workerSideCosts;
      this.benefits = calc.result.workerCalc.benefits;
      this.totalExpenses = calc.result.totals.totalExpenses;
      this.totalWorkerPayment = calc.result.workerCalc.totalWorkerPayment;

      // Calculating max y axis limits
      this.employerChartScale = this.totalGrossSalary + this.salaryAdditions + this.allSideCosts + this.totalExpenses;
      this.workerChartScale = this.salaryPayment + this.tax + this.workerSideCosts + this.benefits + this.totalExpenses;

      // Setting max y axis limits
      if (this.scaleYAxis) {
        if (this.workerChartScale > this.employerChartScale) {
          this.setChartYAxisLimit(this.workerChartScale);
        } else {
          this.setChartYAxisLimit(this.employerChartScale);
        }
      } else {
        if (this.role === "worker") {
          this.setChartYAxisLimit(this.workerChartScale);
        } else {
          this.setChartYAxisLimit(this.employerChartScale);
        }
      }
    }
  }

  /**
   * Sets the relevant employer data as chart data
   */

  private doChartEmployer() {
    this.pieData = [
      this.totalGrossSalary,
      this.salaryAdditions,
      this.allSideCosts,
      this.totalExpenses,
    ];

    this.barChartData = [
      {data: [this.totalGrossSalary], label: ("Palkka " + this.totalGrossSalary.toFixed(2) + " €")},
      {data: [this.salaryAdditions], label: ("Palkan lisät " + this.salaryAdditions.toFixed(2) + " €")},
      {data: [this.allSideCosts], label: ("Sivukulut " + this.allSideCosts.toFixed(2) + " €")},
      {data: [[this.totalExpenses]], label: ("Kulut " + this.totalExpenses.toFixed(2) + " €")},
    ];

    this.pieChartLabels = [
      "Palkka " + this.totalGrossSalary.toFixed(2) + " €",
      "Palkan lisät " + this.salaryAdditions.toFixed(2) + " €",
      "Sivukulut " + this.allSideCosts.toFixed(2) + " €",
      "Kulut " + this.totalExpenses.toFixed(2) + " €",
    ];
  }

  /**
   * Sets the relevant worker data as chart data
   */

  private doChartWorker() {
    this.pieData = [
      this.salaryPayment,
      this.tax,
      this.workerSideCosts,
      (this.benefits + this.totalExpenses),
    ];

    this.barChartData = [
      {data: [this.salaryPayment], label: ("Työntekijän nettopalkka " + this.salaryPayment.toFixed(2) + " €")},
      {data: [this.tax], label: "Ennakonpidätys " + this.tax.toFixed(2) + " €"},
      {data: [this.workerSideCosts], label: "Työntekijän sivukulut " + this.workerSideCosts.toFixed(2) + " €"},
      {
        data: [(this.benefits + this.totalExpenses)],
        label: "Kulut ja edut " + this.totalExpenses.toFixed(2) + " €",
      },
    ];

    this.pieChartLabels = [
      "Työntekijän nettopalkka " + this.salaryPayment.toFixed(2) + " €",
      "Ennakonpidätys " + this.tax.toFixed(2) + " €",
      "Työntekijän sivukulut " + this.workerSideCosts.toFixed(2) + " €",
      "Kulut ja edut " + this.totalExpenses.toFixed(2) + " €",
    ];
  }

}
