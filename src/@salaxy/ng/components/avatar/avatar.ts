import { Component, Input } from "@angular/core";
import { Avatar, AvatarPictureType, LegalEntityType } from "@salaxy/core";

/**
 * Shows the Palkkaus Avatar image: This is either an image or a font-icon
 * with a user specific color/initilas combination.
 *
 * @example
 * <salaxy-avatar [avatar]="person.avatar"></salaxy-avatar>
 */
@Component({
  selector: "salaxy-avatar",
  templateUrl: "./avatar.html",
})
export class NgAvatarComponent {

  /**
   * Avatar that is shown by this component
   * Typically, this is an Avatar object, but the component also supports shortcut strings,
   * currently:
   *
   * - "palkkaus" for Palkkaus.fi logo
   * - "anon" for anonymous user
   */
  @Input() public avatar: Avatar;

  /** Gets the model with default values and display logic */
  public getModel(): Avatar {
    if (!this.avatar ) {
      return {
        initials: "?",
        color: "gray",
        pictureType: AvatarPictureType.Icon,
        entityType: LegalEntityType.Company,
      };
    }

    if (this.avatar === "palkkaus" || this.avatar.entityType === "palkkaus" as any) {
      return {
        entityType: LegalEntityType.Company,
        pictureType: AvatarPictureType.Uploaded,
        url: "https://cdn.salaxy.com/img/elems/product-logos/palkkaus.png",
      };
    }

    if (this.avatar === "anon") {
      return {
        entityType: LegalEntityType.Person,
        pictureType: AvatarPictureType.Icon,
        initials: "A",
        color: "gray",
      };
    }

    return this.avatar;
  }

  /** Returns true if the avatar is an image url (as opposed to icon rendering) */
  public get isImage(): boolean {
    return this.getModel().pictureType === AvatarPictureType.Uploaded && this.getModel().url && this.getModel().url.substr(0, 4).toLowerCase() === "http";
  }

  /** Returns true if the avatar should be rendered as a person icon */
  public get isPersonIcon(): boolean {
    return !this.isImage && this.getModel().entityType !== LegalEntityType.Company;
  }

  /** Returns true if the avatar should be rendered as a company icon */
  public get isCompanyIcon(): boolean {
    return !this.isImage && this.getModel().entityType === LegalEntityType.Company;
  }

  /** Returns the avatar color or 'gray' if nothing is defined. */
  public get color(): string {
    return this.getModel().color || "gray";
  }

  /** Returns the initials for rendering the avatar */
  public get initials(): string {
    return this.getModel().initials || "-";
  }
}
