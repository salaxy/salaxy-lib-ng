import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";
import { MaterialModule } from "./material/material.module";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { ChartsModule } from "ng2-charts/ng2-charts";
import { dictionary as coreDictionary } from "@salaxy/core";
import { dictionary } from "../i18n/dictionary";

import {

  NgConditionalContainerComponent,
  NgArticleListComponent,
  NgAvatarComponent,
  NgCalcChartComponent,
  NgCalcDetailComponent,
  NgCalcListComponent,
  NgCalcListSimpleComponent,
  NgCalculationsComponent,
  NgCalculatorComponent,
  NgContractListComponent,
  NgOnboardingBaseComponent,
  NgLoginComponent,
  NgTaxCardComponent,
  NgTaxCardListComponent,
  NgWorkerDetailEditComponent,
  NgWorkerListComponent,
  NgWorkersComponent,

  NgValidationMessages,
  NgFormTextComponent,

  // Directives
  IfRoleDirective,
  IfLoadingDirective,

  // Pipes
  GroupByPipe,
  SafeHtmlPipe,
  SalaxyDatePipe,
  SalaxyPricePipe,
  SalaxyIbanPipe,
  SxyTranslatePipe,

  // Validators
  CompanyIdFiValidator,
  EmailValidator,
  IbanValidator,
  MobilePhoneValidator,
  PersonalIdFiValidator,
  PostalCodeFiValidator,
  SmsVerificationCodeValidator,

} from "./index.components";
import { NgTranslations } from "../services/index.services";

/** Salaxy components that are exported */
const salaxyComponents = [

  NgConditionalContainerComponent,
  NgArticleListComponent,
  NgAvatarComponent,
  NgCalcChartComponent,
  NgCalcDetailComponent,
  NgCalcListComponent,
  NgCalcListSimpleComponent,
  NgCalculationsComponent,
  NgCalculatorComponent,
  NgContractListComponent,
  NgOnboardingBaseComponent,
  NgLoginComponent,
  NgTaxCardComponent,
  NgTaxCardListComponent,
  NgWorkerDetailEditComponent,
  NgWorkerListComponent,
  NgWorkersComponent,
  NgValidationMessages,
  NgFormTextComponent,

  // TODO: Consider should Pipes, Directives and Validators in Services or Components module or should we have just one module

  // Directives
  IfRoleDirective,
  IfLoadingDirective,

  // Pipes
  GroupByPipe,
  SafeHtmlPipe,
  SalaxyDatePipe,
  SalaxyPricePipe,
  SalaxyIbanPipe,
  SxyTranslatePipe,

  // Validators
  CompanyIdFiValidator,
  EmailValidator,
  IbanValidator,
  MobilePhoneValidator,
  PersonalIdFiValidator,
  PostalCodeFiValidator,
  SmsVerificationCodeValidator,
];

/**
 * Plain Angular.io user interface components.
 * Note that more advanced and mobile friendly user interfaces are created in Ionic components.
 */
@NgModule({
  // Declare the view classes (components, directives, and pipes)
  declarations: salaxyComponents,
  // The subset of declarations that should be visible and usable in the component templates of OTHER modules.
  exports: salaxyComponents,
  // Other modules whose exported classes are needed by component templates declared in this module.
  imports: [
    CommonModule,
    FormsModule,
    HttpModule,
    BrowserAnimationsModule,
    MaterialModule,
    ChartsModule,
  ],
})
export class SalaxyNgComponentsModule {
  constructor(translate: NgTranslations) {
    translate.addDictionaries(coreDictionary);
    translate.addDictionaries(dictionary);
  }
}
