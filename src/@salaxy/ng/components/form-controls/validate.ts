import {
    AbstractControl,
    AsyncValidatorFn,
    Validator,
    Validators,
    ValidatorFn,
} from "@angular/forms";
import { Observable } from "rxjs/Observable";

/** Defines a client side validation result. */
export interface ValidationResult {
    [validator: string]: string | boolean;
}

/** Array of asyncronous validator functions. */
export type AsyncValidatorArray = Array<Validator | AsyncValidatorFn>;

/** Array of syncronous simple validators. */
export type ValidatorArray = Array<Validator | ValidatorFn>;

/**
 * Normalizes both sync and async validators as one set of validations
 * TODO: Why? Go thorough.
 */
const normalizeValidator =
    (validator: Validator | ValidatorFn): ValidatorFn | AsyncValidatorFn => {
        const func = (validator as Validator).validate.bind(validator);
        if (typeof func === "function") {
            return (c: AbstractControl) => func(c);
        } else {
            return validator as ValidatorFn | AsyncValidatorFn;
        }
    };

/**
 * Combines both sync and async validators as one array.
 * TODO: Why? Go thorough.
 */
export const composeValidators =
    (validators: ValidatorArray): AsyncValidatorFn | ValidatorFn => {
        if (validators == null || validators.length === 0) {
            return null;
        }
        return Validators.compose(validators.map(normalizeValidator));
    };

/** Single method to run validations on a control.  */
export const validate =
    (validators: ValidatorArray, asyncValidators: AsyncValidatorArray) => {
        return (control: AbstractControl) => {
            const synchronousValid = () => composeValidators(validators)(control);

            if (asyncValidators) {
                const asyncValidator = composeValidators(asyncValidators);

                return asyncValidator(control).map((v) => {
                    const secondary = synchronousValid();
                    if (secondary || v) { // compose async and sync validator results
                        return Object.assign({}, secondary, v);
                    }
                });
            }

            if (validators) {
                return Observable.of(synchronousValid());
            }

            return Observable.of(null);
        };
    };

/** Temporary implementation of validation messages. */
export const message = (validator: ValidationResult, key: string): string => {
  // TODO: We could potentially add some additional information here.
  // e.g. minlength `Value must be ${ (validator[key] as any).requiredLength } characters`
  return "SALAXY.NG.VALIDATION." + key;
};
