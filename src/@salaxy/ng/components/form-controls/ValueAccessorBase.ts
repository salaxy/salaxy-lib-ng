import { ControlValueAccessor } from "@angular/forms";

/**
 * Implements the Control Value Accessor pattern from Angular
 */
export class ValueAccessorBase<T> implements ControlValueAccessor {

    private innerValue: T;
    private changed = new Array<(value: T) => void>();
    private touched = new Array<() => void>();

    /** Gets or sets the inner value of the component */
    public get value(): T {
        return this.innerValue;
    }
    public set value(value: T) {
        if (this.innerValue !== value) {
            this.innerValue = value;
            this.changed.forEach((f) => f(value));
        }
    }

    /**
     * Called when the control receives a touch event.
     * Calls all the functions registered by registerOnTouched().
     */
    public touch() {
        this.touched.forEach((f) => f());
    }

    /**
     * Write a new value to the element.
     * from ControlValueAccessor
     */
    public writeValue(value: T) {
        this.innerValue = value;
    }

    /**
     * Set the function to be called when the control receives a change event.
     * from ControlValueAccessor
     */
    public registerOnChange(fn: (value: T) => void) {
        this.changed.push(fn);
    }

    /**
     * Set the function to be called when the control receives a touch event.
     * from ControlValueAccessor
     */
    public registerOnTouched(fn: () => void) {
        this.touched.push(fn);
    }

    /**
     * This function is called when the control status changes to or from "DISABLED".
     * Depending on the value, it will enable or disable the appropriate DOM element.
     * from ControlValueAccessor
     *
     * @param isDisabled The value that is set to the disabled attribute.
     */
    public setDisabledState?(isDisabled: boolean): void;
}
