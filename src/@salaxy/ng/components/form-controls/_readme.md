Implementation of salaxy form controls is based on this article:  
http://blog.rangle.io/angular-2-ngmodel-and-custom-form-components/

The source code is here:  
https://github.com/clbond/form-example

Access to NgControl is according to this discussion:  
https://github.com/angular/angular/issues/17500

Some additional implementation details are copied / inspired by this:  
https://github.com/angular/material2/blob/master/src/lib/select/select.ts  
...a very good resource to future features.

For future development, I also suggest looking into Ionic source code:  
Input: https://github.com/ionic-team/ionic/tree/master/src/components/input  
And especially its base: https://github.com/ionic-team/ionic/blob/master/src/util/base-input.ts  
...though the general impression is that there are some bugs, so the material is perhaps a cleaner source.
