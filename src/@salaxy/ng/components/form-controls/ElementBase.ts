import { Input, ViewChild, Attribute } from "@angular/core";
import { NgModel, NgControl } from "@angular/forms";
import { Observable } from "rxjs/Observable";

import { ValueAccessorBase } from "./ValueAccessorBase";
import { AsyncValidatorArray, ValidatorArray, ValidationResult, message, validate } from "./validate";
import { NgTranslations } from "../../services/infra/NgTranslations";

/**
 * Provides observable values to its parent about its validity state.
 * Provides a validate method which can also be used in the control and which works off of validators
 * injected through hierarchical dependency injection (eg. a required or minlength directive).
 */
export abstract class ElementBase<T> extends ValueAccessorBase<T> {

  /** Unique id of the element. */
  @Input()
  get id(): string { return this._id || this.name || this._uid; }
  set id(value: string) {
    this._id = value;
  }

  /** If true, label is persistent and sits next the input (side-by-side on one line). */
  @Input("one-line") public oneLineAttribute: string;

  /** Label for the control */
  @Input() public label: string;

  /**
   * Name of the component.
   * NOTE: Will also be set as id, unless id is separately defined, so keep this unique within a view or set id.
   */
  @Input() public name: string;

  /** Placeholder text */
  @Input() public placeholder: string;

  /** The ngModel directive of the inner component */
  @ViewChild(NgModel) protected model: NgModel;

  /** Unique id for this input. */
  private _uid = `sxyComponent${ nextUniqueId++ }`;

  /** Identifier if set by the programmer */
  private _id: string;

  /**
   * Creates a new ElementBase
   * @param validators - Synchronous validators.
   * Use "@Optional() @Inject(NG_VALIDATORS) validators: any[]" to get this in the control component.
   * @param asyncValidators - Asynchronous validators.
   * Use "@Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: any[]" to get this in the control component.
   * @param ngControl - NgControl if one is bound to this component.
   * Use "@Optional() @Self() ngControl: NgControl" to get this in the control component.
   * @param translations - NgTranslations service should be provided by dependency injection.
   */
  constructor(
    protected validators: ValidatorArray,
    protected asyncValidators: AsyncValidatorArray,
    protected ngControl: NgControl,
    protected translations: NgTranslations,
  ) {
    super();
    if (this.ngControl) {
      // Note: we provide the value accessor through here, instead of the `providers` to avoid running into a circular import.
      this.ngControl.valueAccessor = this;
    }
  }

  /**
   * Gets the ng-classes for the input (ng-touched, ng-dirty, ng-invalid etc.).
   * These are used at least in the wrapping ion-item object.
   */
  public getInputStateClasses() {
    if (!this.ngControl) {
      return {};
    }
    return {
      "ng-untouched": this.ngControl.untouched,
      "ng-touched": this.ngControl.touched,
      "ng-pristine": this.ngControl.pristine,
      "ng-dirty": this.ngControl.dirty,
      "ng-valid": this.ngControl.valid,
      "ng-invalid": !this.ngControl.valid,
    };
  }

  /** Returns true if the control is disabled. */
  public getIsDisabled() {
    if (!this.ngControl) {
      return false;
    }
    return this.ngControl.disabled;
  }

  /**
   * Returns an Observable of an object containing keys for any failed validators.
   */
  protected validate(): Observable<ValidationResult> {
    return validate(this.validators, this.asyncValidators)(this.model.control);
  }

  /**
   * Observable returning true when any of the validators attached to
   * the control through the dependency injector are failing
   */
  protected get invalid(): Observable<boolean> {
    return this.validate().map((v) => Object.keys(v || {}).length > 0);
  }

  /**
   * Gets a collection of validation error messages that can be shown to the end-user.
   */
  protected get failures(): Observable<string[]> {
    return this.validate().map((v) => Object.keys(v).map((k) => message(v, k)));
  }

  /**
   * Gets the messages for UI purposes.
   * @param filter Logical filter for the messages.
   * default: All messages except "required" (shown as UI indication only).
   * all: All messages
   */
  protected getMessages(filter: "default" | "all" = "default") {
    switch (filter) {
      case "all": return this.failures;
      default: return this.validate().map((validatorResult) => Object.keys(validatorResult).filter((key) => key !== "required").map((key) => "SALAXY.NG.VALIDATION." + key));
    }
  }

  /** Returns true if the required directive is applied to this element. */
  protected get isRequired() {
    // There may be a more solid way of implementing this: create property "required" OR look for directive.
    if (this.validators) {
      const required = this.validators
        // .filter((v) => Object.keys(v).find((k) => k === "_required"))
        .filter((v: any) => v._required);
      if (required.length > 0) {
        return true;
      }
    }
    return false;
  }

  /** Gets the type of the label used. */
  protected get labelType(): "hidden" | "stacked" | "floating" | "fixed" {
    if (!this.label) {
      return "hidden";
    }
    if (this.oneLineAttribute === "") {
      return "fixed";
    }
    if (this.placeholder) {
      return  "stacked";
    }
    return "floating";
  }

  /** The text that is shown at the end. May contain formatting, transaltions etc. */
  protected get labelText() {
    if (!this.label) {
      return this.label;
    }
    const label = this.translations.get(this.label);
    return this.isRequired ? label + " *" : label;
  }

  /** The placeholder text translated and null converted to empty string */
  protected get placeholderText() {
    if (!this.placeholder) {
      return "";
    }
    return this.translations.get(this.placeholder);
  }
}

/** Counter that assures identifiers are unique. */
let nextUniqueId = 0;
