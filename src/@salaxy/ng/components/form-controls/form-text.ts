import { Component, Input, Optional, Inject, Self } from "@angular/core";
import { NG_VALIDATORS, NG_ASYNC_VALIDATORS, NgControl } from "@angular/forms";

import { ElementBase } from "./ElementBase";
import { NgTranslations } from "../../services/infra/NgTranslations";

/** Specialized input control for text editing with auto validation */
@Component({
  selector: "form-text",
  template: `
    <div>
      <label *ngIf="label" [attr.for]="id + '_inner'">{{label}}</label>
      <input
        type="text"
        [placeholder]="placeholderText"
        [(ngModel)]="value"
        [ngClass]="{invalid: (model.control.touched && invalid | async)}"
        [id]="id + '_inner'"
        (blur)="touch()"
      />
      <validation *ngIf="model.control.touched && invalid | async" [messages]="failures | async"></validation>
    </div>
  `,
  styles: [`.invalid { border: 1px solid red; }`],
})
export class NgFormTextComponent extends ElementBase<string> {

  /**
   * Default contstructor creates a new Form control component.
   * @param validators - Synchronous validators
   * @param asyncValidators - Asynchronous validators
   * @param ngControl - NgControl if one is bound to this component
   * @param translations - Translations service
   */
  constructor(
    @Optional() @Inject(NG_VALIDATORS) validators: any[],
    @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: any[],
    @Optional() @Self() ngControl: NgControl,
    translations: NgTranslations,
  ) {
    super(validators, asyncValidators, ngControl, translations);
  }
}
