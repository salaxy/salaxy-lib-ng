import { Component, Input } from "@angular/core";

/**
 * Shows the validation messages automatically within the input.
 */
@Component({
  selector: "validation",
  template: `
    <div class="container">
      <ul>
        <li *ngFor="let message of messages">{{message}}</li>
      </ul>
    </div>
  `,
  styles: [`.validation { color: #999; margin: 12px; }`],
})
export class NgValidationMessages {

  /** Messages that are shown in the validation messages control */
  @Input() public messages: string[];
}
