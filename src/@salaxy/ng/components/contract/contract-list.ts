import { Component, Input, EventEmitter, Output } from "@angular/core";

import { EmploymentContract, ContractLogic, Avatar } from "@salaxy/core";

import { SalaxyContext } from "../../services/index";

/**
 * Angular level base component for list of calculations.
 *
 * @example
 * <salaxy-contract-list></salaxy-contract-list>
 */
@Component({
  selector: "salaxy-contract-list",
  templateUrl: "./contract-list.html"
})
export class NgContractListComponent {

  /**
   * Events is fired when an item is clicked / otherwise selected.
   */
  @Output() public itemSelect: EventEmitter<EmploymentContract> = new EventEmitter<EmploymentContract>();

  /**
   * Property based on which the groupedList is grouped.
   */
  @Input() public groupBy: "employer" | "worker" | "otherParty" | "year" = "year";

  /**
   * Limit number of items to show in the list
   */
  @Input() public limitTo: number;

  /**
   * Shows contracts based on isSelf on employer or worker
   */
  @Input() public filterBySelfRole: "worker" | "employer" | "household" | "company" | void;

  /**
   * When false (by default), when user sets the calculation, it is automatically set as the current calculation.
   * When true, the current calculation is not set – only itemSelect event are fired.
   */
  @Input() public skipCurrentItemSet = false;

  /**
   * Constructor
   */
  constructor(protected context: SalaxyContext) {}

  /**
   * Gets the Avatar of other party in a contract:
   * Employer or Worker
   */
  public getOtherParty(contract: EmploymentContract) {
    return ContractLogic.getOtherParty(contract, this.context.session.avatar);
  }

  /** The item that is currently in edit / detail view */
  public get current(): EmploymentContract {
    return this.context.contracts.current;
  }

  /**
   * Delete the item from repository.
   * Note that this may not always be possible. At least check model.isReadOnly
   * @param id - Identifier for the item to be deleted.
   */
  public delete(id: string): void {
    this.context.contracts.delete(id);
  }

  /**
   * List grouped based on groupBy value
   */
  public get groupedList(): any {

    let preFilteredObj;
    const sortDescending = (this.groupBy === "year");
    if (this.filterBySelfRole === "employer" || this.filterBySelfRole === "household" || this.filterBySelfRole === "company") {
      preFilteredObj = this.list.filter((item) => item.employer.isSelf);
    } else if (this.filterBySelfRole === "worker") {
      preFilteredObj = this.list.filter((item) => item.worker.isSelf);
    } else {
      preFilteredObj = Object.assign(this.list, {});
    }

    const groupedObj = preFilteredObj.reduce((prev, cur) => {
      // Worker as the default
      let key = "Unknown grouping: " + (this.groupBy || "[null]");
      switch (this.groupBy) {
        case "employer":
          key = cur.employer.avatar.sortableName || "Työnantajaa ei valittu";
          break;
        case "worker":
          key = cur.worker.avatar.sortableName || "Työntekijää ei valittu";
          break;
        case "otherParty":
          key = this.getOtherParty(cur).sortableName;
          break;
        case "year":
          {
            // TODO: Move the getting of a date to a generic helper
            const date: any = cur.updatedAt || cur.createdAt || "0000";
            if (date.getFullYear) {
              key = "" + date.getFullYear();
            } else {
              key = ("" + date).substr(0, 4);
            }
          }
          break;
      }
      if (!prev[key]) {
        prev[key] = [cur];
      } else {
        prev[key].push(cur);
      }
      return prev;
    }, {});

    return Object.keys(groupedObj)
      .map((key) => ({ key, value: groupedObj[key] }))
      .sort((n1, n2) => {
        if (n1.key > n2.key) {
          return sortDescending ? -1 : 1;
        }
        if (n1.key < n2.key) {
          return sortDescending ? 1 : -1;
        }
        return 0;
      });
  }

  /**
   * List of contracts - if 'limitTo' is set, applies limit.
   */
  public get list(): any {
    const result = this.context.contracts.list;
    if (this.limitTo) {
      return result.slice(0, this.limitTo);
    }
    return result;
  }

  /**
   * Derived components should call this method to select an item.
   */
  public select(contract: EmploymentContract) {
    if (!this.skipCurrentItemSet) {
      this.context.contracts.setCurrent(contract);
    }
    this.itemSelect.emit(contract);
  }

  /**
   * Creates a new blank contract in contracts context and sets it as current.
   * For discerning if a contract is an unsaved draft, it's id is null.
   * Emits 'itemSelect' event.
   */
  public createNew(): void {
    this.context.contracts.newCurrent();
    this.itemSelect.emit(this.context.contracts.current);
  }
}
