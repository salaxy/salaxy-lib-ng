/* tslint:disable */
/*
  TODO: Waiting for rewrite / final implementation
  Consider having the field type: "hidden" | "show" | "required" (would reduce to half).
  Consider renaming (e.g. TaxCardTypeRequirements ) and moving to core
*/

/** Settings for displaying a tax card in the user interface */
export interface ITaxCardSettings {
    showMainTaxPercent?: boolean;
    showIncomeLimit?: boolean;
    showAdditionalTaxPercent?: boolean;
    showTaxYear?: boolean;
    showTaxCardStartDate?: boolean;
    showOtherTaxCardInfo?: boolean;
    showPreviousSalariesPaid?: boolean;

    requireMainTaxPercent?: boolean;
    requireIncomeLimit?: boolean;
    requireAdditionalTaxPercent?: boolean;
    requireTaxYear?: boolean;
    requireTaxCardStartDate?: boolean;
    requireStepsTaxPercent3?: boolean;
    requirePreviousSalariesPaid?: boolean;

    disableMainTaxPercent?: boolean;
}
