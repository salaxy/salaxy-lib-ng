import { Component } from "@angular/core";
import { TaxCard } from "@salaxy/core";
import { SalaxyContext } from "../../services/index";

/** Base component for list of tax cards for the current user. */
@Component({
  template: "<p>The base tax card component has no user interface. Use derived component instead.</p>",
})
export class NgTaxCardListComponent {

  // TODO: Waiting for go-through

  /** Model to which the component is bound */
  public taxCardList: TaxCard[];

  constructor(protected context: SalaxyContext) {
  }

  /** Initializes the component */
  public ngOnInit() {
    this.taxCardList = this.getTaxCardList();
  }

  /** List of tax cards */
  public getTaxCardList() {
    return this.context.taxCards.getList();
  }

  /** Remove a single tax card */
  public delete(id: string) {
    this.context.taxCards.delete(id);
  }

  /**
   * List grouped based on the year.
   */
  public get groupedList() {
    const sortDescending = true;
    const groupedObj = this.taxCardList.reduce((prev, cur) => {
      let key;

      // TODO: Move the getting of a date to a generic helper
      const date: any = cur.card.forYear || cur.updatedAt || cur.createdAt || "0000";
      if (date.getFullYear) {
        key = "" + date.getFullYear();
      } else {
        key = ("" + date).substr(0, 4);
      }

      if (!prev[key]) {
        prev[key] = [cur];
      } else {
        prev[key].push(cur);
      }
      return prev;
    }, {});

    return Object.keys(groupedObj)
      .map((key) => ({ key, value: groupedObj[key] }))
      .sort((n1, n2) => {
        if (n1.key > n2.key) {
          return sortDescending ? -1 : 1;
        }
        if (n1.key < n2.key) {
          return sortDescending ? 1 : -1;
        }
        return 0;
      });
  }

}
