import { Component, OnInit } from "@angular/core";

import { TaxCardIncomeType, TaxCardType, Dates } from "@salaxy/core";

import { ITaxCardSettings } from "./taxCardSettings";
import { SalaxyContext } from "../../services/index";

/** Base class for taxcar editor user interfaces. */
@Component({
  template: "<p>The base tax card component has no user interface. Use derived component instead.</p>",
})
export class NgTaxCardComponent implements OnInit {

  /** The model that this component binds to. */
  public model: any = {};

  /** Previous salaries that were paid to this tax card */
  public previousSalariesPaid: number;

  /** Todays date as ISO string */
  public today: string = Dates.getToday();

  /** Settings for the tax card: the default values. */
  public taxCardUISettings = this.getSettingsDefaults();

  constructor(protected context: SalaxyContext) {
  }

  /** Initializes the component */
  public ngOnInit() {
    this.model = this.context.taxCards.getBlank();
  }

  /**
   * Adds previous salaries inputed through UI to the blank tax card
   */
  public addPreviouslyPaidSalaries() {
    // HACK: This is currently not in use. It probably should be.
    const currentYear = new Date().getFullYear();
    this.model.incomeLog[0] = {
      description: "Previous salaries added from tax card component UI",
      endDate: new Date(),
      id: null,
      income: this.previousSalariesPaid,
      startDate: new Date(currentYear, 1, 1),
      tax: 0,
      type: TaxCardIncomeType.PreviousEmployerSalaries,
    };
  }

  /**
   * Updates visibility and requiredness of tax card related fields based on tax card selection
   */
  public updateTaxCardFieldVisibility() {
    // TODO: Move getting the card defaults to core.
    //       Consider whether the taxCardUISettings is general (core) or UX-specific (this file) business logic.
    if (this.model != null) {
      const type = this.model.card.type;
      switch (type) {
        case TaxCardType.Example:
          this.taxCardUISettings = {
          };
          break;
        case TaxCardType.NoTaxCard:
          this.taxCardUISettings = {
            disableMainTaxPercent: true,
            requireMainTaxPercent: true,
            showMainTaxPercent: true,
          };
          this.model.card = {
            taxPercent: 60,
          };
          break;
        case TaxCardType.SideIncome:
          this.taxCardUISettings = {
            requireMainTaxPercent: true,
            showMainTaxPercent: true,
            showTaxYear: true,
          };
          this.model.card = {
            taxPercent: 40,
          };
          break;
        case TaxCardType.TaxCardA:
          this.taxCardUISettings = {
            requireAdditionalTaxPercent: true,
            requireIncomeLimit: true,
            requireMainTaxPercent: true,
            requireTaxCardStartDate: true,

            showAdditionalTaxPercent: true,
            showIncomeLimit: true,
            showMainTaxPercent: true,
            showPreviousSalariesPaid: true,
            showTaxCardStartDate: true,
          };
          this.model.card = {};
          break;
        case TaxCardType.TaxCardB:
          this.taxCardUISettings = {
            requireAdditionalTaxPercent: true,
            requireIncomeLimit: true,
            requireMainTaxPercent: true,

            showAdditionalTaxPercent: true,
            showIncomeLimit: true,
            showMainTaxPercent: true,
            showPreviousSalariesPaid: true,
            showTaxYear: true,
          };
          this.model.card = {};
          break;
        case TaxCardType.Freelancer:
          this.taxCardUISettings = {
            requireMainTaxPercent: true,
            showMainTaxPercent: true,
            showTaxYear: true,
          };
          this.model.card = {};
          break;
        case TaxCardType.Steps:
          this.taxCardUISettings = {
            requireStepsTaxPercent3: true,
            showOtherTaxCardInfo: true,
            showPreviousSalariesPaid: true,
          };
          this.model.card = {
            // Income limits for year 2017
            incomeLimit: 10300,
            stepsCardIncomeLimit2: 17650,
            taxPercent: 0,
            taxPercent2: 10.5,
            stepsCardTaxPercent3: 40,
          };
          break;
      }
      this.model.card.type = type;
    }

  }

  /** Save changes to the tax card. */
  public save() {
    if (this.context.session.isAuthenticated) {
      this.context.taxCards.saveCurrent();
    } else {
      // HACK: This needs to be alerted to the user.
      this.context.taxCards.list.splice(0, 0, this.model);
    }
  }

  private getSettingsDefaults(): ITaxCardSettings {
    return {
      disableMainTaxPercent: false,

      requireMainTaxPercent: false,

      requireAdditionalTaxPercent: false,
      requireIncomeLimit: false,
      requirePreviousSalariesPaid: false,
      requireStepsTaxPercent3: false,
      requireTaxCardStartDate: false,
      requireTaxYear: false,

      showAdditionalTaxPercent: false,
      showIncomeLimit: false,
      showMainTaxPercent: false,
      showOtherTaxCardInfo: false,
      showPreviousSalariesPaid: false,
      showTaxCardStartDate: false,
      showTaxYear: false,
    };
  }

}
