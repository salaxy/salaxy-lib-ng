import { Component, Inject, OnInit } from "@angular/core";
import { AbcSection, Article, Client  } from "@salaxy/core";
import { ClientNg } from "../../services/index";

/**
 * Simple article listing implementation demo.
 * For simplicity, the component now fetches only one category: Entrepreneur.
 */
@Component({
    selector: "salaxy-demo-article-list",
    templateUrl: "./article-list.html",
})
export class NgArticleListComponent implements OnInit {

    /** List of articles */
    public articles: Article[] = [];

    /** Currently selected article */
    public article: Article = {} as any;

    /**
     * Default constructor creates a new ArticleListComponent
     * @param cmsApi - Raw API for the method
     */
    constructor( @Inject(ClientNg) private client: Client) { }

    /** Starts loading article collection called Entrepreneur from the server */
    public ngOnInit() {
        this.client.cmsApi.getArticles(AbcSection.Entrepreneur as any).then ( (articles) => {
            this.articles = articles;
        });
    }

    /**
     * Sets the current article - starts loading from the server
     * @param id - Identifier of the article
     */
    public setCurrent(id: string) {
        this.client.cmsApi.getArticle(id).then( (article) => this.article = article);
    }
}
