/**
 * Interface for Events that SXY components (including Salaxy NG-component) fire when the user does the final action.
 * Typically, this is clicking either OK or Cancel button, but it may be something else.
 * The component will make the actual action and then give control to calling code, which will typically change the view.
 */
export interface ComponentDoneEvent<T> {

    /** Button that the user clicks or otherwise the event that is fired. */
    action: "ok" | "next" | "cancel" | "delete" | "selectionchange";

    /** Data, if provided by the component */
    data: T;
}
