import { Component } from "@angular/core";
import { WorkerAccount } from "@salaxy/core";
import { SalaxyContext } from "../../../services/index";

/* tslint:disable */

/**
 * Provides a standard user interface to edit a worker object.
 * The current implementation is bound to SalaxyContext.workers.current.
 * If you need to bind the model to some other item, you need to extend this code.
 *
 * TODO: Waiting for rewrite / final implementation
 */
@Component({
    selector: "salaxy-worker-detail-edit",
    templateUrl: "./worker-detail-edit.html",
})
export class NgWorkerDetailEditComponent {

    constructor(private context: SalaxyContext) {
    }

    /** The WorkerAccount model that is edited. */
    public get model(): WorkerAccount {
        return this.context.workers.current;
    }

    /** Save the current model back to repository */
    public save() {
        this.context.workers.saveCurrent();
    }

    /**
     * Delete this item from repository.
     * Note that this may not always be possible. At least check model.isReadOnly
     */
    public delete() {
        if (this.model && this.model.id) {
            this.context.workers.delete(this.model.id);
        }
        this.context.workers.current = null;
    }

    /** Creates a new worker component to the currnt connection (does not commit to server yet). */
    public createNew() {
        return this.context.workers.newCurrent();
    }
}
