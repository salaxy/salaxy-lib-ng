import { Component } from "@angular/core";
import { WorkerAccount } from "@salaxy/core";
import { SalaxyContext } from "../../../services/index";

/* tslint:disable */
/* TODO: Waiting for rewrite / final implementation */

/** Workers list implementation based on the repository pattern */
@Component({
    selector: "sxdemo-workers",
    templateUrl: "./workers.html",
})
export class NgWorkersComponent {

    /**
     * Default constructor creates a new WorkersComponent
     * @param context - SalaxyContext
     */
    constructor(
        private context: SalaxyContext,
    ) { }

    public get workers() {
        return this.context.workers.list;
    }

    public get currentId() {
        if (!this.context.workers.current) {
            return null;
        }
        return this.context.workers.current.id;
    }
    public set currentId(value) {
        if (value && (!this.context.workers.current || value !== this.context.workers.current.id)) {
            this.context.workers.setCurrentId(value);
        }
    }

    public createNew() {
        return this.context.workers.newCurrent();
    }

}
