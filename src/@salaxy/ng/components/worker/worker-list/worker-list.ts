import { Component, Input, OnInit, OnChanges, SimpleChange, Output, EventEmitter } from "@angular/core";

import { WorkerAccount } from "@salaxy/core";

import { SalaxyContext } from "../../../services/index";
import { NgTranslations } from './../../../services/infra/NgTranslations';

/**
 * Angular level base component for worker list.
 * Implements component based filtering, as filter pipes are not recommended https://angular.io/guide/pipes#no-filter-pipe
 *
 * @example searching
 * <ion-searchbar *ngIf="itemsCount > 5" placeholder="{{ 'SALAXY.UI_TERMS.searchByName' | sxyTranslate }}" [(ngModel)]="filterByText"></ion-searchbar>
 */
@Component({
  selector: "salaxy-worker-list",
  templateUrl: "./worker-list.html",
})
export class NgWorkerListComponent {

  /**
   * Called when the user clicks the "Create new" button.
   * Before the call, a new item is createad and set to current (unless skipCurrentItemSet=true)
   * and navigation to itemNavPush/itemSxyDetailPush has finalized.
   */
  @Output() public createNewClicked = new EventEmitter<WorkerAccount>();

  /**
   * Called when the user clicks a single item in the list.
   */
  @Output() public itemSelect = new EventEmitter<WorkerAccount>();

    /**
   * If set to to true, hides the "Add New" button on top of the listing.
   * The button is shown by default.
   */
  @Input() public hideCreateButton: boolean = false;

  /**
   * If false (default), when user sets the contract, it is automatically set as the current contract.
   * If true, the current contract is not set - only itemSelect events are fired.
   */
  @Input() public skipCurrentItemSet = false;

  /** Filter list by text variable. Checks against avatar.displayName field. */
  @Input() public filterByText: string;


  constructor(
    protected context: SalaxyContext,
    public translate: NgTranslations,
  ) {}

  /** The item that is currently in edit / detail view */
  public get current(): WorkerAccount { return this.context.workers.current; }

  /** Return filteredItems as the list */
  public get items(): WorkerAccount[] { return this.filteredItems; }

  /** Filtered items list */
  public get filteredItems(): WorkerAccount[] {
    if (this.filterByText) {
      return this.context.workers.list.filter((item) => item.avatar.displayName.toLowerCase().indexOf(this.filterByText.toLowerCase()) > -1);
    } else {
      return this.context.workers.list;
    }
  };

  /** Original items count */
  public get itemsCount(): number {
    return this.context.workers.list.length;
  }

  /** Gets the current role */
  public get role(): "worker" | "household" | "company" { return this.context.session.role; }

  /**
   * Delete the item from repository.
   * Note that this may not always be possible. At least check model.isReadOnly
   * @param id - Identifier for the item to be deleted.
   *
   */
  public delete(id: string) {
    this.context.workers.delete(id)
        .catch((err) => {
          // Possible component level error handling
        });
  }

  /**
   * Set current ID.
   */
  public setCurrentId(id: string) {
    this.context.workers.setCurrentId(id);
  }
}
