import { Component, Input } from "@angular/core";

/**
 * Conditionally wraps contents in a div if multipurpose input parameter "classes" is either true or a string (of classnames).
 * Has a default class .sxy-component-container annd appends additional classes if input parameter was a string instead of boolean.
 *
 * <example-url>/#/examples/ConditionalContainerExample</example-url>
 *
 * @example
 * <sxy-conditional-container (classes)="'container-fixed container-white'">
 *  My awesome contents!
 * </sxy-conditional-container>
 */
@Component({
  selector: "sxy-conditional-container",
  template: `
    <div *ngIf="wrapClasses; else unwrapped" class="{{ getClasses() }}">
      <ng-content *ngTemplateOutlet="unwrapped"></ng-content>
    </div>
    <ng-template #unwrapped>
      <ng-content></ng-content>
    </ng-template>
    `,
})
export class NgConditionalContainerComponent {

  /** Input "classes" takes either boolean or a string of classes that is appended to container's classes. */
  @Input("classes") public wrapClasses: boolean | string = false;

  /*
   * Gives classes string to template if input "classes" was not a boolean.
   */
  private getClasses(): string {
    return (this.wrapClasses !== false && this.wrapClasses !== true) ? this.wrapClasses : "";
  }
}
