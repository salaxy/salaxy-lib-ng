import { Component } from "@angular/core";
import { SalaxyContext } from "../../services/index";

/** Button to show the login screen for anonymous or to logout if the user is already authenticated. */
@Component({
    selector: "salaxy-login",
    templateUrl: "./login.html",
})
export class NgLoginComponent {

    constructor(private context: SalaxyContext) { }

    /**
     * If true, the session has been checked from the server -
     * i.e. isAuthenticated = false means that the user really
     * cannot be authenticated.
     */
    public get isSessionChecked() {
        return this.context.session.isSessionChecked;
    }

    /** If true, the user is authenticated */
    public get isAuthenticated() {
        return this.context.session.isAuthenticated;
    }

    /** Avatar to show in the login screen */
    public get avatar(): any {
        return this.context.session.avatar;
    }

    /** The full session objcet */
    public get session(): any {
        return this.context.session.session;
    }

    /**
     * Show the Login screen
     * @param redirectUrl - Redirect URL after the login. Defaults to root of the current site.
     * @param role - Default role if the user is going to Onboarding wizard for new users.
     * @param partnerSite - Identifier for the partenr site.
     */
    public signIn(redirectUrl: string = null, role: string = null, partnerSite = null) {
        this.context.session.signIn(redirectUrl, role, partnerSite);
    }

    /** Sign the current user out
     * @param redirectUrl - Optional redirect URL after the logout. Defaults to the root of the current site.
     */
    public signOut(redirectUrl?: string) {
        this.context.session.signOut(redirectUrl);
    }

}
