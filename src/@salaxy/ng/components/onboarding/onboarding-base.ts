import { Component, Input, Output, EventEmitter } from "@angular/core";
import { SafeResourceUrl } from "@angular/platform-browser";

import { Avatar, AvatarPictureType, LegalEntityType, Onboarding, OnboardingLogic, ApiValidationError } from "@salaxy/core";

import { OnboardingService } from "../../services/index";
import { ComponentDoneEvent } from "../index";

/**
 * Base component for creating editor components for onboarding process.
 */
@Component({
  selector: "salaxy-onboarding-base",
  template: `
    <div>
      <h1>NgOnboardingBaseComponent</h1>
      <p>
        This is a base component withouth user interface.
        Though we may consider adding a simple basic user interface in the future.
      </p>
    </div>
    `,
})
export class NgOnboardingBaseComponent {

  /**
   * Called when the user clicks the next-button (this component is done).
   */
  @Output() public done = new EventEmitter<ComponentDoneEvent<Onboarding>>();

  constructor(private srv: OnboardingService) {}

  /** Current onboarding item for data binding. */
  public get current(): Onboarding {
    if (!this.srv.current) {
      this.srv.newCurrent();
    }
    return this.srv.current;
  }

  /** Gets VismaSign authentication methods */
  public get signatureMethods() {
    return OnboardingLogic.getTupasMethods();
  }

  /**
   * Returns validation error for key if exists.
   * @param key - Validation error key.
   */
  public getValidationError(key: string): ApiValidationError {
    if (this.current && this.current.validation) {
      return this.current.validation.errors.find((x) => x.key === key);
    }
    return null;
  }

  /**
   * Gets the PDF preview address.
   */
  public getPdfPreviewAddress(): SafeResourceUrl {
    return this.srv.getPdfPreviewAddress();
  }

  /**
   * Going to the next page in the wizard.
   * The inheriting components should asure that the method is called only if data is valid.
   * Typically this can be done using form validation, but you may also override this method.
   */
  public next() {
    this.done.emit({
      action: "next",
      data: this.current,
    });
  }
}
