import { Component } from "@angular/core";

import { SalaxyContext } from "@salaxy/ng";

/** Full Example of Worker List and Details Components */
@Component({
    templateUrl: "worker-list.example.html",
})
export class WorkerListExample {

  constructor(private salaxyContext: SalaxyContext) {}

}
