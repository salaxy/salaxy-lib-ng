import { Component } from "@angular/core";

import { SalaxyContext } from "@salaxy/ng";
import { MasterDetailService, WorkerDetailsComponent } from "@salaxy/ionic";

/**
 * Full Example of Worker Editor (List and Details) Component(s)
 */
@Component({
  templateUrl: "worker-editor.example.html"
})
export class WorkerEditorExample {

  constructor(public salaxyContext: SalaxyContext, public masterDetailSrv: MasterDetailService) {}

  public workerDetailsComponent = WorkerDetailsComponent;
}
