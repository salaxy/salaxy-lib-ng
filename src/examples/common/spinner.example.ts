import { Component } from "@angular/core";

import { SalaxyContext } from "@salaxy/ng";

/** Sample page for Spinner */
@Component({
  templateUrl: "spinner.example.html",
})
export class SpinnerExample {

  constructor(private context: SalaxyContext) {}

}
