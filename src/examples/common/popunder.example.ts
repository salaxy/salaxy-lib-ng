import { Component } from "@angular/core";
import { ModalController } from 'ionic-angular';

import { SalaxyContext } from "@salaxy/ng";
import { PopunderComponent } from "@salaxy/ionic";

/** Sample page for Popunder */
@Component({
  templateUrl: "popunder.example.html",
})
export class PopunderExample {

  constructor(private context: SalaxyContext, private modalCtrl: ModalController) {}

  openModalSimple(): void {
    const modal = this.modalCtrl.create(PopunderComponent, {
      buttons: [{
        text: "Agree",
        value: true,
      }, {
        text: "Cancel",
        value: false,
        transparent: true,
      }],
      heading: "Cool title",
      text: "Reasonable content",
      contentType: "confirm",
    }, {
      showBackdrop: false,
    });
    modal.onDidDismiss((data, role) => {
      if (data.value) {
        // Operation here
      } else {
        // NoOp here
      }
    });
    modal.present();
  }

  openModalAdvanced(): void {
    const modal = this.modalCtrl.create(PopunderComponent, {
      buttons: [{
        text: "Agree",
        value: true,
      }, {
        text: "Cancel",
        value: false,
        transparent: true,
      }],
      heading: "Cool title",
      text: "Reasonable content",
      contentType: "confirm",
    }, {
      showBackdrop: false,
    });
    modal.onDidDismiss((data, role) => {
      if (data.value) {
        // Operation here
      } else {
        // NoOp here
      }
    });
    modal.present();
  }

}
