import { Component } from "@angular/core";

import { SalaxyContext } from "@salaxy/ng";

/** Sample page for Rounded Ion Item Icon */
@Component({
  templateUrl: "rounded-ion-item-icon.example.html",
})
export class RoundedIonItemIconExample {

  constructor(private context: SalaxyContext) {}

}
