import { Component } from "@angular/core";
import { NavController } from "ionic-angular";

import { DebuggerComponent } from "@salaxy/ionic";

/** Sample for Debugger component. */
@Component({
  templateUrl: "debugger.example.html",
})
export class DebuggerExample {

  /** The component "type" for view binding */
  protected debuggerComponent = DebuggerComponent;

  /** Push parameters for navigation push demo. */
  protected pushParams = {
    text: "Demonstrating use of DebuggerComponent with navPush. REFRESH the page to get back.",
    color: "primary",
    dump: {
      info: "This is the object dumped in navPush",
      date: new Date(),
      asContent: true,
    },
  };

  /** Component load time for demo purposes */
  protected tester = new Date();

  constructor(private navCtrl: NavController) {}

  /** Example of navigating to a debugger component in code-behind code */
  public goToDebugger() {
    this.navCtrl.push(DebuggerComponent, {
      text: "DebuggerComponent pushed from code-behind. REFRESH the page to get back.",
      color: "secondary", dump: { title: "Some data here" }, asContent: true,
    });
  }
}
