import { Component } from "@angular/core";

import { SalaxyContext } from "@salaxy/ng";

/** Sample page for Card */
@Component({
  templateUrl: "card.example.html",
})
export class CardExample {

  constructor(private context: SalaxyContext) {}

}
