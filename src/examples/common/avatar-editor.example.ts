import { Component } from "@angular/core";

import { SalaxyContext } from "@salaxy/ng";

/** Sample page for Avatar Editor */
@Component({
  templateUrl: "avatar-editor.example.html",
})
export class AvatarEditorExample {

  constructor(private context: SalaxyContext) {}

}
