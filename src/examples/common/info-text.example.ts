import { Component } from "@angular/core";

import { SalaxyContext } from "@salaxy/ng";

/** Sample page for Info Text */
@Component({
  templateUrl: "info-text.example.html",
})
export class InfoTextExample {

  constructor(private context: SalaxyContext) {}

}
