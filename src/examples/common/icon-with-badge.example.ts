import { Component } from "@angular/core";

import { SalaxyContext } from "@salaxy/ng";

/** Sample page for Icon with Badge */
@Component({
  templateUrl: "icon-with-badge.example.html",
})
export class IconWithBadgeExample {

  constructor(private context: SalaxyContext) {}

}
