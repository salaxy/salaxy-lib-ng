import { Component } from "@angular/core";

import { SalaxyContext, NgAvatarComponent } from "@salaxy/ng";

/** Sample page for Avatar */
@Component({
  templateUrl: "avatar.example.html",
})
export class AvatarExample {

  constructor(private context: SalaxyContext) {}

}
