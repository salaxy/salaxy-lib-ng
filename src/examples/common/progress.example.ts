import { Component } from "@angular/core";

import { SalaxyContext } from "@salaxy/ng";

/** Sample page for Progress */
@Component({
  templateUrl: "progress.example.html",
})
export class ProgressExample {

  constructor(private context: SalaxyContext) {}

}
