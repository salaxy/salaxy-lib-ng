import { Component } from "@angular/core";

import { SalaxyContext } from "@salaxy/ng";
import { SpinnerSecondaryComponent } from "@salaxy/ionic";

/** Sample page for Spinner Secondary */
@Component({
  templateUrl: "spinner-secondary.example.html",
})
export class SpinnerSecondaryExample {

  constructor(private context: SalaxyContext) {}

}
