import { Component } from "@angular/core";

import { SalaxyContext } from "@salaxy/ng";

/** Sample page for sxy-form-button-secondary */
@Component({
  templateUrl: "form-button-secondary.example.html",
})
export class FormButtonSecondaryExample {

  /** Property to bind to */
  public temp: string;

  constructor(private context: SalaxyContext) {}

  /** Does form commit */
  public commit() {
    alert("Committing form with value: " + this.temp);
  }

}
