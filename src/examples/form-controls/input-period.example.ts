import { Component } from "@angular/core";

import { SalaxyContext } from "@salaxy/ng";
import { Calculation, UsecasesLogic } from "@salaxy/core";

/** Sample page for sxy-form-button */
@Component({
  templateUrl: "input-period.example.html",
})
export class InputPeriodExample {

  constructor(private context: SalaxyContext) {
    this.context.calculations.newCurrent();
  }

  /** Claculation that the InputPeriod control modifies. */
  public get calc(): Calculation {
    return this.context.calculations.current;
  }

  /** Gets or sets the value indicating whether the period has been set. */
  public get periodText(): string {
    return UsecasesLogic.getPeriodText(this.calc);
  }

  /** Get text for period from UsecasesLogic */
  public set periodText(value: string) {
    UsecasesLogic.setPeriodText(this.calc, value);
  }
}
