import { Component } from "@angular/core";

import { SalaxyContext } from "@salaxy/ng";
import { InputTextComponent } from "@salaxy/ionic";

/** Sample page for DUMMY */
@Component({
  templateUrl: "input-text.example.html",
})
export class InputTextExample {

  /** Example field for data binding */
  public fieldWithText = "Some value";

  /** Example field for data binding - default is null */
  public nullField = null;

  constructor(private context: SalaxyContext) {}

}
