import { Component } from "@angular/core";

import { SalaxyContext } from "@salaxy/ng";
import { InputNumberComponent } from "@salaxy/ionic";

/** Sample page for DUMMY */
@Component({
  templateUrl: "input-number.example.html",
})
export class InputNumberExample {

  /** Property to bind to */
  public price: 123.45;

  constructor(private context: SalaxyContext) {}

  /** Does form commit */
  public commit() {
    alert("Committing form with value: " + this.price);
  }

}
