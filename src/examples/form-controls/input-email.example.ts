import { Component } from "@angular/core";

import { SalaxyContext } from "@salaxy/ng";
import { InputEmailComponent } from "@salaxy/ionic";

/** Sample page for InputEmailComponent */
@Component({
  templateUrl: "input-email.example.html",
})
export class InputEmailExample {

  /** Property to bind to */
  public temp: string;

  constructor(private context: SalaxyContext) {}

  /** Does form commit */
  public commit() {
    alert("Committing form with value: " + this.temp);
  }

}
