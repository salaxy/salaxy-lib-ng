import { Component } from "@angular/core";
import { NgForm, FormGroup } from "@angular/forms";

import { SalaxyContext } from "@salaxy/ng";
import { ValidationMessagesComponent } from "@salaxy/ionic";

/** Sample page for ValidationMessagesComponent */
@Component({
  templateUrl: "validation-messages.example.html",
})
export class ValidationMessagesExample {

  /** Example field for data binding */
  protected tester = {
    email: "testeri@palkkaus.fi",
    email2: null,
    bankIban: "FI4250001510000023",
    palkkausIban: "FI14POYT0002345678",
    mobilePhone: "+358401234567",
    personalIdFi: "131052-308T",
    companyIdFi: "2554393-3",
    smsVerificationCode: "123456",
    minMaxLength: "abcd",
    pattern: "12,3",
    required: "Any text",
  };

  constructor(private context: SalaxyContext) {}

  /** Gets debugging data for the form */
  public getFormDebugger(form: NgForm) {
    return {
      valid: form.valid,
      pristine: form.pristine,
      invalid: form.invalid,
      dirty: form.dirty,
      disabled: form.disabled,
      enabled: form.enabled,
      options: form.options,
      pending: form.pending,
      status: form.status,
      submitted: form.submitted,
      touched: form.touched,
      untouched: form.untouched,
      value: form.value,
      errors: this.getFormValidationErrors(form.controls),
    };
  }

  private getFormValidationErrors(controls: any) {
    let errors = [];
    Object.keys(controls).forEach((key) => {
      const control = controls[ key ];
      if (control instanceof FormGroup) {
        errors = errors.concat(this.getFormValidationErrors(control.controls));
      }
      const controlErrors = controls[key].errors;
      if (controlErrors !== null) {
        Object.keys(controlErrors).forEach((keyError) => {
          errors.push({
            control_name: key,
            error_name: keyError,
            error_value: controlErrors[keyError],
          });
        });
      }
    });
    return errors;
  }

}
