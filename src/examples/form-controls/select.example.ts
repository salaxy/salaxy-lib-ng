import { Component } from "@angular/core";

import { PensionCompany } from "@salaxy/core";
import { SalaxyContext } from "@salaxy/ng";
import { SelectComponent } from "@salaxy/ionic";

/** Sample page for DUMMY */
@Component({
  templateUrl: "select.example.html",
})
export class SelectExample {

  /** Example property for data binding */
  protected pensionCompany: PensionCompany;

  /**
   * Example of custom options.
   * Note, that you could also add this directly in the HTML.
   */
  protected customOptions = {
    none: "-",
    varma: "Valitse Varma kumppani",
    ilmarinen: "ILMA",
    elo: "Elo",
    etera: "Etera",
  };

  constructor(private context: SalaxyContext) {}

}
