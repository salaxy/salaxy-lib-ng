import { Component } from "@angular/core";

import { SalaxyContext } from "@salaxy/ng";

/** Sample page for sxy-form-button */
@Component({
  templateUrl: "form-button.example.html",
})
export class FormButtonExample {

  /** Demo property for data binding */
  public get testModel() {
    return this._testModel;
  }

  private _testModel = {
    emptyValue: null,
    initialvalue: "Hello world",
  }

  constructor(private context: SalaxyContext) {}

  /** Does form commit */
  public commit() {
    alert("Submit here");
  }

  /** Button off type="button" clicked. */
  public buttonClicked() {
    alert("Button is clicked.");
  }

}
