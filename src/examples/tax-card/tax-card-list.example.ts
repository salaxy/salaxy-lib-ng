import { Component } from "@angular/core";

import { SalaxyContext } from "@salaxy/ng";

/** Sample page for TaxCardListComponent */
@Component({
  templateUrl: "tax-card-list.example.html",
})
export class TaxCardListExample {

  constructor(private context: SalaxyContext) {}

}
