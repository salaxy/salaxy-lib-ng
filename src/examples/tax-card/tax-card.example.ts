import { Component } from "@angular/core";

import { SalaxyContext } from "@salaxy/ng";

/** Sample page for TaxCardComponent */
@Component({
  templateUrl: "tax-card.example.html",
})
export class TaxCardExample {

  constructor(private context: SalaxyContext) {}

}
