import { NgModule } from "@angular/core";
import { IonicModule, DeepLinkMetadata } from "ionic-angular";

import { SalaxyNgComponentsModule, SalaxyNgServicesModule } from "@salaxy/ng";
import { SalaxyIonicModule } from "@salaxy/ionic";

import {
  DUMMYExample,

  // Calculator / Calculations
  CalcCardsExample,
  CalcGroupedListExample,
  CalcListExample,
  CalcPaymentListExample,
  CalcOverviewExample,
  CalcSharingExample,
  CalcUsecaseCleaningExample,
  CalcSelectUsecaseExample,
  CalcWorkerExample,
  CalcCalculatorBasicExample,

  // Common
  AvatarEditorExample,
  AvatarExample,
  CardExample,
  DebuggerExample,
  IconWithBadgeExample,
  InfoTextExample,
  PopunderExample,
  ProgressExample,
  RoundedIonItemIconExample,
  SpinnerExample,
  SpinnerSecondaryExample,

  // Contact
  ContactEditorExample,

  // Contract
  ContractEditorExample,
  ContractListExample,
  ContractEditSummaryExample,

  // Credit Transfer
  CreditTransferExample,

  // Form controls
  FormButtonExample,
  FormButtonSecondaryExample,
  InputEmailExample,
  InputNumberExample,
  InputPeriodExample,
  InputTextExample,
  SelectExample,
  ValidationMessagesExample,

  // Master-detail
  MasterDetailEmbeddedExample,
  MasterDetailExampleHelper,

  // Pipes
  DatePipeExample,
  GroupByPipeExample,
  IbanPipeExample,
  PricePipeExample,
  SafeHtmlPipeExample,
  TranslatePipeExample,

  // Directives
  DetailPopDirectiveExample,
  DetailPushDirectiveExample,
  IfLoadingDirectiveExample,
  IfRoleDirectiveExample,
  MasterPushDirectiveExample,
  RootPushDirectiveExample,

  // Onboarding
  OnboardingEmbeddedExample,
  RecoverUidPwdExample,

  // TaxCard
  TaxCardListExample,
  TaxCardExample,

  // Worker
  WorkerListExample,
  WorkerEditorExample,

} from "./index.components";
import { TranslateModule } from "@ngx-translate/core";

/** List of examples as components. */
const exampleComponents = [
  DUMMYExample,

  // Calculator / Calculations
  CalcCardsExample,
  CalcGroupedListExample,
  CalcListExample,
  CalcPaymentListExample,
  CalcOverviewExample,
  CalcSharingExample,
  CalcUsecaseCleaningExample,
  CalcSelectUsecaseExample,
  CalcWorkerExample,
  CalcCalculatorBasicExample,

  // Common
  AvatarEditorExample,
  AvatarExample,
  CardExample,
  ContactEditorExample,
  DebuggerExample,
  IconWithBadgeExample,
  InfoTextExample,
  PopunderExample,
  ProgressExample,
  RoundedIonItemIconExample,
  SpinnerExample,
  SpinnerSecondaryExample,

  // Contact
  ContactEditorExample,

  // Contract
  ContractEditorExample,
  ContractListExample,
  ContractEditSummaryExample,

  // Credit Transfer
  CreditTransferExample,

  // Form controls
  FormButtonExample,
  FormButtonSecondaryExample,
  InputEmailExample,
  InputNumberExample,
  InputPeriodExample,
  InputTextExample,
  SelectExample,
  ValidationMessagesExample,

  // Master-detail
  MasterDetailEmbeddedExample,
  MasterDetailExampleHelper,

  // Pipes
  DatePipeExample,
  GroupByPipeExample,
  IbanPipeExample,
  PricePipeExample,
  SafeHtmlPipeExample,
  TranslatePipeExample,

  // Directives
  DetailPopDirectiveExample,
  DetailPushDirectiveExample,
  IfLoadingDirectiveExample,
  IfRoleDirectiveExample,
  MasterPushDirectiveExample,
  RootPushDirectiveExample,

  // Onboarding
  OnboardingEmbeddedExample,
  RecoverUidPwdExample,

  // TaxCard
  TaxCardListExample,
  TaxCardExample,

  // Worker
  WorkerListExample,
  WorkerEditorExample,
];

/** Routes to example pages. */
export const EXAMPLE_ROUTES: DeepLinkMetadata[] = exampleComponents.map((c) => {
  return {
    component: c,
    name: c.name,
    segment: "examples/" + c.name,
  };
});

/**
 * Packages examples of how to use Salaxy components and other elements (Validators, Pipes, services)
 * into one module for reuse on separate examples / demo web sites.
 */
@NgModule({
  declarations:    exampleComponents,
  entryComponents: exampleComponents,
  exports:         exampleComponents,
  imports: [
    IonicModule,
    SalaxyIonicModule,
    SalaxyNgComponentsModule,
    SalaxyNgServicesModule,
    TranslateModule,
  ],
  providers: [],
})
export class SalaxyExamplesModule {}
