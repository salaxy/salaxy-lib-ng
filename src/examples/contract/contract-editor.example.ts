import { Component } from "@angular/core";
import { NavController, ModalController } from "ionic-angular";

import { EmploymentContract, ContractLogic } from "@salaxy/core";
import { SalaxyContext } from "@salaxy/ng";
import { SxyUiHelpers, MasterDetailService, ContractEditSummaryComponent, ContractPreviewComponent } from "@salaxy/ionic";

/**
 * Full Contract Editor Page Component / Example
 */
@Component({
  templateUrl: "contract-editor.example.html"
})
export class ContractEditorExample {

  /** Are we showing a 'list' of contracts, or 'edit' view for the chosen contract? */
  public view: "list" | "edit" | "preview" = "list";

  /** When in Edit view, is mode a new "draft" or an already "saved" contract? "saved" as default. */
  public editMode: "draft" | "saved" = "saved";

  /** Getter for current contract */
  public get current(): EmploymentContract { return this.context.contracts.current; }

  /** Geter for the current role */
  public get role(): "worker" | "household" | "company" { return this.context.session.role; }

  constructor(
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    private context: SalaxyContext,
    private uiHelpers: SxyUiHelpers,
    private masterDetailSrv: MasterDetailService,
  ) {}

  /**
   * Navigate back to contracts list.
   */
  public goToList(): void {
    this.masterDetailSrv.closeDetails();
    this.view = "list";
  }

  /**
   * Event handler when a contract has been selected from ContractList component.
   */
  public onContractSelect(): void {
    this.view = "edit";
  }

  /**
   * Event handler when Create New Contract button has been clicked.
   */
  public onCreateNewClicked(): void {
    this.context.contracts.newCurrent();
    this.view = "edit";
    this.editMode = "draft";
  }

  /**
   * Opens preview in a modal.
   */
  public presentPreviewModal(): void {
    const promise = this.context.contracts.getPreview(this.current)
      .then((html: string) => {
        html = html.substring(html.indexOf('<div class="document-preview'));
        // Remove two closing </div> tags from the end
        let nthLastToRemove = 2;
        while (nthLastToRemove > 0) {
          html = html.substring(0, html.lastIndexOf("</div>"));
          nthLastToRemove--;
        }
        return html;
      });
    this.uiHelpers.showContractPreviewModal(promise);
  }

  /**
   * Saves currently active employment contract with a loader.
   */
  public saveContract(): void {
    const loader = this.uiHelpers.showLoading("SALAXY.UI_TERMS.isSaving", "SALAXY.UI_TERMS.pleaseWait");
    this.context.contracts.saveCurrent().then(() => {
      this.editMode = "saved";
      loader.dismiss();
    })
    .catch((err) => {
      loader.dismiss();
    });
  }

  /**
   * Removes currently active employment contract with confirmation.
   */
  public removeContractWithConfirmation(): void {
    this.uiHelpers
      .showConfirm("SALAXY.UI_TERMS.areYouSure", "Haluatko varmasti poistaa sopimuksen?")
      .then(() => {
        const loader = this.uiHelpers.showLoading("SALAXY.UI_TERMS.isDeleting", "SALAXY.UI_TERMS.pleaseWait");
        this.context.contracts.delete((this.current as EmploymentContract).id).then(() => {
          this.view = "list";
          loader.dismiss();
        })
        .catch((err) => {
          loader.dismiss();
        });
      });
  }

  /**
   * Logic for submit button text.
   */
  public submitText(): string {
    return (this.editMode === "draft") ? "Tallenna uusi sopimus" : "Tallenna muutokset";
  }
}
