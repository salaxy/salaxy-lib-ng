import { Component } from "@angular/core";

import { SalaxyContext } from "@salaxy/ng";

/** Sample page for Contract Edit Summary */
@Component({
  templateUrl: "contract-edit-summary.example.html",
})
export class ContractEditSummaryExample {

  constructor(private context: SalaxyContext) {}

}
