import { Component } from "@angular/core";

import { SalaxyContext } from "@salaxy/ng";
import { DebuggerComponent } from "@salaxy/ionic";

/** Sample page for Contract List */
@Component({
  templateUrl: "contract-list.example.html"
})
export class ContractListExample {

  public selectedContract = {};

  constructor(private context: SalaxyContext) {}

  public select(contract) {
    this.selectedContract = contract;
  }

}
