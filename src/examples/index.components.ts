export * from "./_template/DUMMY.example";

export * from "./calc/list/calc-cards.example";
export * from "./calc/list/calc-grouped-list.example";
export * from "./calc/list/calc-list.example";
export * from "./calc/list/calc-payment-list.example";
export * from "./calc/overview/calc-overview.example";
export * from "./calc/overview/calc-sharing.example";
export * from "./calc/usecases/usecase-cleaning.example";
export * from "./calc/usecases/select-usecase.example";
export * from "./calc/worker/calc-worker.example";
export * from "./calc/calc-calculator-basic.example";

export * from "./common/avatar-editor.example";
export * from "./common/avatar.example";
export * from "./common/card.example";
export * from "./common/debugger.example";
export * from "./common/icon-with-badge.example";
export * from "./common/info-text.example";
export * from "./common/popunder.example";
export * from "./common/progress.example";
export * from "./common/rounded-ion-item-icon.example";
export * from "./common/spinner.example";
export * from "./common/spinner-secondary.example";

export * from "./contact/contact-editor.example";

export * from "./contract/contract-editor.example";
export * from "./contract/contract-list.example";
export * from "./contract/contract-edit-summary.example";

export * from "./credit-transfer/credit-transfer.example";

export * from "./form-controls/form-button.example";
export * from "./form-controls/form-button-secondary.example";
export * from "./form-controls/input-email.example";
export * from "./form-controls/input-number.example";
export * from "./form-controls/input-period.example";
export * from "./form-controls/input-text.example";
export * from "./form-controls/select.example";
export * from "./form-controls/validation-messages.example";

export * from "./master-detail/master-detail-embedded.example";
export * from "./master-detail/master-detail-example-helper.example";

export * from "./pipes/date.example";
export * from "./pipes/group-by.example";
export * from "./pipes/iban.example";
export * from "./pipes/price.example";
export * from "./pipes/safe-html.example";
export * from "./pipes/translate.example";

export * from "./directives/detail-pop.example";
export * from "./directives/detail-push.example";
export * from "./directives/if-loading.example";
export * from "./directives/if-role.example";
export * from "./directives/master-push.example";
export * from "./directives/root-push.example";

export * from "./onboarding/onboarding-embedded.example";
export * from "./onboarding/recover-uid-pwd.example";

export * from "./tax-card/tax-card-list.example";
export * from "./tax-card/tax-card.example";

export * from "./worker/worker-list.example";
export * from "./worker/worker-editor.example";
