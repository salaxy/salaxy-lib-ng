import { Component } from "@angular/core";

import { SalaxyContext } from "@salaxy/ng";

/** Sample page for RecoverUidPwdComponent */
@Component({
  templateUrl: "recover-uid-pwd.example.html"
})
export class RecoverUidPwdExample {

  constructor(private context: SalaxyContext) {}

}
