import { Component } from "@angular/core";
import { Tabs, NavParams, NavController } from "ionic-angular";

import { WebSiteUserRole } from "@salaxy/core";
import { OnboardingService } from "@salaxy/ng";
import {
  DebuggerComponent,
  OnboardingPersonComponent,
  OnboardingPhoneComponent,
  OnboardingPhoneVerifyComponent,
  OnboardingIdentifierComponent,
  OnboardingTupasComponent,
} from "@salaxy/ionic";


/** Sample for Onboarding wizard as embedded directly into a page (as aopposed to a Modal dialog). */
@Component({
  templateUrl: "onboarding-embedded.example.html",
})
export class OnboardingEmbeddedExample {

  /** Current step of the wizard. */
  public wizardStep = 0;

  /** Tabs control */
  protected tabsComponent: Tabs;

  constructor(
    public onboardingService: OnboardingService,
    private navCtrl: NavController,
    private navParams: NavParams
  ) {
    this.wizardStep = navParams.get("step") || this.wizardStep;
    onboardingService.newCurrent(WebSiteUserRole.Worker);
    // TODO: Tämä tulisi Auth0 kirjautumisesta.
    onboardingService.current.person.firstName = "Timo";
    onboardingService.current.person.lastName = "Työntekijä";
    onboardingService.current.person.contact.email = "timo.työntekija@palkkaus.fi";
  }

  /** Goes to the next tab. */
  public next() {
    // Simple way - does not add to the path:
    // this.wizardStep++;
    this.navCtrl.push(OnboardingEmbeddedExample, { step: this.wizardStep + 1});
  }

}
