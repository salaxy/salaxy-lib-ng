import { Component } from "@angular/core";

/** Sample page for sxyDetailPop Directive */
@Component({
  templateUrl: "detail-pop.example.html"
})
export class DetailPopDirectiveExample {}
