import { Component } from "@angular/core";

/** Sample page for sxyIfRole Structural Directive */
@Component({
  templateUrl: "if-role.example.html"
})
export class IfRoleDirectiveExample {}
