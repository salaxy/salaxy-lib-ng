import { Component } from "@angular/core";

/** Sample page for sxyIfLoading Structural Directive */
@Component({
  templateUrl: "if-loading.example.html"
})
export class IfLoadingDirectiveExample {}
