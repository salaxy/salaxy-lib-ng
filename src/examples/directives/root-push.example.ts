import { Component } from "@angular/core";

/** Sample page for sxyRootPush Directive */
@Component({
  templateUrl: "root-push.example.html"
})
export class RootPushDirectiveExample {}
