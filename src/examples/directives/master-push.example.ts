import { Component } from "@angular/core";

/** Sample page for sxyMasterPush Directive */
@Component({
  templateUrl: "master-push.example.html"
})
export class MasterPushDirectiveExample {}
