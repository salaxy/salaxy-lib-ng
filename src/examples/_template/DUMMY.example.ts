import { Component } from "@angular/core";

import { SalaxyContext } from "@salaxy/ng";

/** Sample page for DUMMY */
@Component({
  templateUrl: "DUMMY.example.html",
})
export class DUMMYExample {

  constructor(private context: SalaxyContext) {}

}
