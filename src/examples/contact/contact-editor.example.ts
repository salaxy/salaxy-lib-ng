import { Component } from "@angular/core";

import { ContactListComponent, ContactDetailsComponent } from "@salaxy/ionic";

/** Sample page for Contact Editor */
@Component({
  templateUrl: "contact-editor.example.html",
})
export class ContactEditorExample {

  public ContactDetailsComponent = ContactDetailsComponent;

}
