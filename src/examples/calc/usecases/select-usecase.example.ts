import { Component } from "@angular/core";
import { NavController } from "ionic-angular";

import { Calculation, Usecase, UsecaseGroup, UsecasesLogic } from "@salaxy/core";
import { SalaxyContext, ComponentDoneEvent } from "@salaxy/ng";
import { MasterDetailService, CalcSelectUsecaseComponent, CalcUsecaseComponent } from "@salaxy/ionic";

import { CalcCalculatorBasicExample } from "../calc-calculator-basic.example";

/** Sample page for Select usecase component. */
@Component({
  templateUrl: "select-usecase.example.html",
})
export class CalcSelectUsecaseExample {

  constructor(
    private context: SalaxyContext,
    private masterDetailSrv: MasterDetailService,
    private navCtrl: NavController,
  ) {
    if (!this.context.calculations.current) {
      this.context.calculations.newCurrent();
    }
  }

  /** Calculation that the InputPeriod control modifies. */
  public get calc(): Calculation {
    return this.context.calculations.current;
  }

  /**  Called when the user selects a usecase group (not the final usecase selection). */
  public groupSelected = ($event: ComponentDoneEvent<UsecaseGroup>) => {
    this.masterDetailSrv.goToDetailsPage(CalcSelectUsecaseComponent, {
      item: $event.data,
      title: "Valitse työ",
      done: this.usecaseSelected,
    });
  }

  /** Called when the user selects a usecase at the end ot the selection tree. */
  public usecaseSelected = ($event: ComponentDoneEvent<Usecase>) => {
    this.context.calculations.newCurrent();
    UsecasesLogic.setUsecase(this.context.calculations.current, $event.data);
    this.masterDetailSrv.goToDetailsPage(CalcUsecaseComponent, {
      title: "Syötä perustiedot",
      done: this.usecaseSet,
    });
  }

  /** Sets the usecase and moves to the calculator */
  public usecaseSet = ($event: ComponentDoneEvent<Usecase>) => {
    this.navCtrl.setRoot(CalcCalculatorBasicExample);
  }
}
