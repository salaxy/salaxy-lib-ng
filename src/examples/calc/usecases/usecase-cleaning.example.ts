import { Component } from "@angular/core";

import { Calculation, ResultRow, Usecase, UsecasesLogic, CalculatorLogic, EnumerationsLogic, CalcRowsLogic } from "@salaxy/core";
import { SalaxyContext } from "@salaxy/ng";
import { SxyUiHelpers, CalcUsecaseComponent, CalcAddRowComponent, CalcRowComponent } from "@salaxy/ionic";

/** Sample page for sxy-form-button */
@Component({
  templateUrl: "usecase-cleaning.example.html",
})
export class CalcUsecaseCleaningExample {

  /** Component link for view */
  protected calcUsecasePage = CalcUsecaseComponent;
  /** Component link for view */
  protected calcAddRowPage = CalcAddRowComponent;
  /** Component link for view */
  protected calcRowPage = CalcRowComponent;
  /** Shown in the detail view */
  protected detailsPage = CalcUsecaseComponent;

  constructor(private context: SalaxyContext, private uiHelpers: SxyUiHelpers) {
    context.calculations.newCurrent();
    UsecasesLogic.setUsecase(context.calculations.current, UsecasesLogic.findUsecaseById("household", "cleaning/91110"));
  }

  /** Called when the usecase is ready: User clicks recalculate. */
  public usecaseDone($event) {
    // Add potential navigation etc. here.
  }

  /** Gets the current calculation to which the page is bound */
  public get current(): Calculation {
    return this.context.calculations.current;
  }

  /**
   * Null-safe version of result rows:
   * Potentially, we could add some logic here for the time when we are waiting result from server.
   */
  public get editableRows(): ResultRow[] {
    // Potentially, add recalculation logic here
    return this.current.rows;
  }

  /** Gets the read-only rows */
  public get readOnlyRows(): ResultRow[] {
    if (!this.current.result) {
      return [];
    }
    return this.current.result.rows.filter((x) => x.userRowIndex < 0);
  }

  /** Gets all the rows read-only and editable */
  public get allRows(): ResultRow[] {
    return [...this.editableRows, ...this.readOnlyRows];
  }

  /** Gets the current role */
  public get role() {
    return this.context.session.role;
  }

  /**
   * Gets the usecase object for the current calculation
   */
  public get usecase(): Usecase {
    return UsecasesLogic.getUsecaseData(this.current);
  }

  /** If true, the calculation is in such a s state that totals should be shown */
  public shouldShowTotals() {
    return CalculatorLogic.shouldShowTotals(this.current);
  }

  /** Get a label for enumeration */
  public getEnumLabel(enumType, enumValue) {
    return EnumerationsLogic.getEnumLabel(enumType, enumValue);
  }

  /** Gets a color for a calculation row */
  protected getColor(row: ResultRow) {
    const type = CalcRowsLogic.getRowConfig(row.rowType);
    return type ? type.color : "#eee";
  }
}
