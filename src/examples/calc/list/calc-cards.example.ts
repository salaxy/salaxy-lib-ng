import { Component } from "@angular/core";

import { SalaxyContext, NgTranslations } from "@salaxy/ng";
import { CalcOverviewComponent } from "@salaxy/ionic";

/** Sample page for CalcCardsComponent */
@Component({
  templateUrl: "calc-cards.example.html",
})
export class CalcCardsExample {

  /** Component link to the detail page. */
  protected detailComponent = CalcOverviewComponent;

  constructor(private context: SalaxyContext, public translate: NgTranslations) {}

}
