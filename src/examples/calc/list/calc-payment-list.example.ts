import { Component } from "@angular/core";

import { SalaxyContext } from "@salaxy/ng";

/** Sample page for CalcPaymentListComponent */
@Component({
  templateUrl: "calc-payment-list.example.html",
})
export class CalcPaymentListExample {

  constructor(private context: SalaxyContext) {}

}
