import { Component } from "@angular/core";

import { SalaxyContext } from "@salaxy/ng";
import { CalcOverviewComponent } from "@salaxy/ionic";

/** Sample page for CalcListComponent */
@Component({
  templateUrl: "calc-list.example.html",
})
export class CalcListExample {

  constructor(private context: SalaxyContext) {}

}
