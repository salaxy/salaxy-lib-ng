import { Component } from "@angular/core";

import { Calculation } from "@salaxy/core";
import { SalaxyContext } from "@salaxy/ng";
import { MasterDetailService, CalcOverviewComponent } from "@salaxy/ionic";

/** Sample page for CalcGroupedListComponent */
@Component({
  templateUrl: "calc-grouped-list.example.html",
})
export class CalcGroupedListExample {

  constructor(private context: SalaxyContext, private masterDetailSrv: MasterDetailService) {}

  /** Called when an item is selected in the list. */
  public selected($event: Calculation) {
    // The list component already sets the default component as the current calculation.
    // We could do some additional operations here and if necessary you can set
    // skipCurrentItemSet if you do not wish to set the current item.
    this.masterDetailSrv.goToDetailsPage(CalcOverviewComponent);
  }

}
