import { Component } from "@angular/core";

import { CalcWorker, TaxCardType } from "@salaxy/core";
import { SalaxyContext, ComponentDoneEvent } from "@salaxy/ng";
import { SxyUiHelpers } from "@salaxy/ionic";

/** Sample page for CalcWorkerComponent */
@Component({
  templateUrl: "calc-worker.example.html",
})
export class CalcWorkerExample {

  constructor(private context: SalaxyContext, private uiHelpers: SxyUiHelpers) {
    context.calculations.newCurrent();
    context.calculations.current.worker.paymentData.taxCardType = TaxCardType.Example;
  }

  /** Returns the current calculation. */
  public get current() {
    return this.context.calculations.current;
  }

  /** Event that is called when the worker is updated. */
  public done($event: ComponentDoneEvent<CalcWorker>) {
    this.uiHelpers.showAlert("Calculation recalculation started.");
  }

}
