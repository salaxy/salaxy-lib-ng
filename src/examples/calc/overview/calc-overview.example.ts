import { Component } from "@angular/core";
import { SalaxyContext } from "@salaxy/ng";
import { SalaryKind } from "@salaxy/core";

/** Sample page for CalcOverviewComponent */
@Component({
  templateUrl: "calc-overview.example.html",
})
export class CalcOverviewExample {

  /** Set true to hide the chart part of the overview */
  protected hideChart = false;

  /** Set true to hide the tables part of the overview */
  protected hideTables = false;

  /** Set true to show the detail columns in the tables. */
  protected showDetails = false;

  constructor(private context: SalaxyContext) {
    context.calculations.newCurrent();
    context.calculations.current.salary.kind = SalaryKind.FixedSalary;
    context.calculations.current.salary.price = 100;
    // Overview can only be shown if the calculation is calculated on the server.
    context.calculations.recalculateCurrent();
  }

}
