import { Component } from "@angular/core";

import { SalaryKind, UsecasesLogic } from "@salaxy/core";
import { SalaxyContext } from "@salaxy/ng";

/** Sample page for CalcSharingComponent */
@Component({
  templateUrl: "calc-sharing.example.html",
})
export class CalcSharingExample {

  constructor(private context: SalaxyContext) {
    this.createTestCalculation();
  }

  /** Creates a test calculation for DEV purposes. */
  private createTestCalculation() {
    this.context.calculations.newCurrent();
    const calc = this.context.calculations.current;
    calc.worker.isSelf = true;
    calc.salary = {
      amount: 5,
      price: 8.20,
      message: "Palkkaa lastenhoidosta",
      kind: SalaryKind.Compensation,
    };
    // Creates a simple calculation
    const usecase = UsecasesLogic.findUsecaseById("household", "childCare/mll");
    UsecasesLogic.setUsecase(this.context.calculations.current, usecase);
    this.context.calculations.recalculateCurrent();
  }

}
