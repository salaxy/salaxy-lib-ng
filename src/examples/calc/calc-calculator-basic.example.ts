import { Component } from "@angular/core";

import { SalaryKind, UsecasesLogic } from "@salaxy/core";
import { SalaxyContext } from "@salaxy/ng";

/** Calculator example: Household / Worker */
@Component({
  templateUrl: "calc-calculator-basic.example.html",
})
export class CalcCalculatorBasicExample {

  constructor(private context: SalaxyContext) {
    if (!this.context.calculations.current) {
      this.createTestCalculation();
    }
  }

  /** Creates a test calculation for DEV purposes. */
  private createTestCalculation() {
    this.context.calculations.newCurrent();
    const calc = this.context.calculations.current;
    calc.worker.accountId = "example-default";
    calc.salary = {
      amount: 5,
      price: 8.20,
      message: "Palkkaa lastenhoidosta",
      kind: SalaryKind.Compensation,
    };
    // Creates a simple calculation
    const usecase = UsecasesLogic.findUsecaseById("household", "childCare/mll");
    UsecasesLogic.setUsecase(this.context.calculations.current, usecase);
    this.context.calculations.recalculateCurrent();
  }

}
