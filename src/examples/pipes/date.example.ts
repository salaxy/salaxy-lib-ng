import { Component } from "@angular/core";

/** Sample page for Date Pipe */
@Component({
  templateUrl: "date.example.html"
})
export class DatePipeExample {}
