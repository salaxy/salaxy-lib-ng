import { Component } from "@angular/core";

/** Sample page for groupBy Pipe */
@Component({
  templateUrl: "group-by.example.html"
})
export class GroupByPipeExample {}
