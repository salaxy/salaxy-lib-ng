import { Component } from "@angular/core";

/** Sample page for sxyTranslate Pipe */
@Component({
  templateUrl: "translate.example.html"
})
export class TranslatePipeExample {}
