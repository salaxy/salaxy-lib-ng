import { Component } from "@angular/core";

/** Sample page for Iban Pipe */
@Component({
  templateUrl: "iban.example.html"
})
export class IbanPipeExample {}
