import { Component } from "@angular/core";

/** Sample page for safeHtml Pipe */
@Component({
  templateUrl: "safe-html.example.html"
})
export class SafeHtmlPipeExample {}
