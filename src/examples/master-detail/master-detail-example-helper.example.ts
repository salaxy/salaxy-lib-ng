import { Component, Input } from "@angular/core";
import { NavParams } from "ionic-angular";

import { DebuggerComponent, MasterDetailComponent, MasterDetailService } from "@salaxy/ionic";

/** Helper component for Master-detail samples. */
@Component({
  template: `
    <sxy-debugger color="primary" [dump]="masterDetailSrv.masterDetailDebugger" [text]="text"></sxy-debugger>
    <button type="button" ion-button sxyDetailPush="MasterDetailExampleHelper" [navParams]="{ text: 'sxyDetailPush from helper' }">Detail Push &raquo;</button><br />
    <button type="button" ion-button sxyDetailPop color="danger">Detail Pop &raquo;</button><br />
    <button type="button" ion-button sxyMasterPush="MasterDetailExampleHelper" [navParams]="{ text: 'sxyMasterPush from helper' }">Master Push &raquo;</button><br />
    <button type="button" ion-button sxyRootPush="MasterDetailExampleHelper" [navParams]="{ text: 'sxyRootPush from helper' }">Root Push &raquo;</button><br />
  `,
})
export class MasterDetailExampleHelper {

  /** Add text to the component. */
  @Input() public text: string;

  /** Creates a new instance of DebuggerComponent  */
  constructor(navParams: NavParams, private masterDetailSrv: MasterDetailService) {
    this.text = navParams.get("text") || this.text;
  }

  /** Debugger */
  protected get masterDetail(): MasterDetailComponent {
    return this.masterDetailSrv.getComponent() as MasterDetailComponent;
  }

}
