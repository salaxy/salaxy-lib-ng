import { Component } from "@angular/core";

import { DebuggerComponent } from "@salaxy/ionic";

/** Sample for Master-detail component with embedded content. */
@Component({
  templateUrl: "master-detail-embedded.example.html",
})
export class MasterDetailEmbeddedExample {

  /** Link to a component */
  protected debuggerComponent = DebuggerComponent;
}
