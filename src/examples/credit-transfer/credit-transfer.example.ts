import { Component } from "@angular/core";

import { SalaxyContext } from "@salaxy/ng";

/** Sample page for credit transfer forms */
@Component({
  templateUrl: "credit-transfer.example.html",
})
export class CreditTransferExample {

  constructor(private context: SalaxyContext) {}

}
