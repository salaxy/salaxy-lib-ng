import { Component, OnInit } from "@angular/core";
import { NavController } from "ionic-angular";

import { SalaxyContext, NgTranslations } from "@salaxy/ng";

import { HomePage } from "../home/home";

/**
 * Provides a way to change from one profile to another:
 * household, company, or worker.
 */
@Component({
  templateUrl: "switch-profile.html"
})
export class SwitchProfilePage implements OnInit {

  /** Whether current session account can switch to Worker Role */
  public canSwitchToWorker: boolean = false;
  /** Whether current session account can switch to Household Role */
  public canSwitchToHousehold: boolean = false;
  /** Whether current session account can switch to Company Role */
  public canSwitchToCompany: boolean = false;

  /** Gets the current role */
  public get role(): string { return this.context.session.role; }

  constructor(
    private context: SalaxyContext,
    private navCtrl: NavController,
    private translate: NgTranslations,
  ) {}

  /** OnInit */
  public ngOnInit(): void {
    this.canSwitchToWorker = this.context.session.canSwitchRoleTo("worker");
    this.canSwitchToHousehold = this.context.session.canSwitchRoleTo("household");
    this.canSwitchToCompany = this.context.session.canSwitchRoleTo("company");
  }

  /** Gets a text that tells the user her current role. */
  public getCurrentRoleText(): string {
    if (this.context.session.role) {
      return this.translate.get("SALAXY.NG.ROLES.youAreUsingServiceAsRole", {
        asRole: this.translate.get("SALAXY.NG.ROLES.as" + this.capitalizeFirstLetter(this.context.session.role))
      });
    }
    return this.translate.get("SALAXY.NG.ROLES.errorRoleNotDefined");
  }

  /** Switches user to the given role */
  public switchRole(role: "household" | "worker"): void {
    this.context.session.switchRole(role).then((role) => {
      this.navCtrl.setRoot(HomePage);
    });
  }

  /** Signs the user out of the application */
  public signOut(redirectUrl?: string): void {
    this.context.session.signOut(redirectUrl);
  }

  /** Show the login screen. This may be automatic in the final version. */
  public signIn(): void {
    this.context.session.signIn();
  }

  private capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }
}
