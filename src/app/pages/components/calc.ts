import { Component } from "@angular/core";

/** Shows the usage of Calculator components (Calc) */
@Component({
  templateUrl: "calc.html",
})
export class CalcComponentsPage {}
