import { Component } from "@angular/core";

/** Shows the usage of TaxCard components */
@Component({
  templateUrl: "tax-card.html",
})
export class TaxCardComponentsPage {}
