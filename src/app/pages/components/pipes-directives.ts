import { Component } from "@angular/core";

/** Shows the usage of Pipes and Directives */
@Component({
  templateUrl: "pipes-directives.html",
})
export class PipesDirectivesExamplesPage {}
