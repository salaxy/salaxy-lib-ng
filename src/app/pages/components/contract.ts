import { Component } from "@angular/core";

/** Shows the usage of Contract components */
@Component({
  templateUrl: "contract.html",
})
export class ContractComponentsPage {}
