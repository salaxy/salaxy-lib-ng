import { Component } from "@angular/core";

/** Shows the usage of Worker related components */
@Component({
  templateUrl: "worker.html",
})
export class WorkerComponentsPage {}
