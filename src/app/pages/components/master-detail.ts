import { Component } from "@angular/core";

/** Shows the usage of Master-detail components  */
@Component({
  templateUrl: "master-detail.html",
})
export class MasterDetailComponentsPage {}
