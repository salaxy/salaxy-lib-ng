import { Component } from "@angular/core";

/** Shows the usage of Contact components */
@Component({
  templateUrl: "contact.html",
})
export class ContactComponentsPage {}
