import { Component } from "@angular/core";

/** Shows the usage of Credit Transfer components */
@Component({
  templateUrl: "credit-transfer.html",
})
export class CreditTransferComponentsPage {}
