import { Component } from "@angular/core";

/** Shows the usage of Onboarding and Login components */
@Component({
  templateUrl: "onboarding.html",
})
export class OnboardingComponentsPage {}
