import { Component } from "@angular/core";

/** Shows the usage of Calculator components */
@Component({
  templateUrl: "settings.html",
})
export class SettingsComponentsPage {}
