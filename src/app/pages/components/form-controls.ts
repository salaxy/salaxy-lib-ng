import { Component } from "@angular/core";

/** Shows the usage of Form Controls component */
@Component({
  templateUrl: "form-controls.html",
})
export class FormControlsComponentsPage {}
