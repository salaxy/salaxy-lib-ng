import { Component } from "@angular/core";
import { NavController } from "ionic-angular";

/** Shows the Common group of components */
@Component({
  templateUrl: "common.html",
})
export class CommonComponentsPage {

  constructor(public navCtrl: NavController) {}

}
