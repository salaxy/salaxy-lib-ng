export * from "./home/home";
export * from "./switch-profile/switch-profile";

export * from "./components/calc";
export * from "./components/common";
export * from "./components/contact";
export * from "./components/contract";
export * from "./components/form-controls";
export * from "./components/credit-transfer";
export * from "./components/master-detail";
export * from "./components/pipes-directives";
export * from "./components/onboarding";
export * from "./components/settings";
export * from "./components/tax-card";
export * from "./components/worker";

export * from "./reference/reference-component";
export * from "./reference/reference-home";
export * from "./onboarding/onboarding";
export * from "./onboarding/onboarding-landing";
