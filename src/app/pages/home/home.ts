import { Component } from "@angular/core";
import { NavController, ModalController } from "ionic-angular";

import { MarkdownService } from "ngx-md";

import { SalaxyContext } from "@salaxy/ng";

/** Root page for the SXY components documentation page */
@Component({
  templateUrl: "home.html",
})
export class HomePage {

  constructor(
    private context: SalaxyContext,
    private modalCtrl: ModalController,
  ) {}

  /** Test method. */
  public showModal() {
    const profileModal = this.modalCtrl.create(HomePage, { userId: 8675309 });
    profileModal.onDidDismiss((data) => {
      console.debug(data);
    });
    profileModal.present();
  }

}
