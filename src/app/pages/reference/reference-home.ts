import { Component } from "@angular/core";
import { NavController } from "ionic-angular";

/** Shows the home page for full library reference. */
@Component({
  templateUrl: "reference-home.html",
})
export class ReferenceIndexPage {

  constructor(public navCtrl: NavController) {}

}
