import { Component } from "@angular/core";
import { DomSanitizer, SafeUrl } from "@angular/platform-browser";
import { NavParams } from "ionic-angular";

import { SalaxyContext } from "@salaxy/ng";

/** Shows the reference information for the specified component */
@Component({
  templateUrl: "reference-component.html",
})
export class ReferenceComponentPage {

  /** Name of the component to show from reference. */
  public componentName: string;

  /** URL to the reference documentation page for the component. */
  public componentPageUrl: SafeUrl;

  constructor(navParams: NavParams, sanitizer: DomSanitizer) {
    this.componentName = navParams.get("componentName");
    this.componentPageUrl = sanitizer.bypassSecurityTrustResourceUrl(`/compodoc/components/${this.componentName}.html`);
  }

}
