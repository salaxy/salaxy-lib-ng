import { Component } from "@angular/core";
import { NavController, NavParams } from "ionic-angular";

import { OnboardingService, NgTranslations } from "@salaxy/ng";
import { SxyUiHelpers } from "@salaxy/ionic";

import { OnboardingPage } from "./onboarding";

/** Onboarding to this site */
@Component({
  template: "<ion-content></ion-content>",
})
export class OnboardingLandingPage {

  constructor(
    private onboardingService: OnboardingService,
    private navCtrl: NavController,
    private navParams: NavParams,
    private sxyUiHelpers: SxyUiHelpers,
  ) {}

  /** Run when the component is initialized. */
  public ngOnInit() {
    const query: string = this.navParams.get("query");
    if (!query) {
      return;
    }
    const idValue = query.split("id=");
    if (!idValue && idValue.length !== 2) {
      return;
    }
    const loader = this.sxyUiHelpers.showLoading("SALAXY.UI_TERMS.pleaseWait");
    const id = idValue[1];
    this.onboardingService.loadSingle(id).then(() => {
      this.navCtrl.push(OnboardingPage, { step: 0, id});
      loader.dismiss();
    })
    .catch((err) => {
      loader.dismiss();
    });
  }
}
