import { Component } from "@angular/core";
import { NavController, NavParams, Tabs } from "ionic-angular";

import { OnboardingService } from "@salaxy/ng";

/** Onboarding to this site */
@Component({
  templateUrl: "onboarding.html",
})
export class OnboardingPage {

  /** Current step of the wizard. */
  public wizardStep = 0;

  /** Tabs control */
  protected tabsComponent: Tabs;

  private id: string;

  constructor(
    private onboardingService: OnboardingService,
    private navCtrl: NavController,
    private navParams: NavParams,
  ) {}

  /** Run when the component is initialized. */
  public ngOnInit() {
    this.wizardStep = parseInt(this.navParams.get("step") || this.wizardStep , null );

    this.id = this.navParams.get("id");
  }

  /** Goes to the next tab. */
  public next() {
    this.navCtrl.push(OnboardingPage, { step: this.wizardStep + 1, id: this.id});
  }
}
