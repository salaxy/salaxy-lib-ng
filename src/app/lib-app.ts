import { Component, OnInit, ViewChild } from "@angular/core";
import { Nav, Platform, MenuController } from "ionic-angular";

import { NgTranslations } from "@salaxy/ng";

/** Main application component */
@Component({
  templateUrl: "lib-app.html",
})
export class LibApp implements OnInit {

  /**
   * The layout that this page load should use.
   * The layout can be changed by specifying the layout parameter in the raw URL as "dialog"/"default".
   * e.g. http://localhost:8100/?layout=dialog#/reference
   */
  public get layout(): "default" | "dialog" { return this.getParameterByName("layout") === "dialog" ? "dialog" : "default"; }

  /** Default constructor */
  constructor(
    platform: Platform,
    translate: NgTranslations,
  ) {}

  /** Initializes the component */
  public ngOnInit() {
    // Potential initialization code here
  }

  private getParameterByName(name, url = null) {
    if (!url) {
      url = window.location.href;
    }
    name = name.replace(/[\[\]]/g, "\\$&");
    const regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)");
    const results = regex.exec(url);
    if (!results) {
      return null;
    }
    if (!results[2]) {
      return "";
    }
    return decodeURIComponent(results[2].replace(/\+/g, " "));
  }
}
