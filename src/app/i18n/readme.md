## Salaxy App Translations Guide

The SALAXY-LIB-NG web site (the documentation and examples) is not intended to be translated.
Only the components inside src/@salaxy folder should be translated.

However, if you need app-specific transalations for testing / demonstration purposes, you may add them to these app-specific language files:

_/src/app/i18n/lang/en.json_  
_/src/app/i18n/lang/fi.json_  
...future language files may be added.  

After changes to language specific files, use command `grunt translate` to regenerate _/src/app/i18n/dictionary.ts_
