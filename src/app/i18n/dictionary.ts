/* tslint:disable */
var _dictionary = _dictionary || {};
_dictionary["en"] = {
  "APP": {
    "DO-NOT-USE": "Transalations used only for demonstration purposes on this site. See readme.md for details."
  }
};
_dictionary["fi"] = {
  "APP": {
    "DO-NOT-USE": "Transalations used only for demonstration purposes on this site. See readme.md for details."
  }
};
/** Internationalization dictionary
@hidden */
export const dictionary = _dictionary;
