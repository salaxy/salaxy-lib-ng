import { HttpClient } from "@angular/common/http";
import { DocComponent } from "./DocComponent";
import { Inject } from "@angular/core";

/** Provides helpers for reading the Compodoc JSON */
export class CompodocService {

  /** The full Compooc data. Mainly fo future use. */
  public data: any;

  /** Array of components documented. */
  public components: DocComponent[] = [];

  /** Array of directive documented. */
  public directives: any[] = [];

  constructor(@Inject(HttpClient) public http: HttpClient) {
    http.get("/compodoc/documentation.json")
      .subscribe((data: any) => {
        this.components = data.components as DocComponent[];
        this.directives = data.directives;
      });
  }
}
