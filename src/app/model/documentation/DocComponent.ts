/**
 * Approximation of the component data in the Compodoc JSON.
 * TODO: This is created by us just by looking at the data.
 * You could probalby get the real types from Compodoc source code.
 */
export interface DocComponent {
  name: string;
  id: string;
  file: string;
  encapsulation: any[];
  inputs: any[];
  outputs: any[];
  providers: any[];
  selector: string;
  styleUrls: any[];
  styles: any[];
  templateUrl: string[];
  viewProviders: any[];
  inputsClass: any[];
  outputsClass: any[];
  propertiesClass: [
      {
          name: string;
          defaultValue: string
          type: string;
          optional: false;
          description: string;
          line: number;
          modifierKind: number[]
      }
  ];
  methodsClass: [
      {
          name: string;
          args: [
              {
                  name: string;
                  type: string
              }
          ];
          returnType: string;
          line: number;
          description: string;
          modifierKind: number[]
          jsdoctags: [
              {
                  name: {
                      pos: number;
                      end: number;
                      flags: number;
                      text: string
                  };
                  type: string;
                  tagName: {
                      pos: number;
                      end: number;
                      flags: number;
                      text: string
                  };
                  comment: string
              }
          ]
      }
  ];
  hostBindings: any[];
  hostListeners: any[];
  description: string;
  type: string;
  sourceCode: string;
  exampleUrls: string[];
  jsdoctags: [
    {
      pos: number;
      end: number;
      flags: number;
      kind: number;
      atToken: {
        pos: number;
        end: number;
        flags: number;
        kind: number;
      },
      tagName: {
        pos: number;
        end: number;
        flags: number;
        text: string;
      },
      comment: string;
    }
  ];
  constructorObj: {
      name: string;
      description: string;
      args: [
          {
              name: string;
              type: string
          }
      ];
      line: number;
      jsdoctags: [
          {
              name: string;
              type: string;
              tagName: {
                  text: string;
              }
          }
      ]
  };
  implements: string[];
  templateData: string;
}
