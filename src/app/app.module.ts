// Frameworks: Angular and Ionic
import { NgModule, ErrorHandler } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { TranslateModule } from "@ngx-translate/core";
import { IonicApp, IonicModule, IonicPageModule } from "ionic-angular";

// Components
import { MarkdownModule } from "ngx-md";

// Salaxy libraries
import { SalaxyIonicModule } from "@salaxy/ionic";
import {
  AjaxNg, NgTranslations, SalaxyNgComponentsModule,
  SalaxyContext, SalaxyErrorHandler, SalaxyNgServicesModule,
} from "@salaxy/ng";

// Salaxy error handler
import { SalaxyToastErrorHandler } from "@salaxy/ionic";

// App, general
import { LibApp } from "./lib-app";
import { dictionary } from './i18n/dictionary';
import settings from "../env/settings";

import {
  DocsComponent,
  DocsComponentExample,
  LayoutDialog,
  LayoutMenu,
  PalkkausMarkdownComponent,
} from "./components";

// Pages
import {
  HomePage,
  OnboardingPage,
  OnboardingLandingPage,
  SwitchProfilePage,

  CalcComponentsPage,
  CommonComponentsPage,
  ContactComponentsPage,
  ContractComponentsPage,
  FormControlsComponentsPage,
  CreditTransferComponentsPage,
  MasterDetailComponentsPage,
  OnboardingComponentsPage,
  SettingsComponentsPage,
  TaxCardComponentsPage,
  WorkerComponentsPage,
  PipesDirectivesExamplesPage,

  ReferenceComponentPage,
  ReferenceIndexPage,

} from "./pages";

import { CompodocService } from "./model";
import { HttpClient, HttpClientModule } from "@angular/common/http";

import {
  EXAMPLE_ROUTES,
  SalaxyExamplesModule,
  // HACK: Temporary until rewrite.
  FormButtonExample,
  FormButtonSecondaryExample,
  InputPeriodExample,
  CalcUsecaseCleaningExample,
  OnboardingEmbeddedExample,
} from "../examples";

/** Module declarations */
const moduleDeclarations = [
  // App
  LibApp,
  LayoutDialog,
  LayoutMenu,

  // Components
  DocsComponent,
  DocsComponentExample,
  PalkkausMarkdownComponent,

  // Pages
  HomePage,
  OnboardingPage,
  OnboardingLandingPage,
  SwitchProfilePage,

  CalcComponentsPage,
  CommonComponentsPage,
  ContactComponentsPage,
  ContractComponentsPage,
  FormControlsComponentsPage,
  CreditTransferComponentsPage,
  MasterDetailComponentsPage,
  OnboardingComponentsPage,
  SettingsComponentsPage,
  TaxCardComponentsPage,
  WorkerComponentsPage,
  PipesDirectivesExamplesPage,

  ReferenceComponentPage,
  ReferenceIndexPage,
];

/** Main application module for the web site */
@NgModule({
  declarations: moduleDeclarations,
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicPageModule.forChild(CommonComponentsPage),
    IonicModule.forRoot(LibApp, {
      locationStrategy: "hash",
      statusbarPadding: false,
      pageTransition: "ios-transition",
    },{
      links: [
        // Ionic 4 upcoming with interesting changes to routing
        // Should separate these to app-routing.module.ts
        // https://www.joshmorony.com/using-angular-routing-with-ionic-4/
        // https://github.com/ionic-team/ionic/blob/0793d2bf5e447578c31f17ff14805c153441544d/angular/test/nav/src/app/simple-nav/simple-nav-routing.module.ts
        { component: HomePage, name: "HomePage", segment: "home" },
        { component: SwitchProfilePage, name: "SwitchProfilePage", segment: "profile" },
        { component: CalcComponentsPage, name: "CalcComponentsPage", segment: "components/calc" },
        { component: CommonComponentsPage, name: "CommonComponentsPage", segment: "common", defaultHistory: [HomePage] },
        { component: ContactComponentsPage, name: "ContactComponentsPage", segment: "components/contact" },
        { component: ContractComponentsPage, name: "ContractComponentsPage", segment: "components/contract" },
        { component: FormControlsComponentsPage, name: "FormControlsComponentsPage", segment: "components/form-controls" },
        { component: CreditTransferComponentsPage, name: "CreditTransferComponentsPage", segment: "components/credit-transfer" },
        { component: PipesDirectivesExamplesPage, name: "PipesDirectivesExamplesPage", segment: "components/pipes-directives" },
        { component: MasterDetailComponentsPage, name: "MasterDetailComponentsPage", segment: "components/master-detail" },
        { component: OnboardingComponentsPage, name: "OnboardingComponentsPage", segment: "components/onboarding" },
        { component: SettingsComponentsPage, name: "SettingsComponentsPage", segment: "components/settings" },
        { component: TaxCardComponentsPage, name: "TaxCardComponentsPage", segment: "components/tax-card" },
        { component: WorkerComponentsPage, name: "WorkerComponentsPage", segment: "components/worker" },
        { component: ReferenceIndexPage, name: "ReferenceIndexPage", segment: "reference" },
        // -- Pages that are not listed in the layout menu
        { component: ReferenceComponentPage, name: "ReferenceComponentPage", segment: "reference/component/:componentName" },
        // Typical configuration for onboarding pages
        // Authorization server redirects to onboarding/id={onboarding.id}
        { component: OnboardingLandingPage, name: "OnboardingLandingPage", segment: "onboarding/:query" },
        { component: OnboardingPage, name: "OnboardingPage", segment: "onboarding/id/:id/step/:step" },
      ].concat(EXAMPLE_ROUTES as any[]),
    }),
    SalaxyNgComponentsModule,
    SalaxyNgServicesModule,
    SalaxyIonicModule,
    TranslateModule.forRoot(),
    SalaxyExamplesModule,
    MarkdownModule.forRoot(),
  ],
  bootstrap: [IonicApp],
  entryComponents: moduleDeclarations,
  providers: [
    // Salaxy error handling, this is used by AjaxNg for ajax related errors.
    { provide: SalaxyErrorHandler, useClass: SalaxyToastErrorHandler },
    // Angular2 error handling, this is for all uncatched errors, if not set, errors are written to console.
    // {provide: ErrorHandler, useClass: IonicErrorHandler},
    SalaxyContext,
    CompodocService,
  ],
})
export class AppModule {
  constructor(ajax: AjaxNg, translate: NgTranslations) {
    // Set API connection using settings
    ajax.serverAddress = settings.config.apiServer;
    ajax.useCredentials = settings.config.useCredentials;
    // Add dictionaries and set language
    translate.addDictionaries(dictionary);
    translate.setLanguage("fi");
  }
}
