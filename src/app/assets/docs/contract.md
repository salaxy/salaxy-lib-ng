_Contracts in the Salaxy system are by default Employment Contracts. This is a contract between an Employer (Household or
Company) and a Worker about some future work to be performed._

In edge cases, a contract can also be made after the actual work has been performed. The Finnish law states that a contract
is required in employment. However, a verbal contract is also a valid contract, so in practice, a written contract
is not a requirement for work (though it is highly recommended).

There may later be other types of contracts as well. Please also note, that end user authorization / proxy ("Valtakirja")
may be referred in some contexts as "contract".

The current implementation of employment contract is based on the original EmploymentContract feature developed in 2014 using
ASP.NET MVC framework. This original implementation was ported to Salaxy API and UI components only to fulfill the same set
of features that were already available in the Household and Worker user interface since 2014. These user interfaces
were ported from closed source system to API based implementation in Q1/2018.

In the Salaxy roadmap, there is a designed larger set of features that are to make Employment contracts as a feature yet
more useful. This design involves the following features:

- Extending Contracts also for Companies (currently only for Household and Worker)
- Digitally signing a contract
- Paying a salary based on contract
- Usecase based contract variants

Please be in touch if you are in need of these features!

----------------

### Full Example

[Open full ContractEditor example](/#/examples/ContractEditorExample)