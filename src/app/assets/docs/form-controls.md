Form controls are basically enhanced versions of the standard form controls (HTML5 and Ionic):

- `input` » `ion-input`
- `select` » `ion-select`
- `textarea` » `ion-textarea`
- `button` » `ion-button` including submit and reset

_In addition to their HTML and Ionic counterparts_, they provide in a simple package a lot of additional features:

- Mobile friendly (responsive / progressive) user interface implemented with
    - `ion-list`
    - `ion-item`
- Consistent labels
- Automatic, translated validation messages
- Indicating and enforcing required fields