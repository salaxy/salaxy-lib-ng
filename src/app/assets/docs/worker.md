Workers management components for listing, viewing and editing contract workers, and related calculations and tax cards.

--------------

### Full Example

[Open full WorkerEditor example](/#/examples/WorkerEditorExample)