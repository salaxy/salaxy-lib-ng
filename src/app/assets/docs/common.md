## Components for Generic Use

Common components are generic user interface components that have no real dependency to Salaxy framework. They are basically just helpers.