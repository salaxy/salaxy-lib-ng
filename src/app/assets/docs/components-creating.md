Create the component
--------------------

Create a new component to `/src/@salaxy`

- If it contains any [Ionic](https://ionicframework.com/docs) components (user interfaces typically do), create it in `/src/@salaxy/ionic`
- If it is generic [Angular / NGX](https://angular.io/)
    (e.g. [Validators](https://angular.io/api/forms/Validator) and [Pipes](https://angular.io/guide/pipes)),
    you should create it in `/src/@salaxy/ng`
- Even for Ionic-components, you should consider creating an NG base class where you extract the non-Ionic code.
    This would help any future development of non-Ionic UI's (e.g. [Angular Material](https://material.angular.io/)).
- Use one of the existing sub-folders or discuss with Olli / Matti to create a new one.

E.g. if you are creating a component named `HelloWorldComponent` which is a form control, it may have the following files:

- `/src/@salaxy/ionic/components/form-controls/hello-world.ts`: For the actual component code
  - Selector should be `sxy-hello-world`
- `/src/@salaxy/ng/components/form-controls/hello-world.ts`: For code that is not Inonic-specific.
  - Optionally, only if there is a significant amount of such code.
  - Component should be named `NgHelloWorldComponent`
  - Selector, if required, should be `salaxy-hello-world`
- `/src/@salaxy/ionic/components/form-controls/hello-world.html`: The HTML view.
  - The HTML may also be embedded in the ts-file (`@Component{ template: "html here"}`), especially if there is not so much of it.
- `/src/@salaxy/ionic/components/form-controls/hello-world.scss`: If you have component specific styling.
  - If you have styles that are common to several components, you may also add a shared scss-file for a folder.
- `/src/@salaxy/ionic/components/form-controls/hello-world.spec.ts`: Unit tests / specifications
  - Unit testing is encouraged though there are not so many in the existing code.
  - See https://angular.io/guide/testing

Observe the naming (casing) as above. Note also that we do not use convention `hello-world.component.ts` when the
folder already has the information about the object "type" - in this case "component".
The convention may be used in other places.

Create an example
--------------------

Additionally, you should also create an example file:

- copy and rename files from `/src/@salaxy/examples/_template`
  - `DUMMY.example.html` => `hello-world.example.html`
  - `DUMMY.example.ts` => `hello-world.example.ts`
  - `export class DUMMYExample` => `export class HelloWorldExample` =>
- Add references
  - `/examples/index.ts`
  - `/examples/examples.module.ts` - add the component to `exampleComponents` array.
- Before push, find-in-files: "DUMMY".

In this case we use the convention `.example.html` even if the file is in examples folder.
This makes it easier to edit files because in your text editor you typically have both component and its example open at the same time.

Use the example file to develop your component. Add several examples if necessary.

The example is available in this URL:
[/#/examples/HelloWorld&lt;/example-url]

You should also add the example in the component description:
`<example-url>/#/examples/HelloWorldExample</example-url>`

Note that you should also add a (very simple) example for
documentation purposes using `@example`.
This shown inline in the documentation - not as an interactive example.

Example component
-----------------

Here is an example component file
(copy from `/src/@salaxy/ionic/components/_template/DUMMY.ts`):

``` js
// Framework components
import { Component, Input, OnInit } from "@angular/core";
// Salaxy framework
import { SalaxyContext } from "@salaxy/ng";
// Current project
import { SxyUiHelpers } from "../../services/sxy-ui-helpers";

/**
 * This is a Hello World component.
 * It shows the text Hello world, or optionally your name in place of "world".
 *
 * <example-url>/#/examples/HelloWorldExample</example-url>
 *
 * @example
 * <sxy-hello-world name="Matti"></sxy-hello-world>
 */
@Component({
  selector: "sxy-hello-world",
  template: `<ion-content><h1>Hello {{ name }}!</h1></ion-content>`,
})
export class DUMMYComponent implements OnInit {

  @Input() public name: string = null;

  /**
   * Default contstructor creates a new component.
   * @param context - Salaxy context
   * @param uiHelpers - User interface helpers in @salaxy/ionic framework
   */
  constructor(private context: SalaxyContext, private uiHelpers: SxyUiHelpers) {}

  /** Component initialization */
  ngOnInit(): void {
    if (!name) {
      name = "World";
    }
  }
}
```

