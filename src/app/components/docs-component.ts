import { Component, Input } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";

import { NgAvatarComponent, NgTranslations } from "@salaxy/ng";

import { CompodocService, DocComponent } from "../model";
import { ReferenceComponentPage } from "../pages";
import { DocsComponentExample } from ".";

/**
 * Shows the documentation for a specific component
 */
@Component({
  selector: "docs-component",
  styles: [
    "h2 { font-weight: 400 !important; line-height: 3rem; }",
    "h2 > code { display: block; }"
  ],
  templateUrl: "./docs-component.html"
})
export class DocsComponent {

  /** Link to component */
  protected DocsComponentExample = DocsComponentExample;
  /** Link to component */
  protected ReferenceComponentPage = ReferenceComponentPage;

  /** Name of the component */
  @Input() public name: string;

  constructor(
    private compodoc: CompodocService,
    private sanitizer: DomSanitizer,
    public translate: NgTranslations,
  ) {}

  /** Returns the component metadata or null if the metadata is not yet available. */
  public get component(): DocComponent {
    return (
      this.compodoc.components.find(x => x.name === this.name) ||
      // TODO: Not sure if we can really document also the directives using the same component.
      this.compodoc.directives.find(x => x.name === this.name)
    );
  }

  /**
   * Gets the examples that are within the JsDoc (as opposed to the linked example pages)
   */
  public getJsDocExamples(): string[] {
    if (!this.component || !this.component.jsdoctags) {
      return [];
    }
    return this.component.jsdoctags
      .filter(tag => tag.tagName && tag.tagName.text === "example")
      .map(tag => this.formatExample(tag.comment));
  }

  private formatExample(value: string) {
    if (!value) {
      return "";
    }
    if (value.substr(0, 1) === "<") {
      return "```html\n" + value + "\n```";
    } else {
      return "```javascript\n" + value + "\n```";
    }
  }
}
