import { Component, ViewChild, HostBinding } from "@angular/core";
import { Router } from "@angular/router";
import { MenuController, Nav } from "ionic-angular";

import { SalaxyContext } from "@salaxy/ng";

import { HomePage } from "../../pages";

/**
 * Shows the web site layout that has the left-menu implemented with split view (hamburger in mobile).
 * Also contains the configuration and logic for the menu.
 */
@Component({
  selector: "layout-menu",
  templateUrl: "layout-menu.component.html",
  host: {
    "(window:hashchange)": "refreshCurrentPageProperty()",
  },
})
export class LayoutMenu {

  /** Current page name */
  public currentPageName = "HomePage";

  /** Main content Nav component  */
  @ViewChild("mainContent") public navCtrl: Nav;

  /**
   * Default root page. This should always be overriden by the router.
   */
  protected rootPage: any = HomePage;

  constructor(
    private menuCtrl: MenuController,
    private context: SalaxyContext,
  ) {
    this.refreshCurrentPageProperty();
  }

  /** Returns the raw role string. */
  public get role() {
    return this.context.session.role as any;
  }

  /** Gets the salaxy user. */
  public getSalaxyUser() {
    return this.context.session.avatar;
  }

  /** Show the login screen. This may be automatic in the final version. */
  public signIn() {
    this.menuCtrl.close();
    const onboardingUrl = window.location.origin + "/#/onboarding";
    this.context.session.signIn(null, "worker", null, onboardingUrl);
  }

  /** Sign out the user */
  public signOut(redirectUrl?: string) {
    this.menuCtrl.close();
    this.context.session.signOut(redirectUrl);
  }

  /**
   * Closes the menu and navigates the app to given page.
   * @todo Fix deep linking with MasterDetail
   * @param pageName Component name of the page. HACK: hash path
   */
  public menuGoTo(pageName: string) {
    this.currentPageName = pageName;
    this.menuCtrl.close();
    // Line below does not work when the page's template contains master-detail-view component.
    // this.navCtrl.setRoot(pageName);
    // Hack! Triggering window location url change doesn't reinitilize app so works as intended here.
    // Temporary solution (?) before getting to use navCtrl this way in conjuction with MasterDetail view.
    // Using href link i.e. href="/#/examples/ContractEditorExample" is working as well.
    // One approach that might work with MasterDetail is that if this LayoutMenu would have it's own root ion-nav
    // that would have a reference variable like #RootNav, and it MasterDetail would instead of DI, add navCtrl via @ViewChild("RootNav")
    // Navigating from the Root component @ https://ionicframework.com/docs/2.0.1/api/navigation/NavController/
    window.location.hash = this.getPathForPageName(pageName);
  }

  /** Capitalize string's first letter for building asRole translation keys */
  public capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  private refreshCurrentPageProperty(): void {
    const hash = (window.location.hash.length > 1) ? window.location.hash.substr(1) : "";
    this.currentPageName = this.getPageNameForPath(hash);
  }

  private getPageNameForPath(path: string): string {
    return this.getPathForPageName(path, true);
  }

  private getPathForPageName(pageNameOrUrl: string, reverseLookup: boolean = false): string {
    if (pageNameOrUrl === "") {
      return "HomePage";
    }
    const urlLookupTable = {
      HomePage: ["/home"],
      SwitchProfilePage: "/profile",
      CalcComponentsPage: "/components/calc",
      CommonComponentsPage: "/components/common",
      ContactComponentsPage: "/components/contact",
      ContractComponentsPage: "/components/contract",
      FormControlsComponentsPage: "/components/form-controls",
      CreditTransferComponentsPage: "/components/credit-transfer",
      PipesDirectivesExamplesPage: "/components/pipes-directives",
      MasterDetailComponentsPage: "/components/master-detail",
      OnboardingComponentsPage: "/components/onboarding",
      SettingsComponentsPage: "/components/settings",
      TaxCardComponentsPage: "/components/tax-card",
      WorkerComponentsPage: "/components/worker",
      ReferenceIndexPage: "/reference"
    }
    if (!reverseLookup) {
      if (urlLookupTable[pageNameOrUrl].isArray) {
        return urlLookupTable[pageNameOrUrl][0];
      }
      return urlLookupTable[pageNameOrUrl] || "";
    } else {
      for (var pageName in urlLookupTable) {
        if (urlLookupTable.hasOwnProperty(pageName)) {
          if (urlLookupTable[pageName].isArray) {
            for (let i = 0; i < urlLookupTable[pageName].length; i++) {
              if (urlLookupTable[pageName][i] == pageNameOrUrl) {
                return pageName;
              }
            }
          } else if (urlLookupTable[pageName] == pageNameOrUrl) {
            return pageName;
          }
        }
      }
      return "";
    }
  }
}
