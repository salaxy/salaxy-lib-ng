import { HomePage } from "../../pages";
import { Component, ViewChild } from "@angular/core";
import { MenuController, Nav } from "ionic-angular";

import { SalaxyContext } from "@salaxy/ng";

/**
 * Shows the web site layout that has the left-menu implemented with split view (hamburger in mobile).
 * Also contains the configuration and logic for the menu.
 */
@Component({
  selector: "layout-dialog",
  templateUrl: "layout-dialog.component.html",
})
export class LayoutDialog {

  /** Main content Nav component  */
  @ViewChild("mainContent") public navCtrl: Nav;

  /**
   * Default root page. This should always be overriden by the router.
   */
  protected rootPage: any = HomePage;

  constructor(private context: SalaxyContext) {}

  /** Returns the raw role string. */
  public get role() {
    return this.context.session.role as any;
  }

  /** Gets the salaxy user. */
  public getSalaxyUser() {
    return this.context.session.avatar;
  }

  /** Show the login screen. This may be automatic in the final version. */
  public signIn() {
    this.context.session.signIn();
  }

  /** Sign out the user */
  public signOut(redirectUrl?: string) {
    this.context.session.signOut(redirectUrl);
  }

  /**
   * Closes the menu and navigates the app to given page.
   * @param pageName Component name of the page.
   */
  public menuGoTo(pageName: string) {
    this.navCtrl.setRoot(pageName);
  }
}
