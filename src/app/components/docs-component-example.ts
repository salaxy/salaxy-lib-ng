import { Component, Input } from "@angular/core";

import { NgAvatarComponent } from "@salaxy/ng";
import { CompodocService, DocComponent } from "../model";
import { DomSanitizer } from "@angular/platform-browser";
import { NavParams } from "ionic-angular";

/**
 * Shows the documentation for a specific component
 */
@Component({
  selector: "docs-component-example",
  templateUrl: "./docs-component-example.html",
})
export class DocsComponentExample {

  /** Name of the component */
  @Input() public name: string;

  constructor(
    private compodoc: CompodocService,
    private sanitizer: DomSanitizer,
    public navParams: NavParams,
  ) {
    this.name = navParams.get("componentName");
  }

  /** Returns the component metadata or null if the metadata is not yet available. */
  public get component(): DocComponent {
    return this.compodoc.components.find((x) => x.name === this.name)
          // TODO: Not sure if we can really document also the directives using the same component.
          || this.compodoc.directives.find((x) => x.name === this.name) ;
  }

  /** Gets the safe URL for a given string. */
  protected getSafeUrl(url: string) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }

}
