import {Settings} from "@salaxy/ng";
/**
 * Environment specific configuration for Salaxy API's and JavaScript in general
 */
export default {
    config: {
        apiServer: "http://localhost:82",
        isTestData: true,
        useCredentials: false,
        partnerSite: "SalaxyDevelopers",
    },
    getConfigValue(key: string) {
        return this.config[key];
    },
} as Settings;
