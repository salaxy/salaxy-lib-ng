import {Settings} from "@salaxy/ng";
/**
 * Environment specific configuration for Salaxy API's and JavaScript in general
 */
export default {
    config: {
        apiServer: "https://secure.salaxy.com",
        isTestData: false,
        useCredentials: false,
        partnerSite: "SalaxyDevelopers",
    },
    getConfigValue(key: string) {
        return this.config[key];
    },
} as Settings;
