About the documentation generation process
==========================================

Compodoc
--------

The document generation process has been implemented with Compodoc.

`> npm run compodoc_json` creates the JSON file that is used for customized documentation components / pages  
`> npm run compodoc` creates the full docs HTML that is used at least for now - we may switch to completely JSON-based process later

We have just made an initial implementation: There is still a lot we can improve here.

See more here: https://compodoc.github.io/website/
