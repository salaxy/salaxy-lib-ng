For development processes: Build and link npm packages.
*******************************************************

When you are developing this project, you will often want to preview your changes in another project.
It would be very cumbersome to do this through NPM publish or copying.
Instead, we use the NPM symlink feature: https://docs.npmjs.com/cli/link

- First assure that this project builds
  - `npm install`
  - `ionic build`
- @salaxy/ng
  - Make a package: `npm run package_salaxy_ng`
  - Create a symlink:
    - `cd dist\npm\@salaxy\ng`
    - `npm link`
    - `cd ..\..\..\..\`
- @salaxy/ionic
  - Reference to ng-package symlink you created above
    - `npm link @salaxy\ng`
  - Make the @salaxy/ionic package and symlink:
    - `npm run package_salaxy_ionic`
    - `cd dist\npm\@salaxy\ionic` 
    - `npm link`
    - `cd ..\..\..\..\`
- After this, you can use the package in a referencing end-project
  - If the referncing project is e.g. `palkkaus-web-personal`...
  - `cd ..\src\palkkaus-web-personal`
  - `npm link @salaxy\core`
    - Do this if you have made @salaxy/core a symlink in a same way as ng/ionic above.
    - If you are not planning changing the core package, you can just use the package directly from NPM - do not run this line.
  - `npm link @salaxy\ng`
  - `npm link @salaxy\ionic`

- When you go through the above process once, later when you make changes, you can just run Grunt on this project (salaxy-lib-ng) and/or salaxy-lib-core:
  - `grunt`
  - The linking assures that the changed code is available in `palkkaus-web-personal` etc.
  - ...you probably need to run `ionic build` in that project too though, typically just `ionic serve` / watch is not enough.