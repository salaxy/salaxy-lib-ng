Ionic and Angular 5 libraries for Salaxy platform
=================================================

For more information and documentation about Salaxy platform, please go to https://developers.salaxy.com 

For documentation about this project and how to use it, see the docs folder.